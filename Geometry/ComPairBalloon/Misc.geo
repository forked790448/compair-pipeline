//____________//------------------//____________//
//____________//     DSSD TKR     //____________//
//____________//------------------//____________//
//### Contains Miscellaneous things like:
//### the baseplate
//### the copper straps

Volume Baseplate
Baseplate.Material Alu6061
Baseplate.Visibility 1
Baseplate.Color 16
Baseplate.Shape BOX 50 50 0.3175
Baseplate.Position 0 0 -23.0007
Baseplate.Mother MiniWorld

Volume CopperStrapBase
CopperStrapBase.Material Copper
CopperStrapBase.Visibility 1
CopperStrapBase.Color 44
CopperStrapBase.Shape BOX 6.985 3.175 0.15875

CopperStrapBase.Copy CopperStrapBase1
CopperStrapBase1.Position 3.6125 -29.92125 -22.52445
CopperStrapBase1.Mother MiniWorld

CopperStrapBase.Copy CopperStrapBase2
CopperStrapBase2.Rotation 0 0 90
CopperStrapBase2.Position -12.42125 0.1975 -22.52445
CopperStrapBase2.Mother MiniWorld

CopperStrapBase.Copy CopperStrapBase3
CopperStrapBase3.Position 0.755 12.42125 -22.52445
CopperStrapBase3.Mother MiniWorld

CopperStrapBase.Copy CopperStrapBase4
CopperStrapBase4.Position 22.6275 12.42125 -22.52445
CopperStrapBase4.Mother MiniWorld 

Volume CopperStrapPillarWide
CopperStrapPillarWide.Material Copper
CopperStrapPillarWide.Visibility 1
CopperStrapPillarWide.Color 44
CopperStrapPillarWide.Shape BOX 6.985 0.635 16.014

Volume CopperStrapPillarNarrow
CopperStrapPillarNarrow.Material Copper
CopperStrapPillarNarrow.Visibility 1
CopperStrapPillarNarrow.Color 44
CopperStrapPillarNarrow.Shape BOX 3.175 0.635 16.014

CopperStrapPillarNarrow.Copy CopperStrapPillar1
CopperStrapPillar1.Position 4.565 -27.38125 -6.3517
CopperStrapPillar1.Mother MiniWorld

CopperStrapPillarWide.Copy CopperStrapPillar2
CopperStrapPillar2.Rotation 0 0 90
CopperStrapPillar2.Position -9.88125 0.1975 -6.3517
CopperStrapPillar2.Mother MiniWorld

CopperStrapPillarWide.Copy CopperStrapPillar3
CopperStrapPillar3.Position 0.755 9.88125 -6.3517
CopperStrapPillar3.Mother MiniWorld

CopperStrapPillarNarrow.Copy CopperStrapPillar4
CopperStrapPillar4.Position 20.555 9.88125 -6.3517
CopperStrapPillar4.Mother MiniWorld

Volume CopperStrapStubWide
CopperStrapStubWide.Material Copper
CopperStrapStubWide.Visibility 1
CopperStrapStubWide.Color 44
CopperStrapStubWide.Shape BOX 6.985 0.635 1.27

Volume CopperStrapStubNarrow
CopperStrapStubNarrow.Material Copper
CopperStrapStubNarrow.Visibility 1
CopperStrapStubNarrow.Color 44
CopperStrapStubNarrow.Shape BOX 3.175 0.635 1.27

CopperStrapStubNarrow.Copy CopperStrapStub1
CopperStrapStub1.Position 4.565 -26.11125 8.3923
CopperStrapStub1.Mother MiniWorld

CopperStrapStubWide.Copy CopperStrapStub2
CopperStrapStub2.Rotation 0 0 90
CopperStrapStub2.Position -8.61125 0.1975 8.3923
CopperStrapStub2.Mother MiniWorld

CopperStrapStubWide.Copy CopperStrapStub3
CopperStrapStub3.Position 0.755 8.61125 8.3923
CopperStrapStub3.Mother MiniWorld

CopperStrapStubNarrow.Copy CopperStrapStub4
CopperStrapStub4.Position 20.555 8.61125 8.3923
CopperStrapStub4.Mother MiniWorld

Volume CopperStrapFaceWide
CopperStrapFaceWide.Material Copper
CopperStrapFaceWide.Visibility 1
CopperStrapFaceWide.Color 44
CopperStrapFaceWide.Shape BOX 6.985 0.15875 9

Volume CopperStrapFaceNarrow
CopperStrapFaceNarrow.Material Copper
CopperStrapFaceNarrow.Visibility 1
CopperStrapFaceNarrow.Color 44
CopperStrapFaceNarrow.Shape BOX 3.175 0.15875 9

CopperStrapFaceNarrow.Copy CopperStrapFace1
CopperStrapFace1.Position 4.565 -25.15875 8.3923
CopperStrapFace1.Mother MiniWorld

CopperStrapFaceWide.Copy CopperStrapFace2
CopperStrapFace2.Rotation 0 0 90
CopperStrapFace2.Position -7.65875 0.1975 8.3923
CopperStrapFace2.Mother MiniWorld

CopperStrapFaceWide.Copy CopperStrapFace3
CopperStrapFace3.Position 0.755 7.65875 8.3923
CopperStrapFace3.Mother MiniWorld

CopperStrapFaceNarrow.Copy CopperStrapFace4
CopperStrapFace4.Position 20.555 7.65875 8.3923
CopperStrapFace4.Mother MiniWorld
//------------------//
