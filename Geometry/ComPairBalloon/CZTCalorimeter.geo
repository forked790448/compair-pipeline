//____________//------------------//____________//
//____________//      CZT CAL     //____________//
//____________//------------------//____________//
//### BAR: Build a single CZT bar: 0.6x0.6x2 cm
//### BLOCK: Create the single segment of CZT, 2.5x2.5x2 cm
//### LAYER: Building single layer of 1 segment, 10x10x2 cm
//### Put 4x4 CZT bars into the block
//### Put 4x4 CZT blocks into the layer

#center to center distance between bars
#the old .geo file had 0.72, but I measured 0.7 in the STEP file

##Setting up some constants##

#distance between center of bars in crate, old was .7
Constant bar_c2c .72   
#leftmost bar position in crate (start of for loop), old was -1.05         
Constant bar_pos_start -1.08    

Constant czt_hf_w 0.295
Constant czt_hf_l 0.975

Volume CZTBox
CZTBox.Visibility 0
CZTBox.Color 3
CZTBox.Material Alu6061
CZTBox.Shape BRIK 19.8 17.1 4.02525
CZTBox.Position 11.4 -8.7 -5.03295
CZTBox.Mother MiniWorld

Volume CZTLid
CZTLid.Visibility 0
CZTLid.Color 4
CZTLid.Material Alu6061
CZTLid.Shape BRIK 19.8 17.1 0.125
CZTLid.Position 0 0 3.90025
CZTLid.Mother CZTBox

Volume CZTVacuum
CZTVacuum.Visibility 0
CZTVacuum.Color 1
CZTVacuum.Material Vacuum
CZTVacuum.Shape BRIK 19.0 16.3 3.75025
CZTVacuum.Position 0 0 0.025
CZTVacuum.Mother CZTBox

Volume CZTPCB
CZTPCB.Visibility 0
CZTPCB.Color 12
CZTPCB.Material PCB
CZTPCB.Shape BRIK 15 7.5 0.08
CZTPCB.Position -3.875 8.69 -2.97175
CZTPCB.Mother CZTVacuum

#Using CircuitBoard Here

Volume CZTProtection
CZTProtection.Visibility 0
CZTProtection.Color 13
CZTProtection.Material CircuitBoard
CZTProtection.Shape BRIK 5.8625 5.8625 0.476
CZTProtection.Position -11.4 8.7 -2.415
CZTProtection.Mother CZTVacuum

Volume CZTBlock
CZTBlock.Visibility 0
CZTBlack.Color 31
CZTBlock.Material Vacuum
CZTBlock.Shape BRIK 1.5 1.5 2.07

Volume CZT_ASIC
CZT_ASIC.Visibility 0
CZT_ASIC.Color 3
CZT_ASIC.Material CircuitBoard
CZT_ASIC.Shape BRIK 1.425 1.425 0.425
CZT_ASIC.Position 0 0 -1.645
CZT_ASIC.Mother CZTBlock

#Using Plastic Isolation here

Volume CZTElastomeric
CZTElastomeric.Visibility 0
CZTElastomeric.Color 4
CZTElastomeric.Material Plastic_Isolation
CZTElastomeric.Shape BRIK 1.425 1.425 0.025
CZTElastomeric.Position 0 0 -1.195
CZTElastomeric.Mother CZTBlock

#Using CircuitBoard here

Volume CZTAnode
CZTAnode.Visibility 0
CZTAnode.Color 5
CZTAnode.Material CircuitBoard
CZTAnode.Shape BRIK 1.425 1.425 0.15
CZTAnode.Position 0 0 -1.02
CZTAnode.Mother CZTBlock

#Using Plastic Isolation here

Volume CZTWalls
CZTWalls.Visibility 0
CZTWalls.Color 6
CZTWalls.Material Plastic_Isolation
CZTWalls.Shape BRIK 1.4375 1.4375 1.025
CZTWalls.Position 0 0 0.155
#CZTWalls.Position 0 0 0.155
CZTWalls.Mother CZTBlock

Volume CZTCell
CZTCell.Visibility 0
CZTCell.Color 7
CZTCell.Material Vacuum
CZTCell.Shape BRIK .3 .3 1.0

#STEP file has total height of 19.3mm, but Makoto is using 19.5mm, and old .geo file used 20mm

Volume CZTBar
CZTBar.Visibility 1	
CZTBar.Color 29
CZTBar.Material CZT
CZTBar.Shape BRIK czt_hf_w czt_hf_w czt_hf_l
CZTBar.Position 0 0 0
CZTBar.Mother CZTCell

For I 4 bar_pos_start bar_c2c
    For J 4 bar_pos_start bar_c2c
        CZTCell.Copy CZTCell_%I_%J
        CZTCell_%I_%J.Position $I $J 0
        CZTCell_%I_%J.Mother CZTWalls
    Done
Done

Volume CZTCathode
CZTCathode.Visibility 0
CZTCathode.Color 8
CZTCathode.Material CircuitBoard
CZTCathode.Shape BRIK 1.425 1.425 0.08
CZTCathode.Position 0 0 1.265
CZTCathode.Mother CZTBlock

#Check the material for all the plugs

Volume CZT2512Plug1
CZT2512Plug1.Visibility 0
CZT2512Plug1.Color 9
CZT2512Plug1.Material CircuitBoard
CZT2512Plug1.Shape BRIK 0.17 0.23 0.335
CZT2512Plug1.Position -0.25 -1 1.6836
CZT2512Plug1.Mother CZTBlock

Volume CZT2512Plug2
CZT2512Plug2.Visibility 0
CZT2512Plug2.Color 9
CZT2512Plug2.Material CircuitBoard
CZT2512Plug2.Shape BRIK 0.17 0.23 0.335
CZT2512Plug2.Position 0.25 -1 1.6836
CZT2512Plug2.Mother CZTBlock

Volume CZT2512Plug3
CZT2512Plug3.Visibility 0
CZT2512Plug3.Color 9
CZT2512Plug3.Material CircuitBoard
CZT2512Plug3.Shape BRIK 0.23 0.17 0.335
CZT2512Plug3.Position 0.465 0 1.6836
CZT2512Plug3.Mother CZTBlock

Volume CZT2512Plug4
CZT2512Plug4.Visibility 0
CZT2512Plug4.Color 9
CZT2512Plug4.Material CircuitBoard
CZT2512Plug4.Shape BRIK 0.17 0.23 0.335
CZT2512Plug4.Position 0.25 1 1.6836
CZT2512Plug4.Mother CZTBlock

Volume CZT2512Plug5
CZT2512Plug5.Visibility 0
CZT2512Plug5.Color 9
CZT2512Plug5.Material CircuitBoard
CZT2512Plug5.Shape BRIK 0.17 0.23 0.335
CZT2512Plug5.Position -0.25 1 1.6836
CZT2512Plug5.Mother CZTBlock

Volume CZT1825Plug1
CZT1825Plug1.Visibility 0
CZT1825Plug1.Color 9
CZT1825Plug1.Material CircuitBoard 
CZT1825Plug1.Shape BRIK 0.32 0.0375 0.1612
CZT1825Plug1.Position -0.65 -0.395 1.5136
CZT1825Plug1.Mother CZTBlock

Volume CZT1825Plug2
CZT1825Plug2.Visibility 0
CZT1825Plug2.Color 9
CZT1825Plug2.Material CircuitBoard
CZT1825Plug2.Shape BRIK 0.32 0.0375 0.1612
CZT1825Plug2.Position 0.65 -0.395 1.5136
CZT1825Plug2.Mother CZTBlock

Volume CZT1825Plug3
CZT1825Plug3.Visibility 0
CZT1825Plug3.Color 9
CZT1825Plug3.Material CircuitBoard
CZT1825Plug3.Shape BRIK 0.32 0.0375 0.1612
CZT1825Plug3.Position -0.4 0 1.5136
CZT1825Plug3.Mother CZTBlock

Volume CZT1825Plug4
CZT1825Plug4.Visibility 0
CZT1825Plug4.Color 9
CZT1825Plug4.Material CircuitBoard
CZT1825Plug4.Shape BRIK 0.32 0.0375 0.1612
CZT1825Plug4.Position 0.65 0.395 1.5136
CZT1825Plug4.Mother CZTBlock

Volume CZT1825Plug5
CZT1825Plug5.Visibility 0
CZT1825Plug5.Color 9
CZT1825Plug5.Material CircuitBoard
CZT1825Plug5.Shape BRIK 0.32 0.0375 0.1612
CZT1825Plug5.Position -0.65 0.395 1.5136
CZT1825Plug5.Mother CZTBlock

For I 4 -15.9 3
    For J 4 4.2 3
         CZTBlock.Copy CZTBlock_%I_%J
         CZTBlock_%I_%J.Position $I $J 0.173
         #Empirically chose the Z pos above such that all hits were in detector
         #0.175 -> 0.173
         #CZTBlock_%I_%J.Position $I $J .13115
         CZTBlock_%I_%J.Mother CZTVacuum
    Done
Done

BeginComment

For I 4 -4.5 3
    For J 4 -4.5 3
         CZTBlock.Copy CZTBlock_%I_%J
         CZTBlock_%I_%J.Position $I $J 0.13115
         CZTBlock_%I_%J.Mother CZTVacuum
    Done
Done

EndComment

//------------------//

