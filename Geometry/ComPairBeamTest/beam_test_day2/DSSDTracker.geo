//____________//------------------//____________//
//____________//     DSSD TKR     //____________//
//____________//------------------//____________//
//### WAFER: Build single wafer of Si Strips
//### TRACKER: Total volume of the tracker									
//### Tracker Volume, 10x10x10 cmcm							
//### Build One Si wafer, 10x10x0.05cm (active volume = 9.792 cm = 192 strips of 510 micron width)							
//### Add 10 wafer layers (containing wafer) to the tracker volume

# Previously started at -8.55 (-0.025).. There is some empty vacuum region (-9.05 - 8.575)
# Now tracker placed at -8.975 (bottom most). When added 9, it will be at 0.025
# Was 6 layers
Constant Trk_Layer_No 5
Constant Trk_Z_Separation 1.9 // Defined by the vacuum box

Volume TrackerBox
TrackerBox.Material Alu6061
TrackerBox.Visibility 1
TrackerBox.Color 30
TrackerBox.Shape BRIK 16.25 16.25 0.95

# Need to check the TrackerBoxVacuum Dimensions & Position
# The DSSD2F_32** and Misc Circuitry's are currently floating, so probably need to make the vacuum smaller in the z direction
# I believe that I've kept the wafers themselves in the same spot that they were before, but I should double check this with the old versions

Volume TrackerBoxVacuum 
TrackerBoxVacuum.Material Vacuum
TrackerBoxVacuum.Visibility 1
TrackerBoxVacuum.Color 0
TrackerBoxVacuum.Shape BRIK 16.05 16.05 0.875
TrackerBoxVacuum.Position 0.0 0.0 0.075
TrackerBoxVacuum.Mother TrackerBox

# Need to check the anvil material

Volume Anvil
Anvil.Material Alu6061
Anvil.Visibility 1
Anvil.Color 30
Anvil.Shape BOX 6.6 6.6 0.2
Anvil.Position -8.75 8.75 -0.675
Anvil.Mother TrackerBoxVacuum

Volume DSSD2F_3273
DSSD2F_3273.Material CircuitBoard
DSSD2F_3273.Visibility 1
DSSD2F_3273.Color 12
DSSD2F_3273.Shape BOX 7.114 5.6125 0.0785
DSSD2F_3273.Position -8.75 8.75 -0.396
DSSD2F_3273.Mother TrackerBoxVacuum

Volume DSSD2F_3273_Hole
DSSD2F_3273_Hole.Material Vacuum
DSSD2F_3273_Hole.Visibility 1
DSSD2F_3273_Hole.Color 0
DSSD2F_3273_Hole.Shape BOX 4.6 4.6 0.0785
DSSD2F_3273_Hole.Position 0 0 0
DSSD2F_3273_Hole.Mother DSSD2F_3273

# Using Plastic_Isolation instead of Delrin

Volume Spacer
Spacer.Material Plastic_Isolation
Spacer.Visibility 1
Spacer.Color 6
Spacer.Shape BOX 5.6125 5.6125 0.3025
Spacer.Position -8.75 8.75 -0.0148
Spacer.Mother TrackerBoxVacuum

Volume SpacerMiddle
SpacerMiddle.Material Vacuum
SpacerMiddle.Visibility 1
SpacerMiddle.Color 0
SpacerMiddle.Shape BOX 5 5 0.3025
SpacerMiddle.Position 0 0 0
SpacerMiddle.Mother Spacer

Volume Wafer									
Wafer.Material Silicon					
Wafer.Visibility 1
Wafer.Color 17									
Wafer.Shape BOX 4.896 4.896 0.025
Wafer.Position 0.0 0.0 0.0225
Wafer.Mother SpacerMiddle

Volume DSSD2F_3250
DSSD2F_3250.Material CircuitBoard
DSSD2F_3250.Visibility 1
DSSD2F_3250.Color 12
DSSD2F_3250.Shape BOX 5.6125 7.114 0.0794
DSSD2F_3250.Position -8.75 8.75 0.3671
DSSD2F_3250.Mother TrackerBoxVacuum

Volume DSSD2F_3250_Hole
DSSD2F_3250_Hole.Material Vacuum
DSSD2F_3250_Hole.Visibility 1
DSSD2F_3250_Hole.Color 0
DSSD2F_3250_Hole.Shape BOX 4.6 4.6 0.0794
DSSD2F_3250_Hole.Position 0 0 0
DSSD2F_3250_Hole.Mother DSSD2F_3250

Volume Misc_DSSDCircuitry_1
Misc_DSSDCircuitry_1.Material CircuitBoard
Misc_DSSDCircuitry_1.Visibility 1
Misc_DSSDCircuitry_1.Color 12
Misc_DSSDCircuitry_1.Shape BOX 8.793 16.05 0.08
Misc_DSSDCircuitry_1.Position 7.257 0 -0.396
Misc_DSSDCircuitry_1.Mother TrackerBoxVacuum

Volume Misc_DSSDCircuitry_2
Misc_DSSDCircuitry_2.Material CircuitBoard
Misc_DSSDCircuitry_2.Visibility 1
Misc_DSSDCircuitry_2.Color 12
Misc_DSSDCircuitry_2.Shape BOX 7.257 8.793 0.08
Misc_DSSDCircuitry_2.Position -8.793 -7.257 0.3671
Misc_DSSDCircuitry_2.Mother TrackerBoxVacuum

Volume BottomofNextBox
BottomofNextBox.Material Alu6061
BottomofNextBox.Visibility 1
BottomofNextBox.Color 30
BottomofNextBox.Shape BRIK 16.05 16.05 0.05
BottomofNextBox.Position 0.0 0.0 0.825
BottomofNextBox.Mother TrackerBoxVacuum


For J Trk_Layer_No -0.0577 Trk_Z_Separation
   
   TrackerBox.Copy TrackerBox_%J
   TrackerBox_%J.Position 8.75 -8.75 $J
   TrackerBox_%J.Mother World
   
Done

//------------------//
