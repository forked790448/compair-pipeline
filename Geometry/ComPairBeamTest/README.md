# Quick notes on Beam runs and geometry


The geometry of the setup changed for Tracker as some of the layers were giving an issue. 
Initially we had 7 layers. Layer no. 0-6


## Day 1

We had issue with layer 6 and some issue with layer 5.
Layer 6 was turned off but still left in place. 


## Day 2
Layer 5 was taken out for debugging
So we had layers 0-4 working and layer6 was left but not active. 





# Trigger Conditions
The definition and explaination of the trigger conditions are outlined here. 
These are taken from the document [Trigger Condition for the Beam](https://confluence.slac.stanford.edu/download/attachments/240276438/CMP_2023Mar03.pdf?version=2&modificationDate=1677873928000&api=v2)
These trigger conditions are defined and setup at base.geo.setup* level. 


