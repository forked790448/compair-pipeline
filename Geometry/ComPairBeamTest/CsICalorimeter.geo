//____________//------------------//____________//
//____________//      CsI CAL     //____________//
//____________//------------------//____________//
//### Build 5 layers of 6 logs of CsI in hodoscopic configuration;
//### LOG: single log 10.0cm x 1.67cm x 1.67 cm aligned along the y direction.
//### TOWER: one tower of the CsI Detector 11.17x11.17x9.27
//### Fill The tower with 6 logs per layer (5 layers in total)

# DS: updated bar dimensions and implement use of constants
Constant bar_half_w 0.835
Constant bar_CsI_c2c_dist 1.9
Constant bar_half_l 5.0
Constant layer_half_w 5.585

Volume CSIBox
CSIBox.Visibility 1
CSIBox.Color 3
CSIBox.Material Alu6061
CSIBox.Shape BRIK 19.8 17.1 6.8125
CSIBox.Position 11.4 -8.7 -15.8707
CSIBox.Mother World

Volume CSILid
CSILid.Visibility 1
CSILid.Color 4
CSILid.Material Alu6061
CSILid.Shape BRIK 19.8 17.1 0.125
CSILid.Position 0 0 6.6875
CSILid.Mother CSIBox

# Check these dimensions

Volume CSIVacuum
CSIVacuum.Visibility 1
CSIVacuum.Color 0 
CSIVacuum.Material Vacuum
CSIVacuum.Shape BRIK 18.8 16.1 6.5375
CSIVacuum.Position 0 0 0.025
CSIVacuum.Mother CSIBox

Volume CSIStand
CSIStant.Visibility 1
CSIStand.Color 5
CSIStand.Material Alu6061
CSIStand.Shape BRIK 6.975 6.975 0.5
CSIStand.Position -11.525 8.825 -6.0375
CSIStand.Mother CSIVacuum

#Using Plastic Isolation here

Volume CsITower

CsITower.Visibility 1
CsITower.Color 6
CsITower.Material Plastic_Isolation
CsITower.Shape BRIK 5.8875 5.8875 5.17545
CsITower.Position -11.4 8.7 -0.36205
CsITower.Mother CSIVacuum

Volume CSIBar
CSIBar.Visibility 1
CSIBar.Color 7
CSIBar.Material Vacuum
CSIBar.Shape BRIK 5.8875 0.85 0.85

Volume CsILog
CsILog.Visibility 1
CsILog.Color 8
CsILog.Material CsI
CsILog.Shape BRIK 0.835 0.835 5
CsILog.Position 0 0 0
CsILog.Mother CSIBar
CsILog.Rotation 0 90 0

Volume SIPM1
SIPM1.Visibility 1
SIPM1.Color 9
SIPM1.Material Silicon
SIPM1.Shape BRIK 0.22 0.625 0.625
SIPM1.Position -5.22 0 0
SIPM1.Mother CSIBar

Volume SIPM2
SIPM2.Visibility 1
SIPM2.Color 9
SIPM2.Material Silicon
SIPM2.Shape BRIK 0.22 0.625 0.625
SIPM2.Position 5.22 0 0
SIPM2.Mother CSIBar

For I 6 -4.75 1.9
    For J 5 -3.4 1.9
        CSIBar.Copy CSIBar_%I_%J
        CSIBar_%I_%J.Position 0 $I $J
        If { %J == 2}
            CSIBar_%I_%J.Position $I 0 $J
        EndIf
        If { %J == 4}
            CSIBar_%I_%J.Position $I 0 $J
        EndIf
        CSIBar_%I_2.Rotation 0 0 90
        CSIBar_%I_4.Rotation 0 0 90
        CSIBar_%I_%J.Mother CsITower
    Done
Done 

Volume CSIRosspad
CSIRosspad.Visibility 1
CSIRosspad.Color 6
CSIRosspad.Material CircuitBoard
CSIRosspad.Shape BRIK 2.925 5.625 1.7
CSIRosspad.Position 4.625 8.465 -4.825
CSIRosspad.Mother CSIVacuum

Volume CSIPower
CSIPower.Visibility 1
CSIPower.Color 6
CSIPower.Material CircuitBoard
CSIPower.Shape BRIK 6.3 4.175 0.08
CSIPower.Position -9.62 -9.925 -6.445
CSIPower.Mother CSIVacuum

Volume CSILVDS1
CSILVDS1.Visibility 1
CSILVDS1.Color 6
CSILVDS1.Material CircuitBoard
CSILVDS1.Shape BRIK 3.81 3.81 0.08
CSILVDS1.Position 3.37 -10.29 -6.445
CSILVDS1.Mother CSIVacuum

Volume CSILVDS2
CSILVDS2.Visibility 1
CSILVDS2.Color 6
CSILVDS2.Material CircuitBoard
CSILVDS2.Shape BRIK 3.81 3.81 0.08
CSILVDS2.Position 13 -10.29 -6.445
CSILVDS2.Mother CSIVacuum

Volume CSIArduino
CSIArduino.Visibility 1
CSIArduino.Color 6
CSIArduino.Material CircuitBoard
CSIArduino.Shape BRIK 4.825 2.567 0.08
CSIArduino.Position 4.805 -1.933 -6.445
CSIArduino.Mother CSIVacuum

//------------------//

