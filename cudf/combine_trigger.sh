#!/bin/bash

# Check if directory path is provided as a command-line argument
if [ $# -eq 0 ]; then
  echo "Error: Please provide a directory path as a command-line argument!"
  exit 1
fi

directory=$1

# Check if the directory exists
if [[ ! -d "$directory" ]]; then
  echo "Error: Directory not found!"
  exit 1
fi

# Find all files that has the format tm.*.data recursively
file_list=$(find "$directory" -type f -name "tm.*.data")

outfile="$directory/TM.combined.data"

for file in $file_list; do
  cat "$file" >> "$outfile"
done

echo "Concatenation completed. Output file: $outfile"