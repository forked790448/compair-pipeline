#!/usr/bin/env python
"""
The purpose of this script is to gather the L3 (CUD Compatible) files form each subsystem and
combine it to one.

"""
# Created by Sambid Wasti 3/23/22
# Version check. (check commit and compare for more details)
# s1* only deals with things in the directory Events
# s1.0a : It simply appended the files. It could have been executed if the file was missing.
# s1.0b : This is created to align event ids, so all 3 subsystem must present. Also need to check if the cud file present.
# s1.0c : Integrating the new updated format of CsI
# s1.0d : 05/10/22 Removed the CsI sipm data. Also added [...] for execution time of the script.
# s1.0e : 06/07/22 Storing the previous attributes in CUD.
# s1.0f : 07/08/22 Using eventdata/czt/cathode_digitime from the czt file for the Events/CZT/CathodeTime dataset
# s2.0a : 02/16/23 Update it to new CZT L2 file.
# s2.1a : 02/27/23 removed trig file dependency, and removed common event id requirement.
# s2.1b : 03/07/23 moving to the new tracker file version and adding the tracker event selection for pair.
# s3.0  : 03/24/23 adding the ACD data to CUD
#----
# s4.0  : 07/13/23 adding both ACD and Trg file. and they are optional. 
#                  Also the usage is changed as well as how its structured. The get_common_index is not needed/used but is kept for script.
#                  Adding event tm which has the Trigger mode data. 
# s4.0a : 07/22/23 Few additional update where there is slight renaming of things for cudfile.
#


#DELME:TODO:  > 02/16/23
# 5. Rewrite verbose printout in 1 line. 
# 6. Change verbose to console log.
# 7. automate the file selection process from a folder.

import argparse
import h5py
import numpy as np
import os
import pandas as pd
from datetime import datetime

VERSION = 's4.0a'
COMP = True
VERBOSE = False


#DELME:TODO: Move this to an aux script
def get_common_index(trk_evtid, czt_evtid, csi_evtid):
    """ Find the common index using the event id from three subsystems.

    Parameters
    ----------
    trk_evtid : Dataset of Event IDs from tracker
    czt_evtid : Dataset of Event IDs from CZT
    csi_evtid : Dataset of Event IDs from CSI

    Returns
    -------
        trk_cm_index: Intarr
            Array of common index selection for tracker
        czt_cm_index: Intarr
            Array of common index selection for Czt
        csi_cm_index: Intarr
            Array of common index selection for Csi
    """
    delme1, tra_in1, csi_in1 = np.intersect1d(trk_evtid,csi_evtid,
                                              return_indices=True) # common index between tra and csi
    delme2, tra_in2, czt_in1 = np.intersect1d(trk_evtid,czt_evtid,
                                              return_indices=True) # common index between tra and czt
    delme3, csi_in2, czt_in2 = np.intersect1d(csi_evtid,czt_evtid,
                                              return_indices=True) # common index between czt and csi

    trk_cm_index, delme4, delme5 = np.intersect1d(tra_in1, tra_in2,
                                                  return_indices=True)  # get the ARRAY of common index.
    csi_cm_index, delme6, delme7 = np.intersect1d(csi_in1, csi_in2,
                                                  return_indices=True)  # get the ARRAY of common index.
    czt_cm_index, delme8, delme9 = np.intersect1d(czt_in1, czt_in2,
                                                  return_indices=True)  # get the ARRAY of common index.

    return trk_cm_index, czt_cm_index, csi_cm_index


def get_trk_nrg(trk_nrg, evt_id):
    """Get tracker energy in (n_evt, energy) format.
    
    Parameters
    ----------
        trk_nrg : dataset
            trk energy => f_trk['Events/TKR/Energy'][...]
        evt_id : dataset
            event id dataset => evt_id = f_trk['Events/TKR/EventID'][...] 
    Returns
    -------
        Energy array in an (nevents, no.hits) shape. nohits here defined by max hit per event id
    """
    nrg_id = trk_nrg[:,0] # select the tracker energy id
    unique_nrgid, counts = np.unique(nrg_id, return_counts=True) # get unique eventid and the no. of counts
    new_energy_ar = np.zeros(shape=(evt_id.shape[0],max(counts))) # evt_id = unique event id

    for u_id in range(unique_nrgid.shape[0]):
        tid = unique_nrgid[u_id]                # unique id.
        t_loc = np.where(nrg_id ==tid)          # find locations for the ids
        t_loc1 = np.where(tid == evt_id)        # find where the ids are in event ids.
        t_nrg= m_cud_nrg[t_loc[0],4]            # get the energies.
    
        for j in range(len((t_loc[0]))):
            new_energy_ar[t_loc1[0],j] = t_nrg[j]

    return new_energy_ar

def trk_evt_sel(trk_nrg):
    # what does this return?
    # nrg with quality 1 or 2. 
    # later we would check 2/3 with a new function.
    # REVIEW: Currently selecting not 3, we might have to do 1, and then filter event id, and put in one array and then we do sort.
    l3_qloc = np.where(trk_nrg[:,5] != 3) # default returning values that are not 3. 
    return trk_nrg[l3_qloc]


def write_trk(f_cud,f_trk,trk_in):
    """ Write the Tracker dataset.

    Reselect the common event id indices and write the tracker dataset.

    Parameters
    ----------
    f_trk : h5file obj
        tracker h5 file object.
    trk_in: Intarr
        array of common indices for tracker.
    """
    comp = COMP
    trk_grp = f_cud.create_group('Events/TKR')

    #get type.
    id_type = f_trk['tracker/event_id'].dtype
    time_type = f_trk['tracker/event_time'].dtype
    pulse_type = f_trk['tracker/pulse_height'].dtype
    energy_type = f_trk['tracker/energy'].dtype

    #define Shape
    trk_id_size = len(trk_in)
    trk_grp.create_dataset(name = 'EventID', data=f_trk['tracker/event_id'][...][trk_in], shape = (trk_id_size,),
                           dtype=id_type, compression=comp)
    trk_grp.create_dataset(name = 'EventTime', data=f_trk['tracker/event_time'][...][trk_in], shape = (trk_id_size,),
                           dtype=time_type, compression=comp)

    trk_grp.create_dataset(name = 'Pulseheight', data=f_trk['tracker/pulse_height'][...][trk_in,:,:,:], shape = (trk_id_size,10,2,192),
                           dtype=pulse_type,compression=comp)
    
    # Energy array is special.
    trk_nrg = f_trk['tracker/energy'][...]

    # Filter out the pairs:
    fil_trk_nrg = trk_evt_sel(trk_nrg)

    trk_grp.create_dataset(name = 'Energy', data=fil_trk_nrg, shape = fil_trk_nrg.shape,
                           dtype=energy_type, compression=comp)
    # Conserving attributes
    temp_att_dict = dict(f_trk.attrs.items())  # save all the previous attributes.
    for keys in temp_att_dict:
        f_cud[f'Events/TKR/'].attrs[f'{keys}'] = temp_att_dict[f'{keys}']

#DELME:TODO: THIS IS NOT USED ANYMORE. To Be Deleted in future. We are copying the whole keys so no need here.
def write_czt(f_cud,f_czt,czt_in):
    """ Write CZT Data.

    Parameters
    ----------
    f_czt : file object
        CZT file object
    czt_in : Intarr
        CZT common index array
    comp : Bool
        Flag for compression. Default = True.
    """
    comp=COMP
    czt_grp = f_cud.create_group('Events/CZT')

    zid_type = f_czt['CZT/EventID'].dtype
    ztime_type = f_czt['CZT/EventTime'].dtype
    zas_temp_type = f_czt['CZT/AsicTemp'].dtype
    zan_pul_type = f_czt['CZT/AnodePulseheight'].dtype
    zan_time_type = f_czt['CZT/AnodeTime'].dtype
    zcat_pul_type = f_czt['CZT/CathodePulseheight'].dtype
    zcat_time_type = f_czt['CZT/CathodeTime'].dtype
    zpad_pul_type = f_czt['CZT/PadPulseheight'].dtype
    zener_type = f_czt['CZT/Energy'].dtype
    zpos_type = f_czt['CZT/Position'].dtype
    zutc_type = f_czt['CZT/CorUTC'].dtype
    z_badpadflag_type= f_czt['CZT/BadPadflag'].dtype

    czt_id_size = len(czt_in)
    
    # Write Data 
    czt_grp.create_dataset(name = 'EventID', data=f_czt['CZT/EventID'][...][czt_in], shape = (czt_id_size,),
                           dtype=zid_type, compression=comp)
    czt_grp.create_dataset(name = 'EventTime', data=f_czt['CZT/EventTime'][...][czt_in], shape = (czt_id_size,),
                           dtype=ztime_type, compression=comp)
    czt_grp.create_dataset(name='ASICTemp', data=f_czt['CZT/AsicTemp'][...][czt_in,:], shape=(czt_id_size,16),
                           dtype=zas_temp_type, compression=comp)
    czt_grp.create_dataset(name='AnodePulseheight', data=f_czt['CZT/AnodePulseheight'][...][czt_in,:, :],
                           shape=(czt_id_size, 16,16), dtype=zan_pul_type, compression=comp)
    czt_grp.create_dataset(name='AnodeTime', data=f_czt['CZT/AnodeTime'][...][czt_in, :, :],
                           shape=(czt_id_size, 16, 16), dtype=zan_time_type, compression=comp)
    czt_grp.create_dataset(name='CathodePulseheight', data=f_czt['CZT/CathodePulseheight'][...][czt_in, :, :],
                           shape=(czt_id_size, 16, 16), dtype=zcat_pul_type, compression=comp)
    czt_grp.create_dataset(name='CathodeTime', data=f_czt['CZT/CathodeTime'][...][czt_in, :, :],
                           shape=(czt_id_size, 16, 16), dtype=zcat_time_type, compression=comp)
    czt_grp.create_dataset(name='PadPulseheight', data=f_czt['CZT/PadPulseheight'][...][czt_in, :, :, :],
                           shape=(czt_id_size, 16, 16, 4), dtype=zpad_pul_type, compression=comp)
    czt_grp.create_dataset(name='Energy', data=f_czt['CZT/Energy'][...][czt_in, :, :],
                           shape=(czt_id_size, 16, 16), dtype=zener_type, compression=comp)
    czt_grp.create_dataset(name='Position', data= f_czt['CZT/Position'][...][czt_in, :, :, :],
                           shape=(czt_id_size, 16, 16, 3), dtype=zpos_type, compression=comp)
    czt_grp.create_dataset(name='CorUTC', data=f_czt['CZT/CorUTC'][...][czt_in],
                           shape=(czt_id_size,), dtype=zutc_type, compression=comp)
    czt_grp.create_dataset(name='BadPadflag', data=f_czt['CZT/BadPadflag'][...][czt_in],
                           shape=(czt_id_size,16,16), dtype=z_badpadflag_type, compression=comp)

    # Old Attributes.
    temp_att_dict = dict(f_czt.attrs.items())  # save all the previous attributes.
    
    # Transfer Old Attributes.
    for keys in temp_att_dict:
        f_cud[f'Events/CZT/'].attrs[f'czt_{keys}'] = temp_att_dict[f'{keys}']

def write_csi(f_cud,f_csi,csi_in):
    """Write CSI data.

    Parameters
    ----------
    f_csi : file object
        CsI File object
    csi_in : intarr
        CsI Common index array.
    comp : Boolean
        Compression Flag. Default = True

    """
    comp=COMP
    csi_grp = f_cud.create_group('Events/CsI')

    # Get Type
    cid_type = f_csi["EventID/EventID/values"].dtype
    ctime_type = f_csi["time/UTC/values"].dtype
    cuse_pos_type = f_csi["use_pos/block0_values"].dtype
    cuse_erg_type = f_csi["use_erg/block0_values"].dtype
    #cuse_sipm_type = f_csi["use_sipm/block0_values"].dtype
    cerg_type = f_csi["erg/block0_values"].dtype
    cpos_type = f_csi["pos/block0_values"].dtype
    #csipm_type = f_csi["sipm/block0_values"].dtype


    #define Shape variable
    csi_id_size = len(csi_in)

    # Data
    evtid_grp = f_cud.create_group('Events/CsI/EventID')
    evtid_grp.create_dataset(name='EventID', data=f_csi['EventID/EventID/values'][...][csi_in],
                             shape=(csi_id_size,), dtype=cid_type, compression=comp)

    evttime_grp = f_cud.create_group('Events/CsI/time')
    evttime_grp.create_dataset(name='UTC', data=f_csi['time/UTC/values'][...][csi_in],
                               shape=(csi_id_size,), dtype=ctime_type, compression=comp)

    csi_grp.create_dataset(name='use_pos', data=f_csi["use_pos/block0_values"][...][csi_in],
                               shape=(csi_id_size, 30), dtype=cuse_pos_type, compression=comp)

    csi_grp.create_dataset(name='use_erg', data=f_csi[f"use_erg/block0_values"][...][csi_in],
                               shape=(csi_id_size, 30), dtype=cuse_erg_type, compression=comp)

    #csi_grp.create_dataset(name='use_sipm', data=f_csi[f"use_sipm/block0_values"][...][csi_in],
                                #shape=(csi_id_size, 60), dtype=cuse_sipm_type, compression=comp)

    csi_grp.create_dataset(name='erg', data=f_csi[f"erg/block0_values"][...][csi_in],
                           shape=(csi_id_size, 30), dtype=cerg_type, compression=comp)

    csi_grp.create_dataset(name='pos', data=f_csi[f"pos/block0_values"][...][csi_in],
                           shape=(csi_id_size, 30), dtype=cpos_type, compression=comp)

    #csi_grp.create_dataset(name='sipm', data=f_csi[f"sipm/block0_values"][...][csi_in],
                            #shape=(csi_id_size, 60), dtype=csipm_type, compression=comp)

    # L3 level
    # Old Attributes
    temp_att_dict = dict(f_csi.attrs.items())  # save all the previous attributes.
    # Put old attributes.
    for keys in temp_att_dict:
        f_cud[f'Events/CsI/'].attrs[f'csi_{keys}'] = temp_att_dict[f'{keys}']

def create_cudfile(f_cud,trk_file=None,  czt_file=None, csi_file=None, acd_file=None , trg_file=None):
    """Create the Cud file 

    Args:
        f_cud (h5 object): opened file object of cud hdf5
        trk_file (str, optional): file path of Tracker L3 file. Defaults to None.
        czt_file (str, optional): file path of CZT L2 file. Defaults to None.
        csi_file (str, optional): file path of CsI L3 file. Defaults to None.
        acd_file (str, optional): file path of ACD L3 file. Defaults to None.
        trg_file (str, optional): file path of Trigger file. Defaults to None.
    """
    dstart1 = datetime.utcnow() # Start time to calculate execution of script.
    verbose=VERBOSE
    comp=COMP

    #STEP: Check the 3 files and few checks
    if (trk_file is None) or (czt_file is None) or (csi_file is None):
        print("ERROR 404: TRK, CZT or CSI file missing:")
        return

    fname_tra = trk_file.split('/')[-1]
    fname_csi = csi_file.split('/')[-1]
    fname_czt = czt_file.split('/')[-1]
    
    if trg_file is not None:
        fname_trg = trg_file.split('/')[-1]
    else:
        fname_trg='None'

    if acd_file is not None:
        fname_acd = acd_file.split('/')[-1]
    else:
        fname_acd='None'

    #STEP: Store names
    f_cud.attrs['cud_file_trk'] = fname_tra
    f_cud.attrs['cud_file_csi'] = fname_csi
    f_cud.attrs['cud_file_czt'] = fname_czt
    f_cud.attrs['cud_file_trg'] = fname_trg
    f_cud.attrs['cud_file_acd'] = fname_acd

    cud_grp = f_cud.create_group('Events')
    # dend1 = datetime.utcnow()
    # print(f">~~~ Mini execution step time : {(dend1-dstart1).total_seconds()} --- <<<<<<<")
    # dstart1 = datetime.utcnow() # Start time to calculate execution of script.
    #STEP: Tracker first
    print('---- Tracker Started ----')
    with h5py.File(trk_file, 'r') as f_trk:
        trk_evtid = f_trk['tracker/event_id'][...]
        trk_index = np.arange(len(trk_evtid))
        print(f" No evt Trk :{len(trk_evtid)} ")
        write_trk(f_cud,f_trk,trk_index)
    print('---- Tracker Ended ----')
    # dend1 = datetime.utcnow()
    # print(f">~~~ Mini execution step time : {(dend1-dstart1).total_seconds()} --- <<<<<<<")
    # dstart1 = datetime.utcnow() # Start time to calculate execution of script.
    #STEP:  CZT Next
    print('---- CZT started ----')
    with h5py.File(czt_file, 'r') as f_czt:
        f_czt.copy(f_czt["/CZT"],f_cud["/Events"], name='CZT')
        
    print('---- CZT Done ----')
    # dend1 = datetime.utcnow()
    # print(f">~~~ Mini execution step time : {(dend1-dstart1).total_seconds()} --- <<<<<<<")
    # dstart1 = datetime.utcnow() # Start time to calculate execution of script.
    #STEP:  CsI Next
    print('---- CSI Started ----')
    with h5py.File(csi_file, 'r') as f_csi:
        csi_evtid = f_csi['EventID/EventID/values'][...]
        csi_index = np.arange(len(csi_evtid))
        print(f" No evt CsI :{len(csi_evtid)} ")
        write_csi(f_cud,f_csi, csi_index)
    print('----CSI done ----')
    # dend1 = datetime.utcnow()
    # print(f">~~~ Mini execution step time : {(dend1-dstart1).total_seconds()} --- <<<<<<<")
    # dstart1 = datetime.utcnow() # Start time to calculate execution of script.
    # STEP: -- Attributes -- CUD level
    # f_cud.attrs['cud_nevt_trk'] = len(trk_evtid)
    # f_cud.attrs['cud_nevt_czt'] = len(czt_evtid)
    # f_cud.attrs['cud_nevt_csi'] = len(csi_evtid)

    # STEP: -- ACD --
    if acd_file is not None:
        print('----ACD Start ----')
        with h5py.File(acd_file,'r') as f_acd:
            f_acd.copy(f_acd["/"],f_cud["/Events"], name='ACD')
        print('----ACD done ----')
    # dend1 = datetime.utcnow()
    # print(f">~~~ Mini execution step time : {(dend1-dstart1).total_seconds()} --- <<<<<<<")
    # dstart1 = datetime.utcnow() # Start time to calculate execution of script.
    # STEP: -- TM --
    if trg_file is not None:
        print('----Trigger Start ----')
        with h5py.File(trg_file,'r') as f_trg:
            cud_grp = f_cud.create_group('Summary')
            f_trg.copy(f_trg["/trigger"],f_cud["/Summary"], name='Trigger')

            # Old Attributes
            temp_att_dict = dict(f_trg.attrs.items())  # save all the previous attributes.
            for keys in temp_att_dict:
                f_cud['Summary/Trigger'].attrs[f'trg_{keys}'] = temp_att_dict[f'{keys}']
        print('----Trigger Summary ----')
        print('----Trigger Event Start ----')

        write_tm(f_cud,trg_file)    
        print('----Trigger End ----')
    dend1 = datetime.utcnow()
    # print(f">~~~ Mini execution step time : {(dend1-dstart1).total_seconds()} --- <<<<<<<")


def write_tm(f_cud,trg_file):
    """Write a TM event dataset that outlines events and event coinc modes.

    Args:
        f_cud (h5py object): cud file object
        trg_file (filepath): triggger filepath
    """

    comp=COMP
    tm_grp = f_cud.create_group('Events/TM')
    
    df = pd.read_hdf(trg_file)

    # Convert the CoHitCD to binary and create an array of dataset.
    #binary_arr = np.vstack(df['CoHitCD'].apply(lambda x: [bool(int(bit)) for bit in str(bin(x))[2:].zfill(16)]))
    # NOTE: WE have to reverse the binary as LSB is from the right.
    reversed_binary_array = np.vstack(df['CoHitCD'].apply(lambda x: [bool(int(bit)) for bit in str(bin(x))[2:].zfill(16)][::-1]))

    tm_grp.create_dataset(name = 'coincidence', data=reversed_binary_array, shape = reversed_binary_array.shape,
                           dtype=reversed_binary_array.dtype, compression=comp)

    f_cud[f'Events/TM/coincidence'].attrs[f'modes'] = f'0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F'

    df_tm_coin = pd.DataFrame(reversed_binary_array)
    true_cnt = df_tm_coin.sum()
    print()
    print("--- TM Stats ---: Note 10-15 = A,B,C,D,E,F")
    print(true_cnt)
    print()

    # event id. 
    evtid = np.array(df['EventID'][...])
    tm_grp.create_dataset(name = 'EventID', data=evtid, shape = evtid.shape,
                           dtype=evtid.dtype, compression=comp)

def update_comp():
    """A function to change the compression in case we dont want the h5 compression. 
    (Currently not in use but created just in case.)
    """
    global COMP
    COMP = False

def update_verbose(verbose_flag):
    global VERBOSE
    VERBOSE = verbose_flag

def check_tm(tm_file):
    if (tm_file.find(".tm.") >= 0) or (tm_file.find("TM.") >= 0):
        return True
    else:
        return False

def check_acd(acd_file):
    if acd_file.find("ACD") >= 0:
        return True
    else:
        return False

def cli():
    parser = argparse.ArgumentParser(description="Generate CUD using L3 from subsystems")
    parser.add_argument("-v", action='store_true', help=" Verbose flag to monitor progress or to debug")
    
    parser.add_argument('trk_file',help='Tracker File name')
    parser.add_argument('czt_file',help='CZT File name')
    parser.add_argument('csi_file',help='CsI File name')
    
    parser.add_argument('file1',nargs='?',default=None,help='1st optional file (should contain "ACD" or ".tm." in the filename)')
    parser.add_argument('file2',nargs='?',default=None,help='2nd optional file (should contain "ACD" or ".tm." in the filename)')
    
    parser.add_argument("--outfile", help='Name of the CUD file. The extension of *.CUD.h5 is appended to the name. If not defined, tracker filename used.')

    args = parser.parse_args()
    dstart = datetime.utcnow() # Start time to calculate execution of script.
    
    comp = True
    
    verbose_flag = False
    if args.v:
        verbose_flag = True
        update_verbose(verbose_flag)

    trk_file = args.trk_file.rstrip()
    czt_file = args.czt_file.rstrip()
    csi_file = args.csi_file.rstrip()

    #-- check if file1 or file2 is trigger file --
    trg_file = None
    acd_file = None

    if args.file1 is not None:
        if check_tm(args.file1):
            trg_file = args.file1.rstrip()
        elif check_acd(args.file1):
            acd_file = args.file1.rstrip()

    if args.file2 is not None:
        if check_tm(args.file2):
            trg_file = args.file2.rstrip()
        elif check_acd(args.file2):
            acd_file = args.file2.rstrip()
    
    fpath = trk_file[0:trk_file.rfind('/')+1]
    if args.outfile:
        outfile = f"{fpath}{args.outfile}.CUD.h5"
    else:
        outfile = f"{fpath}cud_combined.CUD.h5"
    print()
    print("<--- PLEASE VERIFY THE FILE NAMES BEFORE PROCEEDING --->")
    print()
    print('Note: The TM file is identified via "*.tm.*", and ACD file via "*ACD*" in the file name')
    print(' ---Input files --- ')
    print(f"    TRK :{trk_file}")
    print(f"    CZT :{czt_file}")
    print(f"    CSI :{csi_file}")
    print(f"    ACD :{acd_file}")
    print(f"    TRG :{trg_file}")
    print()
    print(f"Output File: {outfile}")
    print()
    input("Press any key to continue ......")

    if os.path.exists(outfile):
        os.remove(outfile)

    with h5py.File(outfile, "w") as f_cud:
        f_cud.attrs['gencud_version']=VERSION
        create_cudfile(f_cud, trk_file = trk_file, czt_file = czt_file, csi_file=csi_file, acd_file=acd_file, trg_file = trg_file)
    
    dend = datetime.utcnow()
    print(f">>>>>>> --- Ending Script: Execution Time [s] : {(dend-dstart).total_seconds()} --- <<<<<<<")

if __name__ == "__main__":
    cli()

    

    
                 

    

    

    
