#!/usr/bin/env python
""" This is script used to convert a given CUD h5 to Revan Readable (RR) format. This is a modified version of CUD2RR.

    The Revan readable format is simply a record of each event interaction positon and energy (which also defines the
    detector type). So this script, selects each event id, extracts the information from CUD h5 and rewrites in the
    MEGAlib language that Revan understands.


    usage

    >>> python cud2rr file_path

"""

# NOTE:For time, we are taking from CSI, if not CSI then CZT if neither, then Tracker.
# The revan readable is the sim file (we name it evta) that cosima can read.
# STEPS:
# Basic workflow:
#       CUD does not select common event ids anymore
#       1. We get the unique event ids (union) from all subsystems. 
#       2. We loop through each event id to get hits.
#           2.1: We verify if that event id present in the nrg eventid.
#           2.2: For each event id, we grab the energy and position info.
#          
#       And redefine that as hit info for Revan readable.

# Versions
# 2.0a fixed units in Csi.
# 2.1 fixing CZT and modifying for non-common event ids.
# 3.0 : Sambid Wasti 030823
# 3.1a: Redesigned and removed a lot of unnecessary programming block.
# 3.1b: Taking the z position from czt defined by the czt L2
# 3.1c: Quick fix where Tracker time (which is not relevant) was mismatching between event id and
#       Nrg event id one.
# 3.1d: Optimized with few memory and loading things in and outside loops. Notes in notebook.
# 3.1e: Introducing filtering via TM mode.

import numpy as np
import h5py
import time
import argparse
import sys
from datetime import datetime


# -- Global Variables --
# IV.1) TKR
CUD2RR_VERSION = '3.1d' #
MEGA_VERSION = 101 # This is the MEGALib version.

# Software Threshold (kev)
TRK_SOFT_THRS = 1.00
CSI_SOFT_THRS = 1.00
CZT_SOFT_THRS = 1.00

def get_tracker_hits(trk_ene_arr):
    """_summary_

    Args:
        trk_ene_arr (np.array): array of tracker energy and position.

    Returns:
        np.array: returns 4 array of separated, energy, hitpositions.
    """
    hit_trk_ene = trk_ene_arr[:, 4]  # Extract energy column directly

    hit_trk_xpos = trk_ene_arr[:, 1]  # Extract x-position column directly
    hit_trk_ypos = trk_ene_arr[:, 2]  # Extract y-position column directly
    hit_trk_zpos = trk_ene_arr[:, 3]  # Extract z-position column directly

    return hit_trk_ene, hit_trk_xpos, hit_trk_ypos, hit_trk_zpos

def get_csi_hits(csi_ene_arr, csi_pos_arr):
    """ Read in the CSI data and return positon.

    This is called for each event id.

    Parameters
    ----------
    csi_ene_arr : np.array
        Energy Array of shape (30)
    csi_pos_arr : np.array
        Position Array of shape (30)

    Returns
    -------

    """
    hit_csi_ene = []
    hit_csi_xpos = []
    hit_csi_ypos = []
    hit_csi_zpos = []

    rowPos = np.linspace(0, 5, 6) * 1.9
    rowPos = rowPos - np.mean(rowPos)

    layerPos = np.linspace(0, 4, 5) * 1.9
    layerPos = layerPos-np.max(layerPos)-11.172-1.67/2

    c = csi_ene_arr.shape[0]
    for ihit in range(c):
        for layer in range(0, 5):
            for row in range(0, 6):
                logNumber = layer * 6 + row
                if csi_ene_arr[ihit,logNumber] > CSI_SOFT_THRS:
                    if (layer % 2 == 0):
                        xpos = float(-csi_pos_arr[ihit,logNumber])/10.0
                        ypos = rowPos[row]
                    else:
                        xpos = rowPos[row]
                        ypos = float(-csi_pos_arr[ihit,logNumber])/10.0

                    zpos = layerPos[4 - layer]
                    ene = csi_ene_arr[ihit,logNumber]

                    hit_csi_ene.append(float(ene))
                    hit_csi_xpos.append(float(xpos))
                    hit_csi_ypos.append(float(ypos))
                    hit_csi_zpos.append(float(zpos))


    return np.array(hit_csi_ene), np.array(hit_csi_xpos), np.array(hit_csi_ypos), np.array(hit_csi_zpos)

def get_czt_hits(czt_ene_arr, czt_pos_arr, bpf_arr):
    """Get CZT hits

    Args:
        czt_ene_arr (np.array): energy, float
        czt_pos_arr (np.array): position, float
        bpf_arr (np.array): bad pad flag, bool

    Returns:
        _type_: _description_
    """

    hit_czt_ene = []
    hit_czt_xpos = []
    hit_czt_ypos = []
    hit_czt_zpos = []
    
    [c,a,b] = czt_ene_arr.shape
    
    for ihit in np.arange(c):
        for block in np.arange(a):
            for bar in np.arange(b):
                if czt_ene_arr[ihit, block, bar] > CZT_SOFT_THRS:
                    if bpf_arr[ihit,block,bar] == False:
                        ene = czt_ene_arr[ihit, block, bar]
                        
                        hit_x = czt_pos_arr[ihit, block,bar,0]
                        hit_y = czt_pos_arr[ihit, block,bar,1]
                        hit_z = czt_pos_arr[ihit, block,bar,2]
                        
                        hit_czt_ene.append(float(ene))
                        hit_czt_xpos.append(float(hit_x))
                        hit_czt_ypos.append(float(hit_y))
                        hit_czt_zpos.append(float(hit_z))
                        # hit_czt_zpos.append(float(-4.68))
   
    return np.array(hit_czt_ene),np.array(hit_czt_xpos), np.array(hit_czt_ypos), np.array(hit_czt_zpos)

def get_acd_hits(acd_ene_arr, acd_pos_arr):
    
    hit_acd_ene = []
    hit_acd_xpos = []
    hit_acd_ypos = []
    hit_acd_zpos = []

    acd_ene_arr = acd_ene_arr[0]
    acd_pos_arr = acd_pos_arr[0]
    for acd_panel in range(acd_ene_arr.shape[0]): 
        if acd_ene_arr[acd_panel] > 0.0:
            hit_acd_ene.append(acd_ene_arr[acd_panel])
            hit_acd_xpos.append(acd_pos_arr[acd_panel,0])
            hit_acd_ypos.append(acd_pos_arr[acd_panel,1])
            hit_acd_zpos.append(acd_pos_arr[acd_panel,2])
        
    
    return np.array(hit_acd_ene), np.array(hit_acd_xpos), np.array(hit_acd_ypos), np.array(hit_acd_zpos)

def check_trigger_mode(tm_mode, coin_dataset):
    """Checks the trigger mode and returns true if it matches.

    Args:
        tm_mode (str): trig mode letter in strings
        coin_dataset (bool array): boolean array of (16,) for each of the trig conditions.
    """

    # Find the index to compare the boolean flag.
    trigger_modes = '0123456789ABCDEF'
    trig_index = trigger_modes.index(tm_mode)
    
    if coin_dataset[trig_index] == True:
        return True

    return False

def main(infile="", outfile="", verbose =False, tm_mode = None):

    with h5py.File(infile, "r") as f_cud:
        print(f'--> Reading Datasets. ')
        
        # If TM mode is selected. need to verify if TM mode present. 
        if tm_mode is not None:
            tkey = f_cud['Events'].keys()
            if not 'TM' in tkey:
                raise AtrributeError("TM dataset doesn't exist in CUD file")

        trk_nrg = f_cud["Events/TKR/Energy"][...]
        trk_evtid = f_cud['Events/TKR/EventID'][...]        #Note, not all event ids would be present in nrg.
        
        trk_nr_id =trk_nrg[:,0]   # Tracker ID from the energy dataset 
        trk_evt =  trk_nr_id
        
        # since tracker energy is resolved by hits and non-unique need to to this for time.
        trk_indices = []
        for value in trk_evt:
            # Find the indices where the value matches in trk_eventid
            indices = np.where(trk_evtid == value)[0]
            # Extend trk_indices with these indices
            trk_indices.extend(indices)

        # Convert trk_indices to a NumPy array
        trk_indices = np.array(trk_indices)


        print(f'Trk Events :{len(trk_nr_id)}')
        print(f'--> Trk done. ')
        
        czt_evta= f_cud['Events/CZT/EventID'][...]
        czt_evt = czt_evta[np.where(czt_evta>0)]
        czt_nrg = f_cud["Events/CZT/Energy"][...]
        czt_nrg = czt_nrg[np.where(czt_evta>0)]
        czt_pos = f_cud["Events/CZT/Position"][...]
        czt_pos = czt_pos[np.where(czt_evta>0)]
        
        print(f'CZT Events :{len(czt_evt)}')
        print(f'--> CZT done. ')

        csi_evt= f_cud['Events/CsI/EventID/EventID'][...]
        csi_nrg = f_cud["Events/CsI/erg"][...]
        csi_pos = f_cud["Events/CsI/pos"][...]
    
        print(f'CsI Events :{len(csi_evt)}')
        print(f'--> CSI done. ')

        acd_evt = f_cud['Events/ACD/EventID'][...]
        acd_nrg = f_cud['Events/ACD/Energy'][...]
        acd_pos = f_cud['Events/ACD/Position'][...]
        print(f'--> ACD done. ')

        print(f'--- Getting Unique Event ID ---')
        delme_arr = np.union1d(trk_evt,czt_evt)
        delme_arr1= np.union1d(delme_arr,acd_evt)
        uniq_evt_id_arr = np.union1d(delme_arr1,csi_evt)
        n_evts = uniq_evt_id_arr.shape[0]
        print(f'--- trk_evt ---')


        hit_enes = []  # Define a list of hit energies, each input is an array
        hit_xpos = []
        hit_ypos = []
        hit_zpos = []

        # STEP:-- Get time -- 
        # dend1 = datetime.utcnow()
        # print(f">~~~ Mini execution step time : {(dend1-dstart1).total_seconds()} --- <<<<<<<")
        # dstart1 = datetime.utcnow() # Start time to calculate execution of script.
    
        with open(outfile, 'w') as outf1:
            # dend1 = datetime.utcnow()
            # print(f">~~~ Mini execution step time : {(dend1-dstart1).total_seconds()} --- <<<<<<<")
            # dstart1 = datetime.utcnow() # Start time to calculate execution of script.
            
            print("Starting file writing as we define events")
            # HEADER
            outf1.write(f"#Revan Readable Converted File\n")
            outf1.write(f"#Created by cud2rr.py ver: {CUD2RR_VERSION}\n")
            outf1.write(f"\n")

            outf1.write(f"Type      EVTA\n")
            outf1.write(f"Version   {MEGA_VERSION}\n")
            outf1.write(f"\n")
            # ----

            #=== Time ====
            czt_time = f_cud["Events/CZT/EventTime"][...]/1e9
            czt_time = czt_time[np.where(czt_evta>0)]
            csi_time = f_cud["Events/CsI/time/UTC"][...]/1e9
            acd_time = f_cud["Events/ACD/UTC"][...]/1e9
            trk_time_all= f_cud['Events/TKR/EventTime'][...]/1e9
            trk_time = trk_time_all[trk_indices]
            
            #-- 1st event's start time --
            temp_trk_strtime = trk_time[0]
            temp_czt_strtime = czt_time[0]
            temp_csi_strtime = csi_time[0]
            temp_acd_strtime = acd_time[0]
            start_time_sec = min(temp_trk_strtime,temp_czt_strtime,temp_csi_strtime, temp_acd_strtime)



            bpf = f_cud['Events/CZT/BadPadflag'][...]

            if tm_mode is not None:
                tm_id = f_cud["Events/TM/EventID"][...]
                tm_co = f_cud["Events/TM/coincidence"][...]
                print(tm_id.shape, tm_co.shape)

            # dend1 = datetime.utcnow()
            # print(f">~~~ Mini execution step time : {(dend1-dstart1).total_seconds()} --- <<<<<<<")
            # dstart1 = datetime.utcnow() # Start time to calculate execution of script.
            print(f"Starting per event hits collection: {n_evts}")
            for i_evt in range(n_evts):
                tid =  uniq_evt_id_arr[i_evt]
                
                if i_evt % 10000 == 0:
                    print(i_evt)
                    # dend1 = datetime.utcnow()
                    # print(f">~~~ Mini execution step time : {(dend1-dstart1).total_seconds()} --- <<<<<<<")
                    # dstart1 = datetime.utcnow() # Start time to calculate execution of script.

                if tid == 0: # We want to skip the event id which are 0s.
                    continue

                #TM Mode selector:
                if tm_mode is not None:
                    tm_ind = np.where(tm_id == tid)
                    if len(tm_ind[0])>0:
                        coin_dataset = tm_co[tm_ind][0]
                        coin_flag = check_trigger_mode(tm_mode, coin_dataset)
                        if coin_flag == False:
                            continue # If its not the trigger flag, then continue to next loop.
                    else:
                        # if not present then continue.
                        continue
                


                # STEP:-- Tracker -- 
                hit_tkr_ene = np.array([])
                hit_tkr_x= np.array([])
                hit_tkr_y= np.array([])
                hit_tkr_z= np.array([])
                tloc = np.where(trk_nr_id == tid)
                if len(tloc[0])>0:
                    trk_nrg_arr = trk_nrg[tloc]
                    hit_tkr_ene, hit_tkr_x, hit_tkr_y, hit_tkr_z = get_tracker_hits(trk_nrg_arr)
                    evttime = (trk_time [tloc] - start_time_sec)
                
                # STEP:-- CZT --
                hit_czt_ene = np.array([])
                hit_czt_x= np.array([])
                hit_czt_y= np.array([])
                hit_czt_z= np.array([])
                tloc = np.where(czt_evt == tid)
                if len(tloc[0])>0:
                    czt_ene_arr = czt_nrg[tloc]
                    czt_pos_arr = czt_pos[tloc]
                    bpf_arr = bpf[tloc]
                    hit_czt_ene, hit_czt_x, hit_czt_y, hit_czt_z = get_czt_hits(czt_ene_arr,czt_pos_arr, bpf_arr)
                    evttime = (czt_time[tloc]-start_time_sec) # get CZT time

                # STEP: -- ACD --
                hit_acd_ene = np.array([])
                hit_acd_x= np.array([])
                hit_acd_y= np.array([])
                hit_acd_z= np.array([])
                tloc = np.where(acd_evt == tid)
                if len(tloc[0])>0:
                    acd_ene_arr = acd_nrg[tloc[0]]
                    acd_pos_arr = acd_pos[tloc[0]]
                    hit_acd_ene, hit_acd_x, hit_acd_y, hit_acd_z = get_acd_hits(acd_ene_arr, acd_pos_arr)
                    evttime = (acd_time[tloc[0]]  -start_time_sec)

                # STEP:-- CSI --
                hit_csi_ene = np.array([])
                hit_csi_x= np.array([])
                hit_csi_y= np.array([])
                hit_csi_z= np.array([])
                tloc = np.where(csi_evt == tid)
                if len(tloc[0])>0:
                    csi_ene_arr = csi_nrg[tloc]
                    csi_pos_arr = csi_pos[tloc]
                    hit_csi_ene, hit_csi_x, hit_csi_y, hit_csi_z = get_csi_hits(csi_ene_arr, csi_pos_arr)
                    evttime = (csi_time[tloc]  -start_time_sec)
                
                

 
                # Define the number of hits in each subsystem for the event
                n_hit_tkr = hit_tkr_ene.size
                n_hit_czt = hit_czt_ene.size
                n_hit_csi = hit_csi_ene.size
                n_hit_acd = hit_acd_ene.size


                this_n_hits = n_hit_tkr + n_hit_csi + n_hit_czt + n_hit_acd  # total number of hits for the event
                if this_n_hits <= 0:
                    continue
                

                evt_id = tid
                outf1.write(f'SE\n')  # mandatory.
                outf1.write(f'ID {evt_id}\n')
                outf1.write(f'TI {evttime[0]}\n')
                #write Tracker
                for i in range(n_hit_tkr):
                    outf1.write( f"HTsim 1;\t{hit_tkr_x[i]:.5f};\t{hit_tkr_y[i]:.5f};\t{hit_tkr_z[i]:.5f};\t{hit_tkr_ene[i]:.5f}\n")
                #write CZT
                for i in range(n_hit_czt):
                    outf1.write( f"HTsim 7;\t{hit_czt_x[i]:.5f};\t{hit_czt_y[i]:.5f};\t{hit_czt_z[i]:.5f};\t{hit_czt_ene[i]:.5f}\n")
                #write CsI
                for i in range(n_hit_csi):
                    outf1.write( f"HTsim 2;\t{hit_csi_x[i]:.5f};\t{hit_csi_y[i]:.5f};\t{hit_csi_z[i]:.5f};\t{hit_csi_ene[i]:.5f}\n")
                #write ACD
                for i in range(n_hit_acd):
                    outf1.write( f"HTsim 4;\t{hit_acd_x[i]:.5f};\t{hit_acd_y[i]:.5f};\t{hit_acd_z[i]:.5f};\t{hit_acd_ene[i]:.5f}\n")
                

            outf1.write('\n' + 'EN')
        
            if verbose:
                print('--- Energy info decoded.')

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Parse the h5 file to get the Revan Readable")
    parser.add_argument("infile", help="Path to input cud h5 file", default="empty")
    parser.add_argument("--mode", metavar='TM_mode', type=str, help="TM Mode filter." , default=None)

    args = parser.parse_args()

    # Get infile, filepath and filename
    infile = args.infile
    fpath = infile[0:infile.rfind('/')+1]
    fname = infile[infile.rfind('/')+1:]
    
    if args.mode is not None:
        # Check for validation of TM mode.
        if args.mode not in '0123456789ABCDEF':
            raise AttributeError("Invalid trigger mode. The trigger mode should be a value between 0-9, A-F.")
        outfile= f"{fpath}coin{args.mode}_{fname.split('.CUD.h5')[0]}.evta"
        print(f"TM Mode set to : {args.mode}")
    else:
        outfile= f"{infile.split('.CUD.h5')[0]}.evta"
    print()
    print(f"Outfile: {outfile}")
    print()
    dstart = datetime.utcnow() # Start time to calculate execution of script.
    main(infile=infile, outfile=outfile , verbose = True, tm_mode=args.mode)
    dend = datetime.utcnow()
    print(f">>>>>>> --- Ending Script: Execution Time [s] : {(dend-dstart).total_seconds()} --- <<<<<<<")
