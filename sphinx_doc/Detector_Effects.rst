Detector\_Effects package
=========================

.. automodule:: Detector_Effects
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

Detector\_Effects.ACD\_RevCal module
------------------------------------

.. automodule:: Detector_Effects.ACD_RevCal
   :members:
   :undoc-members:
   :show-inheritance:

Detector\_Effects.CZT\_RevCal module
------------------------------------

.. automodule:: Detector_Effects.CZT_RevCal
   :members:
   :undoc-members:
   :show-inheritance:

Detector\_Effects.CsI\_RevCal module
------------------------------------

.. automodule:: Detector_Effects.CsI_RevCal
   :members:
   :undoc-members:
   :show-inheritance:

Detector\_Effects.NRL\_Math module
----------------------------------

.. automodule:: Detector_Effects.NRL_Math
   :members:
   :undoc-members:
   :show-inheritance:

Detector\_Effects.SIM2CUD\_old module
-------------------------------------

.. automodule:: Detector_Effects.SIM2CUD_old
   :members:
   :undoc-members:
   :show-inheritance:

Detector\_Effects.TKR\_RevCal module
------------------------------------

.. automodule:: Detector_Effects.TKR_RevCal
   :members:
   :undoc-members:
   :show-inheritance:

Detector\_Effects.additionalStatistics module
---------------------------------------------

.. automodule:: Detector_Effects.additionalStatistics
   :members:
   :undoc-members:
   :show-inheritance:

Detector\_Effects.create\_calibration\_mask module
--------------------------------------------------

.. automodule:: Detector_Effects.create_calibration_mask
   :members:
   :undoc-members:
   :show-inheritance:

Detector\_Effects.desh5 module
------------------------------

.. automodule:: Detector_Effects.desh5
   :members:
   :undoc-members:
   :show-inheritance:

Detector\_Effects.peakFindingLimits module
------------------------------------------

.. automodule:: Detector_Effects.peakFindingLimits
   :members:
   :undoc-members:
   :show-inheritance:

Detector\_Effects.sim2h5 module
-------------------------------

.. automodule:: Detector_Effects.sim2h5
   :members:
   :undoc-members:
   :show-inheritance:
