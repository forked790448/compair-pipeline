# Compair Pipeline
This repository consists of scripts for the Data pipeline for Compair. It ranges from combining data files from each subsytem at Compair Unified Data (CUD), generating megalib compatible files for Revan and Mimrec and for Detecter Effects Engine (DEE) and simulated files. This document outlines the steps, components, scripts and different aspects of the Compair Pipeline.              

The **forward pipeline** consits of the steps the data takes form its raw form, for each subsystem, to its higher level (L1,L2,L3,etc), combining to the CUD form and generating the MEGAlib readable (evta) file. 

The simulated file, going through the DEE is defined as the reverse pipeline and the DEE takes the simulated (*.sim) file to L0 files for individual subsystem processing. 


A detailed descriptions of the scripts used are found under **modules**.          
The description of the CUD file is found under **CUD File Description**.        

