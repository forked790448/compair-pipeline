# Compair Unified Data (CUD)

Compair Unified Data (CUD) is where we group all of the processed data from different subsystems into one file. The pre-CUD levels are determined by each of the subsystems. Currently, the pre-CUD levels (The last processing level being combined at CUD) are `L3` for tracker and CsI, `L2` for CZT and `L1` for trigger. ACD data are L3 and TM files are hdf5 converted. The cud files are with the extensions `*.CUD.h5`

## CUD Format
> Last Updated: Sambid Wasti - Feb 7, 2023

This outlines the Compair unified data file format. The file is a hdf5 file with the extension `*.h5`. The format defined here is extracted from google slide: [ComPair Unified Data Format](https://docs.google.com/presentation/d/1HXNMCsiqBGnc5Qnm2L_wAVkRMgcpFk0dn8KV58n7MwU/edit#slide=id.g1237604b7c9_0_0)


## H5 Data Structure

H5 files are organized in a directory like structure.      
**dtset = dataset**      
**attrs = attributes**    
**n_ev = no. of events**      

### H5 groups and datasets.

|Type |Path  | Shape | DType| Description |
|-----|------|-------|---------|-------------| 
|group|/Events| | |Events|
||||||
|group|/Events/CZT                  |               |     | CZT Events|
|dtset|/Events/CZT/ASICTemp         |(n_ev,16)      | u16 | ASIC Temperature|
|dtset|/Events/CZT/AnodePulseheight |(n_ev,16,16)   | u16 | Individual anode pulse heights (ADCU)|
|dtset|/Events/CZT/AnodeTime        |(n_ev,16,16)   | u16 | Individual anode times (ADCU)|
|dtset|/Events/CZT/BadPadFlag       |(n_ev,16,16)   | bool| True if all 4 pad good, False otherwise|
|dtset|/Events/CZT/CathodePulseheight|(n_ev,16,16)  | u16 | Individual cathode pulse heights (ADCU)|
|dtset|/Events/CZT/CathodeTime      |(n_ev,16,16)   | u16 | Individual cathode times (ADCU)|
|dtset|/Events/CZT/CorUTC           |(n_ev)         | f64 | Corrected UTC|
|dtset|/Events/CZT/Energy           |(n_ev,16,16)   | f32 | Individual bar pseudo-energies (keV)|
|dtset|/Events/CZT/EventID          |(n_ev)         | u16 | ASIC event ID|
|dtset|/Events/CZT/EventTime        |(n_ev)         | u16 | ASIC event timestamp |
|dtset|/Events/CZT/PadPulseheight   |(n_ev,16,16,4) | u16 | Individual pad pulseheights (ADCU) |
|dtset|/Events/CZT/Position         |(n_ev,16,16,3) | f32 | Interaction position in bar (cm) |
||||||
|group|/Events/CsI                  |               |     |CsI Events|
|group|/Events/CsI/EventID          |               |     | |
|dtset|/Events/CsI/EventID/EventID  |(n_ev)         | u16 | ASIC event ID |
|dtset|/Events/CsI/erg              |(n_ev,30)      | f32 | Energies in log (keV) |
|dtset|/Events/CsI/pos              |(n_ev,30)      | f32 | Position down log (mm)|
|group|/Events/CsI/time             |               |     | |
|dtset|/Events/CsI/time/UTC         |(n_ev)         | f64 | UTC Time|
|dtset|/Events/CsI/time/use_erg     |(n_ev,30)      | u8  | Position validity|
|dtset|/Events/CsI/time/use_pos     |(n_ev,30)      | u8  | Energy validity|
||||||
|group|/Events/TKR                  |               |     | Tracker Events|
|dtset|/Events/TKR/Energy           |(n_hit,6)      | f32 | Hit info: (evt_id, xpos, ypos, zpos, nrg, qflag)  |
| |                                 |               |     | qflag= 1, good. qflag 2 hit resolved. qflag3 is 2hit avoided comb|  
|dtset|/Events/TKR/EventID         |(n_ev)         | u16 | Event ID|
|dtest|/Events/TKR/EventTime       |(n_ev)         | u16 | Event Time (system time in nanosecond)|
|dtset|/Events/TKR/Pulseheight     |(n_ev,10,2,192) | u16 | Individual strip pulseheights (ADCU) |
||||||
|dtset|/Events/ACD/Energy          |(n_ev,10)      | f32 | Energy deposit as measured by each SiPM array (keV)
|dtest|/Events/ACD/UTC             |(n_ev)         | f64 | Event Time (system time in nanosecond)|
|dtset|/Events/ACD/EventID         |(n_ev)         | i64 | Event ID |
||||||
|group|/Events/TM/             |               |     | |
|dtset|/Events/TM/EventID|        (n_ev)         | i64 | Event ID |
|dtset|/Events/TM/coincidence|        (n_ev,16)         | bool | 16-Boolean values represents TM Modes|
||| |  |0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F |
||||||
|group|/Summary                     | | |Summary |
|group|/Summary/Trigger             | | |Trigger|
|dtset|/Summary/Trigger/axis0       | | |Desc|
|dtset|/Summary/Trigger/axis1       | | |Desc|
|dtset|/Summary/Trigger/block0_items| | |Desc|
|dtset|/Summary/Trigger/block0_values|| |Desc|
|dtset|/Summary/Trigger/block1_items| | |Desc|
|dtset|/Summary/Trigger/block1_values|| |Desc|
|dtset|/Summary/Trigger/block2_items| | |Desc|
|dtset|/Summary/Trigger/block2_values|| |Desc|

---

### H5 attributes

Attributes are ways to add description and information about the file, dataset, etc. 
The attributes from each of the subsystems are preserved and transferred.

|Type |Path             | description |
|-----|------           |-------------|
|attrs|/cud_file_csi    |CsI file used in this CUD|
|attrs|/cud_file_czt    |CZT file used in this CUD|
|attrs|/cud_file_trg    |Trigger file used in this CUD|
|attrs|/cud_file_trk    |Tracker file used in this CUD|
|attrs|/cud_nevt_common |No. of Common Event ID (no. of evt id in CUD)|
|attrs|/cud_nevt_csi    |Total no. of Event IDs at CsI L3|
|attrs|/cud_nevt_czt    |Total no. of Event IDs at CZT L2|
|attrs|/cud_nevt_trk    |Total no. of Event IDs at TRK L3|
|attrs|/gencud_version  |Software version of gen_cud.py|
||||
|attrs|/Events/TKR/trk_compair_daq_git_branch       |Tracker DAQ git branch name|
|attrs|/Events/TKR/trk_compair_daq_git_hash         ||
|attrs|/Events/TKR/trk_conv_l2tol3_ver              |conv_l2tol3.py script's version no.|
|attrs|/Events/TKR/trk_ecalib_file                  |energy calibration file name|
|attrs|/Events/TKR/trk_recording_start_time         | |
|attrs|/Events/TKR/trk_recording_stop_time          | |
||||
|attrs|/Summary/Trigger/CLASS                       | |
|attrs|/Summary/Trigger/TITLE                       | |
|attrs|/Summary/Trigger/VERSION                     | |
|attrs|/Summary/Trigger/axis0_variety               | |
|attrs|/Summary/Trigger/axis1_variety               | |
|attrs|/Summary/Trigger/blcok0_items_variety        | |
|attrs|/Summary/Trigger/blcok1_items_variety        | |
|attrs|/Summary/Trigger/blcok2_items_variety        | |
|attrs|/Summary/Trigger/encoding                    | |
|attrs|/Summary/Trigger/errors                      | |
|attrs|/Summary/Trigger/nblocks                     | |
|attrs|/Summary/Trigger/ndim                        | |
|attrs|/Summary/Trigger/pandas_type                 | |
|attrs|/Summary/Trigger/trg_CLASS                   | |
|attrs|/Summary/Trigger/trg_PYTABLES_FORMAT_VERSION | |
|attrs|/Summary/Trigger/trg_TITLE                   | |
|attrs|/Summary/Trigger/trg_VERSION                 | |
|attrs|/Summary/Trigger/axis0/CLASS                 | |
|attrs|/Summary/Trigger/axis0/FLAVOR                | |
|attrs|/Summary/Trigger/axis0/VERSION               | |
|attrs|/Summary/Trigger/axis0/kind                  | |
|attrs|/Summary/Trigger/axis0/name                  | |
|attrs|/Summary/Trigger/axis0/transposed            | |
|attrs|/Summary/Trigger/axis1/CLASS                 | |
|attrs|/Summary/Trigger/axis1/FLAVOR                | |
|attrs|/Summary/Trigger/axis1/TITLE                 | |
|attrs|/Summary/Trigger/axis1/VERSION               | |
|attrs|/Summary/Trigger/axis1/kind                  | |
|attrs|/Summary/Trigger/axis1/name                  | |
|attrs|/Summary/Trigger/axis1/transposed            | |
|attrs|/Summary/Trigger/block0_items/CLASS          | |
|attrs|/Summary/Trigger/block0_items/FLAVOR         | |
|attrs|/Summary/Trigger/block0_items/TITLE          | |
|attrs|/Summary/Trigger/block0_items/VERSION        | |
|attrs|/Summary/Trigger/block0_items/kind           | |
|attrs|/Summary/Trigger/block0_items/name           | |
|attrs|/Summary/Trigger/block0_items/transposed     | |
|attrs|/Summary/Trigger/block0_values/CLASS         | |
|attrs|/Summary/Trigger/block0_values/FLAVOR        | |
|attrs|/Summary/Trigger/block0_values/TITLE         | |
|attrs|/Summary/Trigger/block0_values/VERSION       | |
|attrs|/Summary/Trigger/block0_values/transposed    | |
|attrs|/Summary/Trigger/block1_items/CLASS          | |
|attrs|/Summary/Trigger/block1_items/FLAVOR         | |
|attrs|/Summary/Trigger/block1_items/TITLE          | |
|attrs|/Summary/Trigger/block1_items/VERSION        | |
|attrs|/Summary/Trigger/block1_items/kind           | |
|attrs|/Summary/Trigger/block1_items/name           | |
|attrs|/Summary/Trigger/block1_items/transposed     | |
|attrs|/Summary/Trigger/block1_values/CLASS         | |
|attrs|/Summary/Trigger/block1_values/FLAVOR        | |
|attrs|/Summary/Trigger/block1_values/TITLE         | |
|attrs|/Summary/Trigger/block1_values/VERSION       | |
|attrs|/Summary/Trigger/block1_values/transposed    | |
|attrs|/Summary/Trigger/block2_items/CLASS          | |
|attrs|/Summary/Trigger/block2_items/FLAVOR         | |
|attrs|/Summary/Trigger/block2_items/TITLE          | |
|attrs|/Summary/Trigger/block2_items/VERSION        | |
|attrs|/Summary/Trigger/block2_items/kind           | |
|attrs|/Summary/Trigger/block2_items/name           | |
|attrs|/Summary/Trigger/block2_items/transposed     | |
|attrs|/Summary/Trigger/block2_values/CLASS         | |
|attrs|/Summary/Trigger/block2_values/FLAVOR        | |
|attrs|/Summary/Trigger/block2_values/TITLE         | |
|attrs|/Summary/Trigger/block2_values/VERSION       | |
|attrs|/Summary/Trigger/block2_values/transposed    | |

