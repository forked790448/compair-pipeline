# Forward Pipeline

Forward pipeline consits of the steps the data takes form its raw form, for each subsystem, to its higher level (L1,L2,L3,etc), combining to the CUD form and generating the MEGAlib readable (evta) file. 

**Data files**      

The subsytems processes the files at their own individual pipelines and uploads them in the [Compair Box Folder](https://app.box.com/folder/164803174681). Those files are then combined to generate CUD files.

## Subsystem processing

Each subsytem takes care of the pre-CUD processing steps. Details and documentation could be found by contacting each of the subsystem. The repositories are present here as a **submodule** or in a folder but these can also be retrieved from their original home.

Each subsytem takes care of the pre-CUD processing steps. Details and documentation could be found by contacting each of the subsystem. Here are some of the repository and documentation links.

The tracker pipeline is present at [tracker pipeline gitlab](https://gitlab.com/mev-astronomy/ComPairAPRA/tracker/ComPair-tracker-pipeline) ([Documentation](https://mev-astronomy.gitlab.io/ComPairAPRA/tracker/ComPair-tracker-pipeline/)). 

The CZT pipeline is present at [CZT pipeline gitlab](https://gitlab.com/mev-astronomy/ComPairAPRA/czt/ComPair-CZT-pipeline). 

The details of CsI pipeline could be requested by reaching out to CsI subsystem.

The ACD pipeline is present at the [ACD pipeline gitlab]()      

The trigger module processing is present in the cudf folder. and The steps to process the trigger module files are shown below:

### TM processing ###
There are two python scripts and  a bash script used to process the TM files (under cudf folder). 

`cudf/trigger.py` consists of functions to process the trigger module files.         
`cudf/test.py` is the initial script used to process the trigger module files ending in `tm.*.data`

```bash
python test.py --conv tm.1324523.data 
```

For multiple TM files, we want to combine the trigger module files and then convert that file to hdf5. The combination is done via simple `cat file1 file2` command. For ease of execution, a bash script named `combine_trigger.sh` to do it for multiple files in a folder. 

```bash
combine_trigger.sh *folder_containing_tm_files
```

## combining the files (gencud)
CUD file is generated using the `gen_cud.py` script:

```bash
python gen_cud.py trkfile cztfile csifile file1 file2
```
Here, trkfile, cztfile, csifile are mandatory. file1 and file2 are optional for tm file or acd file which are optional.

`-v` is an optional parameter for verbose.       

`--outfile outfilename` is an optional parameter for a new output file name. By default it is programmed for "cud_combined.CUD.h5" which can be later renamed depending on the run number or description.

This outputs a file with the extension `*.CUD.h5`.

## MEGAlib readable (cud2rr)
evta file which is readable by MEGAlib is generated via `cud2rr.py`. 

```bash
python cud2rr.py *CUD.h5
```

this generates a file with extension `*.evta` that is readable by MEGAlib's Revan. 

