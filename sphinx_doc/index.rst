.. ComPair-tracker-pipeline documentation master file, created by
   sphinx-quickstart on Wed Jun  2 14:12:31 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ComPair-Pipeline's documentation!
============================================

.. toctree::
   :maxdepth: 5
   :caption: Contents:

   README_Sphinx.md
   fwd.md
   cud.md
   sim.md
   dee.md
   misc.md
   modules

   

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
