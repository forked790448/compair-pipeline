#!/bin/zsh
cp -r _temp_files/conf.py . 
sphinx-apidoc -fFM -o . ../
sphinx-apidoc -fM -o . ../
cp -r _temp_files/* .
mv ./index.txt ./index.rst
make clean
make html
