# Simulations 

The instrument simulation is performed using [MEGAlib](https://megalibtoolkit.com/home.html) toolkit. It uses GEANT4 simulation toolkit (based on C++). There are 3 main components that is required to perform simulations. 1) Geometry that defines the instrument, materials and detector volumes. 2) The source description that defines the incoming particle. 3) The physics that describes the interaction processes between the source and the instrument. 

MEGAlib consists of collection of tools that supports the effort to perform a simulation. Further details on these tools can be found in the [MEGAlib Website](https://megalibtoolkit.com/overview.html). A quick outline of some of these components are extracted and outlined below.

### Geomega
Geometry file is defined by the extension *.geo.setup which can be viewed with MEGAlib tool Geomega. 
We have different geometry files for different instrument setups. These different geometries are stored under Geometry folder in this repository. 
```
geomega -f *.geo.setup
```

### Cosima
Cosima ("Cosmic Simulator") is the MEGAlib's Geant4 based simulator. It is the file which has the source description, links to the instrument geometry and defines the physics list to be used. It outputs a textfile that is readable by other MEGAlib programs. The cosima file is generally defined by the extension *.source. The output of the file is a *.sim file.
```
cosima *.source
``` 

### Revan
Revan ("Real event analyzer") performs event reconstruction. It requires the simulated output file `*.sim` (or similar formatted file `*.evta` for measured run) along with the geometry file to be used. The initiation of the commands, opens up a GUI where we can select reconstruction algorithms, event selctions, etc. to do reconstruction for our selections. The output is a *.tra file.
```
revan -f *.sim *.geo.setup
```

### Mimrec
Mimrec is the imaging tool for MEGAlib. Similar to Revan, the usage command opens up a GUI with the respective files loaded. We can do event selections, coordinate system selections, dimensions, etc.

```
mimrec -f *.tra *.geo.setup
```


---