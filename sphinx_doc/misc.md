# Documentaion Guidelines

## Data formats
We are mainly dealing with `*.h5` format till cud level. The post cud (simulations, revan, mimrec, etc) have their own format defined.

## Auto-Documentation
The repo is documented using [sphinx](https://www.sphinx-doc.org/en/master/). With the help of gitlab-ci, it is autodocumented. However, it recognizes documentation style of numpydoc, googledoc or sphinxdoc docstrings.

### Sphinx documentation
To initiate sphinx, just run `00-sphinx.sh` (it's an executable script with all the commands. To run locally, delete everything except the executable and _temp* folder and run the executable.

### Documentation Styles

#### Docstrings
We are attempting to adhere to the
[numpy docstring guide](https://numpydoc.readthedocs.io/en/latest/format.html) or 
[google docstring guide](https://google.github.io/styleguide/pyguide.html). The differences can be seen here:
[google doc string](https://bwanamarko.alwaysdata.net/napoleon/format_exception.html)



#### Code
Of course abide by [PEP 8](https://www.python.org/dev/peps/pep-0008/), with the exception
of maximum line lengths. We are using 90 characters as our maximum line length. For bonus
points, it is recommended that code is formatted with
[python black](https://pypi.org/project/black/).