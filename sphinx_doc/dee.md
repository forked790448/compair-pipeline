# Detector Effects Engine (DEE)

Detector Effects Engine (DEE) encapsulates the steps and processes that introduces the characteristics of the detector to the simulated (ideal) data. This is achieved in multiple steps.

The scripts and functions used in DEE are in `/Detector_Effects` folder.
Initially, the output of the simulation (using MEGAlib) is a cosima file `*.sim` file. 
This document outlines the journey of this file to raw `L1` level via the DEE.



## Step 1: sim2h5

The first step is to convert the simulated output (`*.sim`) to a hdf5 (`*.h5`) file. The structure of this h5 file is based on cud description. We use the script `sim2h5.py` for this via

```bash
python Detector_Effects/sim2h5.py *.sim
```

This generates a h5 file at the path of the simfile with extension `*.sim.h5`

## Step 2: desh

This step takes the `*.sim.h5` and converts to individual sub-system precud level. This generates 4 files, one for each subsytem. The usage is: 

```bash
pthon Detector_Effects/desh.py *.sim.h5
```
The output files are as following
`TKR_*sim.h5`, `CZT_*sim.h5`, `CsI_*sim.h5` and `TRG_*sim.h5`

## Step 3: RevCal

This is the reverse calibration phase where L1 is generated for each of the subsystem. The Energies are converted back to ADC/pulse-height values. It is at this stage the smearing (gaussian broadening) is introduced.

We have different scripts for each of the subsytems and each of these require some auxilary files such as resolution, energy calibration, etc. These files are in the ComPair box-drive under `ComPair/simulated_files/Resoultion & Calibration Files/`

### Trk_RevCal

This reverse calibration focuses on Tracker. It takes in two additional files: energy calibration file and energy resolution file.

```bash
python Detector_Effects/TKR_RevCal.py TKR_*sim.h5 TRKEnergyCalibrationFile -r -v -s
```
`-v` is an optional parameter for verbose.      
`-s` is an optional parameter for smearing %. Default is 5%. 
`-r` is an optional parameter for a resolution file. The resolution file must be an h5 file with the following format:
\<Layer Name>\values_fwhm_coef. Any layers not included in the layer name stage will be zeroes out. The content of the data is
(side #) x (strip #) x (FWHM)

This outputs tracker L1 file (`TKR_*sim.L1.h5`) that is compatible and can be processed with the tracker forward pipeline.

### CZT_RevCal

This reverse calibration focuses on CZT. It takes in one additional file: energy calibration file. 

```bash
python Detector_Effects/CZT_Reval.py CZT_*.sim.h5 *CZTEnergyCalibrationFile
```

This outputs tracker L1 file (`CZT_*sim.L1.h5`) that is compatible and can be processed with the CZT forward pipeline.

### CsI_RevCal

This reverse calibration focuses on CsI. It takes in two additional file: energy calibration file and energy resolution file. 

```bash
python Detector_Effects/CsI_Reval.py CsI_*.sim.h5 *CsIEnergyCalibrationFile *CsIResoulutionFile
```

This outputs tracker L1 file (`CZT_*sim.L1.h5`) that is compatible and can be processed with the CsI forward pipeline.


---