import numpy as np
import argparse
import re

def eventID_fix(eids):
    eid_str = []
    shifted = []
    new_shifted = []
    offsets = []
    junk = []
    j = 0

    # turn all of eids into strings & check that all eids have len 34
    for i in range(len(eids)):
        eid,t = eids[i]
        eid = int(eid)
        bin_str = bin(eid)
        if len(bin_str) < 34:
            nmissing_bits = 34 - len(bin_str)
            for bit in range(nmissing_bits):
                bin_str = '0b0' + bin_str[2:]
        eid_str.append(bin_str)

    #check which eventIDs are shifted
    for eid in eid_str:
        matched = (re.search('1000000000', eid))
        if matched:
            offset = matched.start() - 2
            if offset != 0:
                shifted.append(j)
                offsets.append(offset)
        else:
            junk.append(j) #sometimes it's junk
        j += 1

    # if there's no shift, move on

    # if there is a shift, try to fix it
    new_eids = eids.copy()
    if len(shifted) > 0:
        prior = ''
        for i in range(offsets[0]):
            prior = prior + '0'
        for i in range(len(shifted)):
            j = shifted[i]
            offset = offsets[i]
            if i > 0 and abs(j - shifted[i-1]) > 1:
                prior = ''
                for k in range(offset):
                    prior = prior + '0'
            old_str = eid_str[j]
            t = eids[j][1]
            pre = old_str[2:offset + 2]
            post = old_str[offset + 2:]
            new_str = '0b' + post + prior
            new = int(new_str,2)
            prior = pre
            new_eids[j] = [new,t]

        # Look for any duplicates
        all_duplicates_found = False
        while not all_duplicates_found:
            current_loop = True
            prior = new_eids[0][0]
            for i in range(len(new_eids)-1):
                current = new_eids[i+1][0]
                if current == prior:
                    new_eids[i][0] -= 1
                    current_loop = False
                prior = current
            if current_loop:
                all_duplicates_found = True

        # evaluate if the fix worked
        new_offsets = []
        j = 0
        for i in range(len(new_eids)):
            eid = new_eids[i][0]
            eid = int(eid)
            matched = (re.search('10000000', bin(eid)))
            if matched:
                offset = matched.start() - 2
                if offset != 0:
                    new_shifted.append(j)
                    new_offsets.append(offset)
            # we already caught the junk
            j += 1
            
    # check if anything isn't len = 34
    new_eids2 = []
    for i in range(len(new_eids)):
        eid,t = new_eids[i]
        eid = int(eid)
        if len(bin(eid)) == 34:
            new_eids2.append([eid,t])
    return new_eids2
    

def read_logs(acd_fnames, csi_fnames):
    acd_eids = []
    acd_trigacks = []
    for acd_fname in acd_fnames:
        acd_f = open(acd_fname,'r')
        acd_lines = acd_f.readlines()
        acd_f.close()
    
        for line in acd_lines:
            if 'E,' in line:
                eid = int(line.split(',')[1])
                t = float(line.split(',')[3])
                acd_eids.append([eid,t])
            if 'C,' in line:
                t = float(line.split(',')[-1])
                acd_trigacks.append(t)
    acd_eids = np.array(acd_eids)
    acd_eids = eventID_fix(acd_eids)
    acd_eids = np.array(acd_eids)
    acd_trigacks = np.array(acd_trigacks)
    
    csi_eids = []
    csi_trigacks = []
    for csi_fname in csi_fnames:
        csi_f = open(csi_fname,'r')
        csi_lines = csi_f.readlines()
        csi_f.close()
    
        for line in csi_lines:
            if 'E,' in line:
                eid = int(line.split(',')[1])
                t = float(line.split(',')[3])
                csi_eids.append([eid,t])
            if 'C,' in line:
                t = float(line.split(',')[-1])
                csi_trigacks.append(t)
    csi_eids = np.array(csi_eids)
    csi_eids = eventID_fix(csi_eids)
    csi_eids = np.array(csi_eids)
    csi_trigacks = np.array(csi_trigacks)
    
    new_acd_eids = []
    for i in range(len(acd_eids)-1):
        eid,t = acd_eids[i]
        new_acd_eids.append([eid,t])
    
        next_eid, next_t = acd_eids[i+1]
        dE = next_eid - eid
        Cs = np.where(np.logical_and(acd_trigacks > t, acd_trigacks < next_t))[0]
        nC = len(Cs)
        if dE == nC and dE > 1:
            for j in range(int(dE - 1)):
                new_eid = eid + 1 + j
                new_t = acd_trigacks[Cs[j]] + 1e-5
                new_acd_eids.append([new_eid,new_t])
    new_acd_eids = np.array(new_acd_eids)
    
    new_csi_eids = []
    for i in range(len(csi_eids)-1):
        eid,t = csi_eids[i]
        new_csi_eids.append([eid,t])
    
        next_eid, next_t = csi_eids[i+1]
        dE = next_eid - eid
        Cs = np.where(np.logical_and(csi_trigacks > t, csi_trigacks < next_t))[0]
        nC = len(Cs)
        if dE == nC and dE > 1:
            for j in range(int(dE - 1)):
                new_eid = eid + 1 + j
                new_t = csi_trigacks[Cs[j]] + 1e-5
                new_csi_eids.append([new_eid,new_t])
    new_csi_eids = np.array(new_csi_eids)
    
    return [new_acd_eids,new_csi_eids]

def find_needs(acd_eids,csi_eids):

    start = int(min(min(acd_eids[:,0]),min(csi_eids[:,0])))
    end = int(max(max(acd_eids[:,0]),max(csi_eids[:,0])))
    
    dropped_acds = []
    for eid in range(start,end):
        if not eid in acd_eids[:,0]:
            dropped_acds.append(eid)
    dropped_acds = np.array(dropped_acds)

    dropped_csis = []
    for eid in range(start,end):
        if not eid in csi_eids[:,0]:
            dropped_csis.append(eid)
    dropped_csis = np.array(dropped_csis)

    dropped_both = np.intersect1d(dropped_acds,dropped_csis)
    
    print(f"ACD:  {len(dropped_acds)}")
    print(f"CsI:  {len(dropped_csis)}")
    print(f"Both: {len(dropped_both)}")
    
    need_acd = []
    for i in range(len(csi_eids)):
        eid,t = csi_eids[i]
        if eid in dropped_acds:
            need_acd.append([eid,t])
    need_acd = np.array(need_acd)

    need_csi = []
    for i in range(len(acd_eids)):
        eid,t = acd_eids[i]
        if eid in dropped_csis:
            need_csi.append([eid,t])
    need_csi = np.array(need_csi)

    return need_acd, need_csi


def write_files(acd_path,new_acd,csi_path,new_csi):
    
    with open(acd_path + 'L0.log','w') as acd_outfile:
        eid_index = 0
        for acd_fname in acd_fnames:
            acd_f = open(acd_fname,'r')
            acd_flines = acd_f.readlines()
            acd_f.close()
        
            for i in range(len(acd_flines)):
                line = acd_flines[i]
                t = float(line.split(',')[-1])
                
                while eid_index < len(new_acd) and new_acd[eid_index,1] < t:
                    acd_outfile.write(f"E,{int(new_acd[eid_index,0])},0,{new_acd[eid_index,1]}\n")
                    eid_index += 1
                
                if not 'E,' in line:
                    acd_outfile.write(line)
                
    with open(csi_path + 'L0.log','w') as csi_outfile:
        eid_index = 0
        for csi_fname in csi_fnames:
            csi_f = open(csi_fname,'r')
            csi_flines = csi_f.readlines()
            csi_f.close()
        
            for i in range(len(csi_flines)):
                line = csi_flines[i]
                t = float(line.split(',')[-1])
                
                while eid_index < len(new_csi) and new_csi[eid_index,1] < t:
                    csi_outfile.write(f"E,{int(new_csi[eid_index,0])},0,{new_csi[eid_index,1]}\n")
                    eid_index += 1
                
                if not 'E,' in line:
                    csi_outfile.write(line)
                
if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("ard_fnames",nargs = '+', help="List of arduino filenames, first half is acd, second half is csi")
    args = parser.parse_args()

    ard_fnames = args.ard_fnames
    nargs = len(ard_fnames)
    
    if nargs%2 == 0:
        acd_fnames = ard_fnames[:int(nargs/2)]
        csi_fnames = ard_fnames[int(nargs/2):]
        
        # Read all of the event IDs in both the acd logs and the csi logs
        
        acd_eids, csi_eids = read_logs(acd_fnames, csi_fnames)
        
        need_acd, need_csi = find_needs(acd_eids,csi_eids)
        
        acd_path = '/'.join(acd_fnames[0].split('/')[:-1]) + '/'
        csi_path = '/'.join(csi_fnames[0].split('/')[:-1]) + '/'
        
        new_acd = acd_eids#np.concatenate((acd_eids,need_acd),axis = 0)
        new_csi = csi_eids#np.concatenate((csi_eids,need_csi),axis = 0)
        
        new_acd = np.sort(new_acd,axis = 1)
        new_csi = np.sort(new_csi,axis = 1)
        
        temp = new_acd[:,0].copy()
        new_acd[:,0] = new_acd[:,1].copy()
        new_acd[:,1] = temp.copy()

        temp = new_csi[:,0].copy()
        new_csi[:,0] = new_csi[:,1].copy()
        new_csi[:,1] = temp.copy()
        
        write_files(acd_path,new_acd,csi_path,new_csi)
    
    else:
        print("Even number of argument required")

                
        