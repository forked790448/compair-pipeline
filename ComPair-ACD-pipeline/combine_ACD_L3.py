import h5py as h5
import numpy as np
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("fnames",help=".txt file containing output L3 filename in 1st line, and input L3 filenames in other lines")
args = parser.parse_args()
fnames = args.fnames

inst_file = open(fnames,'r')
L3_filenames = inst_file.readlines()
inst_file.close()

output_filename = L3_filenames[0]
input_filenames = L3_filenames[1:]

first = True
for input_filename in input_filenames:
    input_file = h5.File(input_filename.strip(),'r')
    if first:
        master_energy = input_file['Energy'][...]
        master_eventID = input_file['EventID'][...]
        master_position = input_file['Position'][...]
        master_UTC = input_file['UTC'][...]
        try:
            master_bad_eids = input_file['badEids'][...]
        except:
        	master_bad_eids = np.array([])
        first = False
    else:
        master_energy = np.concatenate((master_energy,input_file['Energy'][...]),axis = 0)
        master_eventID = np.concatenate((master_eventID,input_file['EventID'][...]),axis = 0)
        master_position = np.concatenate((master_position,input_file['Position'][...]),axis = 0)
        master_UTC = np.concatenate((master_UTC,input_file['UTC'][...]),axis = 0)
        try:
            master_bad_eids = np.concatenate((master_bad_eids,input_file['badEids'][...]),axis = 0)
        except:
            pass
        
    input_file.close()
    
output_file = h5.File(output_filename.strip(),'w')
output_file.create_dataset('Energy',data = master_energy)
output_file.create_dataset('EventID',data = master_eventID)
output_file.create_dataset('Position',data = master_position)
output_file.create_dataset('UTC',data = master_UTC)
output_file.create_dataset('badEids',data = master_bad_eids)
output_file.close()