#!/usr/bin/env python3
   
import pandas as pd #Still used for reading the .csv files
import numpy as np
import itertools
from pathlib import Path
import os
import struct
import math
import h5py as h5
from matplotlib import pyplot as plt 



class RossPad:
    def __init__(self):
        """
        """
        
        # Other Global Variables
        self.N_panels = 5
        
        
        # Array header names
        self.binary_column_names = ['frame', 'timestamp', 'gpio']
        tmp = []
        for i in range(0, 8):
            for j in range (0, 8):
                tmp.append([f'pixel({i},{j})'])
        self.binary_column_names += list(itertools.chain(*tmp))
        self.binary_column_names = np.array(self.binary_column_names)
        
        
        self.out_column_names = ['frame', 'timestamp', 'gpio']
        tmp = [[f'P{num}_adc0', f'P{num}_adc1'] for num in range(self.N_panels)] 
        self.out_column_names += list(itertools.chain(*tmp))
        self.out_column_names = np.array(self.out_column_names)
        
        self.gpio_drop = ['frame']
        self.gpio_drop += list(itertools.chain(*tmp))
        self.gpio_drop = np.array(self.gpio_drop)
        
        
        # Variables for file readings
        self.is_end_of_file = False
        self.log_length = 0                       
        
        
        self.prev_frame = 0
        
        # Other initializations
        self.load_maps()

    def read_file(self, name):
        with open(name, "rb") as self.f:
            self.read_binary()
            self.process_binary()
            self.process_gpio()
            self.save_file(Path(name))
                
            
    def read_binary(self):     
        binArray = []
        
        
        while self.is_end_of_file == False:
            tmpArray = np.zeros([len(self.binary_column_names)])
            try:
                self.f.seek(self.log_length)
            except :
                self.is_end_of_file = True
                break                

            try:
                log_length_t = struct.unpack(">H", self.f.read(2))
                self.log_length = self.log_length + log_length_t[0]
            except :
                self.is_end_of_file = True
                break
            
            # bunch of number you don't need
            self.read_byte(self.f,self.log_length)
            self.read_byte(self.f,self.log_length)
            
            # the ROSSPAD designated index (zeros every XXX events)
            frameCount = self.read_word(self.f,self.log_length)

            # more bunch of number you don't need
            self.read_byte(self.f,self.log_length)
            self.read_byte(self.f,self.log_length)
            self.read_byte(self.f,self.log_length)
            self.read_byte(self.f,self.log_length)
            ts_clk = self.read_dword(self.f,self.log_length)
            self.read_word(self.f,self.log_length)
            self.read_word(self.f,self.log_length)
            self.read_word(self.f,self.log_length)

            user_defined     = self.read_dword(self.f,self.log_length)
            self.read_dword(self.f,self.log_length)
            
            # fill up dataframe
            tmpArray[0] = frameCount
            tmpArray[1] = ts_clk
            tmpArray[2] = user_defined

            
            counter = 3
            for i in range(0, 8):
                for j in range (0, 8):
                    tmpArray[counter] = self.read_word(self.f,self.log_length)
                    counter = counter + 1

            binArray.append(tmpArray)

        self.bin_data = np.array(binArray)
                
        del tmpArray
        del binArray

                
    def process_binary(self):                     
        self.save_data = np.zeros((len(self.bin_data),len(self.out_column_names)))
        for panelNumber in range(self.N_panels):
            SiPM_channelA, SiPM_channelB = self.get_channel(panelNumber)
            
            pixel1_x, pixel1_y, pixel2_x, pixel2_y = self.get_pixel_channel(SiPM_channelA, SiPM_channelB)

            idx1 = np.where(self.binary_column_names == f'pixel({pixel1_y},{pixel1_x})')[0][0]
            idx2 = np.where(self.binary_column_names == f'pixel({pixel2_y},{pixel2_x})')[0][0]
            
            odx1 = np.where(self.out_column_names == f'P{panelNumber}_adc0')[0][0]
            odx2 = np.where(self.out_column_names == f'P{panelNumber}_adc1')[0][0]
            
            # Baseline/Pedestal Subtract
            
            #self.bin_data[:,idx1] = self.bin_data[:,idx1] - self.baselines['ADC value'].loc[SiPM_channelA]
            #self.bin_data[:,idx2] = self.bin_data[:,idx2] - self.baselines['ADC value'].loc[SiPM_channelB]

            # Set Values less than zero to zero
            
            self.bin_data[:,idx1][self.bin_data[:,idx1] < 0] = 0
            self.bin_data[:,idx2][self.bin_data[:,idx2] < 0] = 0
    
    
            # Calculate Energy
            self.save_data[:,odx1] = self.bin_data[:,idx1]
            self.save_data[:,odx2] = self.bin_data[:,idx2]               
                
        
        frame_idx = np.where(self.binary_column_names == 'frame')[0][0]
        for i in range(1,len(self.bin_data)):
            if self.bin_data[i,frame_idx] - self.bin_data[i-1,frame_idx] < 0:
                self.bin_data[i:,frame_idx] += 2**16

        
        self.save_data[:,frame_idx] = self.bin_data[:,frame_idx] + self.prev_frame
        self.prev_frame = max(self.save_data[:,frame_idx])
        
        timestamp_idx = np.where(self.binary_column_names == 'timestamp')[0][0]
        gpio_idx = np.where(self.binary_column_names == 'gpio')[0][0]
        self.save_data[:,timestamp_idx] = self.bin_data[:,timestamp_idx].astype(int)
        self.save_data[:,gpio_idx] = self.bin_data[:,gpio_idx]
        del self.bin_data
                
                
    def save_file(self, raw):
        hfpath = raw.with_suffix('.h5')

        try:
            os.remove(hdfpath) 
        except:
            pass


        with h5.File(hfpath,'w') as self.hf:
            hf_keys = list(self.hf.keys())
            
            if not 'column_names' in hf_keys:
                self.hf.create_dataset('column_names',data = np.array(self.out_column_names,dtype = 'S'))
            
            if 'ADC' in hf_keys:
                ADC = self.hf['ADC'][...]
                ADC = np.concatenate((ADC,self.save_data))
            else:
                adc_dset = self.hf.create_dataset('ADC', data = self.save_data)

                

            
        ## housekeeping after saving, just to make sure
        del self.save_data


    def process_gpio(self):       
        # #pick apart the header to get the GPIO data
        gpio_idx = np.where(self.out_column_names == 'gpio')[0][0]
        
        self.save_data[:,gpio_idx] = self.save_data[:,gpio_idx].astype(int)
        tmpArray = np.zeros((len(self.save_data),len(self.out_column_names) + 2))
        tmpArray[:,:len(self.out_column_names)] = self.save_data
        
        pps = self.save_data[:,gpio_idx] == 1
        trigack = self.save_data[:,gpio_idx] == 2
        
        tmpArray[:,-2] = pps
        tmpArray[:,-1] = trigack
        
        self.out_column_names = np.append(self.out_column_names,'pps')
        self.out_column_names = np.append(self.out_column_names,'trigack')
        
        self.save_data = tmpArray
    

              
    def get_channel(self, panelNumber):
         self.channel_panel_map[self.channel_panel_map['Panel'] == panelNumber]
         return self.channel_panel_map['ChannelA'][self.channel_panel_map['Panel'] == panelNumber].values[0], self.channel_panel_map['ChannelB'][self.channel_panel_map['Panel'] == panelNumber].values[0]
        
        
    def get_pixel_channel(self, channelA_sipm, channelB_sipm):
        asicA = self.sipm_rosspad_map['ASIC'][self.sipm_rosspad_map['SIPM Channel'] == channelA_sipm].values[0]
        channelA = self.sipm_rosspad_map['Channel'][self.sipm_rosspad_map['SIPM Channel'] == channelA_sipm].values[0]
        
        asicB = self.sipm_rosspad_map['ASIC'][self.sipm_rosspad_map['SIPM Channel'] == channelB_sipm].values[0]
        channelB = self.sipm_rosspad_map['Channel'][self.sipm_rosspad_map['SIPM Channel'] == channelB_sipm].values[0]

        
        try:
            pixel1_x = self.channel_pixel_map['X'][(self.channel_pixel_map['ASIC'] == asicA) & (self.channel_pixel_map['Channel'] == channelA)].values[0]
            pixel1_y = self.channel_pixel_map['Y'][(self.channel_pixel_map['ASIC'] == asicA) & (self.channel_pixel_map['Channel'] == channelA)].values[0]
                    
            pixel2_x = self.channel_pixel_map['X'][(self.channel_pixel_map['ASIC'] == asicB) & (self.channel_pixel_map['Channel'] == channelB)].values[0]
            pixel2_y = self.channel_pixel_map['Y'][(self.channel_pixel_map['ASIC'] == asicB) & (self.channel_pixel_map['Channel'] == channelB)].values[0]
        except:
            print('welp')
        
        return pixel1_x, pixel1_y, pixel2_x, pixel2_y
        
    def load_maps(self):
        # SiPM Channel to ROSSPAD map
        self.sipm_rosspad_map = pd.read_csv('rosspadConfigData/sipm_rosspad_map.csv', sep=',')

        # ASIC+Channel mapping to ROSSPAD pixel mapping
        # ASIC;Channel;X;Row
        self.channel_pixel_map = pd.read_csv('rosspadConfigData/ROSSPAD-PixelMap-Module.csv', sep=';')
        
        
        # Baseline Values       
        self.baselines = pd.read_csv('rosspadConfigData/Baseline.csv', sep=',')
        
        # SiPM Channel to ROSSPAD map
        self.channel_panel_map = pd.read_csv('rosspadConfigData/panel_channel_map.csv', sep=',')

        
    # ------ Reading BINARY FUNCTIONS ------
    def read_byte(self, file_rd,log_pos):
        byte_rd = 0
        
        try:
            byte_rd = (struct.unpack("B", file_rd.read(1)))[0]
        except :
            self.is_end_of_file = True
        return byte_rd
    
    def read_word(self, file_rd,log_pos):
        word_rd = 0
        
        try:
            word_rd = (struct.unpack(">H", file_rd.read(2)))[0]
        except :
            self.is_end_of_file = True
    
        return word_rd
    
    def read_dword(self, file_rd,log_pos):
        dword_rd = 0
        
        try:
            dword_rd = (struct.unpack(">I", file_rd.read(4)))[0]
        except :
          #  print ("EOF = " + str(log_pos))
            self.is_end_of_file = 1
        return dword_rd


def main(filename=None, **kargs):
    r = RossPad()
    for f in filename:
        r.read_file(f)

if __name__ == '__main__':
    
    import argparse    
    parser = argparse.ArgumentParser(
        description='Lookup some information in a database')
    parser.add_argument("filename", nargs="*")
    args = parser.parse_args()
    main(**vars(args))
