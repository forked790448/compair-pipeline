#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Develop L3 Level Data
 - takes either L2 data and adds overhead requested for the ComPair Unified Dataformat
 - I break it up because my computer cannot handle
     large datafile.
"""

import os
import os.path
import numpy as np
from pathlib import Path
import h5py as h5



class RosspadL2To3:
    def __init__(self,rfname):
        self.rfname = rfname      
        
        self.E_THRESH = 200 #200 keV lower energy threshold
        
        # Other Global Variables
        self.chunk_size = 1000000
        
        
        self.coinc_window = 20 *1000 # 20 thousand nano seconds (20 us)
        self.prev_frame = 0
        
        

    def process(self):
        with h5.File(self.rfname, 'r') as self.f_in:
            self.get_time_align()

            # iterate over table chunks
            path = Path(self.rfname.rsplit('.',2)[0])
            self.hpath = path.with_suffix('.L3.h5')
        
            try:
                os.remove(str(self.hpath))
            except:
                pass 
        
            #self.get_total_log()
            
            #self.add_event_ids()
            #self.remove_overhead()
            self.add_overhead()
            self.set_data_type()

            self.write_files()

                
    def set_data_type(self):
        
        self.e_data = self.e_data.astype('float32')       


    def get_time_align(self):
        # Here we want to align all the trigOuts with EventIDs
        #write to file
        self.pps_data = self.f_in['pps'][...]
        self.frame_data = self.f_in['frame'][...]
        self.trigack_data = self.f_in['trigack'][...]
        self.eventID_data = self.f_in['EventID'][...]
        self.UTC_data = self.f_in['UTC'][...]
        self.e_data = self.f_in['Energy'][...]   
        self.pos_data = self.f_in['Position'][...]       
        try:
            self.bad_eids = self.f_in['badEids'][...]
            self.bad_flag = True
        except:
            self.bad_flag = False
        
        
        # drop PPSs, don't need pps anymore
        self.frame_data = self.frame_data[self.pps_data != True]
        self.trigack_data = self.trigack_data[self.pps_data != True]
        self.eventID_data = self.eventID_data[self.pps_data != True]
        self.UTC_data = self.UTC_data[self.pps_data != True]
        self.e_data = self.e_data[self.pps_data != True,:]
        self.pos_data = self.pos_data[self.pps_data != True,:,:]


        trigAckIdx = np.where(self.trigack_data == True)[0]
        self.eventID_data[trigAckIdx - 1] = self.eventID_data[trigAckIdx]        
        
        timeDiff = abs(self.UTC_data[trigAckIdx-1] - self.UTC_data[trigAckIdx])
        coincPass = timeDiff < self.coinc_window
        
        # only take events before trigAck, don't need trigAck anymore
        self.frame_data = self.frame_data[trigAckIdx - 1]
        self.eventID_data = self.eventID_data[trigAckIdx - 1]
        self.UTC_data = self.UTC_data[trigAckIdx - 1]
        self.e_data = self.e_data[trigAckIdx - 1,:]
        self.pos_data = self.pos_data[trigAckIdx - 1,:,:]
        
        # only take events that pass coincidence window
        self.frame_data = self.frame_data[coincPass]
        self.eventID_data = self.eventID_data[coincPass]
        self.UTC_data = self.UTC_data[coincPass]
        self.e_data = self.e_data[coincPass,:]
        self.pos_data = self.pos_data[coincPass,:,:]

        # only take frame > 0
        self.eventID_data = self.eventID_data[self.frame_data > 0]
        self.UTC_data = self.UTC_data[self.frame_data > 0]
        self.e_data = self.e_data[self.frame_data > 0,:]
        self.pos_data = self.pos_data[self.frame_data > 0,:,:]
        self.frame_data = self.frame_data[self.frame_data > 0]
                
        # remove things without eventID
        self.frame_data = self.frame_data[self.eventID_data > 0]
        self.UTC_data = self.UTC_data[self.eventID_data > 0]
        self.e_data = self.e_data[self.eventID_data > 0,:]
        self.pos_data = self.pos_data[self.eventID_data > 0,:,:]
        self.eventID_data = self.eventID_data[self.eventID_data > 0]
        
        

    def add_overhead(self):
        
        self.e_data[self.e_data < self.E_THRESH] = 0    

       
    #write edited dataframe to new hdf files
    def write_files(self):
        #write to file

        with h5.File(self.hpath, 'w') as f_out:
            #f_out.create_dataset('Frame', data = self.frame_data)
            f_out.create_dataset('UTC', data = self.UTC_data)
            f_out.create_dataset('EventID', data = self.eventID_data)
            f_out.create_dataset('Energy', data = self.e_data)
            f_out.create_dataset('Position', data = self.pos_data)
            if self.bad_flag:
                f_out.create_dataset('badEids',data = self.bad_eids)

        



if __name__ == '__main__':

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("rfname",help="rosspad hdf5 file")
    args = parser.parse_args()

    # print(args.rfname)
    R = RosspadL2To3(args.rfname)
    R.process()

