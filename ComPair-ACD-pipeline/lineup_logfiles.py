# new lineup_logfiles.py


import pandas
import numpy as np
import h5py as h5

def get_bounds(isRosspad=True,frequency=100e-6):
    if isRosspad:
        frequency *= 100e6
    else:
        frequency *= 42e6

    lower_bound = frequency - 0.005*frequency
    upper_bound = frequency + 0.005*frequency
    return lower_bound, upper_bound


def read_data_from_file(rfname,afname,key):

    rosspad_file = h5.File(rfname)
    rosspad_data = rosspad_file['ADC'][...]
    rosspad_keys = rosspad_file['column_names'][...]
    rosspad_file.close()

    tmp = []
    for key in rosspad_keys:
        dkey = key.decode('utf8')
        tmp.append(dkey)
    rosspad_keys = np.array(tmp)

    arduino_f = open(afname,'r')
    arduino_lines = arduino_f.readlines()
    arduino_f.close()

    arduino_data = []
    for line in arduino_lines:
        line_strip = line.strip().split(',')
        if len(line_strip) == 2:
            line_strip.append('')
        if len(line_strip) == 3:
            line_strip.append('')
        arduino_data.append(line_strip)

    arduino_keys = np.array(["ID","Clock","UTCA","UTCC"])

    return np.array(rosspad_data), rosspad_keys, np.array(arduino_data), arduino_keys

def lineup(rfname,afname,key='ADC'):

    rosspad_data = None
    arduino_data = None
    #if passed dataframes
    if key == None:
        rosspad_data = rfname
        arduino_data = afname
    else:
        rosspad_data, rosspad_keys, arduino_data, arduino_keys = read_data_from_file(rfname,afname,key)

    #rosspad_df.index = np.arange(len(rosspad_df)) #DO THIS TO DEAL WITH MISSING ASIC3 LINES
    #rosspad_df.index.rename("index_orig",inplace=True)
    #arduino_df.index.rename("index_orig",inplace=True)

    # rosspad indices
    pps_idx = np.where(rosspad_keys == 'pps')[0][0]
    trigack_idx = np.where(rosspad_keys == 'trigack')[0][0]
    timestamp_idx = np.where(rosspad_keys == 'timestamp')[0][0]

    # arduino indices
    id_idx = np.where(arduino_keys == "ID")[0][0]
    clock_idx = np.where(arduino_keys == "Clock")[0][0]

    #get lines corresponding to trigIn
    rosspad_trigIn = rosspad_data[(rosspad_data[:,pps_idx] == False) & (rosspad_data[:,trigack_idx] == True), :]
    rosspad_orig_rows = np.array(np.arange(len(rosspad_data)))[(rosspad_data[:,pps_idx] == False) & (rosspad_data[:,trigack_idx] == True)]
    arduino_trigIn = arduino_data[arduino_data[:,id_idx] == "C",:]
    arduino_orig_rows = np.arange(len(arduino_data))[arduino_data[:,id_idx] == "C"]

    #search for series of 10 pulses 100 us apart
    #arduino *could* drop a signal (rare), or have rosspad triggers in between
    # (more commmon with more logs going)
    nPulses = 10
    lower_bound, upper_bound = get_bounds(False)
#	print(lower_bound,upper_bound)

    arduino_timestamps = arduino_trigIn[:,clock_idx].astype(float)
    arduino_clock_diff = np.diff(arduino_timestamps)
    arduino_start_index = -1


    #first assume nothing is dropped
    for i in range(len(arduino_clock_diff)-nPulses-1):
        sequence = arduino_clock_diff[i:i+nPulses-1]
        if (sequence > lower_bound).all() and (sequence < upper_bound).all():
            arduino_start_index = arduino_orig_rows[i]
            break

    #if only one is dropped, there will at least be a sequence of 5
    #probably a sequence of 3 is the shortest we should look for, since 2 in a row more likely to happen unrelated to lineup
    #so start at nPulses = 5, search until 3 
    nPulses = 5
    while arduino_start_index == -1 and nPulses >= 3:
        for i in range(len(arduino_clock_diff)-nPulses-1):
            sequence = arduino_clock_diff[i:i+nPulses-1]
            if (sequence > lower_bound).all() and (sequence < upper_bound).all():
                arduino_start_index = arduino_orig_rows[i]
                break
        nPulses -= 1

    #we actually want to return the index of the last PPS, not the first line-up signal,
    # since that's what the comparison code expects
    foundPPS = False
    while not foundPPS:
        if arduino_data[arduino_start_index,id_idx] == "P":
            foundPPS = True
        else:
            arduino_start_index += 1



    #rosspad complicated, since it could drop triggers
    lower_bound, upper_bound = get_bounds(True)
#	print(lower_bound,upper_bound)

    rosspad_timestamps = rosspad_trigIn[:,timestamp_idx]
    rosspad_clock_diff = np.diff(rosspad_timestamps)
    rosspad_start_index = -1

    #first do it the arduino way, assuming nothing is dropped
    for i in range(len(rosspad_clock_diff)-nPulses-1):
        sequence = rosspad_clock_diff[i:i+nPulses-1]
        if (sequence > lower_bound).all() and (sequence < upper_bound).all():
            rosspad_start_index = rosspad_orig_rows[i]
            break

    #if only one is dropped, there will at least be a sequence of 5
    #probably a sequence of 3 is the shortest we should look for, since 2 in a row more likely to happen unrelated to lineup
    #so start at nPulses = 5, search until 3 
    nPulses = 5
    while rosspad_start_index == -1 and nPulses >= 3:
        for i in range(len(rosspad_clock_diff)-nPulses-1):
            sequence = rosspad_clock_diff[i:i+nPulses-1]
            if (sequence > lower_bound).all() and (sequence < upper_bound).all():
                rosspad_start_index = rosspad_orig_rows[i]
                break
        nPulses -= 1

    #what if it drops every other pulse? unlikely, but, could repeat search with frequency of 200 us
    if rosspad_start_index == -1:
        nPulses = 5
        lower_bound, upper_bound = get_bounds(True,200e-6)
    while rosspad_start_index == -1 and nPulses >= 3:
        for i in range(len(rosspad_clock_diff)-nPulses-1):
            sequence = rosspad_clock_diff[i:i+nPulses-1]
            if (sequence > lower_bound).all() and (sequence < upper_bound).all():
                rosspad_start_index = rosspad_orig_rows[i]
                break
        nPulses -= 1

#	print(rosspad_df.trig.to_numpy()[rosspad_start_index-1:rosspad_start_index+10])
#	print(rosspad_df.timestamp.to_numpy()[rosspad_start_index-1:rosspad_start_index+10])

    #again, find index of first PPS
    foundPPS = False
    while not foundPPS:
        if rosspad_data[rosspad_start_index,pps_idx] == True:
            foundPPS = True
        else:
            rosspad_start_index += 1


    return rosspad_start_index, arduino_start_index, rosspad_data, arduino_data, rosspad_keys, arduino_keys

