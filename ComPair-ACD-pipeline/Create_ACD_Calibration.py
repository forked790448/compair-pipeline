import struct
import datetime
import time
import shutil
from os import path
import csv
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pylandau
from scipy.optimize import curve_fit
from scipy.stats import chisquare
from scipy.odr import ODR, Model, Data, RealData
import numpy as np
from pylab import *
import h5py as h5
import argparse
import pandas as pd

##### Stuff from IDEAS #####

READ_IMAGE = 1
STOP_READ = 0
PRINT_LOG_ENTRY = 0
PLOT_IMAGE = 0
PLOT_DELAY = 0.01

def read_byte(file_rd,log_pos):
    byte_rd = 0
    
    try:
        byte_rd = (struct.unpack("B", file_rd.read(1)))[0]
    except :
        STOP_READ = 1
    return byte_rd

def read_word(file_rd,log_pos):
    word_rd = 0
    
    try:
        word_rd = (struct.unpack(">H", file_rd.read(2)))[0]
    except :
        STOP_READ = 1

    return word_rd

def read_dword(file_rd,log_pos):
    dword_rd = 0
    
    try:
        dword_rd = (struct.unpack(">I", file_rd.read(4)))[0]
    except :
      #  print ("EOF = " + str(log_pos))
        STOP_READ = 1
    return dword_rd

def readLogFile(logFileName,output_filename):
    global PLOT_IMAGE, PLOT_DELAY
    print ("**** File Read : " + logFileName + " ******" )
    print (" ")

    log_length = 0
    read_more = 1
    log_entry = 0
    first_entry = 1
    PackageDrop = 0
    ValidLogEntry = 0

    SAVE_IMAGE     = 1
    STOP_READ      = 0
    
    if (READ_IMAGE == 1) and (SAVE_IMAGE == 1):
        try:
            save_file = open(output_filename, "w")
        except :
            SAVE_IMAGE = 0
    else:
        SAVE_IMAGE = 0                    

    
    with open(logFileName, "rb") as f:
        while read_more and STOP_READ == 0:
            ValidLogEntry = 1
            try:
                f.seek(log_length)
                read_more = 1
            except :
                print ("EOF = " + str(log_length))
                read_more = 0
                break                

            try:
                log_length_t = struct.unpack(">H", f.read(2))
                log_length = log_length + log_length_t[0]
            except :
                read_more = 0
                break
            
            log_entry += 1
            system_number     = read_byte(f,log_length)
            Packet_type     = read_byte(f,log_length)
            frameCount         = read_word(f,log_length)

            reserved         = read_byte(f,log_length)
        
            ts_h             = read_byte(f,log_length)
            ts_m             = read_byte(f,log_length)
            ts_s             = read_byte(f,log_length)

            ts_clk             = read_dword(f,log_length)

        
            width             = read_word(f,log_length)
            height             = read_word(f,log_length)
            spectralChannels = read_word(f,log_length)

            user_defined     = read_dword(f,log_length)
            imageDataLen     = read_dword(f,log_length)

            
            if first_entry == 0:
                if frameCount == 0:
                    if lastFrameCount != (65535):
                        PackageDrop = 1
                else:        
                    if frameCount != (lastFrameCount + 1):
                        PackageDrop = 1
            else:
                first_entry = 0
                PackageDrop = 0
                if SAVE_IMAGE == 1:
                    # print header
                    save_file.write("Frame;");
                    save_file.write("UserDefined;");
                    save_file.write("Timestamp;");
                    
                    for ch in range(0,spectralChannels):                    
                        for y in range(0,height):
                            for x in range(0,width):
                                save_file.write("Pixel(y,x)" + str(y)+ "," + str(x)+";")
                    save_file.write("\n");
                
                            
            lastFrameCount = frameCount;    
            
            
            if READ_IMAGE == 1:
                image = [0]*imageDataLen
                if SAVE_IMAGE == 1:
                    save_file.write(str(frameCount)+";");
                    save_file.write(str(user_defined)+";");                    
                    save_file.write(str(ts_clk)+";");

                for i in range(0, imageDataLen):
                    image[i] = read_word(f,log_length)
                    if SAVE_IMAGE == 1:
                        save_file.write(str(image[i])+";");
                if PLOT_IMAGE == 1: 
                    tb.plot(range(0, len(image)), image)
                    time.sleep(PLOT_DELAY)
                        
                if SAVE_IMAGE == 1:
                    save_file.write("\n");
                        
                        
            if STOP_READ == 1:
                print ("Current file pos = " + str(log_length) + " Log no: "+ str(log_entry) + "ERROR! Incomplete Log" )
                
            if PRINT_LOG_ENTRY == 1 and STOP_READ == 0:
                print ("Current file pos = " + str(log_length) + " Log no: "+ str(log_entry) )

                print ("log_Length = " + str(log_length))
                print ("system_number = " + str(system_number))
                print ("Packet_type = " + str(Packet_type))
        
                print ("FrameCount = " + str(frameCount))
                print ("reserved = " + str(reserved))
        
                print ("ts_h = " + str(ts_h))
                print ("ts_m = " + str(ts_m))
                print ("ts_s = " + str(ts_s))
                print ("ts_clk = " + str(ts_clk))
        
                print ("width = " + str(width))
                print ("height = " + str(height))
                print ("spectralChannels = " + str(spectralChannels))
                print ("user_defined = " + str(user_defined))
                print ("Image data length = " + str(imageDataLen))
            
            

    if READ_IMAGE == 1 and SAVE_IMAGE == 1:
        save_file.close()

    print ("Log Info: ")
    print ("Total Log entries: = " + str(log_entry) )
    if PackageDrop == 0:
        print ("Continious log, no package drop detected ")
    else:
        print ("Package drop detected! The frame count is not in sequence!")
        
        
#### From NRL #####

def load_maps():
    # SiPM Channel to ROSSPAD map
    sipm_rosspad_map = pd.read_csv('rosspadConfigData/sipm_rosspad_map.csv', sep=',')

    # ASIC+Channel mapping to ROSSPAD pixel mapping
    # ASIC;Channel;X;Row
    channel_pixel_map = pd.read_csv('rosspadConfigData/ROSSPAD-PixelMap-Module.csv', sep=';')
    
    
    # Baseline Values       
    baselines = pd.read_csv('rosspadConfigData/Baseline.csv', sep=',')
    
    # SiPM Channel to ROSSPAD map
    channel_panel_map = pd.read_csv('rosspadConfigData/panel_channel_map.csv', sep=',')
    
    return sipm_rosspad_map, channel_pixel_map, channel_panel_map, baselines

def get_channel(channel_panel_map, panelNumber):
     return channel_panel_map['ChannelA'][channel_panel_map['Panel'] == panelNumber].values[0], channel_panel_map['ChannelB'][channel_panel_map['Panel'] == panelNumber].values[0]

def get_pixel_channel(sipm_rosspad_map, channel_pixel_map, channelA_sipm, channelB_sipm):
    asicA = sipm_rosspad_map['ASIC'][sipm_rosspad_map['SIPM Channel'] == channelA_sipm].values[0]
    channelA = sipm_rosspad_map['Channel'][sipm_rosspad_map['SIPM Channel'] == channelA_sipm].values[0]
    
    asicB = sipm_rosspad_map['ASIC'][sipm_rosspad_map['SIPM Channel'] == channelB_sipm].values[0]
    channelB = sipm_rosspad_map['Channel'][sipm_rosspad_map['SIPM Channel'] == channelB_sipm].values[0]

    
    try:
        pixel1_x = channel_pixel_map['X'][(channel_pixel_map['ASIC'] == asicA) & (channel_pixel_map['Channel'] == channelA)].values[0]
        pixel1_y = channel_pixel_map['Y'][(channel_pixel_map['ASIC'] == asicA) & (channel_pixel_map['Channel'] == channelA)].values[0]
                
        pixel2_x = channel_pixel_map['X'][(channel_pixel_map['ASIC'] == asicB) & (channel_pixel_map['Channel'] == channelB)].values[0]
        pixel2_y = channel_pixel_map['Y'][(channel_pixel_map['ASIC'] == asicB) & (channel_pixel_map['Channel'] == channelB)].values[0]
    except:
        print('welp')
    
    return asicA, channelA, asicB, channelB, pixel1_x, pixel1_y, pixel2_x, pixel2_y


        
        
##### My own functions #####

    
    
    
def fitlandau(data, A, mu, eta, adc_per_bin):

    bin_num = int((max(data) - min(data))/adc_per_bin)
    [heights,bins,fig] = plt.hist(data,bins = bin_num,density = True)
    plt.close()
    
    heights_renormalized = heights * len(data)*adc_per_bin
    
    shifted_bins = (bins + (bins[1] - bins[0])/2)[:-1]
    
    p0 = A,mu,eta
    fit = curve_fit(landau,shifted_bins,heights,p0,maxfev = 10000)
    [Afit,mufit,etafit] = fit[0]
    
    pcov = fit[1]
    
    return[Afit, mufit, etafit, pcov]
    
    
    
def landau(x, A,mu, eta):
    return A*pylandau.landau_pdf(x,mu,eta)
    

    
def peakFinder(x,y):
    max_height = max(y)
    max_index = np.where(y == max_height)[0][0]
    max_x = x[max_index]
    return(max_x)
    
    
    
def parseargs():
    """
    This function handles the input
    There are 3 inputs - the instruction file, and the calibration file.

    """

    parser = argparse.ArgumentParser(description = "Parse input files")
    parser.add_argument("instfile", help = "Path to instruction file")
    parser.add_argument("calibfile", help = "Path to calibration file")
    args = parser.parse_args()
    return args
    
    
    
def cli():

    args = parseargs()
    
    instructions_filename = args.instfile
    calibration_filename = args.calibfile
    
    sipm_rosspad_map, channel_pixel_map, channel_panel_map, baselines = load_maps()
    
    instructions_file = open(instructions_filename,'r')
    instructions_lines = instructions_file.readlines()
    instructions_file.close()
    
    inst_panels = []
    inst_runs = []
    inst_sims = []

    for line in instructions_lines:
        split_line = line.split(';')
        strip_line = []
        for el in split_line:
            strip_el = el.strip()
            if strip_el.isnumeric():
                strip_el = int(strip_el)
            strip_line.append(strip_el)
        inst_panels.append(strip_line[0])
        inst_runs.append(strip_line[1])
        inst_sims.append(strip_line[2])
        
    inst_panels = np.array(inst_panels)
    inst_runs = np.array(inst_runs)
    inst_sims = np.array(inst_sims)
    
    unique_panels = np.unique(inst_panels)
    unique_runs = np.unique(inst_runs)
    unique_sims = np.unique(inst_sims)
    
    
    # Open the ROSSPAD Log Files
    
    ti = 2

    rate_thresh = 1/1000 # (s) time between events - this is about 20x higher than we expect
    chunk_size = 10000 # events per chunk

    spectrum_data_array = []
    for filename in unique_runs:

        # Read in and initialize files

        spectrum_bin_name = 'CalibrationRuns/' + filename

        spectrum_csv_name = spectrum_bin_name[:-4] + '.csv'

        print("Starting " + filename)
        print("Checking if csv exists")

        if not path.exists(spectrum_csv_name):
            readLogFile(spectrum_bin_name,spectrum_csv_name)

        print("csv exists")

        spectrum_f = open(spectrum_csv_name,'r')
        spectrum = spectrum_f.read()

        spectrum_lines = spectrum.split('\n')
        spectrum_header = spectrum_lines[0].split(';')[:-1]
        spectrum_data = np.zeros((len(spectrum_lines)-2,len(spectrum_header)))

        for i in range(len(spectrum_lines)-2):
            line = spectrum_lines[i+1]
            words = line.split(';')
            spectrum_data[i,:] = np.array(words[:-1]).astype('int')

        print("Spectrum data initialized")

        # Get rid of any glitchy segments

        time = np.zeros(len(spectrum_data[:,ti]))
        time[0] = spectrum_data[0,ti]
        dt = 0
        counter = 0
        max_time = max(spectrum_data[:,ti])

        for i in range(1,len(spectrum_data[:,ti])):
            dt = spectrum_data[i,ti] - spectrum_data[i-1,ti]
            if abs(dt) < 4 * 10 ** 9:
                time[i] = spectrum_data[i,ti] + counter * max_time
            else:
                counter += 1
                time[i] = spectrum_data[i,ti] + counter * max_time

        time = time / 10**8 # Put it in seconds
        eid = np.linspace(1,len(time),len(time))

        print("Cleaning data")

        clean_data = np.zeros(np.shape(spectrum_data[0:1,:]))
        nchunks = int(len(time)/chunk_size)+1
        for i in range(nchunks):
            subdata = time[i*chunk_size:(i+1)*chunk_size]
            P = np.polyfit(np.linspace(1,len(subdata),len(subdata)),subdata,deg = 1)
            slope = P[0]
            if P[0] > rate_thresh:
                clean_data = np.concatenate((clean_data,spectrum_data[i*chunk_size:(i+1)*chunk_size,:]))
        clean_data = clean_data[1:]

        spectrum_data_array.append(clean_data)

        print("Data cleaned")

        spectrum_f.close()
        
        
    # Open the Simulation Files
        
    adc_per_bin = 100
    A = 1
    mu = 5000
    eta = 600

    floor = 3000
    ceiling = 30000

    sim_mus = []
    sim_etas = []
    sim_es = []

    for file_name in unique_sims:
        sim_name = 'Simulations/' + file_name
        sim_f = open(sim_name,'r')
        lines = sim_f.readlines()

        es = []

        for line in lines:
            if line[0:5] == 'HTsim':
                words = line.split(';')
                e = words[4]
                e = float(e)
                es.append(e)

        sim_es.append(es)

        simdata = [i for i in es if i > floor and i < ceiling]
        sim_bin_num = int((max(simdata) - min(simdata))/adc_per_bin)

        [simA,simmu,simeta,simpcov] = fitlandau(simdata,A,mu,eta,adc_per_bin)

        sim_mus.append(simmu)
        sim_etas.append(simeta)    
        
        
    # Create the x and y datasets

    riser_lists = []

    bin_list = np.arange(0,6100,195)

    co_is_list = []

    for j in range(len(unique_runs)):
        co_is = np.ones(len(spectrum_data_array[j][:,0]))
        co_is_list.append(co_is)

    for u_i in unique_panels:
        channels = get_channel(channel_panel_map, u_i)
        a1, c1, a2, c2, x1, y1, x2, y2 = get_pixel_channel(sipm_rosspad_map, channel_pixel_map,channels[0],channels[1])

        idx1 = 3 + x1 + 8 * y1
        idx2 = 3 + x2 + 8 * y2
    
        print(channels)
        print(idx1)
        print(idx2)
    
        riser_list1 = []
        riser_list2 = []

    
        for i in range(len(inst_panels)):
        

            if u_i == inst_panels[i]:

                run_name = inst_runs[i]
                sim_name = inst_sims[i]
                print(sim_name)


                for j in range(len(unique_runs)):

                    unique_run_name = unique_runs[j]

                    if run_name == unique_run_name:

                        specs = [spectrum_data_array[j][:,idx1], spectrum_data_array[j][:,idx2]]
                    
                        co_is = co_is_list[j]
                    
                        specs_sub_ped = [np.array(specs[0]) - 0, np.array(specs[1]) - 0]
                    
                        # do idx1 first
                        orig_spec = specs_sub_ped[0].copy()

                        [y,bin_out,fig] = plt.hist(orig_spec,bins = bin_list)
                        plt.close()

                        x = (bin_list[:-1] + bin_list[1:])/2

                        dy = y[1:]-y[:-1]
                        risers = []
                        prev = -1
                        current = -1
                        for k in range(len(dy)):
                            current = dy[k] > 0
                            if prev == 0 and current == 1:
                                risers.append(k)
                            prev = current

                        plt.semilogy(x,y)
                        plt.vlines(x[risers],1,10**7)
                        plt.title('Spectrum ' + str(idx1))
                        plt.show()

                        plt.plot(x[1:],dy>0)
                        plt.title('Risers ' + str(idx1))
                        plt.show()
                    
                        riser_list1.append([x[risers[-2]],x[risers[-1]]])
                    
                        good_is1 = np.logical_and(orig_spec > x[risers[-2]],orig_spec < x[risers[-1]])
                    
                        co_is = np.logical_and(co_is,good_is1)
                    
                        # Now do idx2
                    
                        orig_spec = specs_sub_ped[1].copy()

                        [y,bin_out,fig] = plt.hist(orig_spec,bins = bin_list)
                        plt.close()

                        x = (bin_list[:-1] + bin_list[1:])/2

                        dy = y[1:]-y[:-1]
                        risers = []
                        prev = -1
                        current = -1
                        for k in range(len(dy)):
                            current = dy[k] > 0
                            if prev == 0 and current == 1:
                                risers.append(k)
                            prev = current

                        plt.semilogy(x,y)
                        plt.vlines(x[risers],1,10**7)
                        plt.title('Spectrum ' + str(idx2))
                        plt.show()

                        plt.plot(x[1:],dy>0)
                        plt.title('Risers ' + str(idx2))
                        plt.show()
                    
                        riser_list2.append([x[risers[-2]],x[risers[-1]]])
                    
                        good_is2 = np.logical_and(orig_spec > x[risers[-2]],orig_spec < x[risers[-1]])
                    
                        co_is = np.logical_and(co_is,good_is2)
                    
                        co_is_list[j] = co_is
                    
        riser_lists.append(riser_list1)
        riser_lists.append(riser_list2)
        
    noise_lists = []
    overbin_lists = []
    for pair in riser_lists:
        noise_lists.append(pair[0][0])
        overbin_lists.append(pair[0][1])
        
    adc_lists = []
    erg_lists = []
    ped_lists = []

    for u_i in unique_panels:

        adc_list1 = []
        erg_list1 = []

        adc_list2 = []
        erg_list2 = []

        channels = get_channel(channel_panel_map, u_i)
        a1, c1, a2, c2, x1, y1, x2, y2 = get_pixel_channel(sipm_rosspad_map, channel_pixel_map,channels[0],channels[1])

        idx1 = 3 + x1 + 8 * y1
        idx2 = 3 + x2 + 8 * y2
    
        print(channels)
        print(idx1)
        print(idx2)


        b1 = baselines['ADC value'][np.logical_and(baselines['ASIC'] == a1, baselines['Channel'] == c1)].to_numpy()[0]
        b2 = baselines['ADC value'][np.logical_and(baselines['ASIC'] == a2, baselines['Channel'] == c2)].to_numpy()[0]

        for i in range(len(inst_panels)):

            if u_i == inst_panels[i]:

                run_name = inst_runs[i]
                sim_name = inst_sims[i]
                print(sim_name)


                for j in range(len(unique_runs)):

                    unique_run_name = unique_runs[j]

                    if run_name == unique_run_name:

                        specs = [spectrum_data_array[j][co_is,idx1], spectrum_data_array[j][co_is,idx2]]
                        specs_sub_ped = [np.array(specs[0]) - 0, np.array(specs[1]) - 0]
                        co_is
                    
                        # do idx1 first
                        orig_spec = specs_sub_ped[0].copy()

                        [y,bin_out,fig] = plt.hist(orig_spec,bins = bin_list)
                        plt.close()

                        x = (bin_list[:-1] + bin_list[1:])/2

                        dy = y[1:]-y[:-1]
                        risers = []
                        prev = -1
                        current = -1
                        for k in range(len(dy)):
                            current = dy[k] > 0
                            if prev == 0 and current == 1:
                                risers.append(k)
                            prev = current

                        plt.semilogy(x,y)
                        plt.title('Spectrum ' + str(idx1))
                        plt.show()
                    
                        if len(risers) > 1:
                            if x[risers[-1]] < 4500:
                                spec = orig_spec[orig_spec > x[risers[-1]]]
                                new_bin_list = bin_list[np.logical_and(bin_list >= min(spec),bin_list <= max(spec))]
                                [heights,bin_out,fig] = plt.hist(spec,bins = new_bin_list)
                                plt.close()
                            else:
                                spec = orig_spec[orig_spec > x[risers[-2]]]
                                new_bin_list = bin_list[np.logical_and(bin_list >= min(spec),bin_list <= max(spec))]
                                [heights,bin_out,fig] = plt.hist(spec,bins = new_bin_list)
                                plt.close()
                        else:                        
                            spec = orig_spec.copy()
                            new_bin_list = bin_list[np.logical_and(bin_list >= min(spec),bin_list <= max(spec))]
                            [heights,bin_out,fig] = plt.hist(spec,bins = new_bin_list)
                            plt.close()
                                                
                        newx = (new_bin_list[:-1] + new_bin_list[1:])/2
                        newy = heights
                        p0 = [max(newy),peakFinder(newx,newy),100]
                        [A,mu,eta],pcov = curve_fit(landau,newx,newy,p0,maxfev = 10000)

                        plt.plot(newx,newy)
                        plt.plot(newx,landau(newx,A,mu,eta))
                        plt.title('Truncated Spectrum ' + str(idx1))
                        plt.show()

                        adc_list1.append(mu - eta)
                        adc_list1.append(mu)
                        adc_list1.append(mu + eta)

                        # now idx2
                    
                        orig_spec = specs_sub_ped[1].copy()

                        [y,bin_out,fig] = plt.hist(orig_spec,bins = bin_list)
                        plt.close()

                        x = (bin_list[:-1] + bin_list[1:])/2

                        dy = y[1:]-y[:-1]
                        risers = []
                        prev = -1
                        current = -1
                        for k in range(len(dy)):
                            current = dy[k] > 0
                            if prev == 0 and current == 1:
                                risers.append(k)
                            prev = current

                        plt.semilogy(x,y)
                        plt.title('Spectrum ' + str(idx2))
                        plt.show()

                        if len(risers) > 1:
                            if x[risers[-1]] < 4500:
                                spec = orig_spec[orig_spec > x[risers[-1]]]
                                new_bin_list = bin_list[np.logical_and(bin_list >= min(spec),bin_list <= max(spec))]
                                [heights,bin_out,fig] = plt.hist(spec,bins = new_bin_list)
                                plt.close()
                            else:
                                spec = orig_spec[orig_spec > x[risers[-2]]]
                                new_bin_list = bin_list[np.logical_and(bin_list >= min(spec),bin_list <= max(spec))]
                                [heights,bin_out,fig] = plt.hist(spec,bins = new_bin_list)
                                plt.close()
                        else:                        
                            spec = orig_spec.copy()
                            new_bin_list = bin_list[np.logical_and(bin_list >= min(spec),bin_list <= max(spec))]
                            [heights,bin_out,fig] = plt.hist(spec,bins = new_bin_list)
                            plt.close()
                        
                        newx = (new_bin_list[:-1] + new_bin_list[1:])/2
                        newy = heights
                        p0 = [max(newy),peakFinder(newx,newy),100]
                        [A,mu,eta],pcov = curve_fit(landau,newx,newy,p0,maxfev = 10000)

                        plt.plot(newx,newy)
                        plt.plot(newx,landau(newx,A,mu,eta))
                        plt.title('Truncated Spectrum ' + str(idx2))
                        plt.show()
                    

                        adc_list2.append(mu - eta)
                        adc_list2.append(mu)
                        adc_list2.append(mu + eta)

                for j in range(len(unique_sims)):

                    unique_sim_name = unique_sims[j]

                    if sim_name == unique_sim_name:

                        sim_mu = sim_mus[j]
                        sim_eta = sim_etas[j]

                        erg_list1.append(sim_mu - sim_eta)
                        erg_list1.append(sim_mu)
                        erg_list1.append(sim_mu + sim_eta)

                        erg_list2.append(sim_mu - sim_eta)
                        erg_list2.append(sim_mu)
                        erg_list2.append(sim_mu + sim_eta)

        adc_lists.append(adc_list1)
        erg_lists.append(erg_list1)
        ped_lists.append(0)

        adc_lists.append(adc_list2)
        erg_lists.append(erg_list2)
        ped_lists.append(0)
            
    # Make the Linear Fits
    
    P_list = []
    mu_noise_list = []
    for i in range(len(unique_panels)):
        
        # do channel 1 first
        
        adcs = adc_lists[2*i]
        ergs = erg_lists[2*i]

        P = np.polyfit(adcs,ergs,deg = 1, cov = False)

        mu_noise_list.append(np.mean(ergs)*0.8)
        P_list.append(P)
        
        # do channel 2 next
        
        adcs = adc_lists[2*i + 1]
        ergs = erg_lists[2*i + 1]

        P = np.polyfit(adcs,ergs,deg = 1, cov = False)

        mu_noise_list.append(np.mean(ergs)*0.8)
        P_list.append(P)
        
    # Write calibration file

    calib_file = h5.File(calibration_filename, 'w')
    
    calib_file.create_dataset("Panel Numbers", data = unique_panels)

    calib_file.create_dataset("Pedestal",  data = ped_lists) # Not used
    calib_file.create_dataset("Threshold", data = mu_noise_list) # in keV
    calib_file.create_dataset("Overbin",   data = overbin_lists) # in ACD value

    calib_file.create_dataset("Calibration Parameters", data = P_list)

    calib_file.close()

if __name__ == '__main__': cli()