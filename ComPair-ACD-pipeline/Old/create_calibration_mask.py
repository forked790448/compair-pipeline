#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Develop energy and position calibration masks
 - loads calibration metadata log by log, and then creates the calibration mask
 
"""
import sys
sys.path.insert(1, 'randomScripts')
import pandas as pd
pd.options.mode.chained_assignment = None

import re
import os
import itertools
import warnings
warnings.simplefilter("ignore")


# plotting
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.pylab as pylab
import matplotlib as mpl

# peak fitting
from scipy.optimize import curve_fit

# math tools
import math
import NRL_Math as nrl
import randomScripts.additionalStatistics as addStat


#for GIF
from celluloid import Camera # getting the camera

# helper functions
import randomScripts.peakFindingLimits as peakFindingLimits


class CreateCalibrationMasks:
    def __init__(self, parent_dir):
        self.parent_dir = parent_dir[0]
        self.energy_of_interest = 2614 # 511, 662, 1173, 1274, 2614
        self.CMIS_gain = 7 # 3 or 7
        
        self.fit_technique = 0 # 0 for peak centroid, 1 for double gaussian
        self.energies_of_interest = [1173.23, 1332.5]
        
        
        # global constants that might change
        self.FULL_LOG_LENGTH = 100 #100 mm, length of log
        
        if self.parent_dir != "void":
            self.get_total_log()

            self.pos_edges = np.linspace(-1.25, 1.25, 75)
            self.pos_edges_final = np.linspace(-1.6, 1.6, 75)*self.FULL_LOG_LENGTH/2
            self.energyDiv = 20
            self.PositionDiv = len(self.pos_edges_final)-1
            self.gainCorrection = np.zeros(shape=(self.PositionDiv,self.total_logs))
            self.centerLoc = np.zeros(shape=(self.total_logs,))
            self.rangeScale = np.zeros(shape=(self.total_logs,))
        

        # plot setting stuff
        plt.rcParams['font.size'] = 15
        plt.rcParams['font.family'] = "serif"
        tdir = 'in'
        major = 5.0
        minor = 3.0
        plt.rcParams['xtick.direction'] = tdir
        plt.rcParams['ytick.direction'] = tdir
        plt.rcParams['xtick.major.size'] = major
        plt.rcParams['xtick.minor.size'] = minor
        plt.rcParams['ytick.major.size'] = major
        plt.rcParams['ytick.minor.size'] = minor
        
        params = {'legend.fontsize': 'x-large',
                 'axes.labelsize': 'x-large',
                 'axes.titlesize':'x-large',
                 'xtick.labelsize':'x-large',
                 'ytick.labelsize':'x-large'}
        pylab.rcParams.update(params)

                         
    def plotCurrentSingleLogDepthEnergy(self, logNumber, position_edges):          
        # plt.hist2d(self.log_df[f'L{logNumber}_adc'], self.log_df[f'L{logNumber}_pos'], bins=(100, position_edges), cmap=plt.cm.Greys)
        f, axarr = plt.subplots(1, 1)
        nrl.set_size(f, (8, 6))


        plt.hist2d(self.log_df[f'L{logNumber}_adc'], self.log_df[f'L{logNumber}_pos'], bins=(75, 75), norm=mpl.colors.LogNorm(), cmap=plt.cm.Greys)

                
        plt.colorbar()
        plt.xlabel('Value [ADC]')
        plt.ylabel('Position')
        
        
        plt.savefig(self.parent_dir + f'L{logNumber}_uncorrected.png', bbox_inches="tight")
        plt.savefig(self.parent_dir + f'L{logNumber}_uncorrected.pdf', bbox_inches="tight")

        
        
    def plotSingleCorrectedLogDepthEnergy(self, logNumber, position_edges):    
        self.loadLogData(logNumber, self.MIN_ADC-100, self.MAX_ADC, False)
        self.applyPosCorrection(logNumber)
        self.applyGainCorrection(logNumber, position_edges)

        self.log_df = self.log_df[self.log_df[f'L{logNumber}_adc'] >  60]
        self.log_df = self.log_df[self.log_df[f'L{logNumber}_adc'] <  self.energy_of_interest+400]
                
        energyEdges = np.linspace(self.energy_of_interest-400, self.energy_of_interest+400, 75)
        pos_edges_tmp = np.linspace(-1, 1, 75)*150.0

        # plt.hist2d(self.log_df[f'L{logNumber}_adc'], self.log_df[f'L{logNumber}_pos'], bins=(energyEdges, pos_edges_tmp), cmap=plt.cm.Greys)
        f, axarr = plt.subplots(1, 1)
        nrl.set_size(f, (8, 6))
        plt.hist2d(self.log_df[f'L{logNumber}_adc'], self.log_df[f'L{logNumber}_pos'], bins=(energyEdges, pos_edges_tmp), norm=mpl.colors.LogNorm(), cmap=plt.cm.Greys)
        
        plt.colorbar()
        plt.xlabel('Energy [keV]')
        plt.ylabel('Position')
        
        plt.savefig(self.parent_dir + f'L{logNumber}_corrected.png', bbox_inches="tight")
        plt.savefig(self.parent_dir + f'L{logNumber}_corrected.pdf', bbox_inches="tight")
        
        
        # Spectra only
        self.loadLogData(logNumber, self.MIN_ADC-100, self.MAX_ADC, False)
        self.applyPosCorrection(logNumber)
        self.applyGainCorrection(logNumber, position_edges)

        self.log_df = self.log_df[self.log_df[f'L{logNumber}_adc'] >  10]
        self.log_df = self.log_df[self.log_df[f'L{logNumber}_adc'] <  self.energy_of_interest+400]
                
        energyEdges = np.linspace(0, 1000, 100)
        pos_edges_tmp = np.linspace(-1, 1, 75)*150.0

        f, axarr = plt.subplots(1, 1)
        nrl.set_size(f, (8, 6))
        
        
        # hist = np.histogram(self.log_df[f'L{logNumber}_adc'], bins=energyEdges)



        # plt.plot( hist[1][0:len(hist[1])-1] + 0.5*(hist[1][1]-hist[1][0]), hist[0], color='k',  linewidth=3)
        plt.hist(self.log_df[f'L{logNumber}_adc'], bins=energyEdges)
                
                
        
        plt.xlabel('Energy [keV]')
        plt.ylabel('Counts per 10 keV')
        plt.xlim([0, 1000])
        
        plt.savefig(self.parent_dir + f'L{logNumber}_spectra.png', bbox_inches="tight")
        plt.savefig(self.parent_dir + f'L{logNumber}_spectra.pdf', bbox_inches="tight")
        
        
                
    def plotAllDepthEnergy(self):
        print('Plotting All Uncorrected Depth-Energy 2D Histograms')

        f, axarr = plt.subplots(5, 6)
        logNumber = 0
        
        for asic_id in range(0, 5):
            for channel in range(0, 6):
                self.setMinMaxLimits(logNumber)
                
                # self.MIN_ADC = 0

                self.loadLogData(logNumber, self.MIN_ADC, self.MAX_ADC)

                self.log_df = self.log_df[self.log_df[f'L{logNumber}_pos'].between(self.MIN_POS, self.MAX_POS)]  
                
                energyEdges = np.linspace(self.MIN_ADC, self.MAX_ADC, 50)
                pos_edges_tmp = np.linspace(-1.5, 1.5, 50)


                axarr[asic_id,channel].hist2d(self.log_df[f'L{logNumber}_adc'], self.log_df[f'L{logNumber}_pos'], bins=(energyEdges, pos_edges_tmp), cmap=plt.cm.Greys)
                # axarr[asic_id,channel].hist2d(self.log_df[f'L{logNumber}_adc'], self.log_df[f'L{logNumber}_pos'], bins=(energyEdges, pos_edges_tmp), norm=mpl.colors.LogNorm(), cmap=plt.cm.Greys)

                axarr[asic_id,channel].set_ylim([-1.5, 1.5])
                # axarr[asic_id,channel].set_xlim([0, self.MAX_ADC])
                self.axisHandler(axarr, asic_id, channel, logNumber)

                    
                del self.log_df

                logNumber = logNumber + 1
                
        f.text(0.5, 0.04, 'Value [ADC]', ha='center')
        f.text(0.04, 0.5, 'Position [arb]', va='center', rotation='vertical')
        
        f.savefig(self.parent_dir +f'depthEnergyPlot_{self.energy_of_interest}.png', dpi = 900, bbox_inches = 'tight', facecolor='w')
        # f.savefig(self.parent_dir +f'depthEnergyPlot_{self.energy_of_interest}_log.png', dpi = 900, bbox_inches = 'tight', facecolor='w')
        
        
    def plotDepthAndEnergySpectra(self):
        print('Plotting All 1D Depth and Energy Spectra')

        lowerThresh = np.zeros(shape=(self.total_logs,))

        f, axarr = plt.subplots(5, 6)

        f2, axarr2 = plt.subplots(5, 6)
        upperEnergy = int(math.ceil(self.energy_of_interest / 100.0)) * 100

        logNumber = 0
        
        for row in range(0, 5):
            for column in range(0, 6):
                print("  " , logNumber)

                self.setMinMaxLimits(logNumber)
                self.loadLogData(logNumber, 5, self.MAX_ADC+200, False)   
                
                self.log_df = self.log_df[self.log_df[f'L{logNumber}_pos'].between(self.MIN_POS, self.MAX_POS)]  
                self.applyPosCorrection(logNumber)
                self.log_df = self.log_df[self.log_df[f'L{logNumber}_pos'].between(-50, 50)]  
                
                self.applyGainCorrection(logNumber, self.pos_edges_final)
                self.log_df = self.log_df[self.log_df[f'L{logNumber}_adc'].between(75, upperEnergy + 100) ]

                energyEdges = np.linspace(250, upperEnergy + 100, int((upperEnergy + 100)/10) )
                pos_edges_tmp = np.linspace(-1, 1, 50)*150.0
                

                hist = np.histogram(self.log_df[f'L{logNumber}_adc'], bins=energyEdges)
                self.log_df = self.log_df[self.log_df[f'L{logNumber}_adc'].between(self.energy_of_interest-0.1*self.energy_of_interest, self.energy_of_interest+0.1*self.energy_of_interest) ]

                hist2 = np.histogram(self.log_df[f'L{logNumber}_pos'], bins=pos_edges_tmp)

                # axarr[row,column].plot( hist[1][0:len(hist[1])-1] + 0.5*(hist[1][1]-hist[1][0]), np.log(hist[0]), color='k')

                axarr[row,column].plot( hist[1][0:len(hist[1])-1] + 0.5*(hist[1][1]-hist[1][0]), hist[0], color='k')
                axarr2[row,column].plot( hist2[1][0:len(hist2[1])-1] + 0.5*(hist2[1][1]-hist2[1][0]) , hist2[0], color='k')


                axarr2[row,column].set_xlim([-150, 150])
                self.axisHandler(axarr, row, column, logNumber)
                self.axisHandler(axarr2, row, column, logNumber)
                
                lowerThresh[logNumber] = addStat.getLowerThreshold( hist[1][0:len(hist[1])-1] + 0.5*(hist[1][1]-hist[1][0]), hist[0], self.energy_of_interest)

                del self.log_df

                logNumber = logNumber + 1
                
                
        f.text(0.5, 0.04, 'Energy [keV]', ha='center')
        f.text(0.04, 0.5, 'Counts per 10 keV', va='center', rotation='vertical')

        
        f2.text(0.5, 0.04, 'Position [mm]', ha='center')
        f2.text(0.04, 0.5, 'Counts per 15 mm', va='center', rotation='vertical')
        
        f.savefig(self.parent_dir +f'energySpectra_{self.energy_of_interest}.png', dpi = 900, bbox_inches = 'tight', facecolor='w')
        f2.savefig(self.parent_dir +f'depthHistogram_{self.energy_of_interest}.png', dpi = 900, bbox_inches = 'tight', facecolor='w')
        
        
        
        # Lower threshold
        f = plt.figure()
        f.clf()
        ax = f.add_subplot(1, 1, 1)

        plt.scatter(np.linspace(0, self.total_logs-1 , self.total_logs), lowerThresh, s=125)
        plt.xlabel('Log Index')
        plt.ylabel('Lower Threshold [keV]')
        plt.axvspan(-0.5, 7.5, color='r', alpha=0.5, lw=0, label="ASIC0")
        plt.axvspan(7.5, 15.5, color='g', alpha=0.5, lw=0, label="ASIC1")
        plt.axvspan(15.5, 23.5, color='b', alpha=0.5, lw=0, label="ASIC2")
        plt.axvspan(23.5, 30.5, color='y', alpha=0.5, lw=0, label="ASIC3")
        
        plt.legend(loc='best',  frameon=False)
        nrl.set_size(f, (8, 6))
        plt.show()
        f.savefig(self.parent_dir +f'lowerThreshold_{self.energy_of_interest}.png', dpi = 900, bbox_inches = 'tight', facecolor='w')







        
    def plotAllCorrectedDepthEnergy(self):
        print('Plotting All Corrected Depth-Energy 2D Histograms')
        
        f, axarr = plt.subplots(5, 6)
        logNumber = 0
        upperEnergy = int(math.ceil(self.energy_of_interest / 100.0)) * 100

        
        for asic_id in range(0, 5):
            for channel in range(0, 6):
                print(logNumber)
                # lowerEnergyBound = 100 # self.energy_of_interest-400
                lowerEnergyBound = self.energy_of_interest-400

                self.setMinMaxLimits(logNumber)
                self.loadLogData(logNumber, 40, upperEnergy + 100, False)   
                
                self.log_df = self.log_df[self.log_df[f'L{logNumber}_pos'].between(-1, 1)]  
                self.applyPosCorrection(logNumber)
                self.log_df = self.log_df[self.log_df[f'L{logNumber}_pos'].between(-200, 200)]  
                
                self.applyGainCorrection(logNumber, self.pos_edges_final)
                self.log_df = self.log_df[self.log_df[f'L{logNumber}_adc'].between(lowerEnergyBound,self.energy_of_interest+400)]

                energyEdges = np.linspace(lowerEnergyBound, self.energy_of_interest+400, 50)
                pos_edges_tmp = np.linspace(-1, 1, 25)*100
                

                # axarr[asic_id,channel].hist2d(self.log_df[f'L{logNumber}_adc'], self.log_df[f'L{logNumber}_pos'], bins=(energyEdges, pos_edges_tmp), cmap=plt.cm.Greys)
                axarr[asic_id,channel].hist2d(self.log_df[f'L{logNumber}_adc'], self.log_df[f'L{logNumber}_pos'], bins=(energyEdges, pos_edges_tmp), norm=mpl.colors.LogNorm(), cmap=plt.cm.Greys)

                
                self.axisHandler(axarr, asic_id, channel, logNumber)


                logNumber = logNumber + 1
                
        f.text(0.5, 0.04, 'Energy [keV]', ha='center')
        f.text(0.04, 0.5, 'Position [mm]', va='center', rotation='vertical')
        
        f.savefig(self.parent_dir +f'correctedDepthEnergyPlot{self.energy_of_interest}.png', dpi = 900, bbox_inches = 'tight', facecolor='w')
        # f.savefig(self.parent_dir +f'correctedDepthEnergyPlot{self.energy_of_interest}_log.png', dpi = 900, bbox_inches = 'tight', facecolor='w')
        del self.log_df
        
        
    def getLogStats(self):
        print ("Getting Stats")
        
        
        extraRange = 0.1
        energyBins  = np.linspace(self.energy_of_interest-extraRange*self.energy_of_interest, self.energy_of_interest+extraRange*self.energy_of_interest, 50)
        self.resolutionPerLog = np.zeros(shape=(self.total_logs,))

        fig, ax = plt.subplots(figsize=(8,6))
        camera = Camera(fig)

        for logNumber in range(0,self.total_logs):
            print(logNumber)
            # fig.clf()
            
            self.setMinMaxLimits(logNumber)
            self.loadLogData(logNumber, self.MIN_ADC-100, self.MAX_ADC)
            self.applyPosCorrection(logNumber)
            self.applyGainCorrection(logNumber, self.pos_edges_final)
            self.log_df = self.log_df[self.log_df[f'L{logNumber}_adc'].between(self.energy_of_interest-extraRange*self.energy_of_interest, self.energy_of_interest+extraRange*self.energy_of_interest)]

            yn, bin_edges = np.histogram(self.log_df[f'L{logNumber}_adc'], bins=energyBins)
            yn = yn/max(yn)
            x = bin_edges[:-1]+0.5*(bin_edges[1]-bin_edges[0])

            fwhmValue = nrl.fwhmIndex(yn) * np.diff(x)[0]
            bestFit = nrl.gaussFitWithExp(x, yn, self.energy_of_interest, fwhmValue)
            comps = bestFit.eval_components(x=x)

            
            self.resolutionPerLog[logNumber] = 2.355*bestFit.best_values['g1_sigma']/bestFit.best_values['g1_center']*100       
            
            plt.plot(x, bestFit.best_fit, c='r')
            plt.plot(x, yn, c='b')
            plt.plot(x, comps['g1_'], 'g--')
            plt.plot(x, comps['exp_'], 'k--')
            plt.legend(loc='best')

            ax.text(0.5, 1.05, f'Log {logNumber}_energy{self.energy_of_interest}', transform=ax.transAxes, horizontalalignment='center', verticalalignment='center', size=40) # making the dynamic title
            plt.legend(('Best Fit', 'Data', 'Gaus.', 'Exp.'),  frameon=False)
            ax.set(xlabel='Energy [keV]', ylabel='Normalized counts per bin')
            # nrl.set_size(fig, (8, 6))

            camera.snap()
            # fig.savefig(self.parent_dir + f'gaussFitL{logNumber}.png', dpi = 900, bbox_inches = 'tight', facecolor='w', transparent=True)

        #Create Gif
        animation = camera.animate()
        animation.save(self.parent_dir +f'gaussFits{self.energy_of_interest}.gif', writer='Pillow', fps=2)

        
        f = plt.figure()
        ax = f.add_subplot(1, 1, 1)
        plt.scatter(np.linspace(0, self.total_logs-1 , self.total_logs), self.resolutionPerLog, s=125)
        plt.xlabel('Log Index')
        plt.ylabel(f'Resolution at {self.energy_of_interest} keV [%]')
        plt.axvspan(-0.5, 7.5, color='r', alpha=0.5, lw=0, label="ASIC0")
        plt.axvspan(7.5, 15.5, color='g', alpha=0.5, lw=0, label="ASIC1")
        plt.axvspan(15.5, 23.5, color='b', alpha=0.5, lw=0, label="ASIC2")
        plt.axvspan(23.5, 30.5, color='y', alpha=0.5, lw=0, label="ASIC3")
        # ax.set_ylim([0, 15])


        plt.legend(loc='best',  frameon=False)
        nrl.set_size(f, (8, 6))
        # plt.ylim(np.floor(self.resolutionPerLog.min())-1, np.ceil(self.resolutionPerLog.max())+1)     # set the ylim to bottom, top
        plt.ylim(0, 14)     # set the ylim to bottom, top

        plt.show()
        
        f.savefig(self.parent_dir +f'resolutionPerLog{self.energy_of_interest}.png', dpi = 900, bbox_inches = 'tight', facecolor='w')
        
        a_file = open(self.parent_dir +f'resolutionData{self.energy_of_interest}.txt', "w")
        for row in self.resolutionPerLog:
            np.savetxt(a_file, [row])
        a_file.close()
        
        print(f'Median: {np.median(self.resolutionPerLog)}, STD: {np.std(self.resolutionPerLog)}')
        
        
    def loadLogData(self, logNumber, min_adc_value, max_adc_value, mother=False):
        # did this so you would not have to load the data everytime
        
        if mother==True:
            self.logMother_df = pd.read_hdf(self.parent_dir + f'calibration_metadata/L{logNumber}.hdf5')
            self.logMother_df = self.logMother_df.dropna()
            self.logMother_df = self.logMother_df[self.logMother_df[f'L{logNumber}_adc'].between(min_adc_value, max_adc_value)]
            self.logMother_df = self.logMother_df[self.logMother_df[f'L{logNumber}_pos'].between(self.MIN_POS, self.MAX_POS)]  

        else:         
            self.log_df = pd.read_hdf(self.parent_dir + f'calibration_metadata/L{logNumber}.hdf5')
            self.log_df = self.log_df.dropna()
            self.log_df = self.log_df[self.log_df[f'L{logNumber}_adc'].between(min_adc_value, max_adc_value)]
            self.log_df = self.log_df[self.log_df[f'L{logNumber}_pos'].between(self.MIN_POS, self.MAX_POS)]

            

    def processInitialCalibration(self):
        self.plotAllDepthEnergy()
        print ("Start Calibrating:")

        for logNumber in range(0,self.total_logs):
            print(f'    Log {logNumber}')
            self.setMinMaxLimits(logNumber)
            
            # self.MIN_ADC = 50
            # self.MAX_ADC = 120

            self.loadLogData(logNumber, self.MIN_ADC, self.MAX_ADC)
            # self.plotCurrentSingleLogDepthEnergy(logNumber, self.pos_edges)
            # # 
            ###################################################################
            # This is the initial calibration to standardize the depth
            # Load Data and coarse gain correct it
            self.loadLogData(logNumber, self.MIN_ADC, self.MAX_ADC, True)
            
            self.log_df = self.logMother_df.copy()
            self.log_index = self.log_df[f'L{logNumber}_adc'].between(self.MIN_ADC, self.MAX_ADC)
            self.getGainCorrection(logNumber, self.pos_edges)
            
            # Load Data Again and finely correct It, 
            # after experimentation, it seems like it does more harm than good at this stage.
            # self.getFineGainCorrection(logNumber, self.pos_edges)
            
            # now lets get position correction
            self.log_df = self.logMother_df.copy()

            energyBounds = 0.1
            if self.fit_technique == 1:
                energyBounds = 0.3
            if self.energy_of_interest == 2614:
                energyBounds = 0.2
            self.getPositionCorrection(logNumber, self.pos_edges, self.energy_of_interest-energyBounds*self.energy_of_interest, self.energy_of_interest+energyBounds*self.energy_of_interest)
            
            del self.log_df
            
            
            ###################################################################
            # This is the secondary to get the energy gain calibration for the standardized the depth
            # Now that you have position correction, now re-do energy correction
            self.log_df = self.logMother_df.copy()
            self.log_index = self.log_df[f'L{logNumber}_adc'].between(self.MIN_ADC, self.MAX_ADC)
            self.applyPosCorrection(logNumber)
            self.getGainCorrection(logNumber, self.pos_edges_final)
            
            # Load Data Again and finely correct It
            self.log_index = self.log_df[f'L{logNumber}_adc'].between(self.MIN_ADC-50, self.MAX_ADC)
            # self.getFineGainCorrection(logNumber, self.pos_edges_final)
            self.processEdgeCase(logNumber, self.pos_edges_final)
            ###################################################################
            
            del self.logMother_df
            del self.log_df

            # self.plotCurrentSingleLogDepthEnergy(logNumber, self.pos_edges_final)
            # self.plotSingleCorrectedLogDepthEnergy(logNumber, self.pos_edges_final)
            
        print ("End Calibrating")


        self.plotAllCorrectedDepthEnergy()
        self.plotDepthAndEnergySpectra()
        addStat.getUpperThreshold(self.parent_dir, self.gainCorrection)



        self.getLogStats()
        self.saveMasks()
    
    def testCalibration(self):
        # self.plotAllDepthEnergy()
        self.loadMasks()

        # self.plotAllCorrectedDepthEnergy()
        # self.plotDepthAndEnergySpectra()
        # addStat.getUpperThreshold(self.parent_dir, self.gainCorrection)


        self.getLogStats()

        
    def loadMasks(self):
        filename = self.parent_dir + f'../calibrations/calibrationData{self.energy_of_interest}.hdf5'
        tmp_df = pd.read_hdf(filename,key='ADC')
        
        self.gainCorrection = tmp_df.to_numpy()
        
        tmp_df = pd.read_hdf(filename,key='POS')
        tmpArr = np.transpose(tmp_df.to_numpy())
        self.centerLoc = tmpArr[0,:]
        self.rangeScale = tmpArr[1,:]

        
    def saveMasks(self):
        folderExists = os.path.isdir(self.parent_dir + '../calibrations')
        if folderExists == False:
            os.mkdir(self.parent_dir + '../calibrations')
            
        
        filename = self.parent_dir + f'../calibrations/calibrationData{self.energy_of_interest}.hdf5'

        print('Writing Calibration Data to File: ' + filename)
        
        posArr = np.vstack((self.centerLoc, self.rangeScale))
        posCal_df = pd.DataFrame(np.transpose(posArr), columns = ['center_loc','range_scale'] )
        
        tmpColNAmes = list(itertools.chain(*[[f'L{num}'] for num in range(self.total_logs)]))
        gainCal_df = pd.DataFrame(self.gainCorrection, columns = tmpColNAmes)
        
        posEdged_df = pd.DataFrame(self.pos_edges_final, columns = ['pos_edges'])
        
        with pd.HDFStore(filename) as hdf:
            posCal_df.to_hdf(hdf, key='POS')
            gainCal_df.to_hdf(hdf, key='ADC')
            posEdged_df.to_hdf(hdf, key='POS_EDGE')
            
        ## to do, XXX write info file
        # with open('filename.txt', 'w') as f:
            # print(f'{self.CMIS_gain}', file=f)

    
    def getPositionCorrection(self, logNumber, position_edges, minEnergy, maxEnergy):
        self.applyGainCorrection(logNumber, position_edges)
                    
        # Depth Correction
        # if logNumber < 18 and logNumber !=15:
        #     cts, xedges = np.histogram(self.log_df[f'L{logNumber}_pos'][self.log_df[f'L{logNumber}_adc'].between(minEnergy, maxEnergy)], bins=35)
        # elif logNumber < 2:
        #     cts, xedges = np.histogram(self.log_df[f'L{logNumber}_pos'][self.log_df[f'L{logNumber}_adc'].between(minEnergy, maxEnergy)], bins=25)

        # else:
        cts, xedges = np.histogram(self.log_df[f'L{logNumber}_pos'][self.log_df[f'L{logNumber}_adc'].between(minEnergy, maxEnergy)], bins=35)
            
            
        if (logNumber == 9) or (logNumber == 15):
            cts[cts > 0.25*np.max(cts)] = 0.25*np.max(cts)

        else:
            cts[cts > 0.75*np.max(cts)] = 0.75*np.max(cts)

        x = xedges[:-1]+0.5*(xedges[1]-xedges[0])
        diffData = np.diff(cts)
        
        minLoc = x[nrl.findPeak(diffData)]
        maxLoc = x[nrl.findPeak(-diffData)]
        
        counter = 0
        # if min > max, try 10 times to find the min location
        # dont know whyI have the try 10 times function
        while (minLoc > maxLoc) and (counter < 10):
            diffData[nrl.findPeak(diffData)] = 0
            minLoc = x[nrl.findPeak(diffData)]
            counter = counter + 1

        self.centerLoc[logNumber] = (maxLoc+minLoc)/2
        self.rangeScale[logNumber] = self.FULL_LOG_LENGTH/(maxLoc-minLoc) 
        

    def getFineGainCorrection(self, logNumber, position_edges):
        self.applyGainCorrection(logNumber, position_edges)

        self.log_index = self.log_df[f'L{logNumber}_adc'].between(self.energy_of_interest-60, self.energy_of_interest+60)
            

        xedges = np.linspace(self.log_df[f'L{logNumber}_adc'][self.log_index].min(), self.log_df[f'L{logNumber}_adc'][self.log_index].max(), self.energyDiv-10)
        cts, xedges, yedge = np.histogram2d(self.log_df[f'L{logNumber}_adc'][self.log_index], self.log_df[f'L{logNumber}_pos'][self.log_index], bins=(xedges, position_edges))
        x = xedges[:-1]+0.5*(xedges[1]-xedges[0])

        
        for i in range (0, len(position_edges)-1):
            # Fit the dummy Gaussian data
            yn = cts[:,i]
            yn = nrl.background_subtract(x, yn)
            
            maxInd = nrl.findPeak(yn)[0]
            fwhmValue = min(math.ceil(nrl.fwhmIndex(yn)), maxInd)
            
            init_vals = [cts[:,i].max(), x[maxInd], fwhmValue*(xedges[1]-xedges[0])]  # for [amp, cen, wid]
                        
            try:
                popt, pcov = curve_fit(nrl.gaussfunc, x[(maxInd-fwhmValue):(maxInd+fwhmValue+1)], yn[(maxInd-fwhmValue):(maxInd+fwhmValue+1)], p0=init_vals)
                if (self.gainCorrection[i, logNumber]*self.energy_of_interest/popt[1] < 0.5):
                    void = 0/0
                else:
                    self.gainCorrection[i, logNumber] = self.gainCorrection[i, logNumber]*self.energy_of_interest/popt[1]
            except:
                self.gainCorrection[i, logNumber] = self.gainCorrection[i, logNumber]*self.energy_of_interest/x[maxInd]
                

        
    def getGainCorrection(self, logNumber, position_edges):
        # Loop throught each log and get the gain and position calibration
        xedges = np.linspace(self.log_df[f'L{logNumber}_adc'][self.log_index].min(), self.log_df[f'L{logNumber}_adc'][self.log_index].max(), self.energyDiv+1)
        cts, xedges, yedge = np.histogram2d(self.log_df[f'L{logNumber}_adc'][self.log_index], self.log_df[f'L{logNumber}_pos'][self.log_index], bins=(xedges, position_edges))
        
        # fig, ax = plt.subplots(figsize=(8,6))
        # camera = Camera(fig)
        
        x = xedges[:-1]+0.5*(xedges[1]-xedges[0])

        
        for i in range (0, len(position_edges)-1):
            # print(i)

            if self.fit_technique == 0:
                yn = cts[:,i]
                yn = nrl.background_subtract(x, yn)
    
                maxInd = nrl.findPeak(yn)[0]
                fwhmValue = min(math.ceil(nrl.fwhmIndex(yn)), maxInd)
    
                init_vals = [cts[:,i].max(), x[maxInd], fwhmValue*(xedges[1]-xedges[0])]  # for [amp, cen, wid]
                try:
                    popt, pcov = curve_fit(nrl.gaussfunc, x[(maxInd-fwhmValue):(maxInd+fwhmValue+1)], cts[(maxInd-fwhmValue):(maxInd+fwhmValue+1),i], p0=init_vals)
                    self.gainCorrection[i, logNumber] = self.energy_of_interest/popt[1]  
                    # ym = nrl.gaussfunc(x, popt[0], popt[1], popt[2])
    
                except:
                    self.gainCorrection[i, logNumber] = self.energy_of_interest/x[maxInd]  
                    # ym = x *0
            else:
                optim2 = nrl.double_gaussian_fit(x, cts[:,i])
                try:
                    self.gainCorrection[i, logNumber] =   np.mean([min(self.energies_of_interest)/min(optim2[1:5:3]), max(self.energies_of_interest)/max(optim2[1:5:3])])
                except:
                    pass

                # plt.clf()
                # plt.plot(x, cts[:,i], lw=5, c='g', label='measurement')
                # plt.plot(x, nrl.two_gaussians(x, *optim2), lw=1, c='r', ls='--', label='fit of 2 Gaussians')
                

            # plt.plot(x, ym, c='r', label='Best fit')
            # plt.plot(x, cts[:,i], label='Data')
            # plt.plot(x[(maxInd-fwhmValue):(maxInd+fwhmValue+1)], cts[(maxInd-fwhmValue):(maxInd+fwhmValue+1),i])
            # ax.text(0.5, 1.05, f'Depth {i}', transform=ax.transAxes, horizontalalignment='center', verticalalignment='center', size=40) # making the dynamic title
            # plt.legend(('Best Fit', 'Data'),  frameon=False)
            # nrl.set_size(fig, (8, 6))
            
            # camera.snap()
            # print(i/len(position_edges))

            # fig.savefig(self.parent_dir + f'gaussFitL{i}.png', dpi = 900, bbox_inches = 'tight', facecolor='w', transparent=True)


        # # Create Gif
        # animation = camera.animate()
        # animation.save(self.parent_dir +'logFits.gif', writer='Pillow', fps=2)
        
        # clean gainCorrection
        for i in range(1, len(self.gainCorrection[:, logNumber])-1):
            if self.gainCorrection[i, logNumber] == 0:
                self.gainCorrection[i, logNumber] = self.gainCorrection[i-1, logNumber]
        

    def processEdgeCase(self, logNumber, position_edges):
        
        #find outlires 
       med = np.median(self.gainCorrection[:, logNumber]) 
       
       
       self.gainCorrection[np.abs(100*(self.gainCorrection[:, logNumber] - med)/med) > 50, logNumber] = med
       
       upperPosition = np.abs(position_edges - 50).argmin() - 1
       lowerPosition = np.abs(position_edges - -50).argmin() + 1
       
       self.gainCorrection[0:(lowerPosition-1), logNumber] = np.mean(self.gainCorrection[(lowerPosition+5):(lowerPosition+10), logNumber])
       self.gainCorrection[(upperPosition+1):len(position_edges), logNumber] = np.mean(self.gainCorrection[(upperPosition-10):(upperPosition-5), logNumber])
       

        
    def get_total_log(self):
        files = os.listdir(self.parent_dir+"calibration_metadata/")
        self.total_logs = 0
        for f in files:
            nameOfFile = (int(re.findall(r'[0-9]+', f)[0]))
            
            if nameOfFile > self.total_logs:
                self.total_logs = nameOfFile
            
        self.total_logs = self.total_logs + 1
        
        
    def applyGainCorrection(self, logNumber, position_edges):
        for i in range (0, len(position_edges)-1):
            self.log_df[f'L{logNumber}_adc'][self.log_df[f'L{logNumber}_pos'].between(position_edges[i], position_edges[i+1]) ] \
                = self.log_df[f'L{logNumber}_adc'][self.log_df[f'L{logNumber}_pos'].between(position_edges[i], position_edges[i+1]) ] \
                    * self.gainCorrection[i, logNumber]
                    
    def applyPosCorrection(self,logNumber):
        self.log_df[f'L{logNumber}_pos'] = self.log_df[f'L{logNumber}_pos'].sub(self.centerLoc[logNumber])
        self.log_df[f'L{logNumber}_pos'] = self.log_df[f'L{logNumber}_pos'].mul(self.rangeScale[logNumber])
        
    def setMinMaxLimits(self, logNumber):
        self.MIN_ADC, self.MAX_ADC = peakFindingLimits.setMinMaxADC(self.CMIS_gain, logNumber, self.energy_of_interest)
        self.MIN_POS, self.MAX_POS = peakFindingLimits.setMinMaxPos(self.CMIS_gain, logNumber, self.energy_of_interest)


    def axisHandler(self, axarr, asic_id, channel, logNumber):
        if logNumber != 24:
            axarr[asic_id,channel].get_xaxis().set_visible(False)
            axarr[asic_id,channel].get_yaxis().set_visible(False)
        else:
            axarr[asic_id,channel].tick_params(axis='both', which='major', labelsize=8)
            axarr[asic_id,channel].tick_params(axis='both', which='minor', labelsize=8)
        
            
        
def main(filename=None, **kargs):
    metaData = CreateCalibrationMasks(filename)
    metaData.processInitialCalibration()
    # metaData.testCalibration()
    
    
if __name__ == '__main__':
    
    import argparse    
    parser = argparse.ArgumentParser(
        description='Lookup some information in a database')
    parser.add_argument("filename", nargs="*")
    args = parser.parse_args()
    
    
    main(**vars(args))
