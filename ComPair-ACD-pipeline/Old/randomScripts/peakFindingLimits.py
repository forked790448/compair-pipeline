#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 15 08:29:07 2021

@author: dshy
"""

def setMinMaxADC(CMIS_gain, logNumber, energy_of_interest):
    MIN_ADC = 400
    MAX_ADC = 1500
    if CMIS_gain == 3:

        if energy_of_interest == 1274:
            MIN_ADC = 300
            MAX_ADC = 800
    
            if logNumber == 5:
                MIN_ADC = 450
            if logNumber == 6:
                MIN_ADC = 275
            if logNumber == 7:
                MIN_ADC = 250
            if logNumber == 8:
                MIN_ADC = 250
            if logNumber == 9:
                MIN_ADC = 150
                MAX_ADC = 500
            if logNumber == 10:
                MIN_ADC = 250
            if logNumber == 12:
                MIN_ADC = 375
            if logNumber == 15:
                MIN_ADC = 135
            if logNumber == 18:
                MIN_ADC = 360
            if logNumber > 21:
                MIN_ADC = 375
            if logNumber == 28:
                MIN_ADC = 375
                MAX_ADC = 600




        if energy_of_interest == 662:
            MIN_ADC = 125
            MAX_ADC = 600
            if logNumber == 5:
                MIN_ADC = 250
            if logNumber == 9:
                MIN_ADC = 75
            if logNumber == 10:
                MIN_ADC = 125
            if logNumber == 12:
                MIN_ADC = 200
            if logNumber == 15:
                MIN_ADC = 60
            if logNumber > 17:
                MIN_ADC = 150
            if logNumber == 18:
                MIN_ADC = 190
            if logNumber == 22:
                MIN_ADC = 175
            if logNumber > 22:
                MIN_ADC = 150
                
        if energy_of_interest == 511:
            MIN_ADC = 125
            MAX_ADC = 250
    
            if logNumber == 5:
                MIN_ADC = 230
                MAX_ADC = 400
            if logNumber == 6:
                MIN_ADC = 110
            if logNumber == 7:
                MIN_ADC = 85
            if logNumber == 8:
                MIN_ADC = 110
            if logNumber == 9:
                MIN_ADC = 55
                MAX_ADC = 150

            if logNumber == 10:
                MIN_ADC = 90
                MAX_ADC = 200
            if logNumber == 11:
                MIN_ADC = 100
            if logNumber == 13:
                MIN_ADC = 100
            if logNumber == 14:
                MIN_ADC = 100
            if logNumber == 15:
                MIN_ADC = 50
                MAX_ADC = 150
            if logNumber == 18:
                MIN_ADC = 135
            if logNumber > 21:
                MIN_ADC = 150
            if logNumber > 23:
                MIN_ADC = 140

        if energy_of_interest == 2614:
            MAX_ADC = 1000
            if logNumber == 0:
                MIN_ADC = 700
            if logNumber == 1 or logNumber == 2:
                MIN_ADC = 720
            if logNumber == 3:
                MIN_ADC = 760
            if logNumber == 4:
                MIN_ADC = 830
            if logNumber == 5:
                MIN_ADC = 1100
                MAX_ADC = 1500
            if logNumber == 6:
                MIN_ADC = 560
            if logNumber == 7:
                MIN_ADC = 560
            if logNumber == 8:
                MIN_ADC = 680
            if logNumber == 9:
                MIN_ADC = 460
                MAX_ADC = 600
            if logNumber == 10:
                MIN_ADC = 550
                MAX_ADC = 700
            if logNumber == 11:
                MIN_ADC = 680
            if logNumber == 12:
                MIN_ADC = 950
                MAX_ADC = 1200
            if logNumber == 13:
                MIN_ADC = 680
            if logNumber == 14:
                MIN_ADC = 850
                MAX_ADC = 1100
            if logNumber == 15:
                MIN_ADC = 320
                MAX_ADC = 500
            if logNumber == 16:
                MIN_ADC = 700
            if logNumber == 17:
                MIN_ADC = 660
            if logNumber == 18:
                MIN_ADC = 830
            if logNumber == 19:
                MIN_ADC = 700
            if logNumber == 20:
                MIN_ADC = 720
            if logNumber == 21:
                MIN_ADC = 670
            if logNumber == 22 or logNumber == 23:
                MIN_ADC = 870
                MAX_ADC = 1100
            if logNumber == 24:
                MIN_ADC = 835
                MAX_ADC = 1100
            if logNumber == 25:
                MIN_ADC = 810
                MAX_ADC = 1100
            if logNumber == 26:
                MIN_ADC = 850
                MAX_ADC = 1100
            if logNumber == 27:
                MIN_ADC = 900
                MAX_ADC = 1000
            if logNumber == 28:
                MIN_ADC = 800
            if logNumber == 29:
                MIN_ADC = 910
                MAX_ADC = 1100
                
    if CMIS_gain == 7:
        MIN_ADC = 100
        MAX_ADC = 500
        if energy_of_interest == 662:
            MIN_ADC = 60
            MAX_ADC = 175

            if logNumber == 4:
                MIN_ADC = 70
                MAX_ADC = 150
        
            if logNumber == 5:
                MIN_ADC = 120
                MAX_ADC = 200

            if logNumber == 9:
                MIN_ADC = 17
                MAX_ADC = 60

            if logNumber == 10:
                MIN_ADC = 40
                MAX_ADC = 150
            if logNumber == 11:
                MIN_ADC = 50
                MAX_ADC = 150

            if logNumber == 12:
                MIN_ADC = 60
                MAX_ADC = 150

            if logNumber == 13:
                MIN_ADC = 65
                MAX_ADC = 150

            if logNumber == 14:
                MIN_ADC = 50
                MAX_ADC = 150

            if logNumber == 15:
                MIN_ADC = 30
                MAX_ADC = 100
            if logNumber > 15:
                MIN_ADC = 90
                MAX_ADC = 175

            if logNumber == 21:
                MIN_ADC = 70
                MAX_ADC = 175

            if logNumber > 23:
                MIN_ADC = 70
                MAX_ADC = 175

        if energy_of_interest == 511:
            MIN_ADC = 50
            MAX_ADC = 125

            if logNumber == 3:
                MIN_ADC = 60
                MAX_ADC = 125
            if logNumber == 5:
                MIN_ADC = 100
                MAX_ADC = 200
            if logNumber == 6:
                MIN_ADC = 50
                MAX_ADC = 100
            if logNumber == 9:
                MIN_ADC = 50
                MAX_ADC = 100
            if logNumber == 10:
                MIN_ADC = 35
                MAX_ADC = 124
            if logNumber == 11:
                MIN_ADC = 45
                MAX_ADC = 124
            if logNumber == 13:
                MIN_ADC = 50
                MAX_ADC = 100
            if logNumber == 14:
                MIN_ADC = 45
                MAX_ADC = 100
            if logNumber == 15:
                MIN_ADC = 60
                MAX_ADC = 100
            if logNumber == 17:
                MIN_ADC = 60
                MAX_ADC = 100
            if logNumber == 18:
                MIN_ADC = 70
                MAX_ADC = 125
            if logNumber == 19:
                MIN_ADC = 70
                MAX_ADC = 125
            if logNumber == 20:
                MIN_ADC = 65
                MAX_ADC = 125
            if logNumber == 21:
                MIN_ADC = 50
                MAX_ADC = 135
            if logNumber > 21:
                MIN_ADC = 75
                MAX_ADC = 135
            if logNumber > 23:
                MIN_ADC = 55
                MAX_ADC = 100
                
                
        if energy_of_interest == 1173:
            MIN_ADC = 130
            MAX_ADC = 210
            
            if logNumber == 2:
                MIN_ADC = 140
            if logNumber == 3:
                MIN_ADC = 145
                MAX_ADC = 230
            if logNumber == 4:
                MIN_ADC = 160
                MAX_ADC = 230
            if logNumber == 5:
                MIN_ADC = 215
                MAX_ADC = 400
            if logNumber == 5:
                MIN_ADC = 200
            if logNumber == 8:
                MIN_ADC = 125
            if logNumber == 9:
                MIN_ADC = 50
                MAX_ADC = 90
            if logNumber == 10:
                MIN_ADC = 100
                MAX_ADC = 150
            if logNumber == 11:
                MIN_ADC = 120
                MAX_ADC = 170
            if logNumber == 11:
                MIN_ADC = 120
            if logNumber == 11:
                MIN_ADC = 140
            if logNumber == 13:
                MIN_ADC = 120
                MAX_ADC = 175
            if logNumber == 15:
                MIN_ADC = 50
                MAX_ADC = 100
            if logNumber == 16:
                MIN_ADC = 150
            if logNumber == 17:
                MIN_ADC = 145
            if logNumber == 18:
                MIN_ADC = 160
                MAX_ADC = 250
            if logNumber == 19:
                MIN_ADC = 150
            if logNumber == 20:
                MIN_ADC = 150
            if logNumber == 22:
                MIN_ADC = 150
            if logNumber == 23:
                MIN_ADC = 180
                MAX_ADC = 300



        if energy_of_interest == 1274:
            MIN_ADC = 130
            MAX_ADC = 210
            
            if logNumber == 4:
                MIN_ADC = 150
                MAX_ADC = 230

            if logNumber == 5:
                MIN_ADC = 200
                MAX_ADC = 400

            if logNumber == 9:
                MIN_ADC = 55
                MAX_ADC = 80

            if logNumber == 10:
                MIN_ADC = 100
                MAX_ADC = 150
            if logNumber == 11:
                MIN_ADC = 90
                MAX_ADC = 160

            if logNumber == 13:
                MIN_ADC = 132
                MAX_ADC = 175

            if logNumber == 15:
                MIN_ADC = 55
                MAX_ADC = 100

            if logNumber == 18:
                MIN_ADC = 150
                MAX_ADC = 250

            if logNumber == 23:
                MIN_ADC = 200
                MAX_ADC = 300
                




        if energy_of_interest == 2614:
            MIN_ADC = 250

            MAX_ADC = 500
            if logNumber == 0:
                MIN_ADC = 300
            if logNumber == 1 or logNumber == 2:
                MIN_ADC = 300
            if logNumber == 3:
                MIN_ADC = 320
            if logNumber == 4:
                MIN_ADC = 340
            if logNumber == 5:
                MIN_ADC = 450
                MAX_ADC = 600
            if logNumber == 6:
                MIN_ADC = 250
                MAX_ADC = 350
            if logNumber == 7:
                MIN_ADC = 275
            if logNumber == 8:
                MIN_ADC = 250
            if logNumber == 9:
                MIN_ADC = 120
                MAX_ADC = 170
            if logNumber == 10:
                MIN_ADC = 220
                MAX_ADC = 300
            if logNumber == 11:
                MIN_ADC = 240
            if logNumber == 12:
                MIN_ADC = 280
            if logNumber == 13:
                MIN_ADC = 260
                MAX_ADC = 330
            if logNumber == 14:
                MIN_ADC = 300

            if logNumber == 15:
                MIN_ADC = 110
                MAX_ADC = 170
            if logNumber == 16:
                MIN_ADC = 350
                MAX_ADC = 400

            if logNumber == 17:
                MIN_ADC = 320
                MAX_ADC = 400

            if logNumber == 18:
                MIN_ADC = 360
                MAX_ADC = 410
            if logNumber == 19:
                MIN_ADC = 325
                MAX_ADC = 410
            if logNumber == 20:
                MIN_ADC = 325
                MAX_ADC = 410
            if logNumber == 21:
                MIN_ADC = 325
                MAX_ADC = 410
            if logNumber == 22:
                MIN_ADC = 325
                MAX_ADC = 410
            if logNumber == 23:
                MIN_ADC = 400
                MAX_ADC = 500
            if logNumber == 24:
                MIN_ADC = 300
                MAX_ADC = 500
            if logNumber == 25:
                MIN_ADC = 300
                MAX_ADC = 500
            if logNumber == 26:
                MIN_ADC = 320
                MAX_ADC = 500
            if logNumber == 27:
                MIN_ADC = 320
                MAX_ADC = 500
            if logNumber == 28:
                MIN_ADC = 300
                MAX_ADC = 500
            if logNumber == 29:
                MIN_ADC = 340
                MAX_ADC = 500

                
    return MIN_ADC, MAX_ADC



def setMinMaxPos(CMIS_gain, logNumber, energy_of_interest):
    MIN_POS = -1.5
    MAX_POS = 1.5

    if CMIS_gain == 7:
        if logNumber == 9:
            MIN_POS = -1.5
            MAX_POS = 0
        if logNumber == 15:
            MIN_POS = -1.5
            MAX_POS = 0.25



    return MIN_POS, MAX_POS



        
        
        
        
                
