#!/usr/bin/env python3
# -*- coding: utf-8 -*-
%reset -f

import pandas as pd
import warnings
warnings.simplefilter("ignore")


# plotting
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.pylab as pylab

import NRL_Math as nrl



# plot setting stuff
plt.rcParams['font.size'] = 15
plt.rcParams['font.family'] = "serif"
tdir = 'in'
major = 5.0
minor = 3.0
plt.rcParams['xtick.direction'] = tdir
plt.rcParams['ytick.direction'] = tdir
plt.rcParams['xtick.major.size'] = major
plt.rcParams['xtick.minor.size'] = minor
plt.rcParams['ytick.major.size'] = major
plt.rcParams['ytick.minor.size'] = minor

params = {'legend.fontsize': 'x-large',
         'axes.labelsize': 'x-large',
         'axes.titlesize':'x-large',
         'xtick.labelsize':'x-large',
         'ytick.labelsize':'x-large'}
pylab.rcParams.update(params)

        

class CalibrationObj:
    def __init__(self, gainCorrection, centerLoc, rangeScale): 
        self.gainCorrection = gainCorrection
        self.centerLoc = centerLoc
        self.rangeScale = rangeScale

def loadMasks(inEnergy):
    energy = inEnergy
    filename = f'calibrations/calibrationData{energy}.hdf5'
    tmp_df = pd.read_hdf(filename,key='ADC')
    
    gainCorrection = tmp_df.to_numpy()
    
    tmp_df = pd.read_hdf(filename,key='POS')
    tmpArr = np.transpose(tmp_df.to_numpy())
    centerLoc = tmpArr[0,:]
    rangeScale = tmpArr[1,:]
    
    c = CalibrationObj(gainCorrection, centerLoc, rangeScale)
    
    return c


def plotCenterLocs(calibList):
    
    f, axarr = plt.subplots(figsize=(8,6))
    colorBank = ['r', 'g', 'b', 'k']
    x = np.linspace(0, 23, 24)

    for i in range(0, len(energies)):
        calibList[i].centerLoc[5]=  None
        calibList[i].centerLoc[18]=  None
        calibList[i].centerLoc[23]=  None
        
        tmpRangeScale = 100/calibList[i].rangeScale

        
        tmp = (calibList[i].centerLoc-calibList[0].centerLoc)/tmpRangeScale
        # tmp = (calibList[i].centerLoc-calibList[0].centerLoc)

        tmp = tmp*100
    
        plt.scatter(x, tmp, c=colorBank[i], s=100)
        # plt.plot(calibList[i].centerLoc, c=colorBank[i])
        # ax.text(0.5, 1.05, f'Log {logNumber}_energy{self.energy_of_interest}', transform=ax.transAxes, horizontalalignment='center', verticalalignment='center', size=40) # making the dynamic title
    
    ergStr = [str(int) for int in energies]
    plt.legend(tuple(ergStr),  frameon=False)
    axarr.set(xlabel='Log Index', ylabel='Value')

    nrl.set_size(f, (8, 6))
            
    f.savefig(parentName +'PositionCentroidDiff.png', dpi = 600, bbox_inches = 'tight', facecolor='w')
    
    
def plotPosRange(calibList):
    
    f, axarr = plt.subplots(figsize=(8,6))
    colorBank = ['r', 'g', 'b', 'k']
    x = np.linspace(0, 23, 24)
    
    
    tmpRangeScaleOrg = 100/calibList[0].rangeScale


    for i in range(0, len(energies)):
        calibList[i].rangeScale[5]=  None
        calibList[i].rangeScale[18]=  None
        calibList[i].rangeScale[23]=  None
        
        tmpRangeScale = ((100/calibList[i].rangeScale)-tmpRangeScaleOrg)
        
        # tmp = (calibList[i].rangeScale-calibList[0].rangeScale)/calibList[0].rangeScale
        # tmp = (calibList[i].rangeScale-calibList[0].rangeScale)

        # tmp = tmp*100
    
        # plt.scatter(x, tmp, c=colorBank[i], s=100)
        plt.plot(tmpRangeScale, c=colorBank[i])
        # ax.text(0.5, 1.05, f'Log {logNumber}_energy{self.energy_of_interest}', transform=ax.transAxes, horizontalalignment='center', verticalalignment='center', size=40) # making the dynamic title
    
    ergStr = [str(int) for int in energies]
    plt.legend(tuple(ergStr),  frameon=False)
    axarr.set(xlabel='Log Index', ylabel='% Change in Position Range')
    # axarr.set_ylim(0, 1)
    nrl.set_size(f, (8, 6))
            
    f.savefig(parentName +'RangeChange.png', dpi = 600, bbox_inches = 'tight', facecolor='w')
    
    
def plotSwoops(calibList):
    f, axarr = plt.subplots(4, 6)
    logNumber = 0
        
    for asic_id in range(0, 4):
        for channel in range(0, 6):
            for i in range(0, len(energies)):
                x = energies[i]
                y = calibList[i].centerLoc[logNumber]
                yerr = 100/calibList[i].rangeScale[logNumber]

                axarr[asic_id,channel].errorbar(x, y, yerr=yerr, label='both limits (default)')
                axarr[asic_id,channel].scatter(x, y, c=colorBank[i], s=50)
                
                
                # axarr[asic_id,channel].set_ylim([-1, 1])

                    
                if logNumber != 18:
                    axarr[asic_id,channel].get_xaxis().set_visible(False)
                    axarr[asic_id,channel].get_yaxis().set_visible(False)
                else:
                    axarr[asic_id,channel].tick_params(axis='both', which='major', labelsize=8)
                    axarr[asic_id,channel].tick_params(axis='both', which='minor', labelsize=8)

                    
            logNumber = logNumber + 1
                
    f.text(0.5, 0.04, 'Energy [keV]', ha='center')
    f.text(0.04, 0.5, 'Position Range [arb]', va='center', rotation='vertical')
    nrl.set_size(f, (8, 6))

    
    f.savefig(parentName +'swoop.png', dpi = 600, bbox_inches = 'tight', facecolor='w')
    
    

    
# %% Initialize variables

parentName = 'calibrations/'
energies = [511, 661.7, 1274.53]

# %% Load Data
calibList = []
for i in range(0, len(energies)):
    calibList.append(loadMasks(energies[i]))
    
# %% some Data processing

# plotPosRange(calibList)








    