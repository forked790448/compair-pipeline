#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Developed plots form the L2 create_test_metadata
 
"""
import pandas as pd
pd.options.mode.chained_assignment = None

import re
import os
import itertools
import warnings
warnings.simplefilter("ignore")


# plotting
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.pylab as pylab
import matplotlib as mpl
import matplotlib.ticker as mtick


# peak fitting
from scipy.optimize import curve_fit

# math tools
import math
import NRL_Math as nrl

#for GIF
from celluloid import Camera # getting the camera


class test_calibration:
    def __init__(self, parent_dir):
        self.parent_dir = parent_dir[0]
        self.energy_of_interest = 1273


        self.get_total_log()
        
        
        # global constants that might change
        self.FULL_LOG_LENGTH = 100 #100 mm, length of log
        self.MIN_ADC = 200
        self.MAX_ADC = 1400
       
        
        self.pos_edges_final = np.linspace(-1.6, 1.6, 100)*self.FULL_LOG_LENGTH/2

        

        # plot setting stuff
        plt.rcParams['font.size'] = 15
        plt.rcParams['font.family'] = "serif"
        tdir = 'in'
        major = 5.0
        minor = 3.0
        plt.rcParams['xtick.direction'] = tdir
        plt.rcParams['ytick.direction'] = tdir
        plt.rcParams['xtick.major.size'] = major
        plt.rcParams['xtick.minor.size'] = minor
        plt.rcParams['ytick.major.size'] = major
        plt.rcParams['ytick.minor.size'] = minor
        
        params = {'legend.fontsize': 'x-large',
                 'axes.labelsize': 'x-large',
                 'axes.titlesize':'x-large',
                 'xtick.labelsize':'small',
                 'ytick.labelsize':'small'}
        pylab.rcParams.update(params)
        
        
    def loadLogData(self, logNumber, min_adc_value, max_adc_value):
        self.log_df = pd.read_hdf(self.parent_dir + f'test_metadata/L{logNumber}.hdf5')
        self.log_df = self.log_df.dropna()
        self.log_df = self.log_df[self.log_df[f'L{logNumber}_adc'].between(min_adc_value, max_adc_value)]


        
    def get_total_log(self):
        files = os.listdir(self.parent_dir+"calibration_metadata/")
        self.total_logs = 0
        for f in files:
            nameOfFile = (int(re.findall(r'[0-9]+', f)[0]))
            
            if nameOfFile > self.total_logs:
                self.total_logs = nameOfFile
            
        self.total_logs = self.total_logs + 1


    def plotAllDepthEnergy(self):
        print('Plotting All Corrected Depth-Energy 2D Histograms')

        f, axarr = plt.subplots(4, 6)
        logNumber = 0
        
        for asic_id in range(0, 4):
            for channel in range(0, 6):

                self.loadLogData(logNumber, self.MIN_ADC, self.MAX_ADC)
                
                
                energyEdges = np.linspace(0, 1400, 100)
                pos_edges_tmp = np.linspace(-50, 50, 76)


                # axarr[asic_id,channel].hist2d(self.log_df[f'L{logNumber}_adc'], self.log_df[f'L{logNumber}_pos'], bins=(energyEdges, pos_edges_tmp), cmap=plt.cm.Greys)
                axarr[asic_id,channel].hist2d(self.log_df[f'L{logNumber}_adc'], self.log_df[f'L{logNumber}_pos'], bins=(energyEdges, pos_edges_tmp), norm=mpl.colors.LogNorm(), cmap=plt.cm.Greys)

                axarr[asic_id,channel].set_ylim([-50, 50])

                
                if logNumber != 18:
                    axarr[asic_id,channel].get_xaxis().set_visible(False)
                    axarr[asic_id,channel].get_yaxis().set_visible(False)
                else:
                    axarr[asic_id,channel].tick_params(axis='both', which='major', labelsize=8)
                    axarr[asic_id,channel].tick_params(axis='both', which='minor', labelsize=8)
                    
                    
                logNumber = logNumber + 1
                
        f.text(0.5, 0.04, 'Energy [keV]', ha='center')
        f.text(0.04, 0.5, 'Position [mm]', va='center', rotation='vertical')
        
        nrl.set_size(f, (8, 6))

        
        f.savefig(self.parent_dir +f'depthEnergyPlot_{self.energy_of_interest}TestMetaLog_expandedE.png', dpi = 600, bbox_inches = 'tight', facecolor='w')


    def plotAllSpectra(self):
        print('Plotting All Spectra')

        f, axarr = plt.subplots(4, 6)
        logNumber = 0
        
        for asic_id in range(0, 4):
            for channel in range(0, 6):

                self.loadLogData(logNumber, self.MIN_ADC, self.MAX_ADC)
                
                tmpidx = self.log_df[f'L{logNumber}_pos'].between(-50, 50) 

                maxEnergy = 1000
                energyEdges = np.linspace(50, maxEnergy, 100)


                axarr[asic_id,channel].hist(self.log_df[f'L{logNumber}_adc'][tmpidx], bins=energyEdges, color = "black", ec="black")
                # axarr[asic_id,channel].set_yscale('log')


                
                if logNumber != 18:
                    axarr[asic_id,channel].get_xaxis().set_visible(False)
                    axarr[asic_id,channel].get_yaxis().set_visible(False)
                else:
                    axarr[asic_id,channel].tick_params(axis='both', which='major', labelsize=8)
                    # axarr[asic_id,channel].tick_params(axis='both', which='minor', labelsize=8)
                    axarr[asic_id,channel].get_yaxis().set_visible(False)
                    axarr[asic_id,channel].set_xticks(np.linspace(0, maxEnergy, 3).tolist()) 
                    axarr[asic_id,channel].set_xticklabels(np.linspace(0, maxEnergy, 3).tolist(), fontsize=10)

                logNumber = logNumber + 1
                
        f.text(0.5, 0.04, 'Energy [keV]', ha='center')
        f.text(0.04, 0.5, 'Counts per 10 keV', va='center', rotation='vertical')
        
        nrl.set_size(f, (8, 6))

        
        f.savefig(self.parent_dir +f'spectraPlot_{self.energy_of_interest}TestMeta.png', dpi = 600, bbox_inches = 'tight', facecolor='w')
        
        
        
    def plotSpectraSum(self):
        print('Plotting The Sum of All Spectrum')
        
        minEnergy = 250
        maxEnergy = 1400
        energyEdges = np.linspace(minEnergy, maxEnergy, int(maxEnergy/10)+1)
        energyCenters = energyEdges[0:len(energyEdges)-1] + 0.5*(energyEdges[1]-energyEdges[0])
        
        histCounts = np.zeros(len(energyEdges)-1, )

        minLog = 0
        maxLog = 5
        for logNumber in range(minLog, maxLog):

            self.loadLogData(logNumber, self.MIN_ADC, self.MAX_ADC)
            tmpidx = self.log_df[f'L{logNumber}_pos'].between(-50, 50) 

            histTmp = np.histogram(self.log_df[f'L{logNumber}_adc'][tmpidx], bins=energyEdges)
            histCounts = histCounts + histTmp[0]
            
            

        fig = plt.figure()
        ax = fig.add_subplot(111)
        
        ax.plot(energyCenters, histCounts)
        
        ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))
        plt.xlabel('Energy (keV)')
        plt.ylabel('Counts per 10 keV')
        ax.set_yscale('log')

        fig.suptitle(f'Spectra of Logs: [{minLog}, {maxLog-1}]', fontsize=16)
        nrl.set_size(fig, (8, 6))

        fig.savefig(self.parent_dir +f'spectrumSumPlot_[{minLog},{maxLog}]TestMeta.png', dpi = 600, bbox_inches = 'tight', facecolor='w')
       
        
    def process(self):
        # self.plotAllDepthEnergy()
        # self.plotAllSpectra()
        self.plotSpectraSum()
        
        
        
        
        
            
        
def main(filename=None, **kargs):
    metaData = test_calibration(filename)
    metaData.process()
    
    
if __name__ == '__main__':
    
    import argparse    
    parser = argparse.ArgumentParser(
        description='Lookup some information in a database')
    parser.add_argument("filename", nargs="*")
    args = parser.parse_args()
    
    
    main(**vars(args))
