# Support python module to hold a bunch of math related functions
import pandas as pd
import numpy as np
import warnings
from scipy.signal import peak_widths
from tempfile import NamedTemporaryFile
from matplotlib.image import imread
from scipy.signal import find_peaks
from scipy.stats import moyal
from lmfit.models import ExponentialModel, GaussianModel
from scipy.optimize import curve_fit
warnings.simplefilter(action='ignore', category=pd.errors.PerformanceWarning)
from scipy import optimize
# Math operations
from scipy.ndimage.filters import uniform_filter1d




# plotting
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.pylab as pylab
import matplotlib as mpl


import heapq



def background_subtract(x,  yn):
    linY = [  yn[0], yn[len(yn)-1] ]
    linX = [  x[0], x[len(yn)-1] ]
    linFit = np.polyfit(linX, linY, 1)
    
    fitY = x*linFit[0] + linFit[1]
    yn = yn-fitY
    yn = yn - yn.min()
    
    return yn


def gaussfunc(x, a, x0, sigma):
    return a*np.exp(-(x-x0)**2/(2*sigma**2))

def findPeak(yn):
    try:
        peaks, _ = find_peaks(yn, height=0)
        maxIndx = np.argmax(yn[peaks])
        maxIndx = [peaks[maxIndx]]    
    except:
        maxIndx = [np.argmax(yn)]
        
    return maxIndx

# double gaussian fit

def gaussian(x, height, center, width, offset):
    return height*np.exp(-(x - center)**2/(2*width**2)) + offset
def three_gaussians(x, h1, c1, w1, h2, c2, w2, h3, c3, w3, offset):
    return (gaussian(x, h1, c1, w1, offset=0) +
        gaussian(x, h2, c2, w2, offset=0) +
        gaussian(x, h3, c3, w3, offset=0) + offset)
def two_gaussians(x, h1, c1, w1, h2, c2, w2, offset):
    return three_gaussians(x, h1, c1, w1, h2, c2, w2, 0,0,1, offset)

def double_gaussian_fit(x, y):
    peaks, propertiesdict = find_peaks(y, height=0.3*max(y), width = [1.5, 10], distance = 3)
    
    q = np.rint(np.array(heapq.nlargest(2, propertiesdict['prominences'])))
    w = np.rint(propertiesdict['prominences'])
    
    peaks = peaks[np.where(np.in1d(w, q))[0]]



    if len(peaks) < 2:
        peaks, _ = find_peaks(y, height=0.3*max(y), width = [3, 10], distance = 3)
        if len(peaks) < 2:
            return [0, 0, 0, 0, 0, 0, 0]
    
    upperLimit = x[max(peaks)]+0.2*x[max(peaks)]
    y = y[x<upperLimit]
    x = x[x<upperLimit]
    
    lowerLimit = x[min(peaks)]-0.08*x[min(peaks)]
    y = y[x>lowerLimit]
    x = x[x>lowerLimit]
    
    peaks, _ = find_peaks(y-min(y), height=0.3*max(y), width = [1, 10], distance = 3)


    try:
        guess2 = [y[peaks[0]], x[peaks[0]], 0.05*x[peaks[0]], y[peaks[1]], x[peaks[1]], 0.05*x[peaks[1]], min(y)]
    except:
        return [0, 0, 0, 0, 0, 0, 0]
    errfunc2 = lambda p, x, y: (two_gaussians(x, *p) - y)**2

    
    # Least squares fit. Starting values found by inspection.
    optim2, success = optimize.leastsq(errfunc2, guess2[:], args=(x, y))
    # plt.plot(x, y, lw=5, c='g', label='measurement')
    # plt.plot(x, two_gaussians(x, *optim2),
        # lw=1, c='r', ls='--', label='fit of 2 Gaussians')
    return (optim2)


# FWHM calculation Geometric
def lin_interp(x, y, i, half):
    """
    Linear interpolation for geometric FWHM calculation
    """
    return x[i] + (x[i+1] - x[i]) * ((half - y[i]) / (y[i+1] - y[i]))

def fwhmGeometric(x, y):
    """
    Takes in energy and counts and returns the FWHM in percent
    Calculates with linear interpolation
    """
    half = max(y)/2.0
    signs = np.sign(np.add(y, -half))
    zero_crossings = (signs[0:-2] != signs[1:-1])
    zero_crossings_i = np.where(zero_crossings)[0]
    fwhm = 100*(lin_interp(x, y, zero_crossings_i[1], half)-lin_interp(x, y, zero_crossings_i[0], half))/x[findPeak(y)]
    
    return fwhm


# other FWHM

def fwhmIndex(y): 
    peaks = findPeak(y)
            
    results_half = peak_widths(y, peaks, rel_height=0.5)
    results_half[0]  # widths

    fwhmValue = results_half[0][0]
    return fwhmValue


def widthIndex(y): 
    # peaks = [np.argmax(y)]  
    peaks = findPeak(y)

    results_full = peak_widths(y, peaks, rel_height=1)
    results_full[0]  # widths
    fwhmValue = results_full[0][0]
    
    return fwhmValue



def gaussFitWithExp(x, y, center, fwhmGuess):
    """
    Takes in a normalized distribution and returns gaussfit
    """
    y = y/max(y)

    exp_mod = ExponentialModel(prefix='exp_')
    pars = exp_mod.guess(y, x=x)
    
    gauss1 = GaussianModel(prefix='g1_')
    pars.update(gauss1.make_params())
    
    pars['g1_center'].set(value=center, min=center-fwhmGuess, max=center+fwhmGuess)
    pars['g1_sigma'].set(value=fwhmGuess/2.355)
    pars['g1_amplitude'].set(value=1)

    mod = gauss1 + exp_mod
    
    out = mod.fit(y, pars, x=x)
    
    return out


def landauFunc(x, a, mu, std):
    l = (x-mu)/std
    y = a*np.exp(-(l+np.exp(-l))/2)

    return y


def landauFit(x, y, data):
    """
    Takes in a distribution and returns characteristics of 
        Myoal distribution
        To do: make this cleaner so that you don not have to pass the entire dataset.
    """
    
    # pick off range
    
    peakLoc = findPeakOfDistribution(x,y)
    
    minIndex = np.argmin(y[0:peakLoc])
    maxIndex = len(y)
    data = data[data > x[np.argmax(y)]]
    data = data.to_numpy()/1000
    stdDev = np.std(data)
    
    
    param, param_cov = curve_fit(landauFunc, x[minIndex:maxIndex], y[minIndex:maxIndex], p0=[max(y), x[np.argmax(y)], stdDev])

    return param

def findPeakOfDistribution(xOrg,yOrg):
    """
    Returns index of the peak 
    """
    N = int(len(yOrg)/20)
    yAvg = uniform_filter1d(yOrg, size=N)
    yAvg = yAvg[::-1]
    y = yOrg[::-1]
    x = xOrg[::-1]

    
    ySub = yAvg[0:len(y):N]
    xSub = x[0:len(y):N]
    yAvgDiff = np.diff(ySub) + 1
    
    zero_crossings = np.where(np.diff(np.sign(yAvgDiff)))[0] 
    
    if len(zero_crossings) > 2:
        zero_crossings[0] = zero_crossings[-2] 
        

    return findNearestIndex(xOrg, xSub[zero_crossings[0]])


def findNearestIndex(a, a0):
    "Element in nd array `a` closest to the scalar value `a0`"
    idx = np.abs(a - a0).argmin()
    return idx
    

# figure operations
def get_size(fig, dpi=100):
    with NamedTemporaryFile(suffix='.png') as f:
        fig.savefig(f.name, bbox_inches='tight', dpi=dpi)
        height, width, _channels = imread(f.name).shape
        return width / dpi, height / dpi

def set_size(fig, size, dpi=100, eps=1e-2, give_up=2, min_size_px=10):
    target_width, target_height = size
    set_width, set_height = target_width, target_height # reasonable starting point
    deltas = [] # how far we have
    while True:
        fig.set_size_inches([set_width, set_height])
        actual_width, actual_height = get_size(fig, dpi=dpi)
        set_width *= target_width / actual_width
        set_height *= target_height / actual_height
        deltas.append(abs(actual_width - target_width) + abs(actual_height - target_height))
        if deltas[-1] < eps:
            return True
        if len(deltas) > give_up and sorted(deltas[-give_up:]) == deltas[-give_up:]:
            return False
        if set_width * dpi < min_size_px or set_height * dpi < min_size_px:
            return False