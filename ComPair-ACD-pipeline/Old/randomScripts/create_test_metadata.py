#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Develop energy and position metadata to test L2 Data

"""
import pandas as pd
pd.options.mode.chained_assignment = None

import re
from os.path import dirname

class CreateCalibrationMetadata:
    def __init__(self):
        pass

    def process(self, filename):
        self.read_files(filename[0])
        
        self.write_to_file()
         
        
    def read_files(self, filename):
        self.pps_adc = pd.read_hdf(filename,key='ADC')
        self.rawpath = dirname(filename)
        self.get_total_log()

        
        
    def write_to_file(self):
            
        for i in range(0, self.total_logs):
            tmp_df = self.pps_adc[[f'L{i}_adc', f'L{i}_pos']].copy()
            tmp_df = tmp_df[tmp_df[f'L{i}_adc'] >  50] #50 is the lower threashold to throw out events, just to save space
      
            store = pd.HDFStore(self.rawpath+f'/test_metadata/L{i}.hdf5')
            store.append('CAL', tmp_df, format='t',  data_columns=True)
            store.close()


    def get_total_log(self):
        name_of_last_column = self.pps_adc.columns[len(self.pps_adc.columns)-1]
        self.total_logs = (int(re.findall(r'[0-9]+', name_of_last_column)[0])+1) #n total channels in system
                

        
def main(filename=None, **kargs):
    metaData = CreateCalibrationMetadata()
    metaData.process(filename)

    
    
    
if __name__ == '__main__':
    
    import argparse    
    parser = argparse.ArgumentParser(
        description='Lookup some information in a database')
    parser.add_argument("filename", nargs="*")
    args = parser.parse_args()
    
    
    main(**vars(args))