#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Develop energy and position calibration
 - takes in ADC Data that has been pedestal subtracted
"""

import pandas as pd
import numpy as np
from pathlib import Path
import warnings
import matplotlib.pyplot as plt # dont need it after you finish plotting
import matplotlib as mpl
import re
import NRL_Math as nrl


warnings.simplefilter(action='ignore', category=pd.errors.PerformanceWarning)



class EnergyPositionCalibrationer:
    def __init__(self,rfname):
        self.rfname = rfname

        self.N_ASICS = 4 #4 asics in system
        self.N_CHANNELS = 16 #16 channels per asic

        self.N_LAYERS = 5;
        self.N_ROWS = 6 #6 logs/row per layer (on this design)

        self.dev_ASIC_channel_map()



        # this is just here to debugging reasons, will need to decide later
        #  if you want this as global constants
        self.MAX_ADC = 700
        self.MIN_ADC = 100

    def read_files(self):
        x = 1+1
        self.pps_adc = pd.read_hdf(self.rfname,key='ADC')

    def get_total_channels(self):
        name_of_last_column = self.pps_adc.columns[len(self.pps_adc.columns)-1]
        self.total_logs = (int(re.findall(r'[0-9]+', name_of_last_column)[0])+1)
        self.total_channels = self.total_logs*2 #n total channels in system

        # might as well calculate number of layers that are active
        self.N_LAYERS = (self.total_channels//self.N_ROWS)//2

    def plot_log_ADC_histogram(self, logIndex, energy, position):
          # log index is standard nomenclature L# log index

        j = self.ASIC_log_map[logIndex]

        f, axarr = plt.subplots(2,1)

        for i in range(2):
            axarr[i].hist(self.pps_adc[f'adc_{j[i]}'][self.pps_adc[f'adc_{j[i]}'] > -100], bins=1000, log=True, color = "r", ec="r")
            axarr[i].set_title(f'ADC Channel Index: {j[i]}')
            axarr[i].set_xlim([0, 1000])



        axarr[i].set_xlabel('Energy [ADC]')
        axarr[i].set_ylabel('Counts per Bin')
        f.suptitle(f'Energy Histogram of Each Channel for Log L{logIndex}')
        plt.tight_layout()
        nrl.set_size(f, (8, 6))


    def get_quarried_indexes(self, logIndex, energy, position):
        # get index of interest


        indexes = self.pps_adc[f'L{logIndex}_adc']==self.pps_adc[f'L{logIndex}_adc'] # xxx look up if there is a better way of doing this.

        if energy != []: # get indexs of energy bound
            indexes = indexes & self.pps_adc[f'L{logIndex}_adc'].between(energy[0], energy[1], inclusive=True)

        if position != []:
            indexes = indexes & self.pps_adc[f'L{logIndex}_pos'].between(position[0], position[1], inclusive=True)


        return indexes

    def plot_single_log_pos(self, logIndex, ROI_indexs):
        i = logIndex[0]
        j = logIndex[1]

        plt.hist(self.pps_adc[f'log_{i}_{j}_pos'][ROI_indexs], bins=100, log=False)
        plt.xlabel('Value [arb]')
        plt.ylabel('Counts per bin')
        plt.title(f'Position Histogram of Log [{i},{j}]')

    def plot_position_histograms(self):

        # ### below is the loop plotter
        f, axarr = plt.subplots(self.N_LAYERS, self.N_ROWS)

        m = 0
        for i in range(self.N_LAYERS):
            for j in range(self.N_ROWS):
                logIndex = [i,j]
                ROI_indexs = self.get_quarried_indexes(logIndex, [self.MIN_ADC, self.MAX_ADC], [-1,1])
                axarr[i,j].hist(self.pps_adc[f'log_{i}_{j}_pos'][ROI_indexs], bins=100, log=False)

                axarr[i,j].get_xaxis().set_visible(False)
                axarr[i,j].get_yaxis().set_visible(False)
                axarr[i,j].set_xlim([-1, 1])
                m = m + 1

        axarr[i,j].get_xaxis().set_visible(True)
        f.suptitle('Position Histograms')
        f.text(0.5, 0.04, 'Geometric Row Index', ha='center')
        f.text(0.04, 0.5, 'Geometric Layer Index', va='center', rotation='vertical')


    def plot_single_log_energy(self, logIndex, ROI_indexs):

        plt.hist(self.pps_adc[f'L{logIndex}_adc'][ROI_indexs], bins=1000, log=True)
        plt.xlabel('Value [ADC]')
        plt.ylabel('Counts per bin')
        plt.title(f'Energy Histogram of Log [{logIndex}]')



    def plot_energy_histograms(self):
        # only looks at energy range between MIN_ADC and MAX_ADC

        # ### below is the loop plotter
        f, axarr = plt.subplots(self.N_LAYERS, self.N_ROWS)

        m = 0
        for i in range(self.N_LAYERS):
            for j in range(self.N_ROWS):
                axarr[i,j].hist(self.pps_adc[f'log_{i}_{j}_adc'][self.pps_adc[f'log_{i}_{j}_adc'].between(self.MIN_ADC, self.MAX_ADC, inclusive=False)], bins=500, log=True)
                axarr[i,j].get_xaxis().set_visible(False)
                axarr[i,j].get_yaxis().set_visible(False)
                m = m + 1

        f.text(0.5, 0.04, 'Value [ADC]', ha='center')
        f.text(0.04, 0.5, 'LOG(Counts per bin)', va='center', rotation='vertical')

    def plot_2D_Histogram(self, i, j):
        
        qmin = 10
        qmax = 400
        x = self.pps_adc[f'adc_{i}'][self.pps_adc[f'adc_{j}'].between(qmin, qmax, inclusive=False) & self.pps_adc[f'adc_{i}'].between(qmin, qmax, inclusive=False)].tolist()
        y = self.pps_adc[f'adc_{j}'][self.pps_adc[f'adc_{j}'].between(qmin, qmax, inclusive=False) & self.pps_adc[f'adc_{i}'].between(qmin, qmax, inclusive=False)].tolist()
        

        # x = self.pps_adc[f'adc_{i}'].tolist()
        # y = self.pps_adc[f'adc_{j}'].tolist()
        fig, ax = plt.subplots(figsize=(8,6))

        plt.hist2d(x,  y, bins=(50, 50), norm=mpl.colors.LogNorm(), cmap=mpl.cm.gray)

        plt.xlabel(f'Value [ADC] of ADC Channel {i}')
        plt.ylabel(f'Value [ADC] of ADC Channel {j}')
        plt.title(f'2D Histogram of Channels {i} and {j}')
        
        ax.set_xlim(qmin, qmax)
        ax.set_ylim(qmin, qmax)
        plt.colorbar()
        nrl.set_size(fig, (8, 6))



    def plot_ADC_histograms(self):
        f, axarr = plt.subplots(self.N_LAYERS, 2*self.N_ROWS)

        m = 0
        for i in range(self.N_LAYERS):
            for j in range(2*self.N_ROWS):
                axarr[i,j].hist(self.pps_adc[f'adc_{m}'][self.pps_adc[f'adc_{m}'].between(50, 1000, inclusive=False)], bins=500, log=True)
                axarr[i,j].get_xaxis().set_visible(False)
                axarr[i,j].get_yaxis().set_visible(False)
                m = m + 1

        f.text(0.5, 0.04, 'Value [ADC]', ha='center')
        f.text(0.04, 0.5, 'Counts per bin', va='center', rotation='vertical')

        # i = 46 #XXX
        # j = 1
        # plt.hist(self.pps_adc[f'adc_{i}'][self.pps_adc[f'adc_{i}'].between(0, 2000, inclusive=False)], bins=1000, log=True)



    #write edited dataframe to new hdf files
    def write_files(self):
        #drop timestamp and sortcol columns
        columns = list(self.rosspad_df.columns)
        columns.remove('timestamp')
        columns.remove('sortcol')
        self.rosspad_df = self.rosspad_df[columns]


        #write to file
        path = Path(self.rfname)
        hdfpath = path.with_suffix('.L1.1.hdf5')
        with pd.HDFStore(hdfpath) as hdf:
            self.rosspad_df.to_hdf(hdf,key='ADC')
        print("File written:")
        print(hdfpath)



    def print_col_names(self):
        for col in self.pps_adc.columns:
            print(col)

    def dev_ASIC_channel_map(self):

        #make an [ASIC, channel] to index map
        self.ASIC_2_index = np.arange(self.N_ASICS*self.N_CHANNELS).reshape(self.N_ASICS,self.N_CHANNELS)

        # make index to [ASIC,channel] mapping
        self.index_2_ASIC = []
        for i in range(self.N_ASICS*self.N_CHANNELS):
            [i,j] = np.where(self.ASIC_2_index == i)
            self.index_2_ASIC.append([i[0], j[0]])



        self.ASIC_log_map = []
        for i in range(self.N_LAYERS*self.N_ROWS):
            j = i*2
            self.ASIC_log_map.append([j, j+1])

        #  dont really need this xxxxxxxxxxxxxxxxxxx
        self.logIndex_GeoCoordinate = []
        m = 0
        for i in range(self.N_LAYERS):
            for j in range(2*self.N_ROWS):
                self.logIndex_GeoCoordinate.append([i,j])



    def process(self):
        self.read_files()
        self.get_total_channels()

        self.plot_ADC_histograms()
        self.plot_energy_histograms()
        self.plot_position_histograms()


        self.plot_2D_Histogram(8,9)


        # energy = [10, 1000]
        # position = [-0.2, 0.2]
        # logIndex = 19
        # ROI_indexs = self.get_quarried_indexes(logIndex, energy, position)
        # self.plot_single_log_energy(logIndex, ROI_indexs)


        # energy = [100, 200]
        # position = [-1, 1]
        # logIndex = [0,5]
        # ROI_indexs = self.get_quarried_indexes(logIndex, energy, position)
        # self.plot_single_log_pos(logIndex, ROI_indexs)

        energy = [-100, 1000]
        position = [-1,1]
        logIndex = 4
        self.plot_log_ADC_histogram(logIndex, energy, position)
        # self.write_files()




if __name__ == '__main__':

    # import argparse
    # parser = argparse.ArgumentParser()
    # parser.add_argument("rfname",help="rosspad hdf5 file")
    # parser.add_argument("afname",nargs='+',help="arduino log file")
    # args = parser.parse_args()

    # R = RosspadToL1_1(args.rfname,args.afname)
    # R.process()

    # rfname = "Measurments/debugging/centerLog00CollimatedMeasurment.hdf5"
    # rfname = "Measurments/debugging/topLog00Collimated.hdf5"
    rfname = "measurments/Na22FloodBottom/SplitData00.hdf5"




    # print(afname)

    R = EnergyPositionCalibrationer(rfname)
    R.process()





