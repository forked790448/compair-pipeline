#!/usr/bin/env python3
   
import pandas as pd
import numpy as np
import itertools
from pathlib import Path
import os
import struct
import math
from matplotlib import pyplot as plt 



class RossPad:
    def __init__(self):
        """
        """
        
        # Other Global Variables
        self.chunk_size = 99999
        self.N_logs = 30
        self.chunk_index = 0
        
        
        # Data Frame header names
        self.binary_column_names = ['frame', 'timestamp', 'gpio']
        tmp = []
        for i in range(0, 8):
            for j in range (0, 8):
                tmp.append([f'pixel({i},{j})'])
        self.binary_column_names += list(itertools.chain(*tmp))
        
        
        self.out_column_names = ['frame', 'timestamp', 'gpio']
        tmp = [[f'L{num}_adc', f'L{num}_pos'] for num in range(self.N_logs)]
        self.out_column_names += list(itertools.chain(*tmp))
        
        self.gpio_drop = ['frame']
        self.gpio_drop += list(itertools.chain(*tmp))
        
        
        # Variables for file readings
        self.is_end_of_file = False
        self.is_end_of_chunk = False
        self.log_length = 0
        
        
        self.prev_frame = 0
        
        # Other initializations
        self.load_maps()

    def read_file(self, name):
        with open(name, "rb") as self.f:
            while self.is_end_of_file == False:
                self.read_chunk()
                self.process_chunk()
                self.process_gpio()
                self.save_chunk(Path(name))
                
            
    def read_chunk(self):     
        tmpArray = np.zeros((self.chunk_size, len(self.binary_column_names)))
        
        lineCounter = 0
        self.is_end_of_chunk=False
        while self.is_end_of_chunk==False and self.is_end_of_file == False:
            try:
                self.f.seek(self.log_length)
            except :
                self.is_end_of_file = True
                break                

            try:
                log_length_t = struct.unpack(">H", self.f.read(2))
                self.log_length = self.log_length + log_length_t[0]
            except :
                self.is_end_of_file = True
                break
            
            # bunch of number you don't need
            self.read_byte(self.f,self.log_length)
            self.read_byte(self.f,self.log_length)
            
            # the ROSSPAD designated index (zeros every XXX events)
            frameCount = self.read_word(self.f,self.log_length)

            # more bunch of number you don't need
            self.read_byte(self.f,self.log_length)
            self.read_byte(self.f,self.log_length)
            self.read_byte(self.f,self.log_length)
            self.read_byte(self.f,self.log_length)
            ts_clk = self.read_dword(self.f,self.log_length)
            self.read_word(self.f,self.log_length)
            self.read_word(self.f,self.log_length)
            self.read_word(self.f,self.log_length)

            user_defined     = self.read_dword(self.f,self.log_length)
            self.read_dword(self.f,self.log_length)
            
            # fill up dataframe
            tmpArray[lineCounter, 0] = frameCount
            tmpArray[lineCounter, 1] = ts_clk
            tmpArray[lineCounter, 2] = user_defined

            
            counter = 3
            for i in range(0, 8):
                for j in range (0, 8):
                    tmpArray[lineCounter, counter] = self.read_word(self.f,self.log_length)
                    counter = counter + 1


            # lets do only centain number of events, then print to file
            lineCounter = lineCounter + 1
            if lineCounter == self.chunk_size:
                self.is_end_of_chunk = True
                
        self.bin_chunk = pd.DataFrame(tmpArray, columns=self.binary_column_names)
        
        # if you get to the end of the file, throw away empty bins
        if self.is_end_of_file == True:
            numberToDrop = lineCounter - self.chunk_size
            self.bin_chunk = self.bin_chunk[:numberToDrop]
                
        del tmpArray

                
    def process_chunk(self):
        self.save_chunk_df = pd.DataFrame(0, index=np.arange(len(self.bin_chunk)), columns=self.out_column_names)
        
        for logNumber in range(0,self.N_logs):
            SiPM_channelA, SiPM_channelB = self.get_channel(logNumber)
            
            pixel1_x, pixel1_y, pixel2_x, pixel2_y = self.get_pixel_channel(SiPM_channelA, SiPM_channelB)
    
            
            
            # Baseline/Pedestal Subtract
            # Ideas for some stupid reason has coordinates (y,x)
            self.bin_chunk[f'pixel({pixel1_y},{pixel1_x})'] = self.bin_chunk[f'pixel({pixel1_y},{pixel1_x})'] - self.baselines['ADC value'].loc[SiPM_channelA]
            self.bin_chunk[f'pixel({pixel2_y},{pixel2_x})'] = self.bin_chunk[f'pixel({pixel2_y},{pixel2_x})'] - self.baselines['ADC value'].loc[SiPM_channelB]

            # Set Values less than zero to zero
            self.bin_chunk[f'pixel({pixel1_y},{pixel1_x})'].loc[self.bin_chunk[f'pixel({pixel1_y},{pixel1_x})'] < 0] = 0
            self.bin_chunk[f'pixel({pixel2_y},{pixel2_x})'].loc[self.bin_chunk[f'pixel({pixel2_y},{pixel2_x})'] < 0] = 0
    
            
            
            # Calculate Energy
            self.save_chunk_df[f'L{logNumber}_adc'] = self.bin_chunk[f'pixel({pixel1_y},{pixel1_x})'] * self.bin_chunk[f'pixel({pixel2_y},{pixel2_x})']
            self.save_chunk_df[f'L{logNumber}_adc'] = np.sqrt(self.save_chunk_df[f'L{logNumber}_adc'])
            
            
            #Calculate Position
            self.save_chunk_df[f'L{logNumber}_pos'] = (self.bin_chunk[f'pixel({pixel1_y},{pixel1_x})'] - self.bin_chunk[f'pixel({pixel2_y},{pixel2_x})'])/ \
                (self.bin_chunk[f'pixel({pixel1_y},{pixel1_x})'] + self.bin_chunk[f'pixel({pixel2_y},{pixel2_x})'])
                
                
            #save sipm adc values
            # self.save_chunk_df[f'sipm_{SiPM_channelA}'] = self.bin_chunk[f'pixel({pixel1_y},{pixel1_x})']
            # self.save_chunk_df[f'sipm_{SiPM_channelB}'] = self.bin_chunk[f'pixel({pixel2_y},{pixel2_x})']


                
        
        
        rollIdx = self.bin_chunk['frame'][(self.bin_chunk['frame'].diff() < 0)].index
        for i in range(0, rollIdx.shape[0]):
            self.bin_chunk.loc[ rollIdx[i]:self.bin_chunk.index.max(), 'frame'] = self.bin_chunk['frame'].loc[ rollIdx[i]:self.bin_chunk.index.max()] + 2**16
            

        
        self.save_chunk_df['frame'] = self.bin_chunk['frame'] + self.prev_frame
        self.prev_frame = self.save_chunk_df['frame'].max()

        self.save_chunk_df['timestamp'] = self.bin_chunk['timestamp'].astype(int)
        self.save_chunk_df['gpio'] = self.bin_chunk['gpio']
        del self.bin_chunk
                
                
    def save_chunk(self, raw):
        hdfpath = raw.with_suffix('.hdf5')
        self.save_chunk_df['timestamp'] = self.save_chunk_df['timestamp'].astype(int)

        
        if self.chunk_index == 0:
            try:
                os.remove(hdfpath) 
            except:
                pass

            
            with pd.HDFStore(hdfpath) as self.hdf:
                self.save_chunk_df.to_hdf(self.hdf, 'ADC', append=True, mode='r+')

        else:
            self.save_chunk_df.index = self.save_chunk_df.index + self.chunk_index*self.chunk_size
            self.save_chunk_df.to_hdf(self.hdf, 'ADC', append=True, mode='r+')

            
        ## housekeeping after saving, just to make sure
        self.chunk_index = self.chunk_index + 1
        del self.save_chunk_df


    def process_gpio(self):       
        # #pick apart the header to get the GPIO data
        self.save_chunk_df['gpio'] = self.save_chunk_df['gpio'].astype(int)
        self.save_chunk_df.insert(len(self.save_chunk_df.columns), "pps", (self.save_chunk_df['gpio']) == 1)
        self.save_chunk_df.insert(len(self.save_chunk_df.columns), "trigack", (self.save_chunk_df['gpio']) == 2)
   
    

              
    def get_channel(self, logNumber):
         self.channel_log_map[self.channel_log_map['Log'] == logNumber]
         return self.channel_log_map['ChannelA'][self.channel_log_map['Log'] == logNumber].values[0], self.channel_log_map['ChannelB'][self.channel_log_map['Log'] == logNumber].values[0]
        
        
    def get_pixel_channel(self, channelA_sipm, channelB_sipm):
        asicA = self.sipm_rosspad_map['ASIC'][self.sipm_rosspad_map['SIPM Channel'] == channelA_sipm].values[0]
        channelA = self.sipm_rosspad_map['Channel'][self.sipm_rosspad_map['SIPM Channel'] == channelA_sipm].values[0]
        
        asicB = self.sipm_rosspad_map['ASIC'][self.sipm_rosspad_map['SIPM Channel'] == channelB_sipm].values[0]
        channelB = self.sipm_rosspad_map['Channel'][self.sipm_rosspad_map['SIPM Channel'] == channelB_sipm].values[0]

        
        try:
            pixel1_x = self.channel_pixel_map['X'][(self.channel_pixel_map['ASIC'] == asicA) & (self.channel_pixel_map['Channel'] == channelA)].values[0]
            pixel1_y = self.channel_pixel_map['Y'][(self.channel_pixel_map['ASIC'] == asicA) & (self.channel_pixel_map['Channel'] == channelA)].values[0]
                    
            pixel2_x = self.channel_pixel_map['X'][(self.channel_pixel_map['ASIC'] == asicB) & (self.channel_pixel_map['Channel'] == channelB)].values[0]
            pixel2_y = self.channel_pixel_map['Y'][(self.channel_pixel_map['ASIC'] == asicB) & (self.channel_pixel_map['Channel'] == channelB)].values[0]
        except:
            print('welp')
        
        return pixel1_x, pixel1_y, pixel2_x, pixel2_y
        
    def load_maps(self):
        # SiPM Channel to ROSSPAD map
        self.sipm_rosspad_map = pd.read_csv('rosspadConfigData/sipm_rosspad_map_old.csv', sep=',')

        # ASIC+Channel mapping to ROSSPAD pixel mapping
        # ASIC;Channel;X;Row
        self.channel_pixel_map = pd.read_csv('rosspadConfigData/ROSSPAD-PixelMap-Module.csv', sep=';')
        
        
        # Baseline Values       
        self.baselines = pd.read_csv('rosspadConfigData/Baseline.csv', sep=',')
        
        # SiPM Channel to ROSSPAD map
        self.channel_log_map = pd.read_csv('rosspadConfigData/log_channel_map.csv', sep=',')
        
        
        
        

        
    # ------ Reading BINARY FUNCTIONS ------
    def read_byte(self, file_rd,log_pos):
        byte_rd = 0
        
        try:
            byte_rd = (struct.unpack("B", file_rd.read(1)))[0]
        except :
            self.is_end_of_file = True
        return byte_rd
    
    def read_word(self, file_rd,log_pos):
        word_rd = 0
        
        try:
            word_rd = (struct.unpack(">H", file_rd.read(2)))[0]
        except :
            self.is_end_of_file = True
    
        return word_rd
    
    def read_dword(self, file_rd,log_pos):
        dword_rd = 0
        
        try:
            dword_rd = (struct.unpack(">I", file_rd.read(4)))[0]
        except :
          #  print ("EOF = " + str(log_pos))
            self.is_end_of_file = 1
        return dword_rd


def main(filename=None, **kargs):
    r = RossPad()
    for f in filename:
        r.read_file(f)

if __name__ == '__main__':
    
    import argparse    
    parser = argparse.ArgumentParser(
        description='Lookup some information in a database')
    parser.add_argument("filename", nargs="*")
    args = parser.parse_args()
    main(**vars(args))
