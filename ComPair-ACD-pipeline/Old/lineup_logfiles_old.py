import pandas
import numpy as np

def get_bounds(isRosspad=True,frequency=100e-6):
	if isRosspad:
		frequency *= 100e6
	else:
		frequency *= 42e6

	lower_bound = frequency - 0.005*frequency
	upper_bound = frequency + 0.005*frequency
	return lower_bound, upper_bound


def read_df_from_file(rfname,afname,key):

	rosspad_df_i = pandas.read_hdf(rfname,key=key, columns=['timestamp', 'pps', 'trigack'], chunksize=99999)
	for rosspad_df in rosspad_df_i:
		if 1 == 1:
			break
    # chunks = pd.read_hdf(self.rfname, key='ADC', chunksize=self.chunk_size)

	arduino_df = pandas.read_csv(afname,names=["ID","Clock","UTCA","UTCC"])
	return rosspad_df, arduino_df

def lineup(rfname,afname,key='ADC'):

	rosspad_df = None
	arduino_df = None
	#if passed dataframes
	if key == None:
		rosspad_df = rfname
		arduino_df = afname
	else:
		rosspad_df, arduino_df = read_df_from_file(rfname,afname,key)

	rosspad_df.index = np.arange(len(rosspad_df)) #DO THIS TO DEAL WITH MISSING ASIC3 LINES
	rosspad_df.index.rename("index_orig",inplace=True)
	arduino_df.index.rename("index_orig",inplace=True)

	#get lines corresponding to trigIn
	try:
		rosspad_df_trigIn = rosspad_df[(rosspad_df.pps == False) & (rosspad_df.trigack == True)]
	except AttributeError:
		rosspad_df_trigIn = rosspad_df[(rosspad_df.pps == False) & (rosspad_df.trig == True)]

	rosspad_df_trigIn = rosspad_df_trigIn.reset_index()
	arduino_df_trigIn = arduino_df[arduino_df.ID == "C"]
	arduino_df_trigIn = arduino_df_trigIn.reset_index()

	#search for series of 10 pulses 100 us apart
	#arduino *could* drop a signal (rare), or have rosspad triggers in between
	# (more commmon with more logs going)
	nPulses = 10
	lower_bound, upper_bound = get_bounds(False)
#	print(lower_bound,upper_bound)

	arduino_timestamps = arduino_df_trigIn.Clock.to_numpy()
	arduino_clock_diff = np.diff(arduino_timestamps)
	arduino_start_index = -1


	#first assume nothing is dropped
	for i in range(len(arduino_clock_diff)-nPulses-1):
		sequence = arduino_clock_diff[i:i+nPulses-1]
		if (sequence > lower_bound).all() and (sequence < upper_bound).all():
			arduino_start_index = int(arduino_df_trigIn.loc[arduino_df_trigIn.index == i].index_orig)
			break

	#if only one is dropped, there will at least be a sequence of 5
	#probably a sequence of 3 is the shortest we should look for, since 2 in a row more likely to happen unrelated to lineup
	#so start at nPulses = 5, search until 3 
	nPulses = 5
	while arduino_start_index == -1 and nPulses >= 3:
		for i in range(len(arduino_clock_diff)-nPulses-1):
			sequence = arduino_clock_diff[i:i+nPulses-1]
			if (sequence > lower_bound).all() and (sequence < upper_bound).all():
				arduino_start_index = int(arduino_df_trigIn.loc[arduino_df_trigIn.index == i].index_orig)
				break
		nPulses -= 1

	#we actually want to return the index of the last PPS, not the first line-up signal,
	# since that's what the comparison code expects
	foundPPS = False
	while not foundPPS:
		if arduino_df.loc[arduino_df.index == arduino_start_index].ID.values[0] == "P":
			foundPPS = True
		else:
			arduino_start_index += 1






	#rosspad complicated, since it could drop triggers
	lower_bound, upper_bound = get_bounds(True)
#	print(lower_bound,upper_bound)

	rosspad_timestamps = rosspad_df_trigIn.timestamp.to_numpy()
	rosspad_clock_diff = np.diff(rosspad_timestamps)
	rosspad_start_index = -1

	#first do it the arduino way, assuming nothing is dropped
	for i in range(len(rosspad_clock_diff)-nPulses-1):
		sequence = rosspad_clock_diff[i:i+nPulses-1]
		if (sequence > lower_bound).all() and (sequence < upper_bound).all():
			rosspad_start_index = int(rosspad_df_trigIn.loc[rosspad_df_trigIn.index == i].index_orig)
			break

	#if only one is dropped, there will at least be a sequence of 5
	#probably a sequence of 3 is the shortest we should look for, since 2 in a row more likely to happen unrelated to lineup
	#so start at nPulses = 5, search until 3 
	nPulses = 5
	while rosspad_start_index == -1 and nPulses >= 3:
		for i in range(len(rosspad_clock_diff)-nPulses-1):
			sequence = rosspad_clock_diff[i:i+nPulses-1]
			if (sequence > lower_bound).all() and (sequence < upper_bound).all():
				rosspad_start_index = int(rosspad_df_trigIn.loc[rosspad_df_trigIn.index == i].index_orig)
				break
		nPulses -= 1

	#what if it drops every other pulse? unlikely, but, could repeat search with frequency of 200 us
	if rosspad_start_index == -1:
		nPulses = 5
		lower_bound, upper_bound = get_bounds(True,200e-6)
	while rosspad_start_index == -1 and nPulses >= 3:
		for i in range(len(rosspad_clock_diff)-nPulses-1):
			sequence = rosspad_clock_diff[i:i+nPulses-1]
			if (sequence > lower_bound).all() and (sequence < upper_bound).all():
				rosspad_start_index = int(rosspad_df_trigIn.loc[rosspad_df_trigIn.index == i].index_orig)
				break
		nPulses -= 1

#	print(rosspad_df.trig.to_numpy()[rosspad_start_index-1:rosspad_start_index+10])
#	print(rosspad_df.timestamp.to_numpy()[rosspad_start_index-1:rosspad_start_index+10])

	#again, find index of first PPS
	foundPPS = False
	while not foundPPS:
		if rosspad_df.loc[rosspad_df.index == rosspad_start_index].pps.values[0] == True:
			foundPPS = True
		else:
			rosspad_start_index += 1


	return rosspad_start_index, arduino_start_index



if __name__ == '__main__':

	import argparse
	parser = argparse.ArgumentParser()
	parser.add_argument("rfname",help="rosspad hdf5 file")
	parser.add_argument("afname",help="arduino log file")
	args = parser.parse_args()

	rosspad_start, arduino_start = lineup(args.rfname,args.afname,key='ADC')

	print("arduino start:",arduino_start)
	print("rosspad start:",rosspad_start)

