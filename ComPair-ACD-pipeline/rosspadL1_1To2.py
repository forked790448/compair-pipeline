#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Develop L2 Level Data
 - takes either L1 or L1,1 data and applies an energy and position calibration
 - loads 'calibrations/calibrationData/*.txt' calibration masks
     for each event
 - I break it up because my computer cannot handle
     large datafile. So I break it up by log, to later process each log 
     at a later time.
"""

import os
import os.path
import numpy as np
import h5py as h5
from pathlib import Path


class RosspadL1_1To2:
    def __init__(self,rfname,calname):
        self.rfname = rfname
        self.loadCalibrationData(calname)
        
        
    def loadCalibrationData(self,filename):
        # print('Loading Calibration Data: ' + filename)
        
        #filename = 'rosspadConfigData/calibration/calibrationData'
        #filename = 'CsI Calibration/calibrationData'
        #filename = 'rosspadConfigData/calibration/ACD_Calib_05122023.h5'    
    
        with h5.File(filename, "r") as f:
            self.panel_numbers = f['Panel Numbers'][...]
            self.pedestals = f['Pedestal'][...] # Not used
            self.threshold = f['Threshold'][...]
            self.overbin = f['Overbin'][...]
            self.calib_params = f['Calibration Parameters'][...]
         

    def process(self):
        
        outfname = self.rfname[:-8] + '_ACD.L2.h5'

        path = Path(outfname)
        
        self.hpath = path #path.with_suffix('').with_suffix('').with_suffix('.L2.h5')
        
        try:
            os.remove(str(self.hpath))
        except:
            pass 
        
        with h5.File(self.rfname,'r') as f_in:
            self.pps_data = f_in['pps'][...]
            self.trigack_data = f_in['trigack'][...]
            self.frame_data = f_in['frame'][...]
            self.eventID_data = f_in['EventID'][...]
            self.UTC_data = f_in['UTC'][...]
            self.adc_data = f_in['ADC'][...]
            try:
                self.bad_eids = f_in['badEids'][...]
                self.bad_flag = True
            except:
                self.bad_flag = False
        
            self.calibrate_energy()
            self.calibrate_position()
            self.write_files()


    def calibrate_energy(self):
    
        [nevents, n_sipms] = np.shape(self.adc_data)
        
        self.e_data = np.zeros([nevents, n_sipms])
        
        # panel numbers from calibration file aren't necessarily in order, but panel numbers from L1_1 file are
        for sipm_idx in range(n_sipms):
            
            n_panel = sipm_idx//2
            ch1_or_ch2 = sipm_idx % 2 # 0 means ch1, 1 means ch2
            
            calib_index_base = np.where(self.panel_numbers == n_panel)[0] * 2 # selects the index in the calibration files corresponding to the panel in the L1_1 file

            if len(calib_index_base) > 0:
                calib_idx = calib_index_base + ch1_or_ch2
            
                sipm_threshold = self.threshold[calib_idx] # this is in keV
                sipm_overbin = self.overbin[calib_idx] # this is in ADC value
                sipm_calib_params = self.calib_params[calib_idx][0]
                
                over_overbin = self.adc_data[:,sipm_idx] > sipm_overbin
            
                self.adc_data[over_overbin, sipm_idx] = sipm_overbin
                
                self.e_data[:,sipm_idx] = sipm_calib_params[0] * self.adc_data[:,sipm_idx] + sipm_calib_params[1]
                
                under_threshold = self.e_data[:,sipm_idx] < sipm_threshold
                self.e_data[under_threshold, sipm_idx] = 0
        
        e_data_temp = np.zeros([nevents,int(n_sipms/2)])
        
        for i in range(int(n_sipms/2)):
            only_even = np.logical_and(self.e_data[:,2*i] > 0, self.e_data[:,2*i + 1] == 0)
            only_odd = np.logical_and(self.e_data[:,2*i] == 0, self.e_data[:,2*i + 1] > 0)
            
            e_data_temp[:,i] = (self.e_data[:,2*i] + self.e_data[:,2*i + 1])/2
            e_data_temp[only_even,i] = self.e_data[only_even,2*i]
            e_data_temp[only_odd,i] = self.e_data[only_odd,2*i+1]
        
        self.e_data = e_data_temp
        
    def calibrate_position(self):
        
        
        panel_positions = [[23.25000, 13.50000,   0.81680],
                           [-8.50000,  -12.75000,   0.81680],
                           [-42.75000, 11.00000,   0.81680],
                           [-11.00000, 37.25000,   0.81680],
                           [-9.60000, 12.10000,  20.56680]]
        
        nevents, n_panels = np.shape(self.e_data)
        
        self.pos_data = np.zeros([nevents,n_panels,3])
        
        for i_panel in range(n_panels):
            panel_hit = self.e_data[:,i_panel] > 0
            self.pos_data[panel_hit,i_panel,:] = panel_positions[i_panel]
        
        
    #write edited dataframe to new hdf files
    def write_files(self):
        #write to file
        
        with h5.File(self.hpath,'w') as f_out:
            f_out.create_dataset('pps',data = self.pps_data)
            f_out.create_dataset('trigack',data = self.trigack_data)
            f_out.create_dataset('frame',data = self.frame_data)
            f_out.create_dataset('EventID',data = self.eventID_data)
            f_out.create_dataset('UTC',data = self.UTC_data)
            f_out.create_dataset('Energy',data = self.e_data)
            f_out.create_dataset('Position',data = self.pos_data)
            if self.bad_flag:
                f_out.create_dataset('badEids',data = self.bad_eids)
        



if __name__ == '__main__':

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("rfname",help="rosspad hdf5 file")
    parser.add_argument("calname", help = "ACD calibration file")
    args = parser.parse_args()

    # print(args.rfname)
    R = RosspadL1_1To2(args.rfname, args.calname)
    R.process()

