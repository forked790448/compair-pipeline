import h5py as h5
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os.path
import pandas as pd

rosspad_freq = 99999908.0 # formerly 100e6 #supposed rosspad clock frequency
TM_freq = TM_freq = 49999072.36363637 # formerly 50e6

class LineupFailed(Exception):
    def __init__(self,msg="ROSSPAD and Arduino lineup failed", *args, **kwargs):
        super().__init__(msg, *args, **kwargs)

class DroppedPPS(Exception):
    def __init__(self,msg="Dropped PPS: deal with this", *args, **kwargs):
        super().__init__(msg, *args, **kwargs)

class SortError(Exception):
    def __init__(self,msg="Sort failed: timestamps out of order", *args, **kwargs):
        super().__init__(msg, *args, **kwargs)

# mostly copy from lineup_logfiles.lineup()

# new lineup_logfiles.py


import pandas
import numpy as np
import h5py as h5

def get_bounds(isRosspad=True,frequency=100e-6):
    if isRosspad:
        frequency *= 100e6
    else:
        frequency *= 42e6

    lower_bound = frequency - 0.005*frequency
    upper_bound = frequency + 0.005*frequency
    return lower_bound, upper_bound


def read_data_from_file(rfname,key):

    rosspad_file = h5.File(rfname)
    rosspad_data = rosspad_file['ADC'][...]
    rosspad_keys = rosspad_file['column_names'][...]
    rosspad_file.close()

    tmp = []
    for key in rosspad_keys:
        dkey = key.decode('utf8')
        tmp.append(dkey)
    rosspad_keys = np.array(tmp)

    return np.array(rosspad_data), rosspad_keys

def lineup(rfname,key='ADC'):

    rosspad_data = None
    #if passed dataframes
    if key == None:
        rosspad_data = rfname
    else:
        rosspad_data, rosspad_keys = read_data_from_file(rfname,key)

    #rosspad_df.index = np.arange(len(rosspad_df)) #DO THIS TO DEAL WITH MISSING ASIC3 LINES
    #rosspad_df.index.rename("index_orig",inplace=True)
    #arduino_df.index.rename("index_orig",inplace=True)

    # rosspad indices
    pps_idx = np.where(rosspad_keys == 'pps')[0][0]
    trigack_idx = np.where(rosspad_keys == 'trigack')[0][0]
    timestamp_idx = np.where(rosspad_keys == 'timestamp')[0][0]

    #get lines corresponding to trigIn
    rosspad_trigIn = rosspad_data[(rosspad_data[:,pps_idx] == False) & (rosspad_data[:,trigack_idx] == True), :]
    rosspad_orig_rows = np.array(np.arange(len(rosspad_data)))[(rosspad_data[:,pps_idx] == False) & (rosspad_data[:,trigack_idx] == True)]
    

    #rosspad complicated, since it could drop triggers
    lower_bound, upper_bound = get_bounds(True)
#    print(lower_bound,upper_bound)

    rosspad_timestamps = rosspad_trigIn[:,timestamp_idx]
    rosspad_clock_diff = np.diff(rosspad_timestamps)
    rosspad_start_index = -1
    
    nPulses = 10
    #first do it the arduino way, assuming nothing is dropped
    for i in range(len(rosspad_clock_diff)-nPulses-1):
        sequence = rosspad_clock_diff[i:i+nPulses-1]
        if (sequence > lower_bound).all() and (sequence < upper_bound).all():
            rosspad_start_index = rosspad_orig_rows[i]
            break

    #if only one is dropped, there will at least be a sequence of 5
    #probably a sequence of 3 is the shortest we should look for, since 2 in a row more likely to happen unrelated to lineup
    #so start at nPulses = 5, search until 3 
    nPulses = 5
    while rosspad_start_index == -1 and nPulses >= 3:
        for i in range(len(rosspad_clock_diff)-nPulses-1):
            sequence = rosspad_clock_diff[i:i+nPulses-1]
            if (sequence > lower_bound).all() and (sequence < upper_bound).all():
                rosspad_start_index = rosspad_orig_rows[i]
                break
        nPulses -= 1

    #what if it drops every other pulse? unlikely, but, could repeat search with frequency of 200 us
    if rosspad_start_index == -1:
        nPulses = 5
        lower_bound, upper_bound = get_bounds(True,200e-6)
    while rosspad_start_index == -1 and nPulses >= 3:
        for i in range(len(rosspad_clock_diff)-nPulses-1):
            sequence = rosspad_clock_diff[i:i+nPulses-1]
            if (sequence > lower_bound).all() and (sequence < upper_bound).all():
                rosspad_start_index = rosspad_orig_rows[i]
                break
        nPulses -= 1

#    print(rosspad_df.trig.to_numpy()[rosspad_start_index-1:rosspad_start_index+10])
#    print(rosspad_df.timestamp.to_numpy()[rosspad_start_index-1:rosspad_start_index+10])

    #again, find index of first PPS
    foundPPS = False
    while not foundPPS:
        if rosspad_data[rosspad_start_index,pps_idx] == True:
            foundPPS = True
        else:
            rosspad_start_index += 1


    return rosspad_start_index, rosspad_data, rosspad_keys

class RosspadToL1_1:
    def __init__(self,rfname,tfname,thresh):
        self.rfname = rfname
        self.tfname = tfname
        self.bad_thresh = int(float(thresh))

    def process(self):
        self.read_trigger()
        self.read_rosspad()
        self.sort_rosspad()
        self.fix_rosspad_rollover()
        self.check_pps()
        self.assign_rosspad_UTC()
        self.find_bad_times()
        self.fix_trigger_rollover()
        self.count_pps()
        self.assign_eventIDs()
        self.write_file()
        
    
    
    def read_trigger(self):
        f = h5.File(self.tfname,'r')

        self.npps = f['npps'][...]
        self.nacd = f['nacd'][...]
        self.clkRaw = f['clkRaw'][...]
        self.clkPPS = f['clkPPS'][...]
        self.eid = f['eid'][...]

        f.close()
        
    def read_rosspad(self):
        rosspad_start, self.rosspad_data, self.rosspad_keys = lineup(self.rfname)
        self.rosspad_data = self.rosspad_data[rosspad_start:,:]
        #self.rosspad_data = self.rosspad_data[:100000] # take this out later
    
        self.pps_idx = np.where(self.rosspad_keys == 'pps')[0][0]
        self.trigack_idx = np.where(self.rosspad_keys == 'trigack')[0][0]
        self.timestamp_idx = np.where(self.rosspad_keys == 'timestamp')[0][0]
        self.frame_idx = np.where(self.rosspad_keys == 'frame')[0][0]
        self.gpio_idx = np.where(self.rosspad_keys == 'gpio')[0][0]
        
    def sort_rosspad(self):
        #separate to find the rollovers by looking at timestamp diff
        #call .copy() to get rid of SettingWithCopy warning
        pps_data = self.rosspad_data[self.rosspad_data[:,self.pps_idx] == True,:].copy()
        trigOut_data = self.rosspad_data[(self.rosspad_data[:,self.pps_idx] == False) & (self.rosspad_data[:,self.trigack_idx] == False),:].copy()
        trigIn_data = self.rosspad_data[self.rosspad_data[:,self.trigack_idx] == True,:].copy()

        #first pps
        pps_tstamps = pps_data[:,self.timestamp_idx]
        pps_tstamp_diff = np.diff(pps_tstamps)
        pps_rollover_inds = np.argwhere(pps_tstamp_diff < 0).flatten()
        #start at -1 or else the sortcol and the timestamps are offset by 1
        # and it will take you 10 hours to figure out why nothing works
        pps_rollover_inds = np.insert(pps_rollover_inds,0,-1)
        pps_rollover_inds = np.append(pps_rollover_inds,len(pps_tstamps))

        pps_sortcol = []
        counter = 0
        for i,j in zip(pps_rollover_inds[:-1],pps_rollover_inds[1:]):
            pps_sortcol.extend([counter for x in range(j-i)])
            counter += 1

        #remove last element of sortcol, since it's extra
        pps_sortcol = np.array(pps_sortcol[:-1])
        pps_sortcol = (pps_sortcol << 32) | pps_tstamps.astype(int)


        #next trigout
        trigOut_tstamps = trigOut_data[:,self.timestamp_idx]
        trigOut_tstamp_diff = np.diff(trigOut_tstamps)
        trigOut_rollover_inds = np.argwhere(trigOut_tstamp_diff < 0).flatten()
        trigOut_rollover_inds = np.insert(trigOut_rollover_inds,0,-1)
        trigOut_rollover_inds = np.append(trigOut_rollover_inds,len(trigOut_tstamps))

        trigOut_sortcol = []
        counter = 0
        for i,j in zip(trigOut_rollover_inds[:-1],trigOut_rollover_inds[1:]):
            trigOut_sortcol.extend([counter for x in range(j-i)])
            counter += 1

        trigOut_sortcol = np.array(trigOut_sortcol[:-1])
        trigOut_sortcol = (trigOut_sortcol << 32) | trigOut_tstamps.astype(int)


        #finally trigin
        trigIn_tstamps = trigIn_data[:,self.timestamp_idx]
        trigIn_tstamp_diff = np.diff(trigIn_tstamps)
        trigIn_rollover_inds = np.argwhere(trigIn_tstamp_diff < 0).flatten()
        trigIn_rollover_inds = np.insert(trigIn_rollover_inds,0,-1)
        trigIn_rollover_inds = np.append(trigIn_rollover_inds,len(trigIn_tstamps))

        trigIn_sortcol = []
        counter = 0
        for i,j in zip(trigIn_rollover_inds[:-1],trigIn_rollover_inds[1:]):
            trigIn_sortcol.extend([counter for x in range(j-i)])
            counter += 1

        trigIn_sortcol = np.array(trigIn_sortcol[:-1])
        trigIn_sortcol = (trigIn_sortcol << 32) | trigIn_tstamps.astype(int)



        #now re-combine dataframes
        combined_data = np.concatenate((pps_data,trigOut_data,trigIn_data))
        sc = np.concatenate((pps_sortcol,trigOut_sortcol,trigIn_sortcol))

        #sort combined df by sortcol
        combined_data = combined_data[sc.argsort(),:]
        sc = sc[sc.argsort()]

        new_tstamps = combined_data[:,self.timestamp_idx]
        old_tstamps = self.rosspad_data[:,self.timestamp_idx]

        #check if there are multiple PPS's in a row at the beginning, and if so, drop
        # this can happen if it was out of order before
        new_start_ind = 0
        first_trig_out = 0
        isPPS = combined_data[:,self.pps_idx]
        isTrigIn = combined_data[:,self.trigack_idx]
        for i in range(len(isPPS)):
            if isPPS[i] == False and isTrigIn[i] == False:
                first_trig_out = i
                break
        new_start_ind = first_trig_out-1
        while isPPS[new_start_ind] != True:
            new_start_ind -= 1
        combined_data = combined_data[new_start_ind:,:]
        sc = sc[new_start_ind:]

        #check sorting just in case
        rpd_tstamps = combined_data[:,self.timestamp_idx]
        rpd_tstamps_diff = np.diff(rpd_tstamps)
        rpd_tstamps_diff = np.array([x+2**32 if x < -1000000000 else x for x in rpd_tstamps_diff])


        if np.any(rpd_tstamps_diff < 0):
            combined_data[np.where(rpd_tstamps_diff < 0)[0],self.timestamp_idx] = combined_data[np.where(rpd_tstamps_diff < 0)[0]-1,self.timestamp_idx] + 1
            rpd_tstamps = combined_data[:,self.timestamp_idx]
            rpd_tstamps_diff = np.diff(rpd_tstamps)
            rpd_tstamps_diff = np.array([x+2**32 if x < -100000000 else x for x in rpd_tstamps_diff])

            if np.any(rpd_tstamps_diff < 0):
                raise(SortError)

        self.rosspad_data = combined_data
        self.rosspad_sortcol = sc

    def check_pps(self):

        #find and remove any extra rosspad PPS's
        #Get rid of extra PPS's
        diff_pps_timestamp = np.diff(self.rosspad_data[self.rosspad_data[:,self.pps_idx].astype(bool),self.timestamp_idx])
        extra_pps_inds = (diff_pps_timestamp > -0.1) & (diff_pps_timestamp < 0.999)

        if sum(extra_pps_inds) > 0:
            print("     Extra PPS signals Found.")
        while sum(extra_pps_inds) > 0:
            pps_tot_inds = np.where(self.rosspad_data[:,self.pps_idx].astype(bool) == True)[0]
            idx = pps_tot_inds[1:][extra_pps_inds][0]

            print(idx)
            self.rosspad_data = np.concatenate((self.rosspad_data[:idx,:],self.rosspad_data[idx + 1:,:]),axis = 0)
            self.rosspad_sortcol = np.concatenate((self.rosspad_sortcol[:idx],self.rosspad_sortcol[idx + 1:]))
    
            diff_pps_timestamp = np.diff(self.rosspad_data[self.rosspad_data[:,self.pps_idx].astype(bool),self.timestamp_idx])
            extra_pps_inds = (diff_pps_timestamp > -0.1) & (diff_pps_timestamp < 0.999)

    
        # Check again for ones that might have had a drop and a miss next to each other
        diff_pps_timestamp = np.diff(self.rosspad_data[self.rosspad_data[:,self.pps_idx].astype(bool),self.timestamp_idx])
        extra_pps_inds = (diff_pps_timestamp%1 > 0.001) & (diff_pps_timestamp%1 < 0.999) & (diff_pps_timestamp > 0)

        while sum(extra_pps_inds) > 0:
            pps_tot_inds = np.where(self.rosspad_data[:,self.pps_idx].astype(bool) == True)[0]
            idx = pps_tot_inds[1:][extra_pps_inds][0]

            print(idx)
            self.rosspad_data = np.concatenate((self.rosspad_data[:idx,:],self.rosspad_data[idx + 1:,:]),axis = 0)
            self.rosspad_sortcol = np.concatenate((self.rosspad_sortcol[:idx],self.rosspad_sortcol[idx + 1:]))
    
            diff_pps_timestamp = np.diff(self.rosspad_data[self.rosspad_data[:,self.pps_idx].astype(bool),self.timestamp_idx])
            extra_pps_inds = (diff_pps_timestamp%1 > 0.001) & (diff_pps_timestamp%1 < 0.999) & (diff_pps_timestamp > 0)

        rosspad_pps = self.rosspad_data[self.rosspad_data[:,self.pps_idx] == True,self.timestamp_idx]
        rosspad_pps_inds = np.array(range(len(self.rosspad_data)))[self.rosspad_data[:,self.pps_idx] == True]
        rosspad_pps_diff = np.diff(rosspad_pps)

        dropped_pps = np.argwhere(rosspad_pps_diff > 1.5).flatten()

        while len(dropped_pps) > 0:
    
            dpps = dropped_pps[0]
            print(f'dpps = {dpps}')
            nmissing = round(rosspad_pps_diff[dpps]) - 1
            missing_clocks = []

            clock_times = self.rosspad_data[rosspad_pps_inds[dpps]:rosspad_pps_inds[dpps+1]+1,self.timestamp_idx].copy()
            indices = np.arange(len(self.rosspad_data))[rosspad_pps_inds[dpps]:rosspad_pps_inds[dpps+1]+1]

            clock_high = clock_times[-1]
            clock_low = clock_times[0]
    
            if not (clock_high in rosspad_pps and clock_low in rosspad_pps):
                print('break')
                break

            missing_clock = clock_low + (clock_high - clock_low)/(nmissing + 1)

            insertAfterLocal = max(np.argwhere(missing_clock > clock_times).flatten())

            insertAfter = insertAfterLocal + indices[0]

            #now that we know where to insert missing PPS, make dataframe line and insert it
            ncolumns = np.shape(self.rosspad_data)[1]

            row = np.zeros(ncolumns)

            row[self.timestamp_idx] = missing_clock

            row[self.pps_idx] = True
            row[self.trigack_idx] = False

            before_row = self.rosspad_data[:insertAfter+1]
            after_row = self.rosspad_data[insertAfter + 1:]

            self.rosspad_data = np.concatenate((before_row,[row],after_row))
            self.rosspad_sortcol = np.concatenate((self.rosspad_sortcol[:insertAfter+1],
                                                       [missing_clock],
                                                       self.rosspad_sortcol[insertAfter+1:]))
    
            rosspad_pps = self.rosspad_data[self.rosspad_data[:,self.pps_idx] == True,self.timestamp_idx]
            rosspad_pps_inds = np.array(range(len(self.rosspad_data)))[self.rosspad_data[:,self.pps_idx] == True]
            rosspad_pps_diff = np.diff(rosspad_pps)

            dropped_pps = np.argwhere(rosspad_pps_diff > 1.5).flatten()
                                                           
                                                           
    def calculate_time_since_pps(self):

        ppsInds = np.arange(len(self.rosspad_data))[self.rosspad_data[:,self.pps_idx] == True]
        if min(ppsInds) > 0:
            self.rosspad_data = self.rosspad_data[min(ppsInds):,:]
            self.rosspad_sortcol = self.rosspad_sortcol[min(ppsInds):]
            ppsInds = ppsInds - min(ppsInds)
        ppsTimestamps = self.rosspad_data[self.rosspad_data[:,self.pps_idx] == True,self.timestamp_idx].astype(float)

        #get number of clock ticks per second
        ticksPerSec = np.diff(ppsTimestamps)
        #append mean to get frequency of counts after last PPS
        ticksPerSec = np.append(ticksPerSec,np.mean(ticksPerSec))
        #handle rollover
        ticksPerSec = np.array([x + 2**32-1 if x < 0 else x for x in ticksPerSec])
    
        indices = np.arange(len(self.rosspad_data))

        indexOfPrevPPS = [np.argwhere(ppsInds <= x).max() for x in indices]
        ppsTimestamps = ppsTimestamps[indexOfPrevPPS]
        ticksPerSec = ticksPerSec[indexOfPrevPPS]
    
        timestamps = self.rosspad_data[:,self.timestamp_idx].astype(float)
        timeSinceLastPPS = timestamps - ppsTimestamps
        timeSinceLastPPS = np.array([x + 2**32-1 if x < 0 else x for x in timeSinceLastPPS])
        timeSinceLastPPS = timeSinceLastPPS / ticksPerSec

        return timeSinceLastPPS
        
    def fix_rosspad_rollover(self):
        # Fix rollover in rosspad timestamp

        timestampRPD = self.rosspad_data[:,self.timestamp_idx].copy()
        dt = np.diff(timestampRPD)

        RPD_rollover_idx = np.where(dt < -0)[0] + 1
        for idx in RPD_rollover_idx:
            timestampRPD[idx:] += 2**32

        self.rosspad_data[:,self.timestamp_idx] = timestampRPD/rosspad_freq
        
        
    def assign_rosspad_UTC(self):
        
        # Assign UTC times to Rosspad data

        #give UTC time to rosspad events
        # figure out time since last PPS
        rTimeSinceLastPPS = self.calculate_time_since_pps()

        # since we have already checked that the PPS's correlate,
        # can reuse ppsUTC and do the same as above
        ppsInds = np.arange(len(self.rosspad_data))[self.rosspad_data[:,self.pps_idx] == True]
        indices = np.arange(len(self.rosspad_data))
        indexOfPrevPPS = [np.argwhere(ppsInds <= x).max() for x in indices]
        #        print(len(ppsUTC),len(indexOfPrevPPS))
        timeOfLastPPS = self.rosspad_data[ppsInds[indexOfPrevPPS],self.timestamp_idx]

        utcTimes = timeOfLastPPS + rTimeSinceLastPPS
        utcTimes = utcTimes * 1e9 #convert to nanoseconds
        rosspad_UTC = utcTimes

        lastRpdPPS = float(rosspad_UTC[ppsInds[-1]])

        self.rosspad_data = self.rosspad_data[rosspad_UTC < lastRpdPPS,:]
        self.rosspad_sortcol = self.rosspad_sortcol[rosspad_UTC < lastRpdPPS]
        self.rosspad_UTC = rosspad_UTC[rosspad_UTC < lastRpdPPS]
    
    def fix_trigger_rollover(self):
        # First have to check that there are no PPS's missing        
        
        nfixed = 0
        fixed = False
        while not fixed:
            fixed = True
            TM_pps_diff = np.diff(self.clkPPS)/TM_freq
            for i in range(len(TM_pps_diff)-1):
                dpps = TM_pps_diff[i]
                if dpps < 0:
                    next_dpps = TM_pps_diff[i+1]
                    if next_dpps > 1.5: # We missed one
                        orig_pps = self.clkPPS[i+1]
                        self.clkPPS[i+1] = np.mean([self.clkPPS[i],self.clkPPS[i+2]]) #clkPPS[i]
                        self.clkRaw[i+1] = np.mean([self.clkRaw[i],self.clkRaw[i+2]])#clkPPS[i+1] + clkRaw[i+1] - orig_pps
                        fixed = False

        # Needed for some reason 
        fixed = False
        while not fixed:
            fixed = True               
            if (np.diff(self.npps) > 3).any():
                cutoff = np.where(np.diff(self.npps) > 3)[0][0]
                self.clkPPS = self.clkPPS[cutoff + 1:]
                self.clkRaw = self.clkRaw[cutoff + 1:]
                self.npps = self.npps[cutoff + 1:]
                self.nacd = self.nacd[cutoff + 1:]
                self.eid = self.eid[cutoff + 1:]
                fixed = False

        TM_pps_diff = np.diff(self.clkPPS)/TM_freq
        TM_raw_diff = np.diff(self.clkRaw)/TM_freq

        TM_pps_rollover_idx = np.where(TM_pps_diff < -20)[0] + 1
        for idx in TM_pps_rollover_idx:
            self.clkPPS[idx:] += 2**32
    
        TM_raw_rollover_idx = np.where(TM_raw_diff < -20)[0] + 1
        for idx in TM_raw_rollover_idx:
            self.clkRaw[idx:] += 2**32
            
        largeJumpinds = np.where(np.diff(self.clkPPS > 1e8))[0]
        for i in largeJumpinds:
            self.nacd = self.nacd[i+1:]
            self.npps = self.npps[i+1:]
            self.clkPPS = self.clkPPS[i+1:]
            self.clkRaw = self.clkRaw[i+1:]
            self.eid = self.eid[i+1:]
            
        # check for extra pps:
        extraPPSinds = np.intersect1d(np.where(np.diff(self.clkPPS) > 1e7),np.where(np.diff(self.clkPPS) < 4e7))
        if len(extraPPSinds) > 0 and len(extraPPSinds)%2 == 0:
            for i in range(int(len(extraPPSinds)/2)):
                start_idx = extraPPSinds[2*i]
                end_idx = extraPPSinds[2*i + 1]
                self.clkPPS[start_idx + 1:end_idx + 1] = self.clkPPS[start_idx]
                self.npps[start_idx + 1:] -= 1
        # Saw this once

        if self.clkRaw[0] > 2**32:
            fixed = False
            while not fixed:
                fixed = True
                nbits = len(bin(int(self.clkRaw[0])))-3
                self.clkRaw -= 2**nbits
                if self.clkRaw[0] > 2**32:
                    fixed = False

        # Remove stuff after last PPS
        lastTMPPS = self.npps[-1]
        firstTMPPS = self.npps[0]
        
        self.nacd = self.nacd[self.npps < lastTMPPS]
        self.clkRaw = self.clkRaw[self.npps < lastTMPPS]
        self.clkPPS = self.clkPPS[self.npps < lastTMPPS]
        self.eid = self.eid[self.npps < lastTMPPS]
        self.npps = self.npps[self.npps < lastTMPPS]

        self.nacd = self.nacd[self.npps > firstTMPPS]
        self.clkRaw = self.clkRaw[self.npps > firstTMPPS]
        self.clkPPS = self.clkPPS[self.npps > firstTMPPS]
        self.eid = self.eid[self.npps > firstTMPPS]
        self.npps = self.npps[self.npps > firstTMPPS]
        
    def find_bad_times(self):
    	
        trigOutTimes = self.rosspad_UTC[np.logical_and(self.rosspad_data[:,self.pps_idx] == False,self.rosspad_data[:,self.trigack_idx] == False)]
        trigOutDt = np.diff(trigOutTimes)
        '''
        heights, bins, fig = plt.hist(trigOutDt, bins = np.logspace(4,8,20))
        plt.close()
        
        centers = (bins[1:] - bins[:-1])/2
        dh = np.diff(heights)
        negative = False
        stop_dh = 1e10
        for d in dh:
            if d < 0:
                negative = True
            if negative:
                if d > 0:
                    stop_dh = d
                    break
        thresh = centers[np.where(dh == stop_dh)[0][0] + 1]
        '''
        thresh = self.bad_thresh
        
        bad_rosspad_intervals = []
        for i in range(len(trigOutDt)):
            if trigOutDt[i] > thresh:
                bad_rosspad_intervals.append([trigOutTimes[i-1],trigOutTimes[min(i+2,len(trigOutTimes)-1)]])
        bad_rosspad_intervals = np.array(bad_rosspad_intervals)
        '''
        fixed = False
        while not fixed:
            fixed = True
            for i in range(len(bad_rosspad_intervals)-1):
                bad_interval1 = bad_rosspad_intervals[i]
                bad_interval2 = bad_rosspad_intervals[i+1]
                current_bad = bad_interval1[1] - bad_interval1[0] + bad_interval2[1] - bad_interval2[0]
                potential_bad = bad_interval2[1] - bad_interval1[0]
                ratio = current_bad/potential_bad
                print(f'{i} \t {bad_interval1} \t {bad_interval2} \t {ratio*100}')
                if ratio > 0.75:
                    bad_rosspad_intervals[i] = np.array([bad_interval1[0],bad_interval2[1]])
                    bad_rosspad_intervals = np.delete(bad_rosspad_intervals,i+1,axis = 0)
                    fixed = False
                    print('reset')
                    break
        '''
        self.bad_rosspad_intervals = bad_rosspad_intervals
        
    def count_pps(self):
        # Verify that acd and TM have the same # of PPS's
        self.totPPSTM = self.npps[-1] - self.npps[0] + 1
        self.totPPSRPD = sum(self.rosspad_data[:,self.pps_idx])
        self.pps_index_offset = 0
        print(f"ACD pps count: {int(self.totPPSRPD)}")
        print(f" TM pps count: {self.totPPSTM}")
        if self.totPPSTM == self.totPPSRPD:
            print('Same # of PPS')
        else:
            print('Not the same # of PPS')
            pps_cutoff = int(min(self.totPPSTM,self.totPPSRPD))
            if self.totPPSTM != pps_cutoff:
                ppsIdx = np.where(np.diff(self.npps) > 0.5)[0]
                ppsIdx = ppsIdx[:pps_cutoff+1]
                tm_cutoff = max(ppsIdx)
        
                self.npps = self.npps[:tm_cutoff]
                self.nacd = self.nacd[:tm_cutoff]
                self.eid = self.eid[:tm_cutoff]
                self.clkRaw = self.clkRaw[:tm_cutoff]
                self.clkPPS = self.clkPPS[:tm_cutoff]
                
                self.totPPSTM = self.npps[-1] - self.npps[0] + 1
                self.totPPSRPD = sum(self.rosspad_data[:,self.pps_idx])
                print('Truncating TM')
                print(f'New ACD pps count: {int(self.totPPSRPD)}')
                print(f'New  TM pps count: {int(self.totPPSTM)}')
        
            else:
                #ppsIdx = np.where(self.rosspad_data[:,self.pps_idx] == True)[0]
                #ppsIdx = ppsIdx[:pps_cutoff+1]
                #rpd_cutoff = max(ppsIdx)
                #self.rosspad_data = self.rosspad_data[:rpd_cutoff]
                #self.rosspad_UTC = self.rosspad_UTC[:rpd_cutoff]
                print('Not going to truncate')            
            
            

    def assign_eventIDs(self):
        # convert TM times over to UTC
        # Calculate time since last pps
        timeSinceLastPPSTM = self.clkRaw - self.clkPPS # in TM time units
        timeSinceLastPPSTM = timeSinceLastPPSTM/TM_freq # in seconds
        timeSinceLastPPSTM = timeSinceLastPPSTM*1e9 # in ns
        
        
        match_found = False
        ppsUTC = self.rosspad_UTC[self.rosspad_data[:,self.pps_idx] == True]
        offset_list = []

        for index_offset in np.concatenate((range(int(self.totPPSTM)),-1*np.arange(1,int(self.totPPSTM)))):
            print(index_offset)
    
            # if index_offset = 0: 1st ROSSPAD PPS lines up with 1st TM PPS
            # if index_offset > 0: nth ROSSPAD PPS lines up with 1st TM PPS
            # if index_offset < 0: 1st ROSSPAD PPS lines up with nth TM PPS
    
            ppsIdx = (self.npps - self.npps[0]).astype(int) + int(index_offset)
            for i in range(len(ppsIdx)):
                idx = ppsIdx[i]
                if idx >= len(ppsUTC):
                    ppsIdx[i] = -1
                if idx < 0:
                    ppsIdx[i] = -1
            self.clkPPS = ppsUTC[ppsIdx]
            self.clkRaw = self.clkPPS + timeSinceLastPPSTM
            self.clkRaw = self.clkRaw[ppsIdx >= 0]

            trigAckUTC = self.rosspad_UTC[self.rosspad_data[:,self.trigack_idx] == True].copy()
            eidUTC = self.clkRaw.copy()
            
            t_offset = 0
            dt_stdev = 1e10
            if len(eidUTC) > 100:
                dt_stdev = np.std(trigAckUTC[:100] - eidUTC[:100])
            if dt_stdev < 1e5:
                t_offset = np.mean(trigAckUTC[:100] - eidUTC[:100])
    
            eidUTC = eidUTC + t_offset
    
            matching_indices = np.zeros((100,3),dtype = int)
            for i in range(min(len(eidUTC),100)):
                min_dt = 1e10
                min_idx = -1
                teid = eidUTC[i]
                first = True
                start = 0
                for j in range(start,len(trigAckUTC)):
                    tack = trigAckUTC[j]
                    dt = abs(teid - tack)
                    if dt < min_dt:
                        min_dt = dt
                        min_idx = j
                        first = False
                    if not first and dt > min_dt * 1e2:
                        break
                matching_indices[i,:] = [i,min_idx,min_dt]
            offset_list.append([index_offset,np.median(matching_indices[:,2]),dt_stdev])
            if np.median(matching_indices[:,2]) > 0 and np.median(matching_indices[:,2]) < 1e5:
                match_found = True
                break
        offset_list = np.array(offset_list)
        
        print(f'index_offset = {index_offset}')
        print(f't_offset = {t_offset}')
        
        eid_save = self.eid.copy()
        # We already have UTC times from rosspad
        ppsUTC = self.rosspad_UTC[self.rosspad_data[:,self.pps_idx] == True]
        ppsIdx = (self.npps - self.npps[0]).astype(int) + int(index_offset)
        for i in range(len(ppsIdx)):
            idx = ppsIdx[i]
            if idx >= len(ppsUTC):
                ppsIdx[i] = len(ppsUTC)-1
            if idx < 0:
                ppsIdx[i] = -1
        self.clkPPS = ppsUTC[ppsIdx]
        self.clkRaw = self.clkPPS + timeSinceLastPPSTM + t_offset
        self.clkRaw = self.clkRaw[ppsIdx >= 0]
        eid = eid_save[ppsIdx>=0]

        # Find closest pairs of trig Acks and Event IDs

        trigAckUTC = self.rosspad_UTC[self.rosspad_data[:,self.trigack_idx] == True].copy()
        eidUTC = self.clkRaw.copy()

        matching_indices = np.zeros((len(eidUTC),3),dtype = int)
        for i in range(len(eidUTC)):
            if i > 0 and i%1000 == 0:
                print(f"{i}/{len(eidUTC)-1}")
            min_dt = 1e10
            min_idx = -1
            teid = eidUTC[i]
            first = True
            if ppsIdx[i] >= 0:
                if i > 1000:
                    start = min(int(i * 0.9),matching_indices[i-1,1] - 10)
                else:
                    start = 0
                if start < 0:
                    start = 0
                for j in range(start,len(trigAckUTC)):
                    tack = trigAckUTC[j]
                    dt = abs(teid - tack)
                    if dt < min_dt:
                        min_dt = dt
                        min_idx = j
                        first = False
                    if not first and dt > min_dt * 1e3:
                        break
                matching_indices[i,:] = [i,min_idx,min_dt]

        print(sum(matching_indices[:,2] < 1e6))
        print(np.median(matching_indices[:,2]))
        
        
        print("Finding event IDs from the bad time zones")
        
        bad_eids = []
        for i in range(len(eidUTC)):
            eid_time = eidUTC[i]
            for bad_interval in self.bad_rosspad_intervals:
                if eid_time >= bad_interval[0] and eid_time <= bad_interval[1]:
                    bad_eids.append(eid[i])
                if eid_time < bad_interval[0]:
                    break
        bad_eids = np.array(bad_eids,dtype = int)
        # Just in case there a unmatched event IDs that weren't in a bad zone:
        
        unmatched = eid[matching_indices[matching_indices[:,2] > 2e4,0]].astype(int)
        
        bad_eids = np.union1d(bad_eids,unmatched)
        
        self.bad_eids = bad_eids
            
        print(f"% matching to within 20 us: {sum(matching_indices[:,2] < 2e4)/len(matching_indices)*100}")
        print(f"# of bad eids: {len(bad_eids)}")
        
        
        print("Assigning Event IDs")
        # Create rosspad_eventIDs
        self.rosspad_eventIDs = np.zeros_like(self.rosspad_UTC)
        for row in matching_indices:
            i,j,k = row
            if i > 0 and i%1000 == 0:
                print(f"{i}/{len(matching_indices)}")
            if not self.eid[i] in self.bad_eids:
                rosspad_index = np.argmax(self.rosspad_UTC == trigAckUTC[j])
                self.rosspad_eventIDs[rosspad_index] = self.eid[i]
            
    def write_file(self):
        
        outname = self.rfname[:-3] + '_TM.L1.1.h5'
        with h5.File(outname,'w') as h5f:
    
            h5f.create_dataset('pps', data = self.rosspad_data[:,self.pps_idx])
            h5f.create_dataset('trigack', data = self.rosspad_data[:,self.trigack_idx])
            h5f.create_dataset('frame', data = self.rosspad_data[:,self.frame_idx])
            h5f.create_dataset('badEids',data = self.bad_eids)

            self.rosspad_data = np.delete(self.rosspad_data,[self.frame_idx,
                                                            self.timestamp_idx,
                                                            self.gpio_idx,
                                                            self.pps_idx,
                                                            self.trigack_idx], axis = 1)
            h5f.create_dataset('ADC', data = self.rosspad_data)

            h5f.create_dataset('EventID', data = self.rosspad_eventIDs)
            h5f.create_dataset('UTC', data = self.rosspad_UTC)

if __name__ == '__main__':

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("rfname",help="rosspad hdf5 file")
    parser.add_argument("tfname",help="trigger module log file")
    parser.add_argument("-t",default = 1e10,help = 'Rosspad trigOut timing threshold in ns')
    args = parser.parse_args()

    R = RosspadToL1_1(args.rfname,args.tfname,args.t)
    R.process()







