# new rosspadToL1_1.py



import numpy as np
from pathlib import Path
import lineup_logfiles
import h5py as h5
from functools import reduce
import re


class LineupFailed(Exception):
    def __init__(self,msg="ROSSPAD and Arduino lineup failed", *args, **kwargs):
        super().__init__(msg, *args, **kwargs)

class DroppedPPS(Exception):
    def __init__(self,msg="Dropped PPS: deal with this", *args, **kwargs):
        super().__init__(msg, *args, **kwargs)

class SortError(Exception):
    def __init__(self,msg="Sort failed: timestamps out of order", *args, **kwargs):
        super().__init__(msg, *args, **kwargs)


class RosspadToL1_1:
    def __init__(self,rfname,afname):
        self.rfname = rfname
        self.afname = afname

    def process(self):
        print("")
        try:
            self.read_files()
            print('read')
            
            self.sort_rosspad()
            print('r sort')

            self.arduino_cleanup()
            print('a clean')

            self.sort_arduino()
            print('a sort')

            self.check_pps()
            print('pps')
            
            self.count_dropped_trigs()
            print('count')

            self.assign_UTC_times()
            print('UTC')

            self.check_trigOut_times()
            print('trigOut')

            self.insert_trigacks()
            print('trigAck')

            self.write_files()
            print('write')
        except Exception as e:
            print(e)

    def read_files(self):
        #get starting index 
        rosspad_start, arduino_start, _rosspad_data, _arduino_data, rosspad_keys, arduino_keys = lineup_logfiles.lineup(self.rfname,self.afname[0])
        
        #Rosspad indices
        self.pps_idx = np.where(rosspad_keys == 'pps')[0][0]
        self.trigack_idx = np.where(rosspad_keys == 'trigack')[0][0]
        self.timestamp_idx = np.where(rosspad_keys == 'timestamp')[0][0]
        self.frame_idx = np.where(rosspad_keys == 'frame')[0][0]
        self.gpio_idx = np.where(rosspad_keys == 'gpio')[0][0]
        
        #Arduino indices
        self.id_idx = np.where(arduino_keys == 'ID')[0][0]
        self.clock_idx = np.where(arduino_keys == "Clock")[0][0]
        self.utca_idx = np.where(arduino_keys == "UTCA")[0][0]
        self.utcc_idx = np.where(arduino_keys == "UTCC")[0][0]
        
        self.rosspad_data = _rosspad_data[rosspad_start:,:]
        

        __arduino_data = _arduino_data[arduino_start:,:]
        for i in range(1,len(self.afname)):
        
            arduino_f = open(self.afname[i],'r')
            arduino_lines = arduino_f.readlines()
            arduino_f.close()

            tmp_arduino_data = []
            for line in arduino_lines:
                line_strip = line.strip().split(',')
                if len(line_strip) == 2:
                    line_strip.append('')
                if len(line_strip) == 3:
                    line_strip.append('')
                tmp_arduino_data.append(line_strip)
            
            _arduino_data = np.array(tmp_arduino_data)
            __arduino_data = np.append(__arduino_data,_arduino_data,axis = 0)
        self.arduino_data = __arduino_data.copy()

        #make sure both dataframes start on a PPS
        if self.rosspad_data[0,self.pps_idx] == False or self.arduino_data[0,self.id_idx] != "P":
            raise LineupFailed


    #Sort the timestamps in the rosspad, since they are often out of order due to being on different buffers
    def sort_rosspad(self):
        #separate to find the rollovers by looking at timestamp diff
        #call .copy() to get rid of SettingWithCopy warning
        pps_data = self.rosspad_data[self.rosspad_data[:,self.pps_idx] == True,:].copy()
        trigOut_data = self.rosspad_data[(self.rosspad_data[:,self.pps_idx] == False) & (self.rosspad_data[:,self.trigack_idx] == False),:].copy()
        trigIn_data = self.rosspad_data[self.rosspad_data[:,self.trigack_idx] == True,:].copy()

        #first pps
        pps_tstamps = pps_data[:,self.timestamp_idx]
        pps_tstamp_diff = np.diff(pps_tstamps)
        pps_rollover_inds = np.argwhere(pps_tstamp_diff < 0).flatten()
        #start at -1 or else the sortcol and the timestamps are offset by 1
        # and it will take you 10 hours to figure out why nothing works
        pps_rollover_inds = np.insert(pps_rollover_inds,0,-1)
        pps_rollover_inds = np.append(pps_rollover_inds,len(pps_tstamps))
    
        pps_sortcol = []
        counter = 0
        for i,j in zip(pps_rollover_inds[:-1],pps_rollover_inds[1:]):
            pps_sortcol.extend([counter for x in range(j-i)])
            counter += 1
    
        #remove last element of sortcol, since it's extra
        pps_sortcol = np.array(pps_sortcol[:-1])
        pps_sortcol = (pps_sortcol << 32) | pps_tstamps.astype(int)
    
    
        #next trigout
        trigOut_tstamps = trigOut_data[:,self.timestamp_idx]
        trigOut_tstamp_diff = np.diff(trigOut_tstamps)
        trigOut_rollover_inds = np.argwhere(trigOut_tstamp_diff < 0).flatten()
        trigOut_rollover_inds = np.insert(trigOut_rollover_inds,0,-1)
        trigOut_rollover_inds = np.append(trigOut_rollover_inds,len(trigOut_tstamps))
    
        trigOut_sortcol = []
        counter = 0
        for i,j in zip(trigOut_rollover_inds[:-1],trigOut_rollover_inds[1:]):
            trigOut_sortcol.extend([counter for x in range(j-i)])
            counter += 1
    
        trigOut_sortcol = np.array(trigOut_sortcol[:-1])
        trigOut_sortcol = (trigOut_sortcol << 32) | trigOut_tstamps.astype(int)
        
    
        #finally trigin
        trigIn_tstamps = trigIn_data[:,self.timestamp_idx]
        trigIn_tstamp_diff = np.diff(trigIn_tstamps)
        trigIn_rollover_inds = np.argwhere(trigIn_tstamp_diff < 0).flatten()
        trigIn_rollover_inds = np.insert(trigIn_rollover_inds,0,-1)
        trigIn_rollover_inds = np.append(trigIn_rollover_inds,len(trigIn_tstamps))
    
        trigIn_sortcol = []
        counter = 0
        for i,j in zip(trigIn_rollover_inds[:-1],trigIn_rollover_inds[1:]):
            trigIn_sortcol.extend([counter for x in range(j-i)])
            counter += 1
    
        trigIn_sortcol = np.array(trigIn_sortcol[:-1])
        trigIn_sortcol = (trigIn_sortcol << 32) | trigIn_tstamps.astype(int)
    
    
    
        #now re-combine dataframes
        combined_data = np.concatenate((pps_data,trigOut_data,trigIn_data))
        sc = np.concatenate((pps_sortcol,trigOut_sortcol,trigIn_sortcol))
    
        #sort combined df by sortcol
        combined_data = combined_data[sc.argsort(),:]
        sc = sc[sc.argsort()]
    
        new_tstamps = combined_data[:,self.timestamp_idx]
        old_tstamps = self.rosspad_data[:,self.timestamp_idx]
    
        #check if there are multiple PPS's in a row at the beginning, and if so, drop
        # this can happen if it was out of order before
        new_start_ind = 0
        first_trig_out = 0
        isPPS = combined_data[:,self.pps_idx]
        isTrigIn = combined_data[:,self.trigack_idx]
        for i in range(len(isPPS)):
            if isPPS[i] == False and isTrigIn[i] == False:
                first_trig_out = i
                break
        new_start_ind = first_trig_out-1
        while isPPS[new_start_ind] != True:
            new_start_ind -= 1
        combined_data = combined_data[new_start_ind:,:]
    
        #check sorting just in case
        rpd_tstamps = combined_data[:,self.timestamp_idx]
        rpd_tstamps_diff = np.diff(rpd_tstamps)
        rpd_tstamps_diff = np.array([x+2**32 if x < -1000000000 else x for x in rpd_tstamps_diff])
        

        if np.any(rpd_tstamps_diff < 0):
            combined_data[np.where(rpd_tstamps_diff < 0)[0],self.timestamp_idx] = combined_data[np.where(rpd_tstamps_diff < 0)[0]-1,self.timestamp_idx] + 1
            rpd_tstamps = combined_data[:,self.timestamp_idx]
            rpd_tstamps_diff = np.diff(rpd_tstamps)
            rpd_tstamps_diff = np.array([x+2**32 if x < -100000000 else x for x in rpd_tstamps_diff])

            if np.any(rpd_tstamps_diff < 0):
                raise(SortError)

        self.rosspad_data = combined_data
        self.rosspad_sortcol = sc

    #sometimes arduino events out of order, sort here
    def sort_arduino(self):


        #separate to find the rollovers by looking at timestamp diff
        pps_data = self.arduino_data[self.arduino_data[:,self.id_idx] == "P",:].copy()
        trigOut_data = self.arduino_data[self.arduino_data[:,self.id_idx] == "R",:].copy()
        trigIn_data = self.arduino_data[self.arduino_data[:,self.id_idx] == "C",:].copy()

        pps_eventIDs = self.eventIDs[self.arduino_data[:,self.id_idx] == "P"].copy()
        trigOut_eventIDs = self.eventIDs[self.arduino_data[:,self.id_idx] == "R"].copy()
        trigIn_eventIDs = self.eventIDs[self.arduino_data[:,self.id_idx] == "C"].copy()

        pps_missedInterrupt = self.arduino_missedInterrupt[self.arduino_data[:,self.id_idx] == "P"].copy()
        trigOut_missedInterrupt = self.arduino_missedInterrupt[self.arduino_data[:,self.id_idx] == "R"].copy()
        trigIn_missedInterrupt = self.arduino_missedInterrupt[self.arduino_data[:,self.id_idx] == "C"].copy()

        #first pps
        pps_tstamps = pps_data[:,self.clock_idx].astype(int)
        pps_tstamp_diff = np.diff(pps_tstamps)
        pps_rollover_inds = np.argwhere(pps_tstamp_diff < 0).flatten()
        #start at -1 or else the sortcol and the timestamps are offset by 1
        # and it will take you 10 hours to figure out why nothing works
        pps_rollover_inds = np.insert(pps_rollover_inds,0,-1)
        pps_rollover_inds = np.append(pps_rollover_inds,len(pps_tstamps))

        pps_sortcol = []
        counter = 0
        for i,j in zip(pps_rollover_inds[:-1],pps_rollover_inds[1:]):
            pps_sortcol.extend([counter for x in range(j-i)])
            counter += 1

        #remove last element of sortcol, since it's extra
        pps_sortcol = np.array(pps_sortcol[:-1])
        pps_tstamps = np.array(pps_tstamps,dtype=int)
        pps_sortcol = (pps_sortcol << 32) | pps_tstamps


        #next trigout
        trigOut_tstamps = trigOut_data[:,self.clock_idx].astype(int)
        trigOut_tstamp_diff = np.diff(trigOut_tstamps)
        trigOut_rollover_inds = np.argwhere(trigOut_tstamp_diff < 0).flatten()
        trigOut_rollover_inds = np.insert(trigOut_rollover_inds,0,-1)
        trigOut_rollover_inds = np.append(trigOut_rollover_inds,len(trigOut_tstamps))

        trigOut_sortcol = []
        counter = 0
        for i,j in zip(trigOut_rollover_inds[:-1],trigOut_rollover_inds[1:]):
            trigOut_sortcol.extend([counter for x in range(j-i)])
            counter += 1

        trigOut_sortcol = np.array(trigOut_sortcol[:-1])
        trigOut_tstamps = np.array(trigOut_tstamps,dtype=int)
        trigOut_sortcol = (trigOut_sortcol << 32) | trigOut_tstamps

        #finally trigin
        trigIn_tstamps = trigIn_data[:,self.clock_idx].astype(int)
        trigIn_tstamp_diff = np.diff(trigIn_tstamps)
        trigIn_rollover_inds = np.argwhere(trigIn_tstamp_diff < 0).flatten()
        trigIn_rollover_inds = np.insert(trigIn_rollover_inds,0,-1)
        trigIn_rollover_inds = np.append(trigIn_rollover_inds,len(trigIn_tstamps))

        trigIn_sortcol = []
        counter = 0
        for i,j in zip(trigIn_rollover_inds[:-1],trigIn_rollover_inds[1:]):
            trigIn_sortcol.extend([counter for x in range(j-i)])
            counter += 1

        trigIn_sortcol = np.array(trigIn_sortcol[:-1])
        trigIn_tstamps = np.array(trigIn_tstamps,dtype=int)
        trigIn_sortcol = (trigIn_sortcol << 32) | trigIn_tstamps

        #now re-combine dataframes
        combined_data = np.concatenate((pps_data,trigOut_data,trigIn_data))
        combined_eventIDs = np.concatenate((pps_eventIDs,trigOut_eventIDs, trigIn_eventIDs))
        combined_missedInterrupt = np.concatenate((pps_missedInterrupt, trigOut_missedInterrupt,trigIn_missedInterrupt))
        sc = np.concatenate((pps_sortcol,trigOut_sortcol,trigIn_sortcol))


        #sort combined df by sortcol
        combined_data = combined_data[sc.argsort(),:]
        combined_eventIDs = combined_eventIDs[sc.argsort()]
        combined_missedInterrupt = combined_missedInterrupt[sc.argsort()]
        sc = sc[sc.argsort()]

        new_tstamps = combined_data[:,self.clock_idx].astype(int)
        old_tstamps = self.arduino_data[:,self.clock_idx]
        #        print(len(new_tstamps),len(old_tstamps))


        #check timestamps are in order    
        ard_tstamps = new_tstamps.copy()
        ard_tstamps_diff = np.diff(ard_tstamps)
        ard_tstamps_diff = np.array([x+2**32 if x < -1000000000 else x for x in ard_tstamps_diff])
        if np.any(ard_tstamps_diff < 0):
            combined_data[np.where(ard_tstamps_diff < 0)[0],self.clock_idx] = combined_data[np.where(ard_tstamps_diff < 0)[0]-1,self.clock_idx].astype(int) + 1

            ard_tstamps = combined_data[:,self.clock_idx].astype(int)
            ard_tstamps_diff = np.diff(ard_tstamps)
            ard_tstamps_diff = np.array([x+2**32 if x < -100000000 else x for x in ard_tstamps_diff])
            if np.any(ard_tstamps_diff < 0):
                raise SortError
        self.arduino_data = combined_data
        self.arduino_sortcol = sc
        self.eventIDs = combined_eventIDs
        self.missedInterrupt = combined_missedInterrupt


    #Makes the data frame columns a little bit nicer and combines the TrigIns and EventIDs
    def arduino_cleanup(self):
        origUTCA = self.arduino_data[:,self.utca_idx]
        ID = self.arduino_data[:,self.id_idx]

        #column of only PPS UTC times
        ppsUTCA = np.array([origUTCA[i] if ID[i] == 'P' else np.nan for i in range(len(ID))])

        #column for missed interrupt flag on the arduino
        missedInterrupt = np.zeros(len(origUTCA))
        for i in range(len(origUTCA)):
            x = origUTCA[i]
            try:
                if float(x) == 2:
                    missedInterrupt[i] = True
                else:
                    missedInterrupt[i] = False
            except:
                missedInterrupt[i] = False

        #add new / modified columns
        self.arduino_data[:,self.utca_idx] = ppsUTCA
        self.arduino_missedInterrupt = missedInterrupt




        #combine TrigAcks and EventIDs
        eventID_data = self.arduino_data[self.arduino_data[:,self.id_idx] == 'E',:].copy()
        eventID_indices = np.arange(len(self.arduino_data))[self.arduino_data[:,self.id_idx] == 'E']

        #sometimes the bits are shifted
        eids = eventID_data[:,1].astype(int)
        eid_str = []
        shifted = []
        new_shifted = []
        offsets = []
        junk = []
        j = 0

        # turn all of eids into strings & check that all eids have len 34
        for i in range(len(eids)):
            eid = eids[i]
            bin_str = bin(eid)
            if len(bin_str) < 34:
                nmissing_bits = 34 - len(bin_str)
                for bit in range(nmissing_bits):
                    bin_str = '0b0' + bin_str[2:]
            eid_str.append(bin_str)

        #check which eventIDs are shifted
        for eid in eid_str:
            matched = (re.search('1000000000', eid))
            if matched:
                offset = matched.start() - 2
                if offset != 0:
                    shifted.append(j)
                    offsets.append(offset)
            else:
                junk.append(j) #sometimes it's junk
            j += 1

        # if there's no shift, move on

        # if there is a shift, try to fix it
        new_eids = eids.copy()
        if len(shifted) > 0:
            prior = ''
            for i in range(offsets[0]):
                prior = prior + '0'
            for i in range(len(shifted)):
                j = shifted[i]
                offset = offsets[i]
                if i > 0 and abs(j - shifted[i-1]) > 1:
                    prior = ''
                    for k in range(offset):
                        prior = prior + '0'
                old_str = eid_str[j]
                pre = old_str[2:offset + 2]
                post = old_str[offset + 2:]
                new_str = '0b' + post + prior
                new = int(new_str,2)
                prior = pre
                new_eids[j] = new

            # Look for any duplicates
            all_duplicates_found = False
            while not all_duplicates_found:
                current_loop = True
                prior = new_eids[0]
                for i in range(len(new_eids)-1):
                    current = new_eids[i+1]
                    if current == prior:
                        new_eids[i] -= 1
                        current_loop = False
                    prior = current
                if current_loop:
                    all_duplicates_found = True

            # evaluate if the fix worked
            new_offsets = []
            j = 0
            for i in range(len(new_eids)):
                eid = new_eids[i]
                matched = (re.search('10000000', bin(eid)))
                if matched:
                    offset = matched.start() - 2
                    if offset != 0:
                        new_shifted.append(j)
                        new_offsets.append(offset)
                # we already caught the junk
                j += 1

                
        self.arduino_data[self.arduino_data[:,self.id_idx] == 'E',1] = new_eids.astype(str)
        eventIDs_to_drop = []
        if len(new_shifted) > 0 and len(junk) > 0:
            eventIDs_to_drop = np.concatenate((new_shifted,junk))
        elif len(new_shifted) > 0:
            eventIDs_to_drop = new_shifted
        elif len(junk) > 0:
            eventIDs_to_drop = junk
            
        eventID_indices_to_drop = eventID_indices[eventIDs_to_drop]
        
        print('Number of junk IDs: ' + str(len(eventIDs_to_drop)))
        print('Number of shifted IDs: ' + str(len(shifted)))
        if len(eventID_indices_to_drop) > 0:
            self.arduino_data = np.delete(self.arduino_data,eventID_indices_to_drop,axis = 0)
            self.arduino_missedInterrupt = np.delete(self.arduino_missedInterrupt,eventID_indices_to_drop,axis = 0)


        #since EventID comes after TrigACK, drop index by 1 for merging
        eventID_indices = np.arange(len(self.arduino_data))[self.arduino_data[:,self.id_idx] == 'E'] - 1
        trigIn_indices = np.arange(len(self.arduino_data))[self.arduino_data[:,self.id_idx] == 'C']
        unionIdx =   reduce(np.intersect1d, (trigIn_indices, eventID_indices))



        #self.arduino_data = self.arduino_data[self.arduino_data[:,self.id_idx] != 'E',:].copy()
        #self.arduino_eventID = np.empty(len(self.arduino_data))                                        # May want to add this to arduino_data


        #because event IDs are sometimes dropped, need to go through and check
        #maybe there is a cleverer way but adding manually seems simplest
        eventIDs = np.zeros(len(self.arduino_data))
        eventIDs[unionIdx] = self.arduino_data[unionIdx + 1,self.timestamp_idx]

        eventID_paired = np.zeros(len(eventIDs))
        eventID_paired[unionIdx] = 1

        nDropped = trigIn_indices.shape[0] - unionIdx.shape[0]

        #save eventIDs without TrigAcks to be added in insert_trigacks() function 
        # and convert UTC to nanoseconds
        eventIDsNoTrigAcks_indices = [i for i in range(len(eventIDs)) if (not i in unionIdx) and (i in eventID_indices) ]
        self.eventIDsNoTrigAcks = self.arduino_data[np.array(eventIDsNoTrigAcks_indices,dtype = int) + 1,:].astype('<U24')
        self.eventIDsNoTrigAcks[:,self.utcc_idx] = self.eventIDsNoTrigAcks[:,self.utcc_idx].astype(float) * 1e9
        #        print(self.eventIDsNoTrigAcks)

        self.eventIDs = eventIDs[self.arduino_data[:,self.id_idx] != 'E']
        self.eventID_paired = eventID_paired[self.arduino_data[:,self.id_idx] != 'E']
        self.arduino_missedInterrupt = self.arduino_missedInterrupt[self.arduino_data[:,self.id_idx] != 'E'].copy()
        self.arduino_data = self.arduino_data[self.arduino_data[:,self.id_idx] != 'E',:].copy()

        print("EVENT IDs:")
        print(nDropped,  "dropped event IDs", "out of" , sum(self.arduino_data[:,self.id_idx] == 'C'))
        print(self.arduino_data[:,self.id_idx] == 'C')
        print(nDropped/sum(self.arduino_data[:,self.id_idx] == 'C')*100, "Percent of dropped IDs")
        print(len(eventID_indices) - len(unionIdx),"event IDs without a TrigAck")
        #print("Min:", self.arduino_data['EventID'][self.arduino_df['EventID'].diff().between(1, 100)].iloc[1],
        #      "Max: ", self.arduino_df['EventID'][self.arduino_df['EventID'] > 0].iloc[-1], "\n")


#check for extra PPS's and remove them; check for dropped PPS's
    def check_pps(self):
        rosspad_freq = 100e6 #supposed rosspad clock frequency
        arduino_freq = 42e6  #supposed arduino clock frequency

        #find and remove any extra rosspad PPS's
        #Get rid of extra PPS's
        diff_pps_timestamp = np.diff(self.rosspad_data[self.rosspad_data[:,self.pps_idx].astype(bool),self.timestamp_idx])/rosspad_freq
        extra_pps_inds = (diff_pps_timestamp > -0.1) & (diff_pps_timestamp < 0.75)
        if sum(extra_pps_inds) > 0:
            pps_tot_inds = np.where(self.rosspad_data[:,self.pps_idx].astype(bool) == True)[0]
            bad_inds = pps_tot_inds[1:][extra_pps_inds]

            mod = 0
            for idx in bad_inds:
                self.rosspad_data = np.concatenate((self.rosspad_data[:idx - mod,:],self.rosspad_data[idx - mod + 1:,:]),axis = 0)
                mod += 1
            print("     Extra PPS signals Found.")

        #add dropped PPS's back in

        rosspad_pps = self.rosspad_data[self.rosspad_data[:,self.pps_idx] == True,self.timestamp_idx]
        rosspad_pps_inds = np.array(range(len(self.rosspad_data)))[self.rosspad_data[:,self.pps_idx] == True]
        rosspad_pps_diff = np.diff(rosspad_pps)
        #don't forget rollover!
        rosspad_pps_diff = np.array([x+2**32-1 if x<0 else x for x in rosspad_pps_diff])
        #convert to seconds
        rosspad_pps_diff = rosspad_pps_diff/rosspad_freq

        dropped_pps = np.argwhere(rosspad_pps_diff > 1.1).flatten()

        for j in range(len(dropped_pps)):
            dpps = dropped_pps[j]
            nmissing = round(rosspad_pps_diff[dpps]) - 1
            for i in range(nmissing):
                clock_times = self.rosspad_data[rosspad_pps_inds[dpps] + j:rosspad_pps_inds[dpps+1]+1+j,self.timestamp_idx]
                indices = np.arange(len(self.rosspad_data))[rosspad_pps_inds[dpps] + j:rosspad_pps_inds[dpps+1]+1 + j]

                #ROLLOVER
                rollover = False
                if (clock_times[-1] < clock_times[0]):
                    clock_diff = np.diff(clock_times)
                    rollInd = int(np.argwhere(clock_diff < 0))

                    clock_times[rollInd+1:] += 2**32
                    rollover = True

                clock_high = clock_times[-1]
                if i == 0:
                    clock_low = clock_times[0]
                else:
                    clock_low = missing_clock
                missing_clock = clock_low + (clock_high - clock_low)/(nmissing - i + 1)

                insertAfterLocal = max(np.argwhere(missing_clock > clock_times).flatten())

                insertAfter = insertAfterLocal + indices[0]

                #now that we know where to insert missing PPS, make dataframe line and insert it
                ncolumns = np.shape(self.rosspad_data)[1]

                row = np.zeros(ncolumns)

                if rollover:
                    if insertAfterLocal > rollInd:
                        missing_clock = missing_clock - 2**32
                row[self.timestamp_idx] = missing_clock

                row[self.pps_idx] = True
                row[self.trigack_idx] = False

                before_row = self.rosspad_data[:insertAfter+1]
                after_row = self.rosspad_data[insertAfter + 1:]

                self.rosspad_data = np.concatenate((before_row,[row],after_row))
                self.rosspad_sortcol = np.concatenate((self.rosspad_sortcol[:insertAfter+1],
                                                           [missing_clock],
                                                           self.rosspad_sortcol[insertAfter+1:]))

        #find and remove any extra arduino PPS's
        arduino_pps = self.arduino_data[self.arduino_data[:,self.id_idx] == "P",self.clock_idx].astype(float)
        arduino_pps_inds = np.arange(len(self.arduino_data))[self.arduino_data[:,self.id_idx] == "P"]
        arduino_pps_diff = np.diff(arduino_pps)
        #don't forget rollover!
        arduino_pps_diff = np.array([x+2**32-1 if x<0 else x for x in arduino_pps_diff])
        #convert to seconds
        arduino_pps_diff = arduino_pps_diff/arduino_freq
        extra_pps_inds = (arduino_pps_diff > -0.1) & (arduino_pps_diff < 0.75)

        if sum(extra_pps_inds) > 0:
            pps_tot_inds = np.where(self.arduino_data[:,self.id_idx] == 'P')[0]
            bad_inds = pps_tot_inds[1:][extra_pps_inds]

            mod = 0
            for idx in bad_inds:
                self.arduino_data = np.concatenate((self.arduino_data[:idx - mod,:],self.arduino_data[idx - mod + 1:,:]),axis = 0)
                self.arduino_sortcol = np.concatenate((self.arduino_sortcol[:idx - mod],self.arduino_sortcol[idx - mod + 1:]))
                mod += 1
            print("     Extra PPS signals Found.")


        #look for dropped arduino PPS's

        arduino_pps = self.arduino_data[self.arduino_data[:,self.id_idx] == "P",self.clock_idx].astype(float)
        arduino_pps_inds = np.arange(len(self.arduino_data))[self.arduino_data[:,self.id_idx] == "P"]
        arduino_pps_diff = np.diff(arduino_pps)
        #don't forget rollover!
        arduino_pps_diff = np.array([x+2**32-1 if x<0 else x for x in arduino_pps_diff])
        #convert to seconds
        arduino_pps_diff = arduino_pps_diff/arduino_freq

        dropped_pps = np.argwhere(arduino_pps_diff > 1.1).flatten()

        for dpps in dropped_pps:
            nmissing = round(arduino_pps_diff[dpps]) - 1
            for i in range(nmissing):
                clock_times = self.arduino_data[arduino_pps_inds[dpps]:arduino_pps_inds[dpps+1]+1,self.clock_idx].astype(float)
                indices = np.arange(len(self.arduino_data))[arduino_pps_inds[dpps]:arduino_pps_inds[dpps+1]+1]

                #ROLLOVER
                rollover = False
                if (clock_times[-1] < clock_times[0]):
                    clock_diff = np.diff(clock_times)
                    rollInd = int(np.argwhere(clock_diff < 0))

                    clock_times[rollInd+1:] += 2**32
                    rollover = True

                clock_high = clock_times[-1]
                if i == 0:
                    clock_low = clock_times[0]
                else:
                    clock_low = missing_clock
                missing_clock = clock_low + (clock_high - clock_low)/(nmissing - i + 1)

                insertAfterLocal = max(np.argwhere(missing_clock > clock_times).flatten())

                insertAfter = insertAfterLocal + indices[0]

                #now that we know where to insert missing PPS, make dataframe line and insert it
                ncolumns = np.shape(self.rosspad_data)[1]

                row = ['','','','']

                if rollover:
                    if insertAfterLocal > rollInd:
                        missing_clock = missing_clock - 2**32
                row[self.clock_idx] = missing_clock

                row[self.id_idx] = 'P'

                before_row = self.arduino_data[:insertAfter+1]
                after_row = self.arduino_data[insertAfter + 1:]

                row[self.utca_idx] = str(self.arduino_data[arduino_pps_inds[dpps],self.utca_idx].astype(float) + 1.0)
                row[self.utcc_idx] = str(self.arduino_data[arduino_pps_inds[dpps],self.utcc_idx].astype(float) + 1.0)

                self.arduino_data = np.concatenate((before_row,[row],after_row))
                self.arduino_sortcol = np.concatenate((self.arduino_sortcol[:insertAfter+1],
                                                           [missing_clock],
                                                           self.arduino_sortcol[insertAfter+1:]))
                self.eventIDs = np.concatenate((self.eventIDs[:insertAfter+1],[0],self.eventIDs[insertAfter + 1:]))
                self.missedInterrupt = np.concatenate((self.missedInterrupt[:insertAfter+1],[False],self.missedInterrupt[insertAfter + 1:]))

        #make sure rosspad and arduino have same # of PPS's
        #they may not, it depends on when data collection was stopped
        #so truncate whichever dataframe goes on for longer
        #  regenerate the index arrays in case some PPS's were removed above
        arduino_pps_inds = np.arange(len(self.arduino_data))[self.arduino_data[:,self.id_idx] == "P"]
        rosspad_pps_inds = np.arange(len(self.rosspad_data))[self.rosspad_data[:,self.pps_idx] == True]


        minNumPPS = min(len(arduino_pps_inds),len(rosspad_pps_inds))
        lastArdInd = arduino_pps_inds[minNumPPS-1]
        lastRpdInd = rosspad_pps_inds[minNumPPS-1]

        self.arduino_data = self.arduino_data[:lastArdInd+1,:]
        self.eventIDs = self.eventIDs[:lastArdInd+1]
        self.missedInterrupt = self.missedInterrupt[:lastArdInd+1]
        self.arduino_sortcol = self.arduino_sortcol[:lastArdInd+1]

        self.rosspad_data = self.rosspad_data[:lastRpdInd+1,:]
        self.rosspad_sortcol = self.rosspad_sortcol[:lastRpdInd+1]





    #check number of triggers per second and make sure it basically agrees
    def count_dropped_trigs(self):
        #first check number of rosspad TrigOuts
        rpd_TO = self.rosspad_data[(self.rosspad_data[:,self.trigack_idx] == False),:]
        ard_TO = self.arduino_data[(self.arduino_data[:,self.id_idx] == "P") | (self.arduino_data[:,self.id_idx] == "R"),:]

        rpd_pps_inds = np.array(range(len(rpd_TO)))[rpd_TO[:,self.pps_idx] == True]
        ard_pps_inds = np.array(range(len(ard_TO)))[ard_TO[:,self.id_idx] == "P"]

        rpd_trigsPerSec = np.diff(rpd_pps_inds)-1
        ard_trigsPerSec = np.diff(ard_pps_inds)-1
        minLen = min(len(rpd_trigsPerSec),len(ard_trigsPerSec))
        rpd_trigsPerSec = rpd_trigsPerSec[0:minLen]
        ard_trigsPerSec = ard_trigsPerSec[0:minLen]

        ard_extra = np.sum(ard_trigsPerSec - rpd_trigsPerSec)
        print("Arduino has",ard_extra/np.sum(rpd_trigsPerSec)*100,"% extra TrigOuts (normally has ~10% extra TrigOuts with all logs on for background run)")

        #check number of TrigAcks
        #want to know where # of TrigAcks disagree so we can add them in later
        rpd_TA = self.rosspad_data[(self.rosspad_data[:,self.trigack_idx].astype(bool)) | (self.rosspad_data[:,self.pps_idx].astype(bool)),:]
        ard_TA = self.arduino_data[(self.arduino_data[:,self.id_idx] == "P") | (self.arduino_data[:,self.id_idx] == "C"),:]

        rpd_pps_inds = np.arange(len(rpd_TA))[rpd_TA[:,self.pps_idx] == True]
        ard_pps_inds = np.arange(len(ard_TA))[ard_TA[:,self.id_idx] == "P"]

        rpd_trigsPerSec = np.diff(rpd_pps_inds)-1
        ard_trigsPerSec = np.diff(ard_pps_inds)-1
        minLen = min(len(rpd_trigsPerSec),len(ard_trigsPerSec))
        rpd_trigsPerSec = rpd_trigsPerSec[0:minLen]
        ard_trigsPerSec = ard_trigsPerSec[0:minLen]

        n_rosspad_drops = np.sum(ard_trigsPerSec - rpd_trigsPerSec)
        print("ROSSPAD dropped",n_rosspad_drops/np.sum(ard_trigsPerSec)*100,"% of TrigACKs (normally drops ~2.7%)\n")



    #calculate and assign UTC times to rosspad and arduino events
    def assign_UTC_times(self):
        #give UTC time to all arduino events
        # figure out time since last PPS
        aTimeSinceLastPPS = self.calculate_time_since_pps(True)

        # get UTC time of last PPS to add to
        ppsUTC = self.arduino_data[self.arduino_data[:,self.id_idx] == "P",self.utca_idx].astype(float)
        ppsInds = np.arange(len(self.arduino_data))[self.arduino_data[:,self.id_idx] == "P"]
        indices = np.arange(len(self.arduino_data))
        indexOfPrevPPS = [np.argwhere(ppsInds <= x).max() for x in indices]
        timeOfLastPPS = ppsUTC[indexOfPrevPPS]

        utcTimes = timeOfLastPPS + aTimeSinceLastPPS
        utcTimes = utcTimes * 1e9 #convert to nanoseconds
        self.arduino_UTC = utcTimes

        #give UTC time to rosspad events
        # figure out time since last PPS
        rTimeSinceLastPPS = self.calculate_time_since_pps(False)

        # since we have already checked that the PPS's correlate,
        # can reuse ppsUTC and do the same as above
        ppsInds = np.arange(len(self.rosspad_data))[self.rosspad_data[:,self.pps_idx] == True]
        indices = np.arange(len(self.rosspad_data))
        indexOfPrevPPS = [np.argwhere(ppsInds <= x).max() for x in indices]
        #        print(len(ppsUTC),len(indexOfPrevPPS))
        timeOfLastPPS = ppsUTC[indexOfPrevPPS]

        utcTimes = timeOfLastPPS + rTimeSinceLastPPS
        utcTimes = utcTimes * 1e9 #convert to nanoseconds
        self.rosspad_UTC = utcTimes

        #line up the end
        lastArdPPS = float(self.arduino_UTC[self.arduino_data[:,self.id_idx] == 'P'][-1:])
        lastRpdPPS = float(self.rosspad_UTC[self.rosspad_data[:,self.pps_idx] == True][-1:])
        lastPPS = min(lastArdPPS,lastRpdPPS)
        print(lastPPS)

        self.arduino_data = self.arduino_data[self.arduino_UTC < lastPPS,:]
        self.missedInterrupt = self.missedInterrupt[self.arduino_UTC < lastPPS]
        self.eventIDs = self.eventIDs[self.arduino_UTC < lastPPS]
        self.arduino_sortcol = self.arduino_sortcol[self.arduino_UTC < lastPPS]
        self.arduino_UTC = self.arduino_UTC[self.arduino_UTC < lastPPS]

        self.rosspad_data = self.rosspad_data[self.rosspad_UTC < lastPPS,:]
        self.rosspad_sortcol = self.rosspad_sortcol[self.rosspad_UTC < lastPPS]
        self.rosspad_UTC = self.rosspad_UTC[self.rosspad_UTC < lastPPS]

        #now that they're lined up, get count rate and run time
        pps = self.arduino_UTC[self.arduino_data[:,self.id_idx] == 'P']
        runTime = (pps[-1]-pps[0])/1e9
        counts = len(self.rosspad_data[(self.rosspad_data[:,self.pps_idx] == False) & (self.rosspad_data[:,self.trigack_idx] == False),:])
        countRate = counts/runTime
        print("\n ACD Count Rate:",countRate,"cps")
        print("EventID Count Rate:", sum(self.arduino_data[:,self.id_idx] == "C")/((max(self.arduino_UTC)-min(self.arduino_UTC))/1000000000) ,"cps")
        print("Run length:",runTime,"s\n")


    def calculate_time_since_pps(self,arduino):
        if arduino:
            ppsInds = np.arange(len(self.arduino_data))[self.arduino_data[:,self.id_idx] == "P"]
            ppsTimestamps = self.arduino_data[self.arduino_data[:,self.id_idx] == "P",self.timestamp_idx].astype(float)
        else:
            ppsInds = np.arange(len(self.rosspad_data))[self.rosspad_data[:,self.pps_idx] == True]
            if min(ppsInds) > 0:
                self.rosspad_data = self.rosspad_data[min(ppsInds):,:]
                self.rosspad_sortcol = self.rosspad_sortcol[min(ppsInds):]
                ppsInds = ppsInds - min(ppsInds)
            ppsTimestamps = self.rosspad_data[self.rosspad_data[:,self.pps_idx] == True,self.clock_idx].astype(float)

        #get number of clock ticks per second
        ticksPerSec = np.diff(ppsTimestamps)
        #append mean to get frequency of counts after last PPS
        ticksPerSec = np.append(ticksPerSec,np.mean(ticksPerSec))
        #handle rollover
        ticksPerSec = np.array([x + 2**32-1 if x < 0 else x for x in ticksPerSec])
        if arduino:
            indices = np.arange(len(self.arduino_data))
        else:
            indices = np.arange(len(self.rosspad_data))

        indexOfPrevPPS = [np.argwhere(ppsInds <= x).max() for x in indices]
        ppsTimestamps = ppsTimestamps[indexOfPrevPPS]
        ticksPerSec = ticksPerSec[indexOfPrevPPS]
        if arduino:
            timestamps = self.arduino_data[:,self.timestamp_idx].astype(float)
        else:
            timestamps = self.rosspad_data[:,self.clock_idx].astype(float)
        timeSinceLastPPS = timestamps - ppsTimestamps
        timeSinceLastPPS = np.array([x + 2**32-1 if x < 0 else x for x in timeSinceLastPPS])
        timeSinceLastPPS = timeSinceLastPPS / ticksPerSec

        return timeSinceLastPPS


    #check that the lined-up arduino and rosspad TrigOut times agree well
    def check_trigOut_times(self):
        lineup_index_tuple = self.lineup_times()
        rpdInds = lineup_index_tuple[:,0]
        ardInds = lineup_index_tuple[:,1]

        #times are in nanoseconds
        rosspad_trigOut_times = self.rosspad_UTC[rpdInds]
        arduino_trigOut_times = self.arduino_UTC[ardInds]

        tstamp_diff = abs(rosspad_trigOut_times - arduino_trigOut_times)
        nDiffGreaterThan1us = np.count_nonzero(tstamp_diff > 1e3)

        print(len(rosspad_trigOut_times)-nDiffGreaterThan1us,"out of",len(rosspad_trigOut_times),"TrigOut times agree between arduino and rosspad to within 1 us\n")

    #lineup TrigOut times between arduino and rosspad, since there are extra arduino TrigOuts
    def lineup_times(self):
        rosspad_tstamps = self.rosspad_UTC[(self.rosspad_data[:,self.pps_idx] == False) & (self.rosspad_data[:,self.trigack_idx] == False)]
        rosspad_index = np.arange(len(self.rosspad_UTC))[(self.rosspad_data[:,self.pps_idx] == False) & (self.rosspad_data[:,self.trigack_idx] == False)]
        arduino_tstamps = self.arduino_UTC[self.arduino_data[:,self.id_idx] == "R"]
        arduino_index = np.arange(len(self.arduino_UTC))[self.arduino_data[:,self.id_idx] == "R"]
        lineup_index_tuple = []

        index = np.arange(len(self.arduino_data))

        lastArd = 0
        failedToMatch = []

        for rpdT,rpdI in zip(rosspad_tstamps,rosspad_index):
            bestMatchI = -1
            lastDiff = 1e12

            for i in range(lastArd,len(arduino_tstamps)):
                currentDiff = abs(rpdT-arduino_tstamps[i])

                if currentDiff > lastDiff:
                    bestMatchI = i-1
                    break

                else:
                    lastDiff = currentDiff

            if bestMatchI == -1:
                failedToMatch.append(rpdI)

            else:
                lineup_index_tuple.append((rpdI,arduino_index[bestMatchI]))
            lastArd = bestMatchI+1


        lineup_index_tuple = np.array(lineup_index_tuple)

        #sanity check that I get the same number of extra arduino triggers
        lastIndRpd = lineup_index_tuple[-1][0]
        lastIndArd = lineup_index_tuple[-1][1]
        numExtra = np.argwhere(arduino_index == lastIndArd) - np.argwhere(rosspad_index == lastIndRpd)

        nMatched = len(rosspad_tstamps)
        print("Matched",nMatched-len(failedToMatch),"ROSSPAD TrigOuts (",len(failedToMatch),"failed to match)")
        #        print(failedToMatch)
        #        print(rosspad_index[-1])

        return lineup_index_tuple



    #insert trigacks that are missing from rosspad stream and add eventIDs
    def insert_trigacks(self):
        rowsToDrop = np.arange(len(self.rosspad_data))[self.rosspad_data[:,self.trigack_idx] == 1]
        self.rosspad_data = np.delete(self.rosspad_data,rowsToDrop, 0)
        self.rosspad_UTC = np.delete(self.rosspad_UTC,rowsToDrop)

        #add eventID column
        self.rosspad_eventIDs = [0 for x in range(len(self.rosspad_data))]
        self.rosspad_droppedTrigAck = [0 for x in range(len(self.rosspad_data))]

        timesToAdd = self.arduino_UTC[self.arduino_data[:,self.id_idx] == "C"]
        eventIDsToAdd = self.eventIDs[self.arduino_data[:,self.id_idx] == "C"].astype(int)
        noTrigAckFlag = np.zeros(len(timesToAdd))
        nAddedWithTrigAck = len(timesToAdd)
        #add eventIDs that came without TrigAcks
        timesToAdd = np.append(timesToAdd,self.eventIDsNoTrigAcks[:,self.utcc_idx])
        eventIDsToAdd = np.append(eventIDsToAdd,self.eventIDsNoTrigAcks[:,self.timestamp_idx].astype(int))
        noTrigAckFlag = np.append(noTrigAckFlag,np.ones(len(timesToAdd)-nAddedWithTrigAck))
        nRowsToAdd = len(timesToAdd)

        # XXX DS-202201, dont know what this is or if I need it
        # print(len(noTrigAckFlag),nRowsToAdd)

        trigack_data = np.zeros((len(timesToAdd),np.shape(self.rosspad_data)[1]))
        trigack_data[:,self.pps_idx] = False
        trigack_data[:,self.trigack_idx] = True
        trigack_UTC = timesToAdd
        trigack_eventIDs = eventIDsToAdd
        trigack_droppedTrigAck = noTrigAckFlag

        self.rosspad_data = np.append(self.rosspad_data,trigack_data,axis = 0)
        self.rosspad_UTC = np.append(self.rosspad_UTC,trigack_UTC).astype(float)
        self.rosspad_eventIDs = np.append(self.rosspad_eventIDs,trigack_eventIDs)
        self.rosspad_droppedTrigAck = np.append(self.rosspad_droppedTrigAck,trigack_droppedTrigAck)

        self.rosspad_data = self.rosspad_data[self.rosspad_UTC.argsort(),:]
        self.rosspad_eventIDs = self.rosspad_eventIDs[self.rosspad_UTC.argsort()]
        self.rosspad_droppedTrigAck = self.rosspad_droppedTrigAck[self.rosspad_UTC.argsort()]
        self.rosspad_UTC = self.rosspad_UTC[self.rosspad_UTC.argsort()]


    #write edited dataframe to new hdf files
    def write_files(self):
        #drop timestamp and sortcol columns

        path = Path(self.rfname)
        outpath = path.with_suffix('.L1.1.h5')

        with h5.File(outpath,'w') as h5f:
            h5f.create_dataset('pps', data = self.rosspad_data[:,self.pps_idx])
            h5f.create_dataset('trigack', data = self.rosspad_data[:,self.trigack_idx])
            h5f.create_dataset('frame', data = self.rosspad_data[:,self.frame_idx])
            
            rosspad_data = np.delete(self.rosspad_data,[self.frame_idx,
                                                            self.timestamp_idx,
                                                            self.gpio_idx,
                                                            self.pps_idx,
                                                            self.trigack_idx], axis = 1)
            h5f.create_dataset('ADC', data = rosspad_data)

            h5f.create_dataset('EventID', data = self.rosspad_eventIDs)
            h5f.create_dataset('UTC', data = self.rosspad_UTC)

        print("File written: ")
        print(outpath)

if __name__ == '__main__':

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("rfname",help="rosspad hdf5 file")
    parser.add_argument("afname",nargs='+',help="arduino log file")
    args = parser.parse_args()

    R = RosspadToL1_1(args.rfname,args.afname)
    R.process()
