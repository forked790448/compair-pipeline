#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
sys.path.insert(1, 'randomScripts')
import os


import pandas as pd
pd.options.mode.chained_assignment = None

import warnings
warnings.simplefilter("ignore")


# plotting
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.pylab as pylab


#saving lists
import pickle

# random
import NRL_Math as nrl

# Calibration functions
import create_calibration_mask as calibration


# Edge Detection
from PIL import Image as im
from PIL import ImageFilter
from numpy import asarray



# plot setting stuff
plt.rcParams['font.size'] = 15
plt.rcParams['font.family'] = "serif"
tdir = 'in'
major = 5.0
minor = 3.0
plt.rcParams['xtick.direction'] = tdir
plt.rcParams['ytick.direction'] = tdir
plt.rcParams['xtick.major.size'] = major
plt.rcParams['xtick.minor.size'] = minor
plt.rcParams['ytick.major.size'] = major
plt.rcParams['ytick.minor.size'] = minor

params = {'legend.fontsize': 'x-large',
         'axes.labelsize': 'x-large',
         'axes.titlesize':'x-large',
         'xtick.labelsize':'x-large',
         'ytick.labelsize':'x-large'}
pylab.rcParams.update(params)

        

class CalibrationObj:
    def __init__(self):
        pass
        
    def append(self, gainCorrection, centerLoc, rangeScale):
        
        try:
            self.gainCorrection = np.dstack((self.gainCorrection, gainCorrection))
            self.centerLoc = np.vstack([self.centerLoc, centerLoc])
            self.rangeScale = np.vstack([self.rangeScale, rangeScale])
        except:    
            self.gainCorrection = gainCorrection
            self.centerLoc = centerLoc
            self.rangeScale = rangeScale
            
            
def loadMasks(inEnergy):
    energy = inEnergy
    filename = parentName + f'/calibrationData{energy}.hdf5'
    tmp_df = pd.read_hdf(filename,key='ADC')
    
    gainCorrection = tmp_df.to_numpy()
    
    tmp_df = pd.read_hdf(filename,key='POS')
    tmpArr = np.transpose(tmp_df.to_numpy())
    centerLoc = tmpArr[0,:]
    rangeScale = tmpArr[1,:]
    
    
        
    return (gainCorrection, centerLoc, rangeScale);

def addEnergies():
    # pull these log-energies from older calibration
    logsToAdd = [9,15]
    energiesToAdd = [[1274, 2614], [1274, 2614]]
                    
    # add energies 
    for i in range(0, len(logsToAdd)):
        for j in range(0, len(energiesToAdd[i]) ):
            idx = np.where(np.array(energies) == energiesToAdd[i][j])[0][0] 

            cali.gainCorrection[idx, logsToAdd[i]] = caliOld.gainCorrection[idx, logsToAdd[i]]
            cali.centerLoc[idx, logsToAdd[i]] = caliOld.centerLoc[idx, logsToAdd[i]]
            cali.rangeScale[idx, logsToAdd[i]] = caliOld.rangeScale[idx, logsToAdd[i]]

    

def censorEnergies(x, yGain, logNumber, cali):
    # remove these log-energies from calibration
    logsToCensor = [9, 15]
    energiesToCensor = [[511], [511]]
    

    
    # remove energies
    for i in range(0, len(logsToCensor)):
        for j in range(0, len(energiesToCensor[i]) ):
            if logNumber == logsToCensor[i] and cali.cmis_gain == 7:
                idx = np.where(yGain == energiesToCensor[i][j])[0][0] 
                x = np.delete(x, idx)
                yGain = np.delete(yGain, idx)


    return x, yGain


def getPositionNonLinearity(energies, cali, toPlot):
    
    calibMode = 2 # 1 for max position, 2 for non-linearity
    
    posCenterNonLinearity = []
    posRangeNonLinearity = []
    logNumber = 0

    if toPlot:
        f, axarr = plt.subplots(5, 6)
        
    for layer_id in range(0, 5):
        for row_id in range(0, 6):
            x = np.array(energies)

            
            if calibMode == 1:
                ### take max of each each calibration
            
                # posCenterNonLinearity.append(cali.centerLoc[:,logNumber].max())
                # posRangeNonLinearity.append(cali.rangeScale[:,logNumber].max())
            
                # for now, lets pick 511 uncomment below
                posCenterNonLinearity.append(cali.centerLoc[0,logNumber])
                posRangeNonLinearity.append(cali.rangeScale[0,logNumber])
            
            else:
                # develop quadratic calibration curve
                yCenter = cali.centerLoc[:,logNumber]
                yRange = 100/cali.rangeScale[:,logNumber]
                
                yCenter, _ = censorEnergies(yCenter, x, logNumber, cali)
                yRange, x = censorEnergies(yRange, x, logNumber, cali)

    
                
                modelCenter = np.poly1d(np.polyfit(x, yCenter, 2))
                modelRange = np.poly1d(np.polyfit(x, yRange, 2))
                # coefficients are c[0] x^2 + c[1] x + c[2]
                # x is energy
    
                polyline = np.linspace(1, max(x), 200)
                posCenterNonLinearity.append(modelCenter)
                posRangeNonLinearity.append(modelRange)
    
                ### below is for plotting
                if toPlot:
                    axarr[layer_id,row_id].scatter(x, yCenter, s=50)
                    axarr[layer_id,row_id].errorbar(x, yCenter, yerr=yRange/2, label='both limits (default)')
    
                    axarr[layer_id,row_id].plot(polyline, modelCenter(polyline), 'r')
                    axarr[layer_id,row_id].plot(polyline, modelRange(polyline)/2 + modelCenter(polyline), 'g')
                    axarr[layer_id,row_id].plot(polyline, -modelRange(polyline)/2 + modelCenter(polyline), 'g')
    
                    axarr[layer_id,row_id].set_ylim([-1, 1])
        
                    axarr[layer_id,row_id].tick_params(axis='both', which='major', labelsize=8)
                    axarr[layer_id,row_id].tick_params(axis='both', which='minor', labelsize=8)
          
                logNumber = logNumber + 1
    
    if toPlot and calibMode == 2:
        f.text(0.5, 0.04, 'Energy [keV]', ha='center')
        f.text(0.04, 0.5, 'Position [arb]', va='center', rotation='vertical')
        nrl.set_size(f, (8, 6))

        f.savefig(parentName +'posQuadFit.png', dpi = 600, bbox_inches = 'tight', facecolor='w')
    
    return posCenterNonLinearity, posRangeNonLinearity


def getEnergyNonLinearity(energies, cali, toPlot):
    
    nDepth = cali.gainCorrection.shape[0]
    gainNonLinearity = []


    logNumber = 0

    if toPlot:
        f, axarr = plt.subplots(5, 6)
        
    for layer_id in range(0, 5):
        for row_id in range(0, 6):
            gainNonLinDepth = []
            
            for depth in range(0, nDepth):
                yGain = np.array(energies)
                yGain = np.append([0], yGain) #add a zero intercept
                
                x = cali.gainCorrection[depth,logNumber, :]
                x = np.append([0], x) #add a zero intercept
                x, yGain = censorEnergies(x, yGain, logNumber, cali)


                x = yGain / x
                nan_idx = np.isnan(x)
                x[nan_idx] = 0

                polyline = np.linspace(1, 3000, 100)

                # c[0] x^2 + c[1] x + c[2]
                modelGain = np.poly1d(np.polyfit(x, yGain, 2))
                
                if modelGain[2] < 0:
                    modelGain[2] = 0
                                
                gainNonLinDepth.append(modelGain)
    
                ### below is for plotting
                if toPlot:
                    axarr[layer_id,row_id].scatter(yGain, x, s=40)
                    axarr[layer_id,row_id].plot(modelGain(polyline), polyline, 'r')
                    
                    axarr[layer_id,row_id].set_xlim([0, 2000])
                    axarr[layer_id,row_id].set_ylim([0, 200])



                    # axarr[layer_id,row_id].tick_params(axis='both', which='minor', labelsize=8)
                    if logNumber != 24:
                        axarr[layer_id,row_id].get_xaxis().set_visible(False)
                        axarr[layer_id,row_id].get_yaxis().set_visible(False)
                    else:
                        axarr[layer_id,row_id].tick_params(axis='y', which='major', labelsize=8, rotation = 90)
                        axarr[layer_id,row_id].tick_params(axis='x', which='major', labelsize=8)

                        axarr[layer_id,row_id].tick_params(axis='both', which='minor', labelsize=8)

                    # axarr[layer_id,row_id].set_ylim([0, 700])

            gainNonLinearity.append(gainNonLinDepth)

            logNumber = logNumber + 1
            print(logNumber)

    
    if toPlot:
        f.text(0.5, 0.04, 'True Energy [keV]', ha='center')
        f.text(0.04, 0.5, 'Recorded ADC Unit', va='center', rotation='vertical')
        nrl.set_size(f, (8, 6))

        f.savefig(parentName +'gainQuadFit.png', dpi = 600, bbox_inches = 'tight', facecolor='w')
    
    return gainNonLinearity
    
      
# %% Initialize variables

parentName = '../../../data/Calibrations/DuringBeam/calibrations'
energies = [511, 662, 1274, 2614]


# %% Load Data
cali = CalibrationObj()
cali.cmis_gain = 7

for i in range(0, len(energies)):
    cali.append(*loadMasks(energies[i]))
    
    
caliOld = CalibrationObj()
caliOld.cmis_gain = 7
parentName = '../../../data/Calibrations/DuringBeam/calibrations'
for i in range(0, len(energies)):
    caliOld.append(*loadMasks(energies[i]))
addEnergies()
parentName = '../../../data/Calibrations/DuringBeam/calibrations'    
# %% some Data processing

posCenterNonLinearity, posRangeNonLinearity = getPositionNonLinearity(energies, cali, True)
gainNonLinearity = getEnergyNonLinearity(energies, cali, True)



# %% replace with old calibrations

#########################


filename = 'rosspadConfigData/calibration/calibrationData'

with open(filename+"_posCenter.txt", "rb") as fp:
    posCenterOld =  pickle.load(fp)

with open(filename+"_posRange.txt", "rb") as fp:
    posRangeNonLinearityOld = pickle.load(fp)
    
with open(filename+"_gain.txt", "rb") as fp:
    gainOld = pickle.load(fp)
    
    
    
logs2Replace = [9, 15]
for i in range(0, len(logs2Replace)):
    print(logs2Replace[i])
    
    posCenterNonLinearity[i] = posCenterOld[i]
    posRangeNonLinearity[i] = posRangeNonLinearityOld[i]
    gainNonLinearity[i] = gainOld[i]

# %% Print Calibrations to File

filename = parentName + '/calibrationData'
print('Writing Calibration Data to File: ' + filename)

# folderExists = os.path.isdir(filename)
# if folderExists == False:
#     os.mkdir(filename)



with open(filename+"_posCenter.txt", "wb") as fp:
    pickle.dump(posCenterNonLinearity, fp)

with open(filename+"_posRange.txt", "wb") as fp:
    pickle.dump(posRangeNonLinearity, fp)
    
with open(filename+"_gain.txt", "wb") as fp:
    pickle.dump(gainNonLinearity, fp)








    
