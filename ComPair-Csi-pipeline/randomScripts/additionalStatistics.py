#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 13 15:39:07 2021

@author: dshy
"""

import numpy as np
import matplotlib.pyplot as plt
import NRL_Math as nrl




def getUpperThreshold(parent_dir, gainCorrection): 
    # peaks = [np.argmax(y)]  
    DRPerLog = np.zeros(shape=(gainCorrection.shape[1],))

    for i in range(gainCorrection.shape[1]):
        DRPerLog[i] = np.mean(gainCorrection[:,i])*(2**12)/1000
    
    
    
    f = plt.figure()
    ax = f.add_subplot(1, 1, 1)
    plt.scatter(np.linspace(0, gainCorrection.shape[1]-1 , gainCorrection.shape[1]), DRPerLog, s=125)
    plt.xlabel('Log Index')
    plt.ylabel('Upper Limit of Dynamic Range')
    plt.axvspan(-0.5, 7.5, color='r', alpha=0.5, lw=0, label="ASIC0")
    plt.axvspan(7.5, 15.5, color='g', alpha=0.5, lw=0, label="ASIC1")
    plt.axvspan(15.5, 23.5, color='b', alpha=0.5, lw=0, label="ASIC2")
    plt.axvspan(23.5, 30.5, color='y', alpha=0.5, lw=0, label="ASIC3")

    plt.legend(loc='best',  frameon=False)
    nrl.set_size(f, (8, 6))
    plt.show()
    
    f.savefig(parent_dir +'upperLimitDynamicRange.png', dpi = 900, bbox_inches = 'tight', facecolor='w')
    
    
    print('Mean: ', np.median(DRPerLog))
    
    
    
    
def getLowerThreshold(x, y, energyOfInterest): 
    # peaks = [np.argmax(y)]
    help = 1
    
    y[np.abs(x - (energyOfInterest-0.3*energyOfInterest)).argmin():len(y)] = 0
    
    y[0:np.abs(x - (60)).argmin()] = 100000000

    
    
    return x[np.argmax(np.diff(y))]
    
    

    
