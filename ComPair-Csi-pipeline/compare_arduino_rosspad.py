import pandas
import numpy as np
import matplotlib.pyplot as plt
plt.style.use('pretty')
import sys
import lineup_logfiles

def compare(args):

	_rosspad_df = pandas.read_hdf(args.rfname,key='ADC')
	_arduino_df = pandas.read_csv(args.afname,names=["ID","Clock","UTCA","UTCC"])

	print(_rosspad_df[_rosspad_df.pps == True].index.to_numpy()[0:50])
#	print(_rosspad_df.index.to_numpy()[0:100])
	_rosspad_df.index = np.arange(len(_rosspad_df))
	print(_rosspad_df[_rosspad_df.pps == True].index.to_numpy()[0:50])

	#get rid of superfluous lines in rosspad
#	_rosspad_df = _rosspad_df[_rosspad_df.asic_0 == 0]

	#It SEEMS like a nice idea to sort here, before lining up the data
	#however, doing so leads to everything going to shit
	#I think because the data is weird while the rosspad UI script is running
	#there are way more TrigOuts than other things,
	# and a different number of rollovers of each signal type
	#easier to sort later and get rid of extra PPS's at beginning than to fix it
#	_rosspad_df = sort_rosspad(_rosspad_df)

	#line up dataframes
	rpd_start, ard_start = lineup_logfiles.lineup(_rosspad_df,_arduino_df,key=None)
	print("rosspad start:",rpd_start)
	print("arduino start:",ard_start)

	rosspad_df = _rosspad_df[rpd_start:]
	arduino_df = _arduino_df[ard_start:]
	rosspad_df = rosspad_df.reset_index(drop=True)
	arduino_df = arduino_df.reset_index(drop=True)

	#check arduino for missing event IDs / Makoto signals
	check_arduino_stream(arduino_df)
	print("-----------------------------------------------------------\n")

	#sort rosspad dataframe by timestamp
	rosspad_df = sort_rosspad(rosspad_df)

	#sort arduino dataframe by timestamp
	arduino_df = sort_arduino(arduino_df)

	#sanity check: are the timestamps in order?
	rpd_tstamps = rosspad_df.timestamp.to_numpy()
	rpd_tstamps_diff = np.diff(rpd_tstamps)
	rpd_tstamps_diff = np.array([x+2**32 if x < -1000000000 else x for x in rpd_tstamps_diff])
	print("rosspad out of order?",np.count_nonzero(rpd_tstamps_diff < 0))

	ard_tstamps = arduino_df[arduino_df.ID != 'E'].Clock.to_numpy()
	ard_tstamps_diff = np.diff(ard_tstamps)
	ard_tstamps_diff = np.array([x+2**32 if x < -1000000000 else x for x in ard_tstamps_diff])
	print("arduino out of order?",np.count_nonzero(ard_tstamps_diff < 0),"/",len(ard_tstamps_diff))
	print(ard_tstamps[np.argwhere(ard_tstamps_diff<0).flatten()])

	print(rosspad_df.index[rosspad_df.pps==True].to_numpy()[0:10])
#	sys.exit(0)

	#check that first row in both dataframes is a PPS...
	if rosspad_df.iloc[0].pps == False:
		print("Check rosspad start line")
		return

	if arduino_df.iloc[0].ID != "P":
		print("Check arduino start line")
		return


	#get total length of dataset where arduino and rosspad overlap and number of rosspad TrigOuts
	ard_pps = arduino_df[arduino_df.ID == 'P'].UTCA.to_numpy()
	ard_pps_inds = arduino_df[arduino_df.ID == "P"].index.to_numpy()
	rpd_pps_inds = rosspad_df[rosspad_df.pps == True].index.to_numpy()
	minLen = min(len(ard_pps_inds),len(rpd_pps_inds))
	lastArdInd = ard_pps_inds[minLen-1]
	lastRpdInd = rpd_pps_inds[minLen-1]
	arduino_df = arduino_df[:lastArdInd+1]
	rosspad_df = rosspad_df[:lastRpdInd+1]

	total_time = ard_pps[minLen-1] - ard_pps[0]
	print("dataset length:",total_time/60,"minutes")
	print("\n")


	rosspad_df, arduino_df = pps_regularity(rosspad_df, arduino_df)
	print("-----------------------------------------------------------\n")

	inds_disagree_trigOut, inds_disagree_trigIn = compareNumTriggersSplit(rosspad_df, arduino_df)
	print("-----------------------------------------------------------\n")

#	print_dropped_trigs(rosspad_df, arduino_df, inds_disagree_trigOut, inds_disagree_trigIn)
#	print("-----------------------------------------------------------\n")

#	timestampAgreementBetweenSystems(rosspad_df, arduino_df, inds_disagree_trigOut, inds_disagree_trigIn)
#	print("-----------------------------------------------------------\n")

#	trigOutTimestampDiff(rosspad_df, arduino_df)
#	extra_arduino_trigOuts(rosspad_df,arduino_df)
	trigOutTimestampAgreementAfterLineup(rosspad_df,arduino_df)
	return

	trigInTrigOutComparison(rosspad_df, arduino_df)


#	compareNumTriggersPerPPS(rosspad_df, arduino_df) #NOTE: this function doesn't work with the 32 bit event ID in the arduino datastream
#	timestampAgreementWithinSystem(rosspad_df, arduino_df) #NOTE: this function is pointless if TrigIn not hooked to TrigOut


def sort_rosspad(rosspad_df):

	#separate to find the rollovers by looking at timestamp diff
	pps_df = rosspad_df[rosspad_df.pps == True]
	trigOut_df = rosspad_df[(rosspad_df.pps == False) & (rosspad_df.trigack == False)]
	trigIn_df = rosspad_df[rosspad_df.trigack == True]

	#first pps
	pps_tstamps = pps_df.timestamp.to_numpy()
	pps_tstamp_diff = np.diff(pps_tstamps)
	pps_rollover_inds = np.argwhere(pps_tstamp_diff < 0).flatten()
	#start at -1 or else the sortcol and the timestamps are offset by 1
	# and it will take you 10 hours to figure out why nothing works
	pps_rollover_inds = np.insert(pps_rollover_inds,0,-1)
	pps_rollover_inds = np.append(pps_rollover_inds,len(pps_tstamps))

	print("PPS",len(pps_rollover_inds))
#	print(pps_df['timestamp'][20:50])

	pps_sortcol = []
	counter = 0
	for i,j in zip(pps_rollover_inds[:-1],pps_rollover_inds[1:]):
		pps_sortcol.extend([counter for x in range(j-i)])
		counter += 1

	#remove last element of sortcol, since it's extra
	pps_sortcol = np.array(pps_sortcol[:-1])
	pps_sortcol = (pps_sortcol << 32) | pps_tstamps


	print("\nI don't know why it makes this warning...")
	pps_df.loc[:,'sortcol'] = pps_sortcol
	print("")


	#next trigout
	trigOut_tstamps = trigOut_df.timestamp.to_numpy()
	trigOut_tstamp_diff = np.diff(trigOut_tstamps)
	trigOut_rollover_inds = np.argwhere(trigOut_tstamp_diff < 0).flatten()
	trigOut_rollover_inds = np.insert(trigOut_rollover_inds,0,-1)
	trigOut_rollover_inds = np.append(trigOut_rollover_inds,len(trigOut_tstamps))

	print("TrigOut",len(trigOut_rollover_inds))
	trigOut_sortcol = []
	counter = 0
	for i,j in zip(trigOut_rollover_inds[:-1],trigOut_rollover_inds[1:]):
		trigOut_sortcol.extend([counter for x in range(j-i)])
		counter += 1

	trigOut_sortcol = np.array(trigOut_sortcol[:-1])
	trigOut_sortcol = (trigOut_sortcol << 32) | trigOut_tstamps

	trigOut_df.loc[:,'sortcol'] = trigOut_sortcol

	#finally trigin
	trigIn_tstamps = trigIn_df.timestamp.to_numpy()
	trigIn_tstamp_diff = np.diff(trigIn_tstamps)
	trigIn_rollover_inds = np.argwhere(trigIn_tstamp_diff < 0).flatten()
	trigIn_rollover_inds = np.insert(trigIn_rollover_inds,0,-1)
	trigIn_rollover_inds = np.append(trigIn_rollover_inds,len(trigIn_tstamps))

	print("TrigIn",len(trigIn_rollover_inds))
	trigIn_sortcol = []
	counter = 0
	for i,j in zip(trigIn_rollover_inds[:-1],trigIn_rollover_inds[1:]):
		trigIn_sortcol.extend([counter for x in range(j-i)])
		counter += 1

	trigIn_sortcol = np.array(trigIn_sortcol[:-1])
	trigIn_sortcol = (trigIn_sortcol << 32) | trigIn_tstamps

	trigIn_df.loc[:,'sortcol'] = trigIn_sortcol

	#now re-combine dataframes
	combined_df = pps_df.append(trigOut_df,sort=True)
	combined_df = combined_df.append(trigIn_df,sort=True)
	combined_df.sort_index(inplace=True)

	sc = combined_df.sortcol.to_numpy()

	#sort combined df by sortcol
	combined_df = combined_df.sort_values(by=['sortcol'])
	sc = combined_df.sortcol.to_numpy()

	combined_df.drop(columns=['sortcol'])

	new_tstamps = combined_df.timestamp.to_numpy()
	old_tstamps = rosspad_df.timestamp.to_numpy()
	print(len(new_tstamps),len(old_tstamps))
	print(np.count_nonzero(new_tstamps-old_tstamps))

#	print(combined_df.index.to_numpy()[40:50])
#	print(rosspad_df.index.to_numpy()[40:50])

#	print(rosspad_df[0:50])
#	print(combined_df[['timestamp','pps','trig']][0:50])
	combined_df = combined_df.reset_index(drop=True)



	#check if there are multiple PPS's in a row at the beginning, and if so, drop
	# this can happen if it was out of order before
	new_start_ind = 0
	first_trig_out = 0
	isPPS = combined_df.pps.to_numpy()
	isTrigIn = combined_df.trigack.to_numpy()
	for i in range(len(isPPS)):
		if isPPS[i] == False and isTrigIn[i] == False:
			first_trig_out = i
			break
	new_start_ind = first_trig_out-1
	while isPPS[new_start_ind] != True:
		new_start_ind -= 1
	combined_df = combined_df[new_start_ind:]

	print("")
	return combined_df


def arduino_cleanup(arduino_df):
	origUTCA = arduino_df.UTCA.to_numpy()
	ID = arduino_df.ID.to_numpy()

	#column of only PPS UTC times
	newUTCA = np.array([origUTCA[i] if ID[i] == 'P' else np.nan for i in range(len(ID))])

	#column for missed interrupt flag on the arduino
	missedInterrupt = np.array([True if x == 2 else False for x in origUTCA])

	#column for event IDs
	timestamps = arduino_df.Clock.to_numpy()
	newTimes = np.array([timestamps[i] if ID[i] != 'E' else np.nan for i in range(len(ID))])
	eventID = np.array([timestamps[i] if ID[i] == 'E' else np.nan for i in range(len(ID))])

	#add new / modified columns
	arduino_df.UTCA = newUTCA
	arduino_df.Clock = newTimes
	arduino_df['MissedInterrupt'] = missedInterrupt

	#combine TrigAcks and EventIDs
	eventIDdf = pandas.DataFrame(eventID,columns=['EventID'])
	#since EventID comes after TrigACK, drop index by 1 for merging
	eventIDdf.index = eventIDdf.index.to_numpy() - 1

	arduino_df = arduino_df[arduino_df.ID != 'E']
	arduino_df = pandas.merge(left=arduino_df,
														right=eventIDdf,
														left_index=True, right_index=True)

	arduino_df = arduino_df.reset_index(drop=True)
	return arduino_df

def sort_arduino(arduino_df):

	#merge event ID with TrigAck before sorting
	arduino_df = arduino_cleanup(arduino_df)

	#separate to find the rollovers by looking at timestamp diff
	pps_df = arduino_df[arduino_df.ID == "P"]
	trigOut_df = arduino_df[arduino_df.ID == "R"]
	trigIn_df = arduino_df[arduino_df.ID == "C"]

	#first pps
	pps_tstamps = pps_df.Clock.to_numpy()
	pps_tstamp_diff = np.diff(pps_tstamps)
	pps_rollover_inds = np.argwhere(pps_tstamp_diff < 0).flatten()
	#start at -1 or else the sortcol and the timestamps are offset by 1
	# and it will take you 10 hours to figure out why nothing works
	pps_rollover_inds = np.insert(pps_rollover_inds,0,-1)
	pps_rollover_inds = np.append(pps_rollover_inds,len(pps_tstamps))

	print("PPS",len(pps_rollover_inds))
#	print(pps_df['timestamp'][20:50])

	pps_sortcol = []
	counter = 0
	for i,j in zip(pps_rollover_inds[:-1],pps_rollover_inds[1:]):
		pps_sortcol.extend([counter for x in range(j-i)])
		counter += 1

	#remove last element of sortcol, since it's extra
	pps_sortcol = np.array(pps_sortcol[:-1])
	pps_tstamps = np.array(pps_tstamps,dtype=int)
	pps_sortcol = (pps_sortcol << 32) | pps_tstamps


	print("\nI don't know why it makes this warning...")
	pps_df.loc[:,'sortcol'] = pps_sortcol
	print("")


	#next trigout
	trigOut_tstamps = trigOut_df.Clock.to_numpy()
	trigOut_tstamp_diff = np.diff(trigOut_tstamps)
	trigOut_rollover_inds = np.argwhere(trigOut_tstamp_diff < 0).flatten()
	trigOut_rollover_inds = np.insert(trigOut_rollover_inds,0,-1)
	trigOut_rollover_inds = np.append(trigOut_rollover_inds,len(trigOut_tstamps))

	print("TrigOut",len(trigOut_rollover_inds))
	trigOut_sortcol = []
	counter = 0
	for i,j in zip(trigOut_rollover_inds[:-1],trigOut_rollover_inds[1:]):
		trigOut_sortcol.extend([counter for x in range(j-i)])
		counter += 1

	trigOut_sortcol = np.array(trigOut_sortcol[:-1])
	trigOut_tstamps = np.array(trigOut_tstamps,dtype=int)
	trigOut_sortcol = (trigOut_sortcol << 32) | trigOut_tstamps

	trigOut_df.loc[:,'sortcol'] = trigOut_sortcol

	#finally trigin
	trigIn_tstamps = trigIn_df.Clock.to_numpy()
	trigIn_tstamp_diff = np.diff(trigIn_tstamps)
	trigIn_rollover_inds = np.argwhere(trigIn_tstamp_diff < 0).flatten()
	trigIn_rollover_inds = np.insert(trigIn_rollover_inds,0,-1)
	trigIn_rollover_inds = np.append(trigIn_rollover_inds,len(trigIn_tstamps))

	print("TrigIn",len(trigIn_rollover_inds))
	trigIn_sortcol = []
	counter = 0
	for i,j in zip(trigIn_rollover_inds[:-1],trigIn_rollover_inds[1:]):
		trigIn_sortcol.extend([counter for x in range(j-i)])
		counter += 1

	trigIn_sortcol = np.array(trigIn_sortcol[:-1])
	trigIn_tstamps = np.array(trigIn_tstamps,dtype=int)
	trigIn_sortcol = (trigIn_sortcol << 32) | trigIn_tstamps

	trigIn_df.loc[:,'sortcol'] = trigIn_sortcol

	#now re-combine dataframes
	combined_df = pps_df.append(trigOut_df,sort=True)
	combined_df = combined_df.append(trigIn_df,sort=True)
	combined_df.sort_index(inplace=True)

	sc = combined_df.sortcol.to_numpy()

	#sort combined df by sortcol
	combined_df = combined_df.sort_values(by=['sortcol'])
	sc = combined_df.sortcol.to_numpy()

	combined_df.drop(columns=['sortcol'])

	new_tstamps = combined_df.Clock.to_numpy()
	old_tstamps = arduino_df.Clock.to_numpy()
	print(len(new_tstamps),len(old_tstamps))
	print(np.count_nonzero(new_tstamps-old_tstamps))

#	print(combined_df.index.to_numpy()[40:50])
#	print(arduino_df.index.to_numpy()[40:50])

#	print(arduino_df[0:50])
#	print(combined_df[['timestamp','pps','trig']][0:50])
	combined_df = combined_df.reset_index(drop=True)

	return combined_df


def pps_regularity(rosspad_df, arduino_df):

	rosspad_freq = 100e6 #supposed rosspad clock frequency
	arduino_freq = 42e6  #supposed arduino clock frequency

	rosspad_pps = rosspad_df[rosspad_df.pps == True].timestamp.to_numpy()
	rosspad_pps_diff = np.diff(rosspad_pps)
	rosspad_pps_diff = np.array([x+2**32-1 if x<0 else x for x in rosspad_pps_diff])
	print("rosspad PPS: average =",np.mean(rosspad_pps_diff),"min =",min(rosspad_pps_diff),"max =",max(rosspad_pps_diff))
	indices_to_plot = np.argwhere((rosspad_pps_diff/rosspad_freq > 0.9) & (rosspad_pps_diff/rosspad_freq < 1.1)).flatten()
	rosspad_pps_diff_to_plot = rosspad_pps_diff[indices_to_plot]
#	plt.hist(rosspad_freq-rosspad_pps_diff_to_plot,bins=20,histtype='step')
#	plt.xlabel("100e6 minus number of clock ticks per second")
#	plt.ylabel("Number of seconds")
#	plt.title("ROSSPAD")
#	plt.show()

	arduino_pps = arduino_df[arduino_df.ID == "P"].Clock.to_numpy()
	arduino_pps_diff = np.diff(arduino_pps)
	arduino_pps_diff = np.array([x+2**32-1 if x<0 else x for x in arduino_pps_diff])
	print("arduino PPS: average =",np.mean(arduino_pps_diff),"min =",min(arduino_pps_diff),"max =",max(arduino_pps_diff))
	indices_to_plot = np.argwhere((arduino_pps_diff/arduino_freq > 0.9) & (arduino_pps_diff/arduino_freq < 1.1)).flatten()
	arduino_pps_diff_to_plot = arduino_pps_diff[indices_to_plot]
#	plt.hist(arduino_freq-arduino_pps_diff_to_plot,bins=20,histtype='step')
#	plt.xlabel("42e6 minus number of clock ticks per second")
#	plt.ylabel("Number of seconds")
#	plt.title("Arduino")
#	plt.show()

	#since the rosspad seems to add PPS's sometimes (very rarely), find them so I can get rid of them in later analysis
	rosspad_pps_diff_secs = rosspad_pps_diff/rosspad_freq
	rosspad_pps_inds = rosspad_df[rosspad_df.pps == True].index.to_numpy()
	extra_pps = np.argwhere(rosspad_pps_diff_secs < 0.9).flatten()
	splits = np.append(-1,np.argwhere(np.diff(extra_pps) != 1).flatten())
	splits = np.append(splits,len(extra_pps))
#	print("extra_pps",extra_pps)
#	print(rosspad_pps_diff_secs[extra_pps])
#	print(splits)
	lost_pps = np.argwhere(rosspad_pps_diff_secs > 1.1).flatten()
#	print("lost_pps",lost_pps)
#	print(rosspad_pps_inds[lost_pps])
#	print(rosspad_pps_diff_secs[lost_pps])
	for indStart, indStop in zip(splits[:-1],splits[1:]):
		inds_to_remove = extra_pps[indStart+1:indStop+1]
#		print(np.sum(rosspad_pps_diff_secs[inds_to_remove]))
#		print("")
		if len(inds_to_remove) > 1:
			inds_to_remove = np.delete(inds_to_remove,np.argwhere(inds_to_remove == max(inds_to_remove)))
			for I in inds_to_remove:
				rosspad_df = rosspad_df.drop([rosspad_pps_inds[I]])
		

	rosspad_df = rosspad_df.reset_index(drop=True)

#	print(rosspad_df[['timestamp','pps','trig']][330:390])

	#and it seems as if the arduino can also add extra PPS's
	arduino_pps_diff_secs = arduino_pps_diff/arduino_freq
	arduino_pps_inds = arduino_df[arduino_df.ID == "P"].index.to_numpy()
	extra_pps_1 = np.argwhere(arduino_pps_diff_secs < 0.5).flatten()
	extra_pps_2 = np.argwhere((arduino_pps_diff_secs > 0.5) & (arduino_pps_diff_secs < 0.9)).flatten()
#	print(arduino_pps_diff_secs[extra_pps_1])
#	print(arduino_pps_diff_secs[extra_pps_2])
	for i1,i2 in zip(extra_pps_1,extra_pps_2):
		index_to_remove = arduino_pps_inds[max(i1,i2)]
#		print(arduino_df.iloc[index_to_remove])
		arduino_df = arduino_df.drop([index_to_remove])
	arduino_df = arduino_df.reset_index(drop=True)

	return rosspad_df,arduino_df


def check_arduino_stream(arduino_df):

	#check if same number of event IDs and TrigIns
	trigIn_df = arduino_df[arduino_df.ID == "C"]
	eventID_df = arduino_df[arduino_df.ID == "E"]
	print("Num TrigIns: ",len(trigIn_df),"\tNum EventIDs: ",len(eventID_df))

	#check if event IDs are skipped
#	print(eventID_df.columns)
	eventIDs = eventID_df.Clock.to_numpy()
	eventID_diff = np.diff(eventIDs)
#	print(eventIDs)
#	print(eventID_diff)
	if np.all(eventID_diff == 1):
		print("no event IDs skipped")
	else:
		nSkipped = np.count_nonzero(eventID_diff != 1)
		print("event IDs skipped",nSkipped,"times!")	

#	sys.exit(0)



def compareNumTriggersPerPPS(rosspad_df, arduino_df):

	#now make sure there are the same number of rosspad triggers per second
	#numpy array of dframe indexes that are PPS's
	rosspad_pps_inds = rosspad_df.index[rosspad_df.pps == True].to_numpy()
	arduino_pps_inds = arduino_df.index[arduino_df.ID == "P"].to_numpy()


	rpd_tps = np.diff(rosspad_pps_inds)-1 
	ard_tps = np.diff(arduino_pps_inds)-1

	#they might be different lengths depending on when I stopped the runs...
	#truncate longer one since it doesn't matter
	minLen = min(len(rpd_tps),len(ard_tps))
	rpd_tps = rpd_tps[0:minLen]
	ard_tps = ard_tps[0:minLen]


	tps_agree = (rpd_tps == ard_tps)
	if np.all(tps_agree):
		print("Number of total triggers between PPS's: data streams agree!")
		print("")
	else:
		inds_disagree = np.argwhere(tps_agree == False).flatten()
		print("Number of total triggers between PPS's:")
		print(len(inds_disagree),"disagree")
		print(inds_disagree)
		print("")

		#print out number of trigger outs and trigger ins for each system
#		for index in inds_disagree:
#			rInd = rosspad_pps_inds[index]
#			rIndEnd = rosspad_pps_inds[index+1]
#			aInd = arduino_pps_inds[index]
#			aIndEnd = arduino_pps_inds[index+1]

#			sub_rpd = rosspad_df.iloc[rInd+1:rIndEnd]
#			print(sub_rpd['chan_1'].value_counts())

#			sub_ard = arduino_df.iloc[aInd+1:aIndEnd]
#			print(sub_ard['ID'].value_counts())
#			print("")


#	compareNumTriggersSplit(rosspad_df, arduino_df)


	#Check if rosspad is dropping PPS's: same number of clock ticks between each PPS timestamp?
	rpd_pps = rosspad_df[rosspad_df.pps].partial_time.to_numpy()
	rpd_pps_diff = np.diff(rpd_pps)
	rpd_pps_diff[np.where(rpd_pps_diff < 0)] += 2**32-1
	#any differences significantly > 400 MHz? if PPS is skipped, diff should be 800e6 clock ticks
	skips = rpd_pps_diff[np.where(rpd_pps_diff > 500e6)]
	print("skipped PPS's?",skips)
	print("")
#	hist, bin_edges = np.histogram(rpd_pps_diff)
#	print(bin_edges)
#	print(hist)



def compareNumTriggersSplit(rosspad_df, arduino_df):

	#Check for number of rosspads and coincidences separately by making sub dataframes and repeating the above
	#first just rosspad trigger out (ADC data)
	#rename index column to keep track of where the number of triggers differ in the grand scheme of things
	rosspad_df.index.rename("index_orig",inplace=True)
	arduino_df.index.rename("index_orig",inplace=True)
	#pull out PPS and rosspad TrigOut
	rosspad_df_trigOut = rosspad_df[(rosspad_df.pps) | (rosspad_df.trigack == False)]
	arduino_df_trigOut = arduino_df[(arduino_df.ID == "P") | (arduino_df.ID == "R")]
	#reindex
	rosspad_df_trigOut = rosspad_df_trigOut.reset_index()
	arduino_df_trigOut = arduino_df_trigOut.reset_index()

	#then can just repeat lines from above
	rosspad_pps_inds = rosspad_df_trigOut.index[rosspad_df_trigOut.pps == True].to_numpy()
	arduino_pps_inds = arduino_df_trigOut.index[arduino_df_trigOut.ID == "P"].to_numpy()

	rpd_tps = np.diff(rosspad_pps_inds)-1 
	ard_tps = np.diff(arduino_pps_inds)-1
	print("rosspad:",rpd_tps[0:65])
	print("arduino:",ard_tps[0:65])

	#they might be different lengths depending on when I stopped the runs...
	#truncate longer one since it doesn't matter
	minLen = min(len(rpd_tps),len(ard_tps))
	rpd_tps = rpd_tps[0:minLen]
	ard_tps = ard_tps[0:minLen]

	print('minLen',minLen)

	rpd_inds_disagree_trigOut = []
	ard_inds_disagree_trigOut = []

	tps_agree = (rpd_tps == ard_tps)
	if np.all(tps_agree):
		print("Number of rosspad ADC triggers between PPS's: data streams agree!")
		print("")
	else:
		#these are the PPS's that disagree: indices of *_pps_inds
		pps_disagree = np.argwhere(tps_agree == False).flatten()
		#these are indices in new dataframe only consisting of PPS and TrigOuts where num TrigOuts disagree
		#really want tuple of PPS indices BETWEEN which num TrigOuts disagree
		rpd_newIndex_disagree = rosspad_pps_inds[pps_disagree]
		rpd_newIndex_disagree_plusOne = rosspad_pps_inds[pps_disagree+1]
		ard_newIndex_disagree = arduino_pps_inds[pps_disagree]
		ard_newIndex_disagree_plusOne = arduino_pps_inds[pps_disagree+1]
		#convert these indices into original indices
		rpd_inds_disagree_trigOut_low = rosspad_df_trigOut[rosspad_df_trigOut.index.isin(rpd_newIndex_disagree)].index_orig.to_numpy()
		rpd_inds_disagree_trigOut_high = rosspad_df_trigOut[rosspad_df_trigOut.index.isin(rpd_newIndex_disagree_plusOne)].index_orig.to_numpy()
		rpd_inds_disagree_trigOut = np.vstack((rpd_inds_disagree_trigOut_low,rpd_inds_disagree_trigOut_high)).T

		ard_inds_disagree_trigOut_low = arduino_df_trigOut[arduino_df_trigOut.index.isin(ard_newIndex_disagree)].index_orig.to_numpy()
		ard_inds_disagree_trigOut_high = arduino_df_trigOut[arduino_df_trigOut.index.isin(ard_newIndex_disagree_plusOne)].index_orig.to_numpy()
		ard_inds_disagree_trigOut = np.vstack((ard_inds_disagree_trigOut_low,ard_inds_disagree_trigOut_high)).T

		print("Number of rosspad ADC triggers between PPS's: disagree during",len(pps_disagree),"seconds out of",minLen,"seconds")
		print("rosspad PPS indices",rpd_inds_disagree_trigOut_low[0:10])
		print("arduino PPS indices",ard_inds_disagree_trigOut_low[0:10])
		#figure out whether rosspad or arduino dropped a trigger
#		print("Num rosspad triggers - num arduino triggers:")
		sub_tps = (rpd_tps-ard_tps)[pps_disagree]
#		print(sub_tps.flatten())
#		n_arduino_drops = np.array([1 if x > 0 else -1 for x in sub_tps.flatten()])
		n_arduino_drops = np.sum(rpd_tps)-np.sum(ard_tps)
		print("total arduino drops:",np.sum(n_arduino_drops),"out of",max(np.sum(rpd_tps),np.sum(ard_tps)),"TrigOuts (",round(np.sum(n_arduino_drops)/max(np.sum(rpd_tps),np.sum(ard_tps))*100,3),"%)")

		print("")

#	print(rosspad_df[['timestamp','pps','trig']].iloc[rpd_inds_disagree_trigOut[0,0]:rpd_inds_disagree_trigOut[0,1]+1])
#	print(arduino_df.iloc[ard_inds_disagree_trigOut[0,0]:ard_inds_disagree_trigOut[0,1]+1])

	#next only rosspad trigger in (Makoto signal)
	rosspad_df_trigIn = rosspad_df[(rosspad_df.pps) | (rosspad_df.trigack == True)]
	arduino_df_trigIn = arduino_df[(arduino_df.ID == "P") | (arduino_df.ID == "C")]
	#reindex
	rosspad_df_trigIn = rosspad_df_trigIn.reset_index()
	arduino_df_trigIn = arduino_df_trigIn.reset_index()

	#then can just repeat lines from above
	rosspad_pps_inds = rosspad_df_trigIn.index[rosspad_df_trigIn.pps == True].to_numpy()
	arduino_pps_inds = arduino_df_trigIn.index[arduino_df_trigIn.ID == "P"].to_numpy()

	rpd_tps = np.diff(rosspad_pps_inds)-1 
	ard_tps = np.diff(arduino_pps_inds)-1

	#they might be different lengths depending on when I stopped the runs...
	#truncate longer one since it doesn't matter
	minLen = min(len(rpd_tps),len(ard_tps))
	rpd_tps = rpd_tps[0:minLen]
	ard_tps = ard_tps[0:minLen]

	rpd_inds_disagree_trigIn = []
	ard_inds_disagree_trigIn = []

	tps_agree = (rpd_tps == ard_tps)
	if np.all(tps_agree):
		print("Number of Makoto triggers between PPS's: data streams agree!")
		print("")
	else:
		#these are the PPS's that disagree: indices of *_pps_inds
		pps_disagree = np.argwhere(tps_agree == False).flatten()
		#these are indices in new dataframe only consisting of PPS and TrigIns where num TrigIns disagree
		#really want tuple of PPS indices BETWEEN which num TrigIns disagree
		rpd_newIndex_disagree = rosspad_pps_inds[pps_disagree]
		rpd_newIndex_disagree_plusOne = rosspad_pps_inds[pps_disagree+1]
		ard_newIndex_disagree = arduino_pps_inds[pps_disagree]
		ard_newIndex_disagree_plusOne = arduino_pps_inds[pps_disagree+1]
		#convert these indices into original indices
		rpd_inds_disagree_trigIn_low = rosspad_df_trigIn[rosspad_df_trigIn.index.isin(rpd_newIndex_disagree)].index_orig.to_numpy()
		rpd_inds_disagree_trigIn_high = rosspad_df_trigIn[rosspad_df_trigIn.index.isin(rpd_newIndex_disagree_plusOne)].index_orig.to_numpy()
		rpd_inds_disagree_trigIn = np.vstack((rpd_inds_disagree_trigIn_low,rpd_inds_disagree_trigIn_high)).T

		ard_inds_disagree_trigIn_low = arduino_df_trigIn[arduino_df_trigIn.index.isin(ard_newIndex_disagree)].index_orig.to_numpy()
		ard_inds_disagree_trigIn_high = arduino_df_trigIn[arduino_df_trigIn.index.isin(ard_newIndex_disagree_plusOne)].index_orig.to_numpy()
		ard_inds_disagree_trigIn = np.vstack((ard_inds_disagree_trigIn_low,ard_inds_disagree_trigIn_high)).T

		print("Number of Makoto triggers between PPS's: disagree during",len(pps_disagree),"seconds out of",minLen,"seconds")
#		print("rosspad PPS indices",rpd_inds_disagree_trigIn[:,0])
#		print("arduino PPS indices",ard_inds_disagree_trigIn[:,0])
		#figure out whether rosspad or arduino dropped a trigger
#		print("Num rosspad triggers - num arduino triggers:")
		sub_tps = (rpd_tps-ard_tps)[pps_disagree]
#		print(sub_tps.flatten())
#		n_rosspad_drops = np.array([1 if x < 0 else -1 for x in sub_tps.flatten()])
		n_rosspad_drops = np.sum(ard_tps-rpd_tps)
		print("total rosspad drops:",np.sum(n_rosspad_drops),"out of",max(np.sum(rpd_tps),np.sum(ard_tps)),"TrigIns (",round(np.sum(n_rosspad_drops)/max(np.sum(rpd_tps),np.sum(ard_tps))*100,3),"%)")

		print("")


	return [rpd_inds_disagree_trigOut, ard_inds_disagree_trigOut], [rpd_inds_disagree_trigIn, ard_inds_disagree_trigIn]


#check timestamp agreement between trigger ins and trigger outs within each system
def timestampAgreementWithinSystem(rosspad_df, arduino_df):

	#arduino
	arduino_df_trigIn = arduino_df[(arduino_df.ID == "P") | (arduino_df.ID == "C")].reset_index(drop=True)
	arduino_df_trigOut = arduino_df[(arduino_df.ID == "P") | (arduino_df.ID == "R")].reset_index(drop=True)

	clk_diff = []
	for rowIn, rowOut in zip(arduino_df_trigIn.itertuples(),arduino_df_trigOut.itertuples()):
		#just check to make sure it's lined up, even though it should be
		if rowIn.ID == "P":
			if rowOut.ID != "P":
				print("ARDUINO: PPS's OUT OF SYNC")
			continue

		if rowIn.ID != "C" or rowOut.ID != "R":
			print("something is wrong...")

		#compare time stamps: diff is rosspad trig out - rosspad trig in / coincidence signal
		clk_diff.append(rowOut.Clock-rowIn.Clock)

	#put in known offset of 18 clock ticks
	clk_diff = np.asarray(clk_diff)
#	clk_diff -= 18
	#look at clock diffs
	print("Arduino:")
	print("Mean clock difference between rosspad and coincidence:",np.mean(clk_diff))
	print("Min difference:",min(abs(clk_diff)))
	print("Max difference:",max(abs(clk_diff)))
	print("")

	#rosspad
	#more complicated since it drops trigger ins
	rosspad_df_trigOut = rosspad_df[(rosspad_df.pps) | (rosspad_df.chan_1 == 1)].reset_index(drop=True)
	rosspad_df_trigIn = rosspad_df[(rosspad_df.pps) | (rosspad_df.chan_1 == 0)].reset_index(drop=True)

	clk_diff = []
	index_offset = 0
	for rowIn in rosspad_df_trigIn.itertuples(): 
		ind = rowIn.Index
		rowOut = rosspad_df_trigOut[rosspad_df_trigOut.index == ind+index_offset].iloc[0]
		if rowIn.pps:
			while rowOut.pps == False:
				index_offset += 1
				rowOut = rosspad_df_trigOut[rosspad_df_trigOut.index == ind+index_offset].iloc[0]

			continue

		print(ind, rowIn.timestamp, rowIn.partial_time, rowIn.chan_1)
		print(ind+index_offset, rowOut.timestamp, rowOut.partial_time, rowOut.chan_1)
		print("")

		if not (rowIn.pps or rowOut.pps):
			clk_diff.append(rowOut.partial_time-rowIn.partial_time)
#			clk_diff.append(rowOut.timestamp-rowIn.timestamp)

	#look at clock diffs
	clk_diff = np.asarray(clk_diff)
	print("Rosspad:")
	print("Mean clock difference between rosspad and coincidence:",np.mean(clk_diff))
	print("Min difference:",min(abs(clk_diff)))
	print("Max difference:",max(abs(clk_diff)))
	print("")


#now check that rosspad triggers are coming in at the same time
#want agreement within one microsecond
def timestampAgreementBetweenSystems(rosspad_df, arduino_df, inds_disagree_trigOut, inds_disagree_trigIn):

	#split out functional arguments
	#this was really not an elegant way to do this, oh well
	rpd_inds_disagree_trigOut = inds_disagree_trigOut[0]
	ard_inds_disagree_trigOut = inds_disagree_trigOut[1]
	rpd_inds_disagree_trigIn = inds_disagree_trigIn[0]
	ard_inds_disagree_trigIn = inds_disagree_trigIn[1]

	#rosspad triggers (i.e. adc events)
	rpd_rel_clk_trg = []
	ard_rel_clk_trg = []
	#coincidence triggers, or rosspad trigger outs, depending on setup
	rpd_rel_clk_cnc = []
	ard_rel_clk_cnc = []

	
	#rosspad
	#make separate dataframe for each type of signal
	rpdPPSdf = rosspad_df.loc[rosspad_df.pps == True]
	rpdTOdf = rosspad_df.loc[(rosspad_df.trigack == False) | (rosspad_df.pps == True)]
	rpdTIdf = rosspad_df.loc[(rosspad_df.trigack == True) | (rosspad_df.pps == True)]

	#get rid of rows where number of triggers is not the same
	for badIndices in rpd_inds_disagree_trigOut:
		rpdTOdf = rpdTOdf.loc[(rpdTOdf.index < badIndices[0]) | (rpdTOdf.index > badIndices[1])]

	for badIndices in rpd_inds_disagree_trigIn:
		rpdTIdf = rpdTIdf.loc[(rpdTIdf.index < badIndices[0]) | (rpdTIdf.index > badIndices[1])]

	#trig out
	indsTO = rpdTOdf.index.to_numpy()
	ppsInds = rpdPPSdf.index.to_numpy()
	ppsTimestamps = rpdPPSdf.timestamp.to_numpy()
	ppsDiff = np.diff(ppsTimestamps)
	#need frequency for counts after last PPS; append mean (not technically correct but whatever)
	ppsDiff = np.append(ppsDiff,np.mean(ppsDiff))
	ppsDiff = np.array([x + 2**32-1 if x < 0 else x for x in ppsDiff])
	ppsIndsTO = [np.argwhere(ppsInds <= x).max() for x in indsTO]
	ppsTimestamps = ppsTimestamps[ppsIndsTO]
	ppsDiff = ppsDiff[ppsIndsTO]

	timestampsTO = rpdTOdf.timestamp.to_numpy()
	rpd_rel_clk_trg = timestampsTO - ppsTimestamps
	rpd_rel_clk_trg = np.array([x + 2**32-1 if x < 0 else x for x in rpd_rel_clk_trg])
	rpd_rel_clk_trg = rpd_rel_clk_trg / ppsDiff

	#trig in
	indsTI = rpdTIdf.index.to_numpy()
	ppsInds = rpdPPSdf.index.to_numpy()
	ppsTimestamps = rpdPPSdf.timestamp.to_numpy()
	ppsDiff = np.diff(ppsTimestamps)
	ppsDiff = np.append(ppsDiff,np.mean(ppsDiff))
	ppsDiff = np.array([x+2**32-1 if x < 0 else x for x in ppsDiff])
	ppsIndsTI = [np.argwhere(ppsInds <= x).max() for x in indsTI]
	ppsTimestamps = ppsTimestamps[ppsIndsTI]
	ppsDiff = ppsDiff[ppsIndsTI]

	timestampsTI = rpdTIdf.timestamp.to_numpy()
	rpd_rel_clk_cnc = timestampsTI - ppsTimestamps
	rpd_rel_clk_cnc = np.array([x + 2**32-1 if x < 0 else x for x in rpd_rel_clk_cnc])
	rpd_rel_clk_cnc = rpd_rel_clk_cnc / ppsDiff


	#arduino
	#separate dataframe for each type of signal
	ardPPSdf = arduino_df.loc[arduino_df.ID == 'P']
	ardTOdf = arduino_df.loc[(arduino_df.ID == 'R') | (arduino_df.ID == 'P')]
	ardTIdf = arduino_df.loc[(arduino_df.ID == 'C') | (arduino_df.ID == 'P')]

	print("456:",len(ardTOdf))

	#get rid of rows where the number of triggers is not the same
	for badIndices in ard_inds_disagree_trigOut:
		ardTOdf = ardTOdf.loc[(ardTOdf.index < badIndices[0]) | (ardTOdf.index > badIndices[1])]

	for badIndices in ard_inds_disagree_trigIn:
		ardTIdf = ardTIdf.loc[(ardTIdf.index < badIndices[0]) | (ardTIdf.index > badIndices[1])]

	print("465:",len(ardTOdf))

	#trig out
	indsTO = ardTOdf.index.to_numpy()
	ppsInds = ardPPSdf.index.to_numpy()
	ppsTimestamps = ardPPSdf.Clock.to_numpy()
	ppsDiff = np.diff(ppsTimestamps)
	#need frequency for counts after last PPS; append mean (not technically correct but whatever)
	ppsDiff = np.append(ppsDiff,np.mean(ppsDiff))
	ppsDiff = np.array([x + 2**32-1 if x < 0 else x for x in ppsDiff])
	ppsIndsTO = [np.argwhere(ppsInds <= x).max() for x in indsTO]
	ppsTimestamps = ppsTimestamps[ppsIndsTO]
	ppsDiff = ppsDiff[ppsIndsTO]

	timestampsTO = ardTOdf.Clock.to_numpy()
	ard_rel_clk_trg = timestampsTO - ppsTimestamps
	ard_rel_clk_trg = np.array([x + 2**32-1 if x < 0 else x for x in ard_rel_clk_trg])
	ard_rel_clk_trg = ard_rel_clk_trg / ppsDiff

	print("484:",len(ard_rel_clk_trg))

	#trig in
	indsTI = ardTIdf.index.to_numpy()
	ppsInds = ardPPSdf.index.to_numpy()
	ppsTimestamps = ardPPSdf.Clock.to_numpy()
	ppsDiff = np.diff(ppsTimestamps)
	ppsDiff = np.append(ppsDiff,np.mean(ppsDiff))
	ppsDiff = np.array([x+2**32-1 if x < 0 else x for x in ppsDiff])
	ppsIndsTI = [np.argwhere(ppsInds <= x).max() for x in indsTI]
	ppsTimestamps = ppsTimestamps[ppsIndsTI]
	ppsDiff = ppsDiff[ppsIndsTI]

	timestampsTI = ardTIdf.Clock.to_numpy()
	ard_rel_clk_cnc = timestampsTI - ppsTimestamps
	ard_rel_clk_cnc = np.array([x + 2**32-1 if x < 0 else x for x in ard_rel_clk_cnc])
	ard_rel_clk_cnc = ard_rel_clk_cnc / ppsDiff


	rpd_rel_clk_trg = rpd_rel_clk_trg*1e6 #convert to microseconds
	#get rid of decimals since we only care about microseconds
#	rpd_rel_clk_trg = np.around(rpd_rel_clk_trg)

	ard_rel_clk_trg = ard_rel_clk_trg*1e6
#	ard_rel_clk_trg = np.around(ard_rel_clk_trg)

	rpd_rel_clk_cnc = rpd_rel_clk_cnc*1e6
#	rpd_rel_clk_cnc = np.around(rpd_rel_clk_cnc)

	ard_rel_clk_cnc = ard_rel_clk_cnc*1e6
#	ard_rel_clk_cnc = np.around(ard_rel_clk_cnc)

#	print("rosspad:",rpd_rel_clk_trg[0:20])
#	print("")
#	print("arduino:",ard_rel_clk_trg[0:20])
#	print("")

	#now compare
	#they might be different lengths depending on when I stopped the runs...
	#truncate longer one since it doesn't matter
	minLen = min(len(rpd_rel_clk_trg),len(ard_rel_clk_trg))
	rpd_rel_clk_trg = rpd_rel_clk_trg[0:minLen]
	ard_rel_clk_trg = ard_rel_clk_trg[0:minLen]

	print("528:",len(ard_rel_clk_trg))

	#get rid of indices larger than 1 second (can happen VERY rarely)
	ok_inds = np.intersect1d(np.argwhere(rpd_rel_clk_trg < 1e6).flatten(),np.argwhere(ard_rel_clk_trg < 1e6).flatten())

	print(len(rpd_rel_clk_trg),len(ard_rel_clk_trg))

	rpd_rel_clk_trg = rpd_rel_clk_trg[ok_inds]
	ard_rel_clk_trg = ard_rel_clk_trg[ok_inds]

	print(len(rpd_rel_clk_trg),len(ard_rel_clk_trg))

	clock_diff = rpd_rel_clk_trg-ard_rel_clk_trg

#	print(np.argwhere(np.absolute(clock_diff) > 5).flatten())

#	plt.hist(clock_diff,bins=100)
#	plt.show()

	print("Rosspad trigger out / Arduino rosspad trigger")
	print("mean time difference:",np.mean(clock_diff))
	nTotal = len(clock_diff)
	nWithinOneUS = np.count_nonzero(np.absolute(clock_diff) < 1)
	print(nWithinOneUS,"/",nTotal,"=",round(nWithinOneUS/nTotal*100,2),"% of triggers agree within 1 us")


#	plt.hist(clock_diff,bins=20,histtype='step')
#	plt.xlabel("ROSSPAD TrigOut - Arduino TrigOut")
#	plt.ylabel("Counts")
#	plt.show()

	pps_inds = np.argwhere(rpd_rel_clk_trg == 0).flatten()
	nIncreasing = 0
	isIncreasing = True
	for i in range(1,len(clock_diff)):
		if i in pps_inds:
			if isIncreasing:
				nIncreasing += 1
			isIncreasing = True
		else:
			if clock_diff[i] < clock_diff[i-1]:
				isIncreasing = False

#	print(len(pps_inds),nIncreasing)
	print("")

	#repeat above but for trigger in / cnc
	minLen = min(len(rpd_rel_clk_cnc),len(ard_rel_clk_cnc))
	rpd_rel_clk_cnc = rpd_rel_clk_cnc[0:minLen]
	ard_rel_clk_cnc = ard_rel_clk_cnc[0:minLen]

	#get rid of indices larger than 1 second (can happen VERY rarely)
	ok_inds = np.intersect1d(np.argwhere(rpd_rel_clk_cnc < 1e6).flatten(),np.argwhere(ard_rel_clk_cnc < 1e6).flatten())
	rpd_rel_clk_cnc = rpd_rel_clk_cnc[ok_inds]
	ard_rel_clk_cnc = ard_rel_clk_cnc[ok_inds]

	clock_diff_cnc = rpd_rel_clk_cnc-ard_rel_clk_cnc

#	for i in range(len(clock_diff)):
#		print(rpd_rel_clk_trg[i],ard_rel_clk_trg[i])
#	print("\n\n")

#	for i in range(len(clock_diff_cnc)):
#		print(rpd_rel_clk_cnc[i],ard_rel_clk_cnc[i])

#	plt.hist(clock_diff_cnc,bins=100)
#	plt.show()

	print("Rosspad trigger in / arduino coincidence")
	print("mean time difference:",np.mean(clock_diff_cnc))
	nTotal = len(clock_diff_cnc)
	nWithinOneUS = np.count_nonzero(np.absolute(clock_diff_cnc) < 1)
	print(nWithinOneUS,"/",nTotal,"=",round(nWithinOneUS/nTotal*100,2),"% of triggers agree within 1 us")

#	plt.hist(clock_diff_cnc,bins=20,histtype='step')
#	plt.xlabel("ROSSPAD TrigACK - Arduino TrigACK")
#	plt.ylabel("Counts")
#	plt.show()

	pps_inds = np.argwhere(rpd_rel_clk_cnc == 0).flatten()
	nIncreasing = 0
	isIncreasing = True
	for i in range(1,len(clock_diff_cnc)):
		if i in pps_inds:
			if isIncreasing:
				nIncreasing += 1
			isIncreasing = True
		else:
			if clock_diff_cnc[i] < clock_diff_cnc[i-1]:
				isIncreasing = False

#	print(len(pps_inds),nIncreasing)

def trigOutTimestampDiff(rosspad_df, arduino_df):

	aTimesSinceLastPPS = calculate_time_since_pps(arduino_df)
	ppsUTC = arduino_df[arduino_df.ID == "P"].UTCA.to_numpy()
	ppsInds = arduino_df[arduino_df.ID == "P"].index.to_numpy()
	indices = arduino_df.index.to_numpy()
	indexOfPrevPPS = [np.argwhere(ppsInds <= x).max() for x in indices]
	timeOfLastPPS = ppsUTC[indexOfPrevPPS]
	utcTimes = timeOfLastPPS + aTimesSinceLastPPS
	utcTimes = utcTimes * 1e6 #convert to microseconds
	arduino_df['UTC'] = utcTimes

	rTimesSinceLastPPS = calculate_time_since_pps(rosspad_df)
	ppsInds = rosspad_df[rosspad_df.pps].index.to_numpy()
	indices = rosspad_df.index.to_numpy()
	indexOfPrevPPS = [np.argwhere(ppsInds <= x).max() for x in indices]
	timeOfLastPPS = ppsUTC[indexOfPrevPPS]
	utcTimes = timeOfLastPPS + rTimesSinceLastPPS
	utcTimes = utcTimes * 1e6 #convert to microseconds
	rosspad_df['UTC'] = utcTimes


	arduino_trigOut_times = arduino_df[arduino_df.ID == "R"].UTC.to_numpy()
	arduino_trigOut_times_diff = np.diff(arduino_trigOut_times)

	rosspad_trigOut_times = rosspad_df[rosspad_df.chan_1 == 1].UTC.to_numpy()
	rosspad_trigOut_times_diff = np.diff(rosspad_trigOut_times)


	plt.hist(rosspad_trigOut_times_diff,bins=np.linspace(0,100),histtype='step',label='ROSSPAD')
	plt.hist(arduino_trigOut_times_diff,bins=np.linspace(0,100),histtype='step',label='Arduino')
	plt.legend()
	plt.xlabel('TrigOut time difference ($\mu$s)')
	plt.ylabel("Counts")
	plt.yscale('log')
	plt.show()


def extra_arduino_trigOuts(rosspad_df,arduino_df):

	lineup_index_tuples = lineup_times(rosspad_df, arduino_df)
	rpdInds = lineup_index_tuples[:,0]
	ardInds = lineup_index_tuples[:,1]

	rpdTrigOutEquiv = np.full(len(arduino_df),-1)
	ppsInds = arduino_df[arduino_df.ID == 'P'].index.to_numpy()
	trigInInds = arduino_df[arduino_df.ID == 'C'].index.to_numpy()
	
	rpdTrigOutEquiv[ardInds] = rpdInds
	rpdTrigOutEquiv[ppsInds] = -2
	rpdTrigOutEquiv[trigInInds] = -3
	#there are a bunch of unmatched arduino TrigOuts at the end, don't look at those
	rpdTrigOutEquiv[ardInds[-1]:] = -4

	#look at number of extras in a row
	nInRow = []
	_nInRow = 0
	lastExtra = False

	for i in rpdTrigOutEquiv:
		if i == -1:
			_nInRow += 1
			lastExtra = True
		else:
			if lastExtra == True:
				nInRow.append(_nInRow)
				_nInRow = 0
				lastExtra = False

	plt.hist(nInRow,bins=np.linspace(0.5,5.5,num=6))
	plt.xlabel("Number of extra triggers in a row")
	plt.ylabel("Counts")
	plt.yscale('log')
	plt.show()


	#time difference of extras
	timestamps = arduino_df.UTC.to_numpy()
	tstamp_diff = np.diff(timestamps)
	extraDiffs = tstamp_diff[np.argwhere(rpdTrigOutEquiv == -1).flatten()-1]

	plt.hist(extraDiffs,bins=np.linspace(0,100))
	plt.xlabel('time between extra TrigOut and previous TrigOut')
	plt.ylabel('Counts')
	plt.yscale('log')
	plt.show()


	#energy of rosspad event before extra
#	print(rosspad_df.columns)
#	pandas.set_option('display.max_rows', None)
#	pandas.set_option('display.max_columns', None)
#	pandas.set_option('display.width', None)
#	pandas.set_option('display.max_colwidth', -1)
#	print(rosspad_df.iloc[2])


def trigOutTimestampAgreementAfterLineup(rosspad_df, arduino_df):

	lineup_index_tuples = lineup_times(rosspad_df, arduino_df)
	rpdInds = lineup_index_tuples[:,0]
	ardInds = lineup_index_tuples[:,1]

	#these times are in nanoseconds
	rosspad_trigOut_times = rosspad_df['UTC'][rpdInds].to_numpy()
	arduino_trigOut_times = arduino_df['UTC'][ardInds].to_numpy()

	tstamp_diff = abs(rosspad_trigOut_times - arduino_trigOut_times)
	nDiff = np.count_nonzero(tstamp_diff != 0)
	nDiffAbove1us = np.count_nonzero(tstamp_diff > 1e3)
	nTrigOuts = len(rosspad_trigOut_times)

	print(nTrigOuts," TrigOuts")
	print(nDiff,"(",nDiff/nTrigOuts*100,"%) differ")
	print(nDiffAbove1us,"(",nDiffAbove1us/nTrigOuts*100,"%) differ by more than 1 us")



def lineup_times(rosspad_df, arduino_df):

	aTimesSinceLastPPS = calculate_time_since_pps(arduino_df)
	ppsUTC = arduino_df[arduino_df.ID == "P"].UTCA.to_numpy()
	ppsInds = arduino_df[arduino_df.ID == "P"].index.to_numpy()
	indices = arduino_df.index.to_numpy()
	indexOfPrevPPS = [np.argwhere(ppsInds <= x).max() for x in indices]
	timeOfLastPPS = ppsUTC[indexOfPrevPPS]
	utcTimes = timeOfLastPPS + aTimesSinceLastPPS
	utcTimes = utcTimes * 1e9 #convert to nanoseconds
	arduino_df['UTC'] = utcTimes

	rTimesSinceLastPPS = calculate_time_since_pps(rosspad_df)
	ppsInds = rosspad_df[rosspad_df.pps].index.to_numpy()
	indices = rosspad_df.index.to_numpy()
	indexOfPrevPPS = [np.argwhere(ppsInds <= x).max() for x in indices]
	print(len(ppsUTC),len(indexOfPrevPPS))
	timeOfLastPPS = ppsUTC[indexOfPrevPPS]
	utcTimes = timeOfLastPPS + rTimesSinceLastPPS
	utcTimes = utcTimes * 1e9 #convert to nanoseconds
	rosspad_df['UTC'] = utcTimes

#	np.set_printoptions(suppress=True,formatter={'float_kind':'{:f}'.format})
#	print(arduino_df[arduino_df.ID == 'P'].UTC.to_numpy())
#	print(rosspad_df[rosspad_df.pps].UTC.to_numpy())

	#line up the end
	lastArdPPS = float(arduino_df[arduino_df.ID == 'P'].UTC.to_numpy()[-1:])
	lastRpdPPS = float(rosspad_df[rosspad_df.pps].UTC.to_numpy()[-1:])
	lastPPS = min(lastArdPPS,lastRpdPPS)
	arduino_df = arduino_df[arduino_df['UTC'] < lastPPS]
	rosspad_df = rosspad_df[rosspad_df['UTC'] < lastPPS]

	#now actually line them up
	rosspad_tstamps = rosspad_df[(rosspad_df.pps == False) & (rosspad_df.trigack == False)].UTC.to_numpy()
	rosspad_index = rosspad_df[(rosspad_df.pps == False) & (rosspad_df.trigack == False)].index.to_numpy()
	arduino_tstamps = arduino_df[arduino_df.ID == "R"].UTC.to_numpy()
	arduino_index = arduino_df[arduino_df.ID == "R"].index.to_numpy()
	lineup_index_tuple = []

	index = arduino_df.index.to_numpy()
	print(np.all(index == np.arange(len(index))))

	lastArd = 0

	for rpdT,rpdI in zip(rosspad_tstamps,rosspad_index):
#		print(rpdI)
		bestMatchI = -1
		lastDiff = 1e12

		for i in range(lastArd,len(arduino_tstamps)):
			currentDiff = abs(rpdT-arduino_tstamps[i])

			if currentDiff > lastDiff:
				bestMatchI = i-1
				break

			else:
				lastDiff = currentDiff

		if bestMatchI == -1:
			print("something is wrong",rpdI,arduino_index[bestMatchI])

		else:
			lineup_index_tuple.append((rpdI,arduino_index[bestMatchI]))
		lastArd = bestMatchI+1


	lineup_index_tuple = np.array(lineup_index_tuple)

	#sanity check that I get the same number of extra arduino triggers
	lastIndRpd = lineup_index_tuple[-1][0]
	lastIndArd = lineup_index_tuple[-1][1]
	numExtra = np.argwhere(arduino_index == lastIndArd) - np.argwhere(rosspad_index == lastIndRpd)
	print("num extra:",float(numExtra.flatten()))

	return lineup_index_tuple


def calculate_time_since_pps(df):
	try:
		ppsInds = df[df.ID == "P"].index.to_numpy()
		ppsTimestamps = df[df.ID == "P"].Clock.to_numpy()
	except AttributeError:
		ppsInds = df[df.pps].index.to_numpy()
		ppsTimestamps = df[df.pps].timestamp.to_numpy()

	#get number of clock ticks per second
	ticksPerSec = np.diff(ppsTimestamps)
	#append mean to get frequency of counts after last PPS
	ticksPerSec = np.append(ticksPerSec,np.mean(ticksPerSec))
	#handle rollover
	ticksPerSec = np.array([x + 2**32-1 if x < 0 else x for x in ticksPerSec])

	indices = df.index.to_numpy()
	indexOfPrevPPS = [np.argwhere(ppsInds <= x).max() for x in indices]
	ppsTimestamps = ppsTimestamps[indexOfPrevPPS]
	ticksPerSec = ticksPerSec[indexOfPrevPPS]

	try:
		timestamps = df.timestamp.to_numpy()
	except AttributeError:
		timestamps = df.Clock.to_numpy()
	timeSinceLastPPS = timestamps - ppsTimestamps
	timeSinceLastPPS = np.array([x + 2**32-1 if x < 0 else x for x in timeSinceLastPPS])
	timeSinceLastPPS = timeSinceLastPPS / ticksPerSec

	return timeSinceLastPPS

	

def trigInTrigOutComparison(rosspad_df, arduino_df):

	rosspad_frequency = 100e6
	arduino_frequency = 42e6

	#get dataframe with only trigger ACKs
	rosspad_trigAck_dframe = rosspad_df.loc[(rosspad_df.adc_0 == 1023) & (rosspad_df.chan_1 == 0) & (rosspad_df.pps == False)]
	rosspad_trigAck_indices = rosspad_trigAck_dframe.index.to_numpy()
	#get rid of lineup sequence
	rosspad_trigAck_indices = rosspad_trigAck_indices[10:]

	rosspadTrigAck = []
	rosspadTrigOut = []
	for ind in rosspad_trigAck_indices:
		foundTrigOut = False
		i = ind
		while i < ind+10 and foundTrigOut == False:
			i += 1
			if(rosspad_df.loc[rosspad_df.index == i].chan_1.values[0] == 1):
				foundTrigOut = True
		if foundTrigOut:
			rosspadTrigAck.append(rosspad_df.loc[rosspad_df.index == ind].timestamp.values[0])
			rosspadTrigOut.append(rosspad_df.loc[rosspad_df.index == i].timestamp.values[0])

	rosspadTrigAck = np.array(rosspadTrigAck)
	rosspadTrigOut = np.array(rosspadTrigOut)

	rosspadAckMinusOut = rosspadTrigAck - rosspadTrigOut
	rosspadAckMinusOut = np.array([x+2**32-1 if x<0 else x for x in rosspadAckMinusOut])
	print("rosspad TrigAck-TrigOut:")
	print("mean =",np.mean(rosspadAckMinusOut),"clock ticks;",np.mean(rosspadAckMinusOut)/rosspad_frequency,"seconds")
	print("min =",min(rosspadAckMinusOut),"clock ticks;",min(rosspadAckMinusOut)/rosspad_frequency,"seconds")
	print("max =",max(rosspadAckMinusOut),"clock ticks;",max(rosspadAckMinusOut)/rosspad_frequency,"seconds")
	print("")

	#now arduino
	arduino_trigAck_dframe = arduino_df.loc[arduino_df.ID == "C"]
	arduino_trigAck_indices = arduino_trigAck_dframe.index.to_numpy()
	#get rid of lineup sequence
	arduino_trigAck_indices = arduino_trigAck_indices[10:]

	arduinoTrigAck = []
	arduinoTrigOut = []
	for ind in arduino_trigAck_indices:
		foundTrigOut = False
		i = ind
		while i > 0 and foundTrigOut == False:
			i -= 1
			if (arduino_df.loc[arduino_df.index == i].ID.values[0] == "R"):
				foundTrigOut = True
		if foundTrigOut:
			arduinoTrigAck.append(arduino_df.loc[arduino_df.index == ind].Clock.values[0])
			arduinoTrigOut.append(arduino_df.loc[arduino_df.index == i].Clock.values[0])

	arduinoTrigAck = np.array(arduinoTrigAck)
	arduinoTrigOut = np.array(arduinoTrigOut)

	arduinoAckMinusOut = arduinoTrigAck - arduinoTrigOut
	arduinoAckMinusOut = np.array([x+2**32-1 if x<0 else x for x in arduinoAckMinusOut])
	print("arduino TrigAck-TrigOut:")
	print("mean =",np.mean(arduinoAckMinusOut),"clock ticks;",np.mean(arduinoAckMinusOut)/arduino_frequency,"seconds")
	print("min =",min(arduinoAckMinusOut),"clock ticks;",min(arduinoAckMinusOut)/arduino_frequency,"seconds")
	print("max =",max(arduinoAckMinusOut),"clock ticks;",max(arduinoAckMinusOut)/arduino_frequency,"seconds")
	print("")

	#plot
	rosspadAckMinusOut = rosspadAckMinusOut*1e6/rosspad_frequency
	arduinoAckMinusOut = arduinoAckMinusOut*1e6/arduino_frequency
	plt.hist(rosspadAckMinusOut,bins=20,histtype='step',label='ROSSPAD')
	plt.hist(arduinoAckMinusOut,bins=20,histtype='step',label='Arduino')
	plt.legend()
	plt.xlabel(r'$TA_n - TO_n$ ($\mu$s)')
	plt.ylabel("Counts")
	plt.show()


	#now look at TOn-1 - TOn compared to TAn-1 - TAn
	rosspadTrigOutDiff = np.diff(rosspadTrigOut)
	rosspadTrigAckDiff = np.diff(rosspadTrigAck)
	rosspadTAdiffMinusTOdiff = (rosspadTrigAckDiff - rosspadTrigOutDiff)*1e6/rosspad_frequency
	
	arduinoTrigOutDiff = np.diff(arduinoTrigOut)
	arduinoTrigAckDiff = np.diff(arduinoTrigAck)
	arduinoTAdiffMinusTOdiff = (arduinoTrigAckDiff - arduinoTrigOutDiff)*1e6/arduino_frequency

	plt.hist(rosspadTAdiffMinusTOdiff,bins=20,histtype='step',label='ROSSPAD')
	plt.hist(arduinoTAdiffMinusTOdiff,bins=20,histtype='step',label='Arduino')
	plt.legend()
	plt.xlabel(r'$(TA_{n-1} - TA_n) - (TO_{n-1} - TO_n)$ ($\mu$s)')
	plt.ylabel("Counts")
	plt.show()


def print_dropped_trigs(rosspad_df, arduino_df, inds_disagree_trigOut, inds_disagree_trigIn):

	#split out functional arguments
	rpd_inds_disagree_trigOut = inds_disagree_trigOut[0]
	ard_inds_disagree_trigOut = inds_disagree_trigOut[1]
	rpd_inds_disagree_trigIn = inds_disagree_trigIn[0]
	ard_inds_disagree_trigIn = inds_disagree_trigIn[1]

	for (rInd, aInd) in zip(rpd_inds_disagree_trigOut[82:90], ard_inds_disagree_trigOut[82:90]):
		print(rosspad_df.iloc[rInd[0]:rInd[1]+1])
		print(arduino_df.iloc[aInd[0]:aInd[1]+1])
		print("\n\n")


if __name__ == '__main__':

	import argparse
	parser = argparse.ArgumentParser()
	parser.add_argument("rfname",help="rosspad hdf5 file")
	parser.add_argument("afname",help="arduino log file")
	args = parser.parse_args()

	compare(args)

