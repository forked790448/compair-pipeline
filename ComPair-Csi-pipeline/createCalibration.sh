#!/bin/bash

function ProgressBar {
# Process data
let _progress=(${1}*100/${2}*100)/100
let _done=(${_progress}*4)/10
let _left=40-$_done
# Build progressbar string lengths
_fill=$(printf "%${_done}s")
_empty=$(printf "%${_left}s")

printf "\rProgress : [${_fill// /#}${_empty// /-}] ${_progress}%%"

}

#clean up from last calibration

parent=$(dirname "$1")

if [ -d "$parent/calibration_metadata" ]; then
rm -r "$parent/calibration_metadata"
fi

mkdir "$parent/calibration_metadata"


echo "Converting to L1.0";
echo "	Datafile: $1";

filenameHDF="$parent/*.hdf5"

python3 ./rosspad.py $1

echo "Creating Metadata";
python3 ./create_calibration_metadata.py $filenameHDF
rm $filenameHDF

echo -e '\r'








