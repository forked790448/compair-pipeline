#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Develop energy and position calibration metadata
 - takes in ADC Data that has been pedestal subtracted
 - will then print a hdf file for each log with energy and position
     for each event
 - I break it up because my computer cannot handle
     large datafile. So I break it up by log, to later process each log 
     at a later time.
"""
import pandas as pd
pd.options.mode.chained_assignment = None

import re
from os.path import dirname
import os

from tqdm import tqdm # progress bar


import shutil # remove filled file


# plotting
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.pylab as pylab
import matplotlib as mpl


class CreateCalibrationMetadata:
    def __init__(self):
        self.chunk_size = 5000000
        
        self.tmp = []
        for i in range(0, 30):
            self.tmp.append(f'L{i}_adc')

    def process(self, filename):
        
        if os.path.isdir(dirname(filename[0])+'/calibration_metadata'):
            shutil.rmtree(dirname(filename[0])+'/calibration_metadata', ignore_errors=True)

        os.mkdir(dirname(filename[0])+'/calibration_metadata')
            
        self.read_files(filename[0])
        
    def read_files(self, filename):
        self.rawpath = dirname(filename)

        chunks = pd.read_hdf(filename, key='ADC', chunksize=self.chunk_size)
        with tqdm(total=chunks.nrows) as pbar:
            for self.pps_adc in chunks:
                pbar.update(self.chunk_size)

                self.get_total_log()
                self.write_to_file()
                
        chunks.close()

    def write_to_file(self):
        # for e in range(0, 1000, 50):
        #     for i in range(0, 30):
        #         self.pps_adc[f'L{i}_adc'][self.pps_adc[f'L{i}_adc'] < e] = 0
        #     # Lower threshold
        #     plt.clf()

        #     q = self.pps_adc[self.tmp].sum(axis=1)[self.pps_adc[self.tmp].sum(axis=1) > 200]
        #     plt.hist(q[self.pps_adc[self.tmp].sum(axis=1) < 12000], bins=range(0, 12000, 10))
            
            
        #     plt.xlabel('Energy (keV)')
        #     plt.ylabel('Counts per bin')
            
        #     plt.legend(loc='best',  frameon=False)
            
        #     plt.savefig(self.rawpath +f'/spectrum_{e}_keV.png', dpi = 900, bbox_inches = 'tight', facecolor='w')


            
        for i in range(0, self.total_logs):
            store = pd.HDFStore(self.rawpath+f'/calibration_metadata/L{i}.hdf5')
            # if i == 9:
            #     store.append('CAL', self.pps_adc[[f'L{i}_adc', f'L{i}_pos']][self.pps_adc[f'L{i}_adc'] > 50], format='t',  data_columns=True)
            # elif i == 15:
            #     store.append('CAL', self.pps_adc[[f'L{i}_adc', f'L{i}_pos']][self.pps_adc[f'L{i}_adc'] > 50], format='t',  data_columns=True)
            # else:
            #     store.append('CAL', self.pps_adc[[f'L{i}_adc', f'L{i}_pos']][self.pps_adc[f'L{i}_adc'] > 150], format='t',  data_columns=True)

            store.append('CAL', self.pps_adc[[f'L{i}_adc', f'L{i}_pos']][self.pps_adc[f'L{i}_adc'] > 20], format='t',  data_columns=True)

            store.close()
            
        for i in range(0, 30):
            self.pps_adc[f'L{i}_adc'][self.pps_adc[f'L{i}_adc'] < 200] = 0
            
        self.pps_adc['sum'] = self.pps_adc[self.tmp].sum(axis=1)
                    
        store = pd.HDFStore(self.rawpath+'/calibration_metadata/sum.hdf5')
        store.append('CAL', self.pps_adc['sum'], format='t',  data_columns=True)
        store.close()

    def get_total_log(self):
        name_of_last_column = self.pps_adc.columns[len(self.pps_adc.columns)-3]
        self.total_logs = (int(re.findall(r'[0-9]+', name_of_last_column)[0])+1) #n total channels in system
        self.total_logs = 30
                

def main(filename=None, **kargs):
    metaData = CreateCalibrationMetadata()
    metaData.process(filename)

if __name__ == '__main__':
    import argparse    
    parser = argparse.ArgumentParser(
        description='Lookup some information in a database')
    parser.add_argument("filename", nargs="*")
    args = parser.parse_args()
    
    main(**vars(args))
