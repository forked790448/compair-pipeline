#!/bin/bash
function ProgressBar {
# Process data
    let _progress=(${1}*100/${2}*100)/100
    let _done=(${_progress}*4)/10
    let _left=40-$_done
# Build progressbar string lengths
    _fill=$(printf "%${_done}s")
    _empty=$(printf "%${_left}s")

printf "\rProgress : [${_fill// /#}${_empty// /-}] ${_progress}%%"

}
#clean up from last calibration

parent="$1"


#clean up from last calibration

parent=$(dirname "$1")

tput setaf 2; echo "Converting to L1.0";
echo "	Datafile: $1";

filenameHDF="$parent/*log.hdf5"

python3 ./rosspad.py $1

echo "Converting to L1.1";
filenameLOG="$parent/ardLog.log"
cat $parent/log*/{0..1000}.log >> $filenameLOG 2>/dev/null
python3 ./rosspadToL1_1.py $filenameHDF $filenameLOG


echo "Converting to L2";
python3 ./rosspadL1_1To2.py $filenameHDF
rm $filenameHDF

#echo "Creating Metadata";
#filenameHDF="$parent/*.L2.hdf5"
#python3 ./create_calibration_metadata.py $filenameHDF
#rm $filenameHDF

echo "Converting to L3";
filenameHDF="$parent/*log.L2.hdf5"
python3 ./rosspadL2To3.py $filenameHDF
rm $filenameHDF
filenameHDF="$parent/*log.L1.1.hdf5"
rm $filenameHDF

echo "--Program Complete--";

echo -e '\r'







