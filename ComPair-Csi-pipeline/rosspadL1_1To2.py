#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Develop L2 Level Data
 - takes either L1 or L1,1 data and applies an energy and position calibration
 - loads 'calibrations/calibrationData/*.txt' calibration masks
     for each event
 - I break it up because my computer cannot handle
     large datafile. So I break it up by log, to later process each log 
     at a later time.
"""

import os
import os.path

#math
import pandas as pd
import numpy as np


from pathlib import Path
from tqdm import tqdm # progress bar
import re

#saving lists
import pickle

## ROSSPAD specific
import create_calibration_mask as calibMasks


class RosspadL1_1To2:
    def __init__(self,rfname):
        self.rfname = rfname
        self.loadCalibrationData()
        
        
        self.FULL_LOG_LENGTH = 100 #100 mm, length of log
        self.pos_edges_final = np.linspace(-1.6, 1.6, int((np.shape(self.gain)[1])))*self.FULL_LOG_LENGTH/2
        
        # Other Global Variables
        self.chunk_size = 999999
        self.cmis_gain = 7
        
        
    def loadCalibrationData(self):
        # print('Loading Calibration Data: ' + filename)
        
        #filename = 'rosspadConfigData/calibration/calibrationData'
        #filename = 'CsI Calibration/calibrationData'
        filename = '../Detector_Effects/CalibrationFiles/CsI/calibrationData'	
	
        with open(filename+"_posCenter.txt", "rb") as fp:
            self.posCenter =  pickle.load(fp)
        
        with open(filename+"_posRange.txt", "rb") as fp:
            self.posRangeNonLinearity = pickle.load(fp)
            
        with open(filename+"_gain.txt", "rb") as fp:
            self.gain = pickle.load(fp)
         

    def process(self):
        self.chunk_index = 0

        # iterate over table chunks
        path = Path(self.rfname)
        
        self.hdfpath = path.with_suffix('').with_suffix('').with_suffix('.L2.hdf5')
        
        try:
            os.remove(str(self.hdfpath))
        except:
            pass 

        self.store = pd.HDFStore(str(self.hdfpath))
        
        chunks = pd.read_hdf(self.rfname, key='ADC', chunksize=self.chunk_size)
        with tqdm(total=chunks.nrows) as pbar:
            for self.pps_adc_df in chunks:
                pbar.update(self.chunk_size)
                
                if self.chunk_index == 0:
                    self.get_total_log()
                    
                self.calibrate_pos()
                self.calibrate_energy()
                self.write_files()
                
        # self.copy_L1p1()
        self.store.close()


    def calibrate_pos(self):
        
        metaData = calibMasks.CreateCalibrationMasks(["void"])
        metaData.CMIS_gain = self.cmis_gain
        metaData.energy_of_interest = 2614
        
        for logNumber in range(0, self.total_logs):
            metaData.setMinMaxLimits(logNumber)
            
            # self.pps_adc_df[f'L{logNumber}_pos'] = self.pps_adc_df[f'L{logNumber}_pos'].sub(self.posCenter[logNumber])
            # self.pps_adc_df[f'L{logNumber}_pos'] = self.pps_adc_df[f'L{logNumber}_pos'].mul(self.posRangeNonLinearity[logNumber])   
            
                        
            # non-linearity portion
            # c[0] x^2 + c[1] x + c[2]
                        
            tmpEModel = np.poly1d( self.gain[ logNumber][int((np.shape(self.gain)[1]/2))] )
            tmpEnergy = tmpEModel(self.pps_adc_df[f'L{logNumber}_adc'])
            
            #energies over the Th 2.6 MeV line should use the 2.6 MeV position calibration
            # so set energies above that to 2.6 MeV
            tmpEnergy[tmpEnergy > metaData.MIN_ADC]  = metaData.MIN_ADC
            
            tmpCenterModel = np.poly1d( self.posCenter[logNumber])
            self.pps_adc_df[f'L{logNumber}_pos'] = self.pps_adc_df[f'L{logNumber}_pos'].sub(  tmpCenterModel( tmpEnergy )  )
            
            tmpPosRangeModel = np.poly1d( self.posRangeNonLinearity[logNumber])
            self.pps_adc_df[f'L{logNumber}_pos'] = self.pps_adc_df[f'L{logNumber}_pos'].mul(   100/tmpPosRangeModel( tmpEnergy ) )


    def calibrate_energy(self):
        for logNumber in range(0, self.total_logs):
            for i in range (0, len(self.pos_edges_final)-1):
                tmpEModel = np.poly1d( self.gain[logNumber][i] ) 
                indexOfInterest = self.pps_adc_df[f'L{logNumber}_pos'].between(self.pos_edges_final[i], self.pos_edges_final[i+1])
                self.pps_adc_df[f'L{logNumber}_adc'][indexOfInterest] \
                    = tmpEModel( self.pps_adc_df[f'L{logNumber}_adc'][indexOfInterest]  )  
        
    def get_total_log(self):
        filter_col = [col for col in self.pps_adc_df if col.startswith('L')]
        
        name_of_last_column = filter_col[len(filter_col)-1]
        self.total_logs = (int(re.findall(r'[0-9]+', name_of_last_column)[0])+1) #n total channels in system
        
        
    #write edited dataframe to new hdf files
    def write_files(self):
        #write to file

        self.pps_adc_df.index = self.pps_adc_df.index + self.chunk_index*self.chunk_size
        self.store.append('ADC', self.pps_adc_df, format='t',  data_columns=True)

        ## housekeeping after saving, just to make sure
        self.chunk_index = self.chunk_index + 1
        del self.pps_adc_df
        
    def copy_L1p1(self):
        # L1.1 file check if file exists
        #write to file
        path = Path(self.rfname)
        hdfpath = path.with_suffix('.L1.1.hdf5')
        
        if os.path.isfile(hdfpath) == True:
            self.time_df = pd.read_hdf(hdfpath,key='TIM')
        
            with pd.HDFStore(path.with_suffix('.L2.hdf5')) as hdf:
                self.time_df.to_hdf(hdf,key='TIM')
        else:
            print('NO L1.1 File Found')
        



if __name__ == '__main__':

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("rfname",help="rosspad hdf5 file")
    args = parser.parse_args()

    # print(args.rfname)
    R = RosspadL1_1To2(args.rfname)
    R.process()

