import pandas as pd
import numpy as np
from pathlib import Path
import lineup_logfiles
import warnings
from matplotlib import pyplot as plt 
warnings.simplefilter(action='ignore', category=pd.errors.PerformanceWarning)

from functools import reduce


class LineupFailed(Exception):
    def __init__(self,msg="ROSSPAD and Arduino lineup failed", *args, **kwargs):
        super().__init__(msg, *args, **kwargs)

class DroppedPPS(Exception):
    def __init__(self,msg="Dropped PPS: deal with this", *args, **kwargs):
        super().__init__(msg, *args, **kwargs)

class SortError(Exception):
    def __init__(self,msg="Sort failed: timestamps out of order", *args, **kwargs):
        super().__init__(msg, *args, **kwargs)


class RosspadToL1_1:
    def __init__(self,rfname,afname):
        self.rfname = rfname
        self.afname = afname

    def process(self):
        print("")
        try:
            self.read_files()
            
            self.sort_rosspad()

            self.arduino_cleanup()

            self.sort_arduino()

            self.check_pps()
            
            self.count_dropped_trigs()

            self.assign_UTC_times()

            self.check_trigOut_times()

            self.insert_trigacks()

            self.write_files()
        except Exception as e:
            print(e)

    def read_files(self):
        #get starting index 
        rosspad_start, arduino_start = lineup_logfiles.lineup(self.rfname,self.afname[0])
        _rosspad_df = pd.read_hdf(self.rfname,key='ADC', columns=['frame', 'timestamp', 'pps', 'trigack'])
        

        
        self.rosspad_df = _rosspad_df.iloc[rosspad_start:].reset_index(drop=True)
        

        _arduino_df = pd.read_csv(self.afname[0],names=['ID','timestamp','UTCA','UTCC'])
        __arduino_df = _arduino_df.iloc[arduino_start:].reset_index(drop=True)
        for i in range(1,len(self.afname)):
            _arduino_df = pd.read_csv(self.afname[i],names=['ID','timestamp','UTCA','UTCC'])
            __arduino_df = __arduino_df.append(_arduino_df,ignore_index=True)
        self.arduino_df = __arduino_df.reset_index(drop=True)

        #make sure both dataframes start on a PPS
        if self.rosspad_df.iloc[0].pps == False or self.arduino_df.iloc[0].ID != "P":
            raise LineupFailed


    #Sort the timestamps in the rosspad, since they are often out of order due to being on different buffers
    def sort_rosspad(self):
        #separate to find the rollovers by looking at timestamp diff
        #call .copy() to get rid of SettingWithCopy warning
        pps_df = self.rosspad_df[self.rosspad_df.pps == True].copy()
        trigOut_df = self.rosspad_df[(self.rosspad_df.pps == False) & (self.rosspad_df.trigack == False)].copy()
        trigIn_df = self.rosspad_df[self.rosspad_df.trigack == True].copy()

        #first pps
        pps_tstamps = pps_df.timestamp.to_numpy()
        pps_tstamp_diff = np.diff(pps_tstamps)
        pps_rollover_inds = np.argwhere(pps_tstamp_diff < 0).flatten()
        #start at -1 or else the sortcol and the timestamps are offset by 1
        # and it will take you 10 hours to figure out why nothing works
        pps_rollover_inds = np.insert(pps_rollover_inds,0,-1)
        pps_rollover_inds = np.append(pps_rollover_inds,len(pps_tstamps))
    
        pps_sortcol = []
        counter = 0
        for i,j in zip(pps_rollover_inds[:-1],pps_rollover_inds[1:]):
            pps_sortcol.extend([counter for x in range(j-i)])
            counter += 1
    
        #remove last element of sortcol, since it's extra
        pps_sortcol = np.array(pps_sortcol[:-1])
        pps_sortcol = (pps_sortcol << 32) | pps_tstamps
    
        pps_df.loc[:,'sortcol'] = pps_sortcol
    
    
        #next trigout
        trigOut_tstamps = trigOut_df.timestamp.to_numpy()
        trigOut_tstamp_diff = np.diff(trigOut_tstamps)
        trigOut_rollover_inds = np.argwhere(trigOut_tstamp_diff < 0).flatten()
        trigOut_rollover_inds = np.insert(trigOut_rollover_inds,0,-1)
        trigOut_rollover_inds = np.append(trigOut_rollover_inds,len(trigOut_tstamps))
    
        trigOut_sortcol = []
        counter = 0
        for i,j in zip(trigOut_rollover_inds[:-1],trigOut_rollover_inds[1:]):
            trigOut_sortcol.extend([counter for x in range(j-i)])
            counter += 1
    
        trigOut_sortcol = np.array(trigOut_sortcol[:-1])
        trigOut_sortcol = (trigOut_sortcol << 32) | trigOut_tstamps
    
        trigOut_df.loc[:,'sortcol'] = trigOut_sortcol
    
        #finally trigin
        trigIn_tstamps = trigIn_df.timestamp.to_numpy()
        trigIn_tstamp_diff = np.diff(trigIn_tstamps)
        trigIn_rollover_inds = np.argwhere(trigIn_tstamp_diff < 0).flatten()
        trigIn_rollover_inds = np.insert(trigIn_rollover_inds,0,-1)
        trigIn_rollover_inds = np.append(trigIn_rollover_inds,len(trigIn_tstamps))
    
        trigIn_sortcol = []
        counter = 0
        for i,j in zip(trigIn_rollover_inds[:-1],trigIn_rollover_inds[1:]):
            trigIn_sortcol.extend([counter for x in range(j-i)])
            counter += 1
    
        trigIn_sortcol = np.array(trigIn_sortcol[:-1])
        trigIn_sortcol = (trigIn_sortcol << 32) | trigIn_tstamps
    
        trigIn_df.loc[:,'sortcol'] = trigIn_sortcol
    
        #now re-combine dataframes
        combined_df = pps_df.append(trigOut_df,sort=True)
        combined_df = combined_df.append(trigIn_df,sort=True)
        combined_df.sort_index(inplace=True)
    
        sc = combined_df.sortcol.to_numpy()
    
        #sort combined df by sortcol
        combined_df = combined_df.sort_values(by=['sortcol'])
        sc = combined_df.sortcol.to_numpy()
    
        combined_df.drop(columns=['sortcol'])
    
        new_tstamps = combined_df.timestamp.to_numpy()
        old_tstamps = self.rosspad_df.timestamp.to_numpy()
    
        combined_df = combined_df.reset_index(drop=True)
    
        #check if there are multiple PPS's in a row at the beginning, and if so, drop
        # this can happen if it was out of order before
        new_start_ind = 0
        first_trig_out = 0
        isPPS = combined_df.pps.to_numpy()
        isTrigIn = combined_df.trigack.to_numpy()
        for i in range(len(isPPS)):
            if isPPS[i] == False and isTrigIn[i] == False:
                first_trig_out = i
                break
        new_start_ind = first_trig_out-1
        while isPPS[new_start_ind] != True:
            new_start_ind -= 1
        combined_df = combined_df[new_start_ind:]
    
        #check sorting just in case
        rpd_tstamps = combined_df.timestamp.to_numpy()
        rpd_tstamps_diff = np.diff(rpd_tstamps)
        rpd_tstamps_diff = np.array([x+2**32 if x < -1000000000 else x for x in rpd_tstamps_diff])
        

        if np.any(rpd_tstamps_diff < 0):
            combined_df.loc[np.where(rpd_tstamps_diff < 0)[0], 'timestamp']   = combined_df.timestamp[np.where(rpd_tstamps_diff < 0)[0]-1]+1
            rpd_tstamps = combined_df.timestamp.to_numpy()
            rpd_tstamps_diff = np.diff(rpd_tstamps)
            rpd_tstamps_diff = np.array([x+2**32 if x < -1000000000 else x for x in rpd_tstamps_diff])

            if np.any(rpd_tstamps_diff < 0):
                raise(SortError)

        self.rosspad_df = combined_df

    #sometimes arduino events out of order, sort here
    def sort_arduino(self):
        #separate to find the rollovers by looking at timestamp diff
        pps_df = self.arduino_df[self.arduino_df.ID == "P"].copy()
        trigOut_df = self.arduino_df[self.arduino_df.ID == "R"].copy()
        trigIn_df = self.arduino_df[self.arduino_df.ID == "C"].copy()
    
        #first pps
        pps_tstamps = pps_df.timestamp.to_numpy()
        pps_tstamp_diff = np.diff(pps_tstamps)
        pps_rollover_inds = np.argwhere(pps_tstamp_diff < 0).flatten()
        #start at -1 or else the sortcol and the timestamps are offset by 1
        # and it will take you 10 hours to figure out why nothing works
        pps_rollover_inds = np.insert(pps_rollover_inds,0,-1)
        pps_rollover_inds = np.append(pps_rollover_inds,len(pps_tstamps))
    
        pps_sortcol = []
        counter = 0
        for i,j in zip(pps_rollover_inds[:-1],pps_rollover_inds[1:]):
            pps_sortcol.extend([counter for x in range(j-i)])
            counter += 1
    
        #remove last element of sortcol, since it's extra
        pps_sortcol = np.array(pps_sortcol[:-1])
        pps_tstamps = np.array(pps_tstamps,dtype=int)
        pps_sortcol = (pps_sortcol << 32) | pps_tstamps
    
        pps_df.loc[:,'sortcol'] = pps_sortcol
    
    
        #next trigout
        trigOut_tstamps = trigOut_df.timestamp.to_numpy()
        trigOut_tstamp_diff = np.diff(trigOut_tstamps)
        trigOut_rollover_inds = np.argwhere(trigOut_tstamp_diff < 0).flatten()
        trigOut_rollover_inds = np.insert(trigOut_rollover_inds,0,-1)
        trigOut_rollover_inds = np.append(trigOut_rollover_inds,len(trigOut_tstamps))
    
        trigOut_sortcol = []
        counter = 0
        for i,j in zip(trigOut_rollover_inds[:-1],trigOut_rollover_inds[1:]):
            trigOut_sortcol.extend([counter for x in range(j-i)])
            counter += 1
    
        trigOut_sortcol = np.array(trigOut_sortcol[:-1])
        trigOut_tstamps = np.array(trigOut_tstamps,dtype=int)
        trigOut_sortcol = (trigOut_sortcol << 32) | trigOut_tstamps
    
        trigOut_df.loc[:,'sortcol'] = trigOut_sortcol
    
        #finally trigin
        trigIn_tstamps = trigIn_df.timestamp.to_numpy()
        trigIn_tstamp_diff = np.diff(trigIn_tstamps)
        trigIn_rollover_inds = np.argwhere(trigIn_tstamp_diff < 0).flatten()
        trigIn_rollover_inds = np.insert(trigIn_rollover_inds,0,-1)
        trigIn_rollover_inds = np.append(trigIn_rollover_inds,len(trigIn_tstamps))
    
        trigIn_sortcol = []
        counter = 0
        for i,j in zip(trigIn_rollover_inds[:-1],trigIn_rollover_inds[1:]):
            trigIn_sortcol.extend([counter for x in range(j-i)])
            counter += 1
    
        trigIn_sortcol = np.array(trigIn_sortcol[:-1])
        trigIn_tstamps = np.array(trigIn_tstamps,dtype=int)
        trigIn_sortcol = (trigIn_sortcol << 32) | trigIn_tstamps
    
        trigIn_df.loc[:,'sortcol'] = trigIn_sortcol
    
        #now re-combine dataframes
        combined_df = pps_df.append(trigOut_df,sort=True)
        combined_df = combined_df.append(trigIn_df,sort=True)
        combined_df.sort_index(inplace=True)
    
        sc = combined_df.sortcol.to_numpy()
    
        #sort combined df by sortcol
        combined_df = combined_df.sort_values(by=['sortcol'])
        sc = combined_df.sortcol.to_numpy()
    
        combined_df.drop(columns=['sortcol'])
    
        new_tstamps = combined_df.timestamp.to_numpy()
        old_tstamps = self.arduino_df.timestamp.to_numpy()
#        print(len(new_tstamps),len(old_tstamps))

        combined_df = combined_df.reset_index(drop=True)

        #check timestamps are in order    
        ard_tstamps = combined_df.timestamp.to_numpy()
        ard_tstamps_diff = np.diff(ard_tstamps)
        ard_tstamps_diff = np.array([x+2**32 if x < -1000000000 else x for x in ard_tstamps_diff])
        if np.any(ard_tstamps_diff < 0):
            combined_df.loc[np.where(ard_tstamps_diff < 0)[0], 'timestamp']   = combined_df.timestamp[np.where(ard_tstamps_diff < 0)[0]-1]+1
            
            ard_tstamps = combined_df.timestamp.to_numpy()
            ard_tstamps_diff = np.diff(ard_tstamps)
            ard_tstamps_diff = np.array([x+2**32 if x < -1000000000 else x for x in ard_tstamps_diff])
            if np.any(ard_tstamps_diff < 0):
                raise SortError

        self.arduino_df = combined_df


    #Makes the data frame columns a little bit nicer and combines the TrigIns and EventIDs
    def arduino_cleanup(self):
        origUTCA = self.arduino_df.UTCA.to_numpy()
        ID = self.arduino_df.ID.to_numpy()

        #column of only PPS UTC times
        newUTCA = np.array([origUTCA[i] if ID[i] == 'P' else np.nan for i in range(len(ID))])

        #column for missed interrupt flag on the arduino
        missedInterrupt = np.array([True if x == 2 else False for x in origUTCA])

        #add new / modified columns
        self.arduino_df.UTCA = newUTCA
        self.arduino_df['MissedInterrupt'] = missedInterrupt


        

        #combine TrigAcks and EventIDs
        #first separate the EventIDs out
        eventIDdf = self.arduino_df[self.arduino_df.ID == 'E'].copy()
        #since EventID comes after TrigACK, drop index by 1 for merging
        eventIDdf.index = eventIDdf.index.to_numpy() - 1

        self.arduino_df = self.arduino_df[self.arduino_df.ID != 'E'].copy()
        self.arduino_df['EventID'] = np.empty(len(self.arduino_df))

        #because event IDs are sometimes dropped, need to go through and check
        #maybe there is a cleverer way but adding manually seems simplest
        trigInIndex = self.arduino_df[self.arduino_df.ID == 'C'].index.to_numpy()
        eventIDdf['paired'] = np.zeros(len(eventIDdf))

        

                
        unionIdx =   reduce(np.intersect1d, (trigInIndex, eventIDdf.index))
        self.arduino_df.loc[unionIdx,'EventID'] = eventIDdf.loc[unionIdx].timestamp
        eventIDdf.loc[unionIdx,'paired'] =  1
        nDropped = trigInIndex.shape[0] - unionIdx.shape[0]
        
    
        #save eventIDs without TrigAcks to be added in insert_trigacks() function
        self.eventIDsNoTrigAcks = eventIDdf[eventIDdf['paired'] == 0].copy()
        #        print(self.eventIDsNoTrigAcks)

        #convert UTC to nanoseconds
        self.eventIDsNoTrigAcks.UTCC = self.eventIDsNoTrigAcks.UTCC.to_numpy()*1e9
        
        
        print("EVENT IDs:")
        print(nDropped,  "dropped event IDs", "out of" , self.arduino_df['ID'][self.arduino_df['ID'] == 'C'].size)
        print(nDropped/self.arduino_df['ID'][self.arduino_df['ID'] == 'C'].size*100, "Percent of dropped IDs")
        print(len(self.eventIDsNoTrigAcks),"event IDs without a TrigAck")
        print("Min:", self.arduino_df['EventID'][self.arduino_df['EventID'].diff().between(1, 100)].iloc[1],
              "Max: ", self.arduino_df['EventID'][self.arduino_df['EventID'] > 0].iloc[-1], "\n")



    #check for extra PPS's and remove them; check for dropped PPS's
    def check_pps(self):
        rosspad_freq = 100e6 #supposed rosspad clock frequency
        arduino_freq = 42e6  #supposed arduino clock frequency

        #find and remove any extra rosspad PPS's
        rosspad_pps = self.rosspad_df[self.rosspad_df.pps == True].timestamp.to_numpy()
        rosspad_pps_inds = self.rosspad_df[self.rosspad_df.pps == True].index.to_numpy()
        rosspad_pps_diff = np.diff(rosspad_pps)
        #don't forget rollover!
        rosspad_pps_diff = np.array([x+2**32-1 if x<0 else x for x in rosspad_pps_diff])
        #convert to seconds
        rosspad_pps_diff = rosspad_pps_diff/rosspad_freq

        #Get rid of extra PPS's
        diff_pps_timestamp = self.rosspad_df[self.rosspad_df.pps].timestamp.diff()/rosspad_freq
        extra_pps_inds = diff_pps_timestamp[(diff_pps_timestamp > -0.1) & (diff_pps_timestamp < 0.75)].index
        if len(extra_pps_inds) > 0:
            self.rosspad_df = self.rosspad_df.drop(extra_pps_inds)
            print("     Extra PPS signals Found.")

        #add dropped PPS's back in
        dropped_pps = np.argwhere(rosspad_pps_diff > 1.1).flatten()
        for dpps in dropped_pps:
            clock_times = self.rosspad_df[rosspad_pps_inds[dpps]:rosspad_pps_inds[dpps+1]+1].timestamp.to_numpy()
            indices = self.rosspad_df[rosspad_pps_inds[dpps]:rosspad_pps_inds[dpps+1]+1].index.to_numpy()

            #ROLLOVER
            rollover = False
            if (clock_times[-1] < clock_times[0]):
                clock_diff = np.diff(clock_times)
                rollInd = int(np.argwhere(clock_diff < 0))

                clock_times[rollInd+1:] += 2**32
                rollover = True

            clock_high = clock_times[-1]
            clock_low = clock_times[0]
            missing_clock = clock_low + (clock_high - clock_low)/2

            insertAfterLocal = max(np.argwhere(missing_clock > clock_times).flatten())

            insertAfter = insertAfterLocal + indices[0]

            #now that we know where to insert missing PPS, make dataframe line and insert it
            columns = list(self.rosspad_df.columns)

            d = {col:[0] for col in columns}
            d['timestamp'] = [missing_clock]
            if rollover:
                if insertAfterLocal > rollInd:
                    d['timestamp'] = [missing_clock - 2**32]
            d['pps'] = [True]
            d['trigack'] = [False]

            newInd = insertAfter + 0.5
#            print(newInd)
            d['index'] = [newInd]
            pps_df = pd.DataFrame.from_dict(d)
            pps_df.set_index('index',inplace=True)

            self.rosspad_df = self.rosspad_df.append(pps_df, ignore_index = False)


        self.rosspad_df = self.rosspad_df.sort_index().reset_index(drop=True)

        #find and remove any extra arduino PPS's
        arduino_pps = self.arduino_df[self.arduino_df.ID == "P"].timestamp.to_numpy()
        arduino_pps_inds = self.arduino_df[self.arduino_df.ID == "P"].index.to_numpy()
        arduino_pps_diff = np.diff(arduino_pps)
        #don't forget rollover!
        arduino_pps_diff = np.array([x+2**32-1 if x<0 else x for x in arduino_pps_diff])
        #convert to seconds
        arduino_pps_diff = arduino_pps_diff/arduino_freq

        extra_pps_1 = np.argwhere(arduino_pps_diff < 0.5).flatten()
        extra_pps_2 = np.argwhere((arduino_pps_diff > 0.5) & (arduino_pps_diff < 0.9)).flatten()
        for i1,i2 in zip(extra_pps_1,extra_pps_2):
            index_to_remove = arduino_pps_inds[max(i1,i2)]
            self.arduino_df = self.arduino_df.drop([index_to_remove])
        self.arduino_df = self.arduino_df.reset_index(drop=True)

        #look for dropped arduino PPS's
        nDroppedPPS = np.sum(np.round(arduino_pps_diff[np.argwhere(arduino_pps_diff > 1.5)]))-len(np.argwhere(arduino_pps_diff > 1.5))
        nDroppedPPS = int(nDroppedPPS)
        if nDroppedPPS > 0:
            droppedPPS_df = pd.DataFrame(np.zeros((nDroppedPPS, len(self.arduino_df.columns))), columns=self.arduino_df.columns)
            droppedPPS_df['ID'] = droppedPPS_df['ID'].astype(object)
            droppedPPS_df['MissedInterrupt'] = droppedPPS_df['MissedInterrupt'].astype(bool)
            droppedPPS_df['timestamp'] = droppedPPS_df['timestamp'].astype(np.float64)


            # looks like a few pps were missed. Lets calculate them
            counter = 0
            for pps_inds in range(0,len(arduino_pps_inds)-1):
                nMissed = round(arduino_pps_diff[pps_inds])-1
                if nMissed > 0:
                    for i in range(nMissed):
                        #insert extrapolated PPS
                        droppedPPS_df.ID.at[counter] = "P"
                        droppedPPS_df.MissedInterrupt.at[counter] = False
                        
                        if i == 0:
                            prev_ard_pps_inds = arduino_pps_inds[pps_inds]
    
                            droppedPPS_df.UTCA.at[counter] = self.arduino_df[self.arduino_df.ID == "P"].loc[prev_ard_pps_inds].UTCA + 1.0
                            droppedPPS_df.UTCC.at[counter] = self.arduino_df[self.arduino_df.ID == "P"].loc[prev_ard_pps_inds].UTCC + 1.0
                            
                            next_timestamp = self.arduino_df[self.arduino_df.ID == "P"].loc[prev_ard_pps_inds].timestamp + arduino_freq*1.0
                            
                        else:
                            droppedPPS_df.UTCA.at[counter] = droppedPPS_df.UTCA.at[counter-1] + 1.0
                            droppedPPS_df.UTCC.at[counter] = droppedPPS_df.UTCC.at[counter-1] + 1.0
                            
                            next_timestamp = droppedPPS_df.timestamp.at[counter-1] + arduino_freq*1.0
     
                        if next_timestamp > 2**32:
                            next_timestamp = next_timestamp - 2**32
                        droppedPPS_df.timestamp.at[counter] = next_timestamp
                        counter = counter + 1
                
            print("arduino")
            # raise DroppedPPS
            
            self.arduino_df = pd.concat([self.arduino_df, droppedPPS_df])
            
            self.arduino_df = self.arduino_df.sort_values('UTCC')
            self.arduino_df = self.arduino_df.reset_index(drop=True)
            

        #make sure rosspad and arduino have same # of PPS's
        #they may not, it depends on when data collection was stopped
        #so truncate whichever dataframe goes on for longer
        #  regenerate the index arrays in case some PPS's were removed above
        arduino_pps_inds = self.arduino_df[self.arduino_df.ID == "P"].index.to_numpy()
        rosspad_pps_inds = self.rosspad_df[self.rosspad_df.pps].index.to_numpy()


        minNumPPS = min(len(arduino_pps_inds),len(rosspad_pps_inds))
        lastArdInd = arduino_pps_inds[minNumPPS-1]
        lastRpdInd = rosspad_pps_inds[minNumPPS-1]

        self.arduino_df = self.arduino_df[:lastArdInd+1]
        self.rosspad_df = self.rosspad_df[:lastRpdInd+1]


    #check number of triggers per second and make sure it basically agrees
    def count_dropped_trigs(self):
        #first check number of rosspad TrigOuts
        rpd_TO = self.rosspad_df[(self.rosspad_df.trigack == False)].reset_index(drop=True)
        ard_TO = self.arduino_df[(self.arduino_df.ID == "P") | (self.arduino_df.ID == "R")].reset_index(drop=True)

        rpd_pps_inds = rpd_TO.index[rpd_TO.pps == True].to_numpy()
        ard_pps_inds = ard_TO.index[ard_TO.ID == "P"].to_numpy()

        rpd_trigsPerSec = np.diff(rpd_pps_inds)-1
        ard_trigsPerSec = np.diff(ard_pps_inds)-1
        minLen = min(len(rpd_trigsPerSec),len(ard_trigsPerSec))
        rpd_trigsPerSec = rpd_trigsPerSec[0:minLen]
        ard_trigsPerSec = ard_trigsPerSec[0:minLen]

        ard_extra = np.sum(ard_trigsPerSec - rpd_trigsPerSec)
        print("Arduino has",ard_extra/np.sum(rpd_trigsPerSec)*100,"% extra TrigOuts (normally has ~10% extra TrigOuts with all logs on for background run)")

        #check number of TrigAcks
        #want to know where # of TrigAcks disagree so we can add them in later
        rpd_TA = self.rosspad_df[(self.rosspad_df.trigack) | (self.rosspad_df.pps)]
        rpd_TA.index.rename("index_orig",inplace=True)
        rpd_TA = rpd_TA.reset_index()

        ard_TA = self.arduino_df[(self.arduino_df.ID == "P") | (self.arduino_df.ID == "C")]
        ard_TA.index.rename("index_orig",inplace=True)
        ard_TA = ard_TA.reset_index()

        rpd_pps_inds = rpd_TA.index[rpd_TA.pps == True].to_numpy()
        ard_pps_inds = ard_TA.index[ard_TA.ID == "P"].to_numpy()

        rpd_trigsPerSec = np.diff(rpd_pps_inds)-1
        ard_trigsPerSec = np.diff(ard_pps_inds)-1
        minLen = min(len(rpd_trigsPerSec),len(ard_trigsPerSec))
        rpd_trigsPerSec = rpd_trigsPerSec[0:minLen]
        ard_trigsPerSec = ard_trigsPerSec[0:minLen]

        n_rosspad_drops = np.sum(ard_trigsPerSec - rpd_trigsPerSec)
        print("ROSSPAD dropped",n_rosspad_drops/np.sum(ard_trigsPerSec)*100,"% of TrigACKs (normally drops ~2.7%)\n")


    #calculate and assign UTC times to rosspad and arduino events
    def assign_UTC_times(self):
        #give UTC time to all arduino events
        # figure out time since last PPS
        aTimeSinceLastPPS = self.calculate_time_since_pps(self.arduino_df)

        # get UTC time of last PPS to add to
        ppsUTC = self.arduino_df[self.arduino_df.ID == "P"].UTCA.to_numpy()
        ppsInds = self.arduino_df[self.arduino_df.ID == "P"].index.to_numpy()
        indices = self.arduino_df.index.to_numpy()
        indexOfPrevPPS = [np.argwhere(ppsInds <= x).max() for x in indices]
        timeOfLastPPS = ppsUTC[indexOfPrevPPS]

        utcTimes = timeOfLastPPS + aTimeSinceLastPPS
        utcTimes = utcTimes * 1e9 #convert to nanoseconds
        self.arduino_df['UTC'] = utcTimes

        #give UTC time to rosspad events
        # figure out time since last PPS
        rTimeSinceLastPPS = self.calculate_time_since_pps(self.rosspad_df)

        # since we have already checked that the PPS's correlate,
        # can reuse ppsUTC and do the same as above
        ppsInds = self.rosspad_df[self.rosspad_df.pps].index.to_numpy()
        indices = self.rosspad_df.index.to_numpy()
        indexOfPrevPPS = [np.argwhere(ppsInds <= x).max() for x in indices]
#        print(len(ppsUTC),len(indexOfPrevPPS))
        timeOfLastPPS = ppsUTC[indexOfPrevPPS]

        utcTimes = timeOfLastPPS + rTimeSinceLastPPS
        utcTimes = utcTimes * 1e9 #convert to nanoseconds
        self.rosspad_df['UTC'] = utcTimes

        #line up the end
        lastArdPPS = float(self.arduino_df[self.arduino_df.ID == 'P'].UTC.to_numpy()[-1:])
        lastRpdPPS = float(self.rosspad_df[self.rosspad_df.pps].UTC.to_numpy()[-1:])
        lastPPS = min(lastArdPPS,lastRpdPPS)
        self.arduino_df = self.arduino_df[self.arduino_df['UTC'] < lastPPS]
        self.rosspad_df = self.rosspad_df[self.rosspad_df['UTC'] < lastPPS]

        #now that they're lined up, get count rate and run time
        pps = self.arduino_df[self.arduino_df.ID == 'P'].UTC.to_numpy()
        runTime = (pps[-1]-pps[0])/1e9
        counts = len(self.rosspad_df[(self.rosspad_df.pps == False) & (self.rosspad_df.trigack == False)])
        countRate = counts/runTime
        print("\n CsI Count Rate:",countRate,"cps")
        print("EventID Count Rate:", self.arduino_df['ID'][self.arduino_df['ID'] == 'C'].size/((self.arduino_df.UTC.max()-self.arduino_df.UTC.min())/1000000000) ,"cps")
        print("Run length:",runTime,"s\n")


    def calculate_time_since_pps(self,df):
        try:
            ppsInds = df[df.ID == "P"].index.to_numpy()
            ppsTimestamps = df[df.ID == "P"].timestamp.to_numpy()
        except AttributeError:
            ppsInds = df[df.pps].index.to_numpy()
            ppsTimestamps = df[df.pps].timestamp.to_numpy()

        #get number of clock ticks per second
        ticksPerSec = np.diff(ppsTimestamps)
        #append mean to get frequency of counts after last PPS
        ticksPerSec = np.append(ticksPerSec,np.mean(ticksPerSec))
        #handle rollover
        ticksPerSec = np.array([x + 2**32-1 if x < 0 else x for x in ticksPerSec])

        indices = df.index.to_numpy()
        indexOfPrevPPS = [np.argwhere(ppsInds <= x).max() for x in indices]
        ppsTimestamps = ppsTimestamps[indexOfPrevPPS]
        ticksPerSec = ticksPerSec[indexOfPrevPPS]

        timestamps = df.timestamp.to_numpy()
        timeSinceLastPPS = timestamps - ppsTimestamps
        timeSinceLastPPS = np.array([x + 2**32-1 if x < 0 else x for x in timeSinceLastPPS])
        timeSinceLastPPS = timeSinceLastPPS / ticksPerSec

        return timeSinceLastPPS


    #check that the lined-up arduino and rosspad TrigOut times agree well
    def check_trigOut_times(self):
        lineup_index_tuple = self.lineup_times()
        rpdInds = lineup_index_tuple[:,0]
        ardInds = lineup_index_tuple[:,1]

        #times are in nanoseconds
        rosspad_trigOut_times = self.rosspad_df['UTC'][rpdInds].to_numpy()
        arduino_trigOut_times = self.arduino_df['UTC'][ardInds].to_numpy()

        tstamp_diff = abs(rosspad_trigOut_times - arduino_trigOut_times)
        nDiffGreaterThan1us = np.count_nonzero(tstamp_diff > 1e3)

        print(len(rosspad_trigOut_times)-nDiffGreaterThan1us,"out of",len(rosspad_trigOut_times),"TrigOut times agree between arduino and rosspad to within 1 us\n")

    #lineup TrigOut times between arduino and rosspad, since there are extra arduino TrigOuts
    def lineup_times(self):
        rosspad_tstamps = self.rosspad_df[(self.rosspad_df.pps == False) & (self.rosspad_df.trigack == False)].UTC.to_numpy()
        rosspad_index = self.rosspad_df[(self.rosspad_df.pps == False) & (self.rosspad_df.trigack == False)].index.to_numpy()
        arduino_tstamps = self.arduino_df[self.arduino_df.ID == "R"].UTC.to_numpy()
        arduino_index = self.arduino_df[self.arduino_df.ID == "R"].index.to_numpy()
        lineup_index_tuple = []
    
        index = self.arduino_df.index.to_numpy()
    
        lastArd = 0
        failedToMatch = []
    
        for rpdT,rpdI in zip(rosspad_tstamps,rosspad_index):
            bestMatchI = -1
            lastDiff = 1e12
    
            for i in range(lastArd,len(arduino_tstamps)):
                currentDiff = abs(rpdT-arduino_tstamps[i])
    
                if currentDiff > lastDiff:
                    bestMatchI = i-1
                    break
    
                else:
                    lastDiff = currentDiff
    
            if bestMatchI == -1:
                failedToMatch.append(rpdI)
    
            else:
                lineup_index_tuple.append((rpdI,arduino_index[bestMatchI]))
            lastArd = bestMatchI+1
    
    
        lineup_index_tuple = np.array(lineup_index_tuple)
    
        #sanity check that I get the same number of extra arduino triggers
        lastIndRpd = lineup_index_tuple[-1][0]
        lastIndArd = lineup_index_tuple[-1][1]
        numExtra = np.argwhere(arduino_index == lastIndArd) - np.argwhere(rosspad_index == lastIndRpd)

        nMatched = len(rosspad_tstamps)
        print("Matched",nMatched-len(failedToMatch),"ROSSPAD TrigOuts (",len(failedToMatch),"failed to match)")
#        print(failedToMatch)
#        print(rosspad_index[-1])
    
        return lineup_index_tuple



    #insert trigacks that are missing from rosspad stream and add eventIDs
    def insert_trigacks(self):
        rowsToDrop = self.rosspad_df[self.rosspad_df.trigack].index.to_numpy()
        self.rosspad_df = self.rosspad_df.drop(rowsToDrop)

        #add eventID column
        self.rosspad_df["EventID"] = [0 for x in range(len(self.rosspad_df))]
        self.rosspad_df["DroppedTrigAck"] = [0 for x in range(len(self.rosspad_df))]

        timesToAdd = self.arduino_df[self.arduino_df.ID == "C"].UTC.to_numpy()
        eventIDsToAdd = self.arduino_df[self.arduino_df.ID == "C"].EventID.to_numpy(dtype=int)
        noTrigAckFlag = np.zeros(len(timesToAdd))
        nAddedWithTrigAck = len(timesToAdd)
        #add eventIDs that came without TrigAcks
        timesToAdd = np.append(timesToAdd,self.eventIDsNoTrigAcks.UTCC.to_numpy())
        eventIDsToAdd = np.append(eventIDsToAdd,self.eventIDsNoTrigAcks.timestamp.to_numpy(dtype=int))
        noTrigAckFlag = np.append(noTrigAckFlag,np.ones(len(timesToAdd)-nAddedWithTrigAck))
        nRowsToAdd = len(timesToAdd)
        
        # XXX DS-202201, dont know what this is or if I need it
        # print(len(noTrigAckFlag),nRowsToAdd)

        columns = list(self.rosspad_df.columns)
        columns.remove("UTC")
        columns.remove("pps")
        columns.remove("trigack")
        columns.remove("EventID")

        d = {col:[0 for x in range(nRowsToAdd)] for col in columns}
        d['UTC'] = timesToAdd
        d['pps'] = [False for x in range(nRowsToAdd)]
        d['trigack'] = [True for x in range(nRowsToAdd)]
        d['EventID'] = eventIDsToAdd
        d['DroppedTrigAck'] = noTrigAckFlag

        trigack_df = pd.DataFrame.from_dict(d)

        self.rosspad_df = self.rosspad_df.append(trigack_df,sort=False)
        self.rosspad_df = self.rosspad_df.sort_values(by=['UTC']).reset_index(drop=True)


    #write edited dataframe to new hdf files
    def write_files(self):
        #drop timestamp and sortcol columns
        columns = list(self.rosspad_df.columns)
        columns.remove('timestamp')
        columns.remove('sortcol')
        self.rosspad_df = self.rosspad_df[columns]


        #write to file
        path = Path(self.rfname)
        hdfpath = path.with_suffix('.L1.1.hdf5')
        # hdfpath = path.with_suffix('.hdf5')
        with pd.HDFStore(hdfpath) as hdf:
            self.rosspad_df.to_hdf(hdf,key='TIM')
        print("File written:")
        print(hdfpath)
        
        # store = pd.HDFStore(hdfpath)
        # store.append('name_of_frame', ohlcv_candle, format='t',  data_columns=True)



if __name__ == '__main__':

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("rfname",help="rosspad hdf5 file")
    parser.add_argument("afname",nargs='+',help="arduino log file")
    args = parser.parse_args()

    R = RosspadToL1_1(args.rfname,args.afname)
    R.process()

