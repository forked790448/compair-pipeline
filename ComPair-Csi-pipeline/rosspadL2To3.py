#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Develop L3 Level Data
 - takes either L2 data and adds overhead requested for the ComPair Unified Dataformat
 - I break it up because my computer cannot handle
     large datafile.
"""

import os
import os.path

#math
import pandas as pd
import numpy as np


from pathlib import Path
from tqdm import tqdm # progress bar
import re

#math tools
from functools import reduce
import itertools



import h5py



class RosspadL2To3:
    def __init__(self,rfname):
        self.rfname = rfname      
        
        self.E_THRESH = 200 #200 keV lower energy threshold
        
        # Other Global Variables
        self.chunk_size = 1000000
        
        
        self.coinc_window = 10 *1000 # 100 thousand nano seconds (100 us)
        self.prev_frame = 0
        
        

    def process(self):
        if(self.get_time_align() == False):
            return
                
        self.chunk_index = 0

        # iterate over table chunks
        path = Path(self.rfname.rsplit('.',2)[0])
        self.hdfpath = path.with_suffix('.L3.h5')
        
        try:
            os.remove(str(self.hdfpath))
        except:
            pass 
        
        chunks = pd.read_hdf(self.rfname, key='ADC', chunksize=self.chunk_size)
        with tqdm(total=chunks.nrows) as pbar:
            for self.pps_adc_df in chunks:
                if self.chunk_index == 0:
                    self.get_total_log()
                
                self.add_event_ids()
                self.remove_overhead()
                self.add_overhead()
                self.set_data_type()

                pbar.update(self.chunk_size)
                self.write_files()

                
    def set_data_type(self):
        
        # for i in range(0, 60):
        #     self.pps_adc_df[f'sipm_{i}'] = self.pps_adc_df[f'sipm_{i}'].astype('float32')
        
        for log in range(0, 30):
            self.pps_adc_df[f'L{log}_adc'] = self.pps_adc_df[f'L{log}_adc'].astype('float32')
            self.pps_adc_df[f'L{log}_pos'] = self.pps_adc_df[f'L{log}_pos'].astype('float32')



    def add_event_ids(self):
        # rollover frames


        
        union_event_ids = reduce(np.intersect1d, (self.pps_adc_df['frame'].values, self.time_df['frame'].values))
        
        

        
        # rearrange df according to frames to be cat later
        self.pps_adc_df = self.pps_adc_df.reset_index(drop=True)
        _, x, _ = np.intersect1d(self.pps_adc_df['frame'], union_event_ids, return_indices=True)
        self.pps_adc_df = self.pps_adc_df.iloc[x]
        self.pps_adc_df = self.pps_adc_df.reset_index(drop=True)

        
        time_df_tmp = self.time_df.reset_index(drop=True)
        _, x, _ = np.intersect1d(time_df_tmp['frame'], union_event_ids, return_indices=True)
        time_df_tmp = time_df_tmp.iloc[x]
        time_df_tmp = time_df_tmp.reset_index(drop=True)

        
        # add EventIDs to column
        self.pps_adc_df['EventID'] = time_df_tmp['EventID'].copy()
        self.pps_adc_df['UTC'] = time_df_tmp['UTC'].copy()
        


    def get_time_align(self):
        # Here we want to align all the trigOuts with EventIDs
        # L1.1 file check if file exists
        #write to file
        path = Path(self.rfname.rsplit('.',1)[0])
        hdfpath = path.with_suffix('.L1.1.hdf5')
        
        if os.path.isfile(hdfpath) == True:
            self.time_df = pd.read_hdf(hdfpath,key='TIM')
        
            # with pd.HDFStore(path.with_suffix('.L2.hdf5')) as hdf:
                # self.time_df.to_hdf(hdf,key='TIM')
        else:
            print('NO L1.1 File Found')
            return False
        
        
        # drop PPSs
        self.time_df = self.time_df[self.time_df['pps'] != True]
        self.time_df = self.time_df.drop('pps', axis=1)
        self.time_df = self.time_df.drop('DroppedTrigAck', axis=1)
        
        self.time_df  = self.time_df.reset_index(drop=True)


        self.time_df2 = self.time_df
        trigAckIdx = self.time_df['trigack'].index[self.time_df['trigack'] == True] ## all indexes that have trig ack
        self.time_df.loc[trigAckIdx-1, 'EventID'] = self.time_df.loc[trigAckIdx, 'EventID'].values 

        
        
        
        timeDiff = self.time_df['UTC'].loc[trigAckIdx-1].subtract(self.time_df['UTC'].loc[trigAckIdx].values).abs()
        coincPass = self.time_df['UTC'].loc[trigAckIdx-1].subtract(self.time_df['UTC'].loc[trigAckIdx].values).abs() < self.coinc_window
        # coincPass = coincPass[coincPass == True]
        
        
        self.time_df = self.time_df.loc[trigAckIdx-1]
        self.time_df = self.time_df[coincPass]

        
        self.time_df = self.time_df[self.time_df['frame'] > 0] #remove trick acks
        self.time_df = self.time_df[self.time_df['EventID'] > 0] #remove things without eventID
        self.time_df = self.time_df.drop('trigack', axis=1)
        
        
        
        
        
            
            
    def remove_overhead(self):
        self.pps_adc_df = self.pps_adc_df.drop('frame', axis=1)
        self.pps_adc_df = self.pps_adc_df.drop('timestamp', axis=1)
        self.pps_adc_df = self.pps_adc_df.drop('gpio', axis=1)

        
        self.pps_adc_df = self.pps_adc_df[self.pps_adc_df['pps'] != True]
        self.pps_adc_df = self.pps_adc_df.drop('pps', axis=1)
        
        self.pps_adc_df = self.pps_adc_df[self.pps_adc_df['trigack'] != True]
        self.pps_adc_df = self.pps_adc_df.drop('trigack', axis=1)


        
        

    def add_overhead(self):
        # for i in range(0, 60):
            # self.pps_adc_df[f'use_sipm_{i}'] = True
        
        for log in range(0, 30):
            self.pps_adc_df[f'use_L{log}_adc'] = (self.pps_adc_df[f'L{log}_adc'] > self.E_THRESH)
            self.pps_adc_df[f'use_L{log}_pos'] = self.pps_adc_df[f'L{log}_pos'].between(-50,50)
            
            self.pps_adc_df.loc[self.pps_adc_df[f'L{log}_adc'] < self.E_THRESH, f'L{log}_adc'] = 0
            self.pps_adc_df.loc[self.pps_adc_df[f'L{log}_pos'] < -50, f'L{log}_pos'] = -50
            self.pps_adc_df.loc[self.pps_adc_df[f'L{log}_pos'] > 50, f'L{log}_pos'] = 50



            
        
        
    def get_total_log(self):
        filter_col = [col for col in self.pps_adc_df if col.startswith('L')]
        
        name_of_last_column = filter_col[len(filter_col)-1]
        self.total_logs = (int(re.findall(r'[0-9]+', name_of_last_column)[0])+1) #n total channels in system
        
        
    #write edited dataframe to new hdf files
    def write_files(self):
        #write to file

        self.pps_adc_df.index = self.pps_adc_df.index + self.chunk_index*self.chunk_size
        
        
        base = os.path.splitext(self.rfname)[0]
        h5File = str(self.hdfpath)
        
        adc_tmp = [f'L{num}_adc' for num in range(30)] 
        pos_tmp = [f'L{num}_pos' for num in range(30)]
        use_erg_tmp = [f'use_L{num}_adc' for num in range(30)] 
        use_pos_tmp = [f'use_L{num}_pos' for num in range(30)] 
        
        # sipm_tmp = [f'sipm_{num}' for num in range(60)] 
        # use_sipm_tmp = [f'use_sipm_{num}' for num in range(60)] 
        
        with h5py.File(h5File, 'a') as hf:
            if self.chunk_index == 0:
                hf.create_dataset("/EventID/EventID/values", data=self.pps_adc_df['EventID'].values, maxshape=(None,), chunks=True)
                hf.create_dataset("/time/UTC/values", data=self.pps_adc_df['UTC'].values, maxshape=(None,), chunks=True)

                hf.create_dataset("/erg/block0_values", data=self.pps_adc_df[adc_tmp].values, maxshape=(None,None), chunks=True)
                hf.create_dataset("/pos/block0_values", data=self.pps_adc_df[pos_tmp].values, maxshape=(None,None), chunks=True)
                    
                hf.create_dataset("/use_erg/block0_values", data=self.pps_adc_df[use_erg_tmp].values, maxshape=(None,None), chunks=True)
                hf.create_dataset("/use_pos/block0_values", data=self.pps_adc_df[use_pos_tmp].values, maxshape=(None,None), chunks=True)
                
                # hf.create_dataset("/sipm/block0_values", data=self.pps_adc_df[sipm_tmp].values, maxshape=(None,None), chunks=True)
                # hf.create_dataset("/use_sipm/block0_values", data=self.pps_adc_df[use_sipm_tmp].values, maxshape=(None,None), chunks=True)
            else:
                hf["/EventID/EventID/values"].resize((hf["/EventID/EventID/values"].shape[0] + self.pps_adc_df['EventID'].shape[0]), axis = 0)
                hf["/EventID/EventID/values"][-self.pps_adc_df['EventID'].shape[0]:] = self.pps_adc_df['EventID'].values
                
                hf["/time/UTC/values"].resize((hf["/time/UTC/values"].shape[0] + self.pps_adc_df['UTC'].shape[0]), axis = 0)
                hf["/time/UTC/values"][-self.pps_adc_df['UTC'].shape[0]:] = self.pps_adc_df['UTC'].values

                hf["/erg/block0_values"].resize((hf["/erg/block0_values"].shape[0] + self.pps_adc_df[adc_tmp].shape[0]), axis = 0)
                hf["/erg/block0_values"][-self.pps_adc_df[adc_tmp].shape[0]:] = self.pps_adc_df[adc_tmp].values
                    
                hf["/pos/block0_values"].resize((hf["/pos/block0_values"].shape[0] + self.pps_adc_df[pos_tmp].shape[0]), axis = 0)
                hf["/pos/block0_values"][-self.pps_adc_df[pos_tmp].shape[0]:] = self.pps_adc_df[pos_tmp].values
                    
                hf[f"/use_erg/block0_values"].resize((hf[f"/use_erg/block0_values"].shape[0] + self.pps_adc_df[use_erg_tmp].shape[0]), axis = 0)
                hf[f"/use_erg/block0_values"][-self.pps_adc_df[use_erg_tmp].shape[0]:] = self.pps_adc_df[use_erg_tmp].values
                    
                hf["/use_pos/block0_values"].resize((hf["/use_pos/block0_values"].shape[0] + self.pps_adc_df[use_pos_tmp].shape[0]), axis = 0)
                hf["/use_pos/block0_values"][-self.pps_adc_df[use_pos_tmp].shape[0]:] = self.pps_adc_df[use_pos_tmp].values
                    
                # hf["/sipm/block0_values"].resize((hf[f"/sipm/block0_values"].shape[0] + self.pps_adc_df[sipm_tmp].shape[0]), axis = 0)
                # hf["/sipm/block0_values"][-self.pps_adc_df[sipm_tmp].shape[0]:] = self.pps_adc_df[sipm_tmp].values
                
                # hf["/use_sipm/block0_values"].resize((hf[f"/use_sipm/block0_values"].shape[0] + self.pps_adc_df[use_sipm_tmp].shape[0]), axis = 0)
                # hf["/use_sipm/block0_values"][-self.pps_adc_df[use_sipm_tmp].shape[0]:] = self.pps_adc_df[use_sipm_tmp].values
                    


        

        ## housekeeping after saving, just to make sure
        self.chunk_index = self.chunk_index + 1
        del self.pps_adc_df
        

        



if __name__ == '__main__':

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("rfname",help="rosspad hdf5 file")
    args = parser.parse_args()

    # print(args.rfname)
    R = RosspadL2To3(args.rfname)
    R.process()

