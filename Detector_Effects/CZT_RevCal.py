# This file will convert the CZT output of desh5.py from energy and relative position to anode, cathode, and pad pulseheights in each asic/block

# CZT Calibration temp has been 32C

# Steps:
# 1. read in CZT.sim.h5
# 2. for each event, get the energy and position from input file
# 3. Currently Lucas doesn't use pad pulseheight *******************************************************
# 4. Set ASIC temps to 32C
# 5. Set Cathode/Anode Times to event times ************************************************************
# 6. Set CorUTC to 1e9*event time ************************************************************************
# 7. relative z position ~ c2a ratio
#	c2a means = (c2a max - c2a min)/(bar height) * (z_rel - barheight/2) + c2a min
# 8. energy = anode_ph*interpolated function
#	interpolated function accounts for depth effects:
#	c2a means are the ratio of cathode pulseheight to anode pulseheight (x axis)
#	c2a scalars are 661.7/anode_value in ADC (y axis)


import h5py
import numpy as np
import argparse
from scipy.interpolate import interp1d
import datetime
import time
from abc import ABC, abstractmethod

CZT_BAR_HEIGHT = 1

class CZTChannelMapping(ABC):
    """ Abstract base class for associating array indices in the L0-HDF5 data
        with channels and for associating those channels with bars meaningfully.
    """
    def __init__(self, version='0'):
        self.__version = version
        self.__bardict = { '1-1': [ 'A1-1', 'C1-1',   'PL1',   'PT1', 'PX1-1', 'PY1-1' ],
                           '1-2': [ 'A1-2', 'C1-1', 'PX1-1',   'PT1', 'PX1-2', 'PY1-2' ],
                           '1-3': [ 'A1-3', 'C1-2', 'PX1-2',   'PT2', 'PX1-3', 'PY1-3' ],
                           '1-4': [ 'A1-4', 'C1-2', 'PX1-3',   'PT2',   'PR1', 'PY1-4' ],
                           '2-1': [ 'A2-1', 'C1-1',   'PL1', 'PY1-1', 'PX2-1', 'PY2-1' ],
                           '2-2': [ 'A2-2', 'C1-1', 'PX2-1', 'PY1-2', 'PX2-2', 'PY2-2' ],
                           '2-3': [ 'A2-3', 'C1-2', 'PX2-2', 'PY1-3', 'PX2-3', 'PY2-3' ],
                           '2-4': [ 'A2-4', 'C1-2', 'PX2-3', 'PY1-4',   'PR1', 'PY2-4' ],
                           '3-1': [ 'A3-1', 'C2-1',   'PL2', 'PY2-1', 'PX3-1', 'PY3-1' ],
                           '3-2': [ 'A3-2', 'C2-1', 'PX3-1', 'PY2-2', 'PX3-2', 'PY3-2' ],
                           '3-3': [ 'A3-3', 'C2-2', 'PX3-2', 'PY2-3', 'PX3-3', 'PY3-3' ],
                           '3-4': [ 'A3-4', 'C2-2', 'PX3-3', 'PY2-4',   'PR2', 'PY3-4' ],
                           '4-1': [ 'A4-1', 'C2-1',   'PL2', 'PY3-1', 'PX4-1',   'PB1' ],
                           '4-2': [ 'A4-2', 'C2-1', 'PX4-1', 'PY3-2', 'PX4-2',   'PB1' ],
                           '4-3': [ 'A4-3', 'C2-2', 'PX4-2', 'PY3-3', 'PX4-3',   'PB2' ],
                           '4-4': [ 'A4-4', 'C2-2', 'PX4-3', 'PY3-4',   'PR2',   'PB2' ] }
        self.__numdict = { '1-1': 0x0,  '1-2': 0x1,  '1-3': 0x2,  '1-4': 0x3,
                           '2-1': 0x4,  '2-2': 0x5,  '2-3': 0x6,  '2-4': 0x7,
                           '3-1': 0x8,  '3-2': 0x9,  '3-3': 0xA,  '3-4': 0xB,
                           '4-1': 0xC,  '4-2': 0xD,  '4-3': 0xE,  '4-4': 0xF }

    @abstractmethod
    def _define_maps(self):
        self._bar_number = np.arange(16)
        # These are the arrays of tags that the channel numbers correspond to...
        self._tag_anodes = np.array([ 'A1-1', 'A1-2', 'A1-3', 'A1-4',
                                      'A2-1', 'A2-2', 'A2-3', 'A2-4',
                                      'A3-1', 'A3-2', 'A3-3', 'A3-4',
                                      'A4-1', 'A4-2', 'A4-3', 'A4-4' ])
        self._tag_cathodes = np.array([ 'C1-1', 'C1-2',
                                        'C2-1', 'C2-2' ])
        self._tag_pads = np.array([   'PT1',   'PT2',   'PR1',   'PR2',   'PB2',
                                      'PB1',   'PL2',   'PL1', 'PX1-1', 'PX1-2',
                                    'PX1-3', 'PY1-4', 'PY2-4', 'PY3-4', 'PX4-3',
                                    'PX4-2', 'PX4-1', 'PY3-1', 'PY2-1', 'PY1-1',
                                    'PY1-2', 'PY1-3', 'PX2-3', 'PX3-3', 'PY3-3',
                                    'PY3-2', 'PX3-1', 'PX2-1', 'PX2-2', 'PY2-3',
                                    'PX3-2', 'PY2-2' ])
        # Derived classes are expected to define these arrays!
        #self._idx_anodes = np.array([])
        #self._idx_cathodes = np.array([])
        #self._idx_pads = np.array([])

    def tags_to_indices(self, tag_list):
        idx_list = []
        for tag in tag_list:
            if tag[0] ==  'A':
                idx_list += [ self._t2i_anodes[tag] ]
            elif tag[0] == 'C':
                idx_list += [ self._t2i_cathodes[tag] ]
            elif tag[0] == 'P':
                idx_list += [ self._t2i_pads[tag] ]
        return np.array(idx_list)

    def bar_to_tags(self, bar):
        return self.__bardict[bar]

    def bar_to_num(self, bar):
        return self.__numdict[bar]

    def num_to_pos_in_crate(self, num):
        row = num // 4
        col = num % 4

        if type(num) == np.ndarray:
            pos = np.zeros(shape=(num.size, 2))
        else:
            pos = np.zeros(2)

        pos[..., 0] = (col - 1.5)*7.2
        pos[..., 1] = -(row - 1.5)*7.2

        return pos

    def asic_to_crate_mid_pos(self, asic):
        row = asic // 4
        col = asic % 4

        if type(asic) == np.ndarray:
            pos = np.zeros(shape=(asic.size, 2))
        else:
            pos = np.zeros(2)

        pos[..., 0] = -(col - 1.5)*30.
        pos[..., 1] = -(row - 1.5)*30.

        return pos

    def asic_orientation(self, asic):
        row = asic // 4

        if type(asic) == np.ndarray:
            scalar = np.ones(asic.size)
            scalar[row % 2 == 1] = -1
        else:
            scalar = 1
            if row % 2 == 1:
                scalar = -1

        return -scalar

    def get_anode_tags(self):
        return self._tag_anodes

    def get_cathode_tags(self):
        return self._tag_cathodes

    def get_pad_tags(self):
        return self._tag_pads

    def get_anode_indices(self):
        return self._idx_anodes

    def get_cathode_indices(self):
        return self._idx_cathodes

    def get_pad_indices(self):
        return self._idx_pads

    def get_bar_anode_tags(self):
        bars = self.__bardict.keys()
        tags = [ self.bar_to_tags(bar)[0] for bar in bars ]
        return tags

    def get_bar_cathode_tags(self):
        bars = self.__bardict.keys()
        tags = [ self.bar_to_tags(bar)[1] for bar in bars ]
        return tags

    def get_bar_left_pad_tags(self):
        bars = self.__bardict.keys()
        tags = [ self.bar_to_tags(bar)[2] for bar in bars ]
        return tags

    def get_bar_top_pad_tags(self):
        bars = self.__bardict.keys()
        tags = [ self.bar_to_tags(bar)[3] for bar in bars ]
        return tags

    def get_bar_right_pad_tags(self):
        bars = self.__bardict.keys()
        tags = [ self.bar_to_tags(bar)[4] for bar in bars ]
        return tags

    def get_bar_bottom_pad_tags(self):
        bars = self.__bardict.keys()
        tags = [ self.bar_to_tags(bar)[5] for bar in bars ]
        return tags

    def get_bar_anode_indices(self):
        return self.tags_to_indices(self.get_bar_anode_tags())

    def get_bar_cathode_indices(self):
        return self.tags_to_indices(self.get_bar_cathode_tags())

    def get_bar_left_pad_indices(self):
        return self.tags_to_indices(self.get_bar_left_pad_tags())

    def get_bar_top_pad_indices(self):
        return self.tags_to_indices(self.get_bar_top_pad_tags())

    def get_bar_right_pad_indices(self):
        return self.tags_to_indices(self.get_bar_right_pad_tags())

    def get_bar_bottom_pad_indices(self):
        return self.tags_to_indices(self.get_bar_bottom_pad_tags())

    def print_tag_array(self):
        print('+------------------------------+------------------------------+')
        print('| C1-1   PT1           PT1     |     PT2           PT2   C1-2 |')
        print('|      +-----+       +-----+   |   +-----+       +-----+      |')
        print('| PL1  |A1-1 | PX1-1 |A1-2 | PX1-2 |A1-3 | PX1-3 |A1-4 | PR1  |')
        print('|      +-----+       +-----+   |   +-----+       +-----+      |')
        print('|       PY1-1         PY1-2    |    PY1-3         PY1-4       |')
        print('|      +-----+       +-----+   |   +-----+       +-----+      |')
        print('| PL1  |A2-1 | PX2-1 |A2-2 | PX2-2 |A2-3 | PX2-3 |A2-4 | PR1  |')
        print('|      +-----+       +-----+   |   +-----+       +-----+      |')
        print('+------ PY2-1 ------- PY2-2 ---+--- PY2-3 ------- PY2-4 ------+')
        print('|      +-----+       +-----+   |   +-----+       +-----+      |')
        print('| PL2  |A3-1 | PX3-1 |A3-2 | PX3-2 |A3-3 | PX3-3 |A3-4 | PR2  |')
        print('|      +-----+       +-----+   |   +-----+       +-----+      |')
        print('|       PY3-1         PY3-2    |    PY3-3         PY3-4       |')
        print('|      +-----+       +-----+   |   +-----+       +-----+      |')
        print('| PL2  |A4-1 | PX4-1 |A4-2 | PX4-2 |A4-3 | PX4-3 |A4-4 | PR2  |')
        print('|      +-----+       +-----+   |   +-----+       +-----+      |')
        print('| C2-1   PB1           PB1     |     PB2           PB2   C2-2 |')
        print('+------------------------------+------------------------------+')

    def print_idx_array(self):
        a = self._t2i_anodes
        c = self._t2i_cathodes
        p = self._t2i_pads
        print('+------------------------------+------------------------------+')
        print('| %3d    %3d           %3d     |     %3d           %3d   %3d  |'
            % (c['C1-1'], p['PT1'], p['PT1'], p['PT2'], p['PT2'], c['C1-2']))
        print('|      +-----+       +-----+   |   +-----+       +-----+      |')
        print('| %3d  | %3d |  %3d  | %3d |  %3d  | %3d |  %3d  | %3d | %3d  |'
            % (p['PL1'], a['A1-1'], p['PX1-1'], a['A1-2'],
               p['PX1-2'], a['A1-3'], p['PX1-3'], a['A1-4'], p['PR1']))
        print('|      +-----+       +-----+   |   +-----+       +-----+      |')
        print('|        %3d           %3d     |     %3d           %3d        |'
            % (p['PY1-1'], p['PY1-2'], p['PY1-3'], p['PY1-4']))
        print('|      +-----+       +-----+   |   +-----+       +-----+      |')
        print('| %3d  | %3d |  %3d  | %3d |  %3d  | %3d |  %3d  | %3d | %3d  |'
            % (p['PL1'], a['A2-1'], p['PX2-1'], a['A2-2'],
               p['PX2-2'], a['A2-3'], p['PX2-3'], a['A2-4'], p['PR1']))
        print('|      +-----+       +-----+   |   +-----+       +-----+      |')
        print('+------  %3d  -------  %3d  ---+---  %3d  -------  %3d  ------+'
            % (p['PY2-1'], p['PY2-2'], p['PY2-3'], p['PY2-4']))
        print('|      +-----+       +-----+   |   +-----+       +-----+      |')
        print('| %3d  | %3d |  %3d  | %3d |  %3d  | %3d |  %3d  | %3d | %3d  |'
            % (p['PL2'], a['A3-1'], p['PX3-1'], a['A3-2'],
               p['PX3-2'], a['A3-3'], p['PX3-3'], a['A3-4'], p['PR2']))
        print('|      +-----+       +-----+   |   +-----+       +-----+      |')
        print('|        %3d           %3d     |     %3d           %3d        |'
            % (p['PY3-1'], p['PY3-2'], p['PY3-3'], p['PY3-4']))
        print('|      +-----+       +-----+   |   +-----+       +-----+      |')
        print('| %3d  | %3d |  %3d  | %3d |  %3d  | %3d |  %3d  | %3d | %3d  |'
            % (p['PL2'], a['A4-1'], p['PX4-1'], a['A4-2'],
               p['PX4-2'], a['A4-3'], p['PX4-3'], a['A4-4'], p['PR2']))
        print('|      +-----+       +-----+   |   +-----+       +-----+      |')
        print('| %3d    %3d           %3d     |     %3d           %3d   %3d  |'
            % (c['C2-1'], p['PB1'], p['PB1'], p['PB2'], p['PB2'], c['C2-2']))
        print('+------------------------------+------------------------------+')

class CZTChannelMapping_v0(CZTChannelMapping):
    """ CZT channel mapping class using the documented AVG2 scheme
    """
    def __init__(self):
        super().__init__('0')
        self._define_maps()

    def _define_maps(self):
        super()._define_maps()
        # Anodes are mapped with upper left corner first, spanning the first
        #  row, continuing to the second row, then third, then fourth
        self._idx_anodes = np.array([ 50, 14,  7,  6,
                                      47, 19, 22, 15,
                                      42, 29, 28, 21,
                                      41, 36, 33, 32 ])
        self._i2t_anodes = dict(zip(self._idx_anodes, self._tag_anodes))
        self._t2i_anodes = dict(zip(self._tag_anodes, self._idx_anodes))
        # Cathodes are mapped with the upper left first, then upper right,
        #  then lower left, then lower right
        self._idx_cathodes = np.array([ 12, 25,
                                        38, 51 ])
        self._i2t_cathodes = dict(zip(self._idx_cathodes, self._tag_cathodes))
        self._t2i_cathodes = dict(zip(self._tag_cathodes, self._idx_cathodes))
        # Pads are laid out kinda strangely, so we start at the top left and
        #  spiral around clockwise until we reach the middle.  More logical
        #  grouping can be provided in the interface
        self._idx_pads = np.array([ 49,  1,  4, 30, 37, 40, 39, 46,  2,  3,
                                     0,  5, 18, 26, 31, 34, 35, 43, 45, 48,
                                    11,  9,  8, 24, 23, 27, 44, 16, 10, 13,
                                    17, 20 ])
        self._i2t_pads = dict(zip(self._idx_pads, self._tag_pads))
        self._t2i_pads = dict(zip(self._tag_pads, self._idx_pads))


def parseargs():

	"""
	This function handles getting arguments when this function is called

	input 1: path to the CZT sim file
	input 2: path to the CZT calibration file

	"""

	parser = argparse.ArgumentParser(description = "Parse the CZT.sim.h5 file")
	parser.add_argument("infile", help = "Path to input CZT.sim.h5 file")
	parser.add_argument("cal_file", help = "Path to calibration file")
	args = parser.parse_args()
	return args

def cli():

	"""
	This function writes everything to the L1 format

	Steps
		1: read the input file and create the output file
		2: copy event energies, positions, times, and ids to the output file
		3: Write all of the ASIC temps to 32C - matches the temp used during calibration
		4: copy the event times to the cathode and anode times 
			This will likely be updated later for travel time differences as a fn of position
		5: write the UTC time as eventtime * 1e9
		6: open the calibration file
		7: Calculate the anode and cathode pulseheights from the energy and positions

	"""

	args = parseargs()
	filename = args.infile
	cal_filename = args.cal_file

	path = '/'.join(filename.split('/')[:-1]) + '/'
	name = filename.split('/')[-1]

	if path == '/':
		outpath = name[:-3] + 'L0.h5'
	else:
		outpath = path + name[:-3] + 'L0.h5'

	with h5py.File(filename, 'r') as fin:
		with h5py.File(outpath, 'w') as fout:
			
			header = fin["Header"][...]
			fout.create_dataset("Header", data = header)
			
			version = fin["DEE Version"][...]
			fout.create_dataset("DEE Version", data = version)
			
			fin.copy(fin["/CZT"],fout["/"])
			czt_grp = fout["/CZT"]

			eventid = czt_grp["event_id"][...]

			asictemp = np.full((len(eventid),16),32) # hardcode 32C for all of the asic temp
			czt_grp.create_dataset("asic_temp",data=asictemp)
			del asictemp
			
			eventtime = czt_grp["event_time"][...]
			
			gps_pps_time = np.floor(eventtime) # event time rounded down to nearest second
			
			days = gps_pps_time // 86400
			hours = (gps_pps_time - days * 86400) // 3600 
			mins = (gps_pps_time - days * 86400 - hours * 3600) // 60 
			secs = (gps_pps_time - days * 86400 - hours * 3600 - mins * 60)
			epoch_time = np.zeros(len(hours))
			for i in range(len(hours)):
				date_time = datetime.datetime(2022,1,1 + int(days[i]),int(hours[i]),int(mins[i]),int(secs[i]))
				epoch_time[i] = time.mktime(date_time.timetuple()) # UNIX time stamp for 1/1/2022 + event time rounded down to 1s
			
			epoch_frac = (eventtime - gps_pps_time) // (10**-7) # remainder from epoch_time in 100 ns
			
			eventtime2 = np.floor(eventtime * (10**8)) # event time in 10 ns

			del eventtime

			czt_grp.create_dataset("gps_pps_time",data=gps_pps_time)
			del gps_pps_time

			czt_grp.create_dataset("epoch_time",data=epoch_time)
			czt_grp.create_dataset("epoch_frac",data=epoch_frac)
			del epoch_time
			del epoch_frac

			czt_grp['event_time'][...] = eventtime2
			del eventtime2
			
			asic_num = np.zeros((len(eventid),16))
			asic_num[:] = np.array([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]) + 2**14
			asic_num = asic_num.astype(int)
			czt_grp.create_dataset("asic_num",data = asic_num)
			del asic_num

			# Now to load positions and energies => calculate pulseheights

			energy = czt_grp["Energy"][...]
			energy_sigma = np.multiply(energy,0.022)
			energy = np.random.normal(energy,energy_sigma)
			pos = czt_grp["Position"][...]

			anode_pulseheight = np.zeros(np.shape(energy))
			anode_ph = np.zeros(np.shape(energy))
			cathode_pulseheight = np.zeros(np.shape(energy))
			cathode_ph = np.zeros(np.shape(energy))
			
			pad0_ph = np.zeros(np.shape(energy))
			pad0 = np.zeros(np.shape(energy))
			pad1_ph = np.zeros(np.shape(energy))
			pad1 = np.zeros(np.shape(energy))
			pad2_ph = np.zeros(np.shape(energy))
			pad2 = np.zeros(np.shape(energy))
			pad3_ph = np.zeros(np.shape(energy))
			pad3 = np.zeros(np.shape(energy))

			with h5py.File(cal_filename,'r') as cal_file:
				anode_ped = cal_file['anode_pedestal']
				anode_scalar = cal_file['anode_scalar']
				cathode_ped = cal_file['cathode_pedestal'][()]
				pad_ped = cal_file['pad_pedestal'][()]
				c2a_anode_means = cal_file['c2a_anode_means'][()]
				c2a_anode_scalar = cal_file['c2a_anode_scalar'][()]
				ch_map = CZTChannelMapping_v0()
				
				for i_asic in range(16):
					for i_bar in range(16):
						f = interp1d(c2a_anode_means,c2a_anode_scalar[i_asic,i_bar,:], kind = 'linear', fill_value = 'extrapolate')
						x = pos[:,i_asic,i_bar,0]
						y = pos[:,i_asic,i_bar,1]
						z = pos[:,i_asic,i_bar,2]
						c2a_max = max(c2a_anode_means)
						c2a_min = min(c2a_anode_means)
						m = (c2a_max-c2a_min)/CZT_BAR_HEIGHT
						c2a = np.add(np.multiply(m,np.add(z,CZT_BAR_HEIGHT)),c2a_min)
						anode_ph[:,i_asic,i_bar] = np.divide(energy[:,i_asic,i_bar],f(c2a))
						cathode_ph[:,i_asic,i_bar] = np.divide(np.multiply(anode_ph[:,i_asic,i_bar],c2a),1e3)
						
						cathode_pulseheight[:,i_asic,i_bar] = np.add(cathode_ph[:,i_asic,i_bar],np.divide(cathode_ped[i_asic,i_bar],4))
						anode_pulseheight[:,i_asic,i_bar] = np.add(np.divide(anode_ph[:,i_asic,i_bar],anode_scalar[i_asic,i_bar]),anode_ped[i_asic,i_bar])
						
						if (ch_map.asic_orientation(i_asic) > 0):
							
							pad0_ph[:,i_asic,i_bar] = 500
							pad2_ph[:,i_asic,i_bar] = pad0_ph[:,i_asic,i_bar] * x / (1 - x)
							
							pad3_ph[:,i_asic,i_bar] = 500
							pad1_ph[:,i_asic,i_bar] = pad3_ph[:,i_asic,i_bar] * y / (1 - y)
							
						else:
						
							pad2_ph[:,i_asic,i_bar] = 500
							pad0_ph[:,i_asic,i_bar] = pad2_ph[:,i_asic,i_bar] * x / (1 - x)
							
							pad1_ph[:,i_asic,i_bar] = 500
							pad3_ph[:,i_asic,i_bar] = pad1_ph[:,i_asic,i_bar] * y / (1 - y)
							
						pad0[:,i_asic,i_bar] += pad_ped[i_asic,i_bar,0]
						pad1[:,i_asic,i_bar] += pad_ped[i_asic,i_bar,1]
						pad2[:,i_asic,i_bar] += pad_ped[i_asic,i_bar,2]
						pad3[:,i_asic,i_bar] += pad_ped[i_asic,i_bar,3]
						

				# Transform to pos & neg pdos and pos tdos
				# neg pdos is just for pads -> zeros(n_events x 52)
				# pos_tods is just for timing differences, but we use pulseheight ratios -> zeros(n_events x 52)
				pos_tdo = np.zeros((len(eventid),16,52))
				neg_pdo = np.zeros((len(eventid),16,52))
				pos_pdo = np.zeros((len(eventid),16,52))
				
				czt_grp.create_dataset("pos_tdo", data = pos_tdo)
				
				del pos_tdo
				
				
				for n_bar in range(16):
					ind1 = n_bar // 4 + 1
					ind2 = n_bar % 4 + 1
					bar_id = str(ind1) + '-' + str(ind2)
					tags = ch_map.bar_to_tags(bar_id)
					[anode_id,cathode_id,pad0_id,pad1_id,pad2_id,pad3_id] = ch_map.tags_to_indices(tags[0:6])
					pos_pdo[:,:,anode_id] += anode_pulseheight[:,:,n_bar]
					pos_pdo[:,:,cathode_id] += cathode_pulseheight[:,:,n_bar]
					
					neg_pdo[:,:,pad0_id] += pad0[:,:,n_bar]
					neg_pdo[:,:,pad1_id] += pad1[:,:,n_bar]
					neg_pdo[:,:,pad2_id] += pad2[:,:,n_bar]
					neg_pdo[:,:,pad3_id] += pad3[:,:,n_bar]
					


				czt_grp.create_dataset("pos_pdo", data = pos_pdo)
				czt_grp.create_dataset("neg_pdo", data = neg_pdo)
				

if __name__ == '__main__': cli()












