# This file will convert the ACD output of desh5.py from energy and relative position to anode, cathode, and pad pulseheights in each asic/block

# Steps:
# 1. read in CsI.sim.h5
# 2. for each event, get the energy and position from input file
# 3. set use_pos & use_erg to 1's
# 7. relative z position ~ c2a ratio
#	c2a means = (c2a max - c2a min)/(bar height) * (z_rel - barheight/2) + c2a min
# 8. energy = anode_ph*interpolated function
#	interpolated function accounts for depth effects:
#	c2a means are the ratio of cathode pulseheight to anode pulseheight (x axis)
#	c2a scalars are 661.7/anode_value in ADC (y axis)



import h5py as h5
import pandas as pd
import numpy as np
import argparse
from scipy.interpolate import interp1d
import pickle
import math

import create_calibration_mask as calibMasks

import datetime
import time

def parseargs():

	"""
	This function handles getting arguments when this function is called

	input 1: path to the ACD sim file
	input 2: path to the ACD calibration file

	"""

	parser = argparse.ArgumentParser(description = "Parse the CsI.sim.h5 file")
	parser.add_argument("infile", help = "Path to input CsI.sim.h5 file")
	parser.add_argument("cal_file", help = "Path to calibration file")
	args = parser.parse_args()
	return args
	
def loadCalibrationData(filename):
        # print('Loading Calibration Data: ' + filename)
        
        #filename = 'rosspadConfigData/calibration/ACD_Calib_05122023.h5'    
    
        with h5.File(filename, "r") as f:
            panel_numbers = f['Panel Numbers'][...]
            pedestals = f['Pedestal'][...]
            threshold = f['Threshold'][...]
            overbin = f['Overbin'][...]
            calib_params = f['Calibration Parameters'][...]
            
        return panel_numbers, calib_params
			

def cli():

	"""
	This function writes everything to the L1 format

	Steps
		1: read the input file and create the output file
		2: copy event times and ids to the output file
			write the UTC time as eventtime * 1e9
		3: open the calibration file
		4: Calculate the pulse heights from the energy and positions

	"""

	args = parseargs()
	filename = args.infile
	cal_filename = args.cal_file

	path = '/'.join(filename.split('/')[:-1]) + '/'
	name = filename.split('/')[-1]

	if path == '/':
		outpath = name[:-3] + '.L1.1.h5'
	else:
		outpath = path + name[:-3] + '.L1.1.h5'

	with h5.File(filename, 'r') as fin:
		
		header = fin['Header'][...]
		version = fin['DEE Version'][...]
		ids = fin["ACD/EventID"][...]
		UTC = fin["ACD/UTC"][...]
			

		# Now to load positions and energies => calculate pulseheights
			
		energies = np.array(fin["/ACD/Energy"][...]) # shape [nevents, 5]
		[nevents,total_panels] = np.shape(energies)
		
		[panel_numbers, calib_params] = loadCalibrationData(cal_filename)
		adcs = np.zeros([nevents,2*total_panels])
		
		for i_panel in range(total_panels):
			
			if i_panel in panel_numbers:
			
				id1 = i_panel * 2                 #id for energies/adcs arrays
				id2 = i_panel * 2 + 1             #id for energies/adcs arrays
				
				cal_panel_idx = np.where(panel_numbers == i_panel)[0][0]
				cal_id1 = cal_panel_idx * 2       #id for calibration parameters
				cal_id2 = cal_panel_idx * 2 + 1   #id for calibration parameters
				
				adcs[:,id1] = (energies[:,i_panel] - calib_params[cal_id1,1])/calib_params[cal_id1,0]
				adcs[:,id2] = (energies[:,i_panel] - calib_params[cal_id2,1])/calib_params[cal_id2,0]
		
		
		# Need to fool get_time_align in rosspadL2ToL3.py
		
		new_ids = np.zeros(2*nevents)
		new_UTC = np.zeros(2*nevents)
		new_adcs = np.zeros([2*nevents,2*total_panels])
		pps = np.zeros(2*nevents)
		frame = np.ones(2*nevents)
		trigack = np.zeros(2*nevents)
		
		
		for i in range(nevents):
			
			new_ids[2*i] = ids[i]
			new_ids[2*i+1] = ids[i]
			
			new_UTC[2*i] = UTC[i]
			new_UTC[2*i+1] = UTC[i]
			
			new_adcs[2*i,:] = adcs[i,:]
			new_adcs[2*i+1,:] = adcs[i,:]
			
			trigack[2*i+1] = 1
			
			
	with h5.File(outpath,'w') as fout:
		fout.create_dataset('frame',data = frame)
		fout.create_dataset('pps',data = pps)
		fout.create_dataset('trigack',data = trigack)
		fout.create_dataset('EventID',data = new_ids)
		fout.create_dataset('UTC',data = new_UTC)
		fout.create_dataset('ADC',data = new_adcs)
		fout.create_dataset('Header', data = header)
		fout.create_dataset('DEE Version', data = version)
			



if __name__ == '__main__': cli()












