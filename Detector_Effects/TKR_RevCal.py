# This file will convert the TKR output of desh5.py from energy to pulseheights in each strip/side/layer

# TKR Calibration temp has been 32C

# Steps:
# 1. read in TKR.sim.h5
# 2. for each event, get the energy from the input file
# 3. convert energy to pulseheight according to ___


import h5py
import numpy as np
import argparse
from scipy.interpolate import interp1d

def read_resolution(filename):
	"""
	Reads data from a binary resolution file.
	
	We are interested in the values_fwhm_coef dataset.
	
	The columns are: detector #, side, strip, flag, noise2, err_noise2, number of peaks used in fit
	
	FWHM(E) = sqrt(noise2 + 8.0 * log(2) * 3.66 * 0.114 * E)
	
	This function will return an array of noise2's if there is a valid flag.
	If there is not a valid flag, I will return -1, which will tell me later to zero out that energy
	
	"""
	
	res_f = h5py.File(filename, 'r')
	
	res_arr = np.zeros([10,2,192])
	
	for i_layer in range(len(res_f.keys())):
		layer_name = list(res_f.keys())[i_layer]
		layer_arr = res_f[str(layer_name) + '/values_fwhm_coef'][...]
		for line in layer_arr:
			[i_side, i_strip, res] = line[[1,2,4]]
			if res > 0:
				res_arr[i_layer,int(i_side),int(i_strip)] = res
			else:
				res_arr[i_layer,int(i_side),int(i_strip)] = -1
	
	return res_arr
		

def read_data(file_name, data_type=None):
  """Reads data from a text file.

  Reads data from a text file. All rows starting with # are treated
  as comment lines. Assumes that there is no footer.

  Parameters
  ----------
  file_name: str
    File name
  data_type: str
    Data type for the input file. Choice is: 'float' for an input file
    with numerical data, 'list' for an input file that contains a list
    (e.g., a list of file names), and 'str' for an input file that is
    to be read as containing strings.

  Returns
  -------
  data_table: array_like
  nrow: int
    Number of rows in 'data_table'
  ncol: int
    Number of columns in 'data_table'
  """
  if (data_type):
    if (data_type == 'float'):
      data_table = np.genfromtxt(file_name, dtype='float', comments='#')
      tmp = data_table.shape
      if (len(tmp) == 1):
        ncol = tmp[0]
        nrow = 1
        data_table = np.array([data_table])
      else:
        ncol = tmp[1]
        nrow = tmp[0]
    elif (data_type == 'list'):
      data_table = np.genfromtxt(file_name, dtype='str', comments='#')
      tmp = data_table.size
      nrow = tmp
      ncol = 0
      if (nrow == 1):
        data_table = np.array([data_table])
    elif (data_type == 'str'):
      data_table = np.genfromtxt(file_name, dtype='str', comments='#')
      tmp = data_table.shape
      if (len(tmp) == 1):
        ncol = tmp[0]
        nrow = 1
        data_table = np.array([data_table])
      else:
        ncol = tmp[1]
        nrow = tmp[0]
  else:
    data_table = np.genfromtxt(file_name, comments='#')
    nrow=-1
    ncol=-1
  return data_table, nrow, ncol


def parseargs():

	"""
	This function handles getting arguments when this function is called

	input 1: path to the TKR sim file
	input 2: path to the TKR calibration file

	"""

	parser = argparse.ArgumentParser(description = "Parse the TKR.sim.h5 file")
	parser.add_argument("infile", help = "Path to input TKR.sim.h5 file")
	parser.add_argument("cal_file", help = "Path to calibration file")
	parser.add_argument("-r", default = "", help = "Path to resolution file")
	parser.add_argument("-v", action='store_true', help=" Verbose flag to monitor progress or to debug") # SW Added
	parser.add_argument("-s", type=int, default=5, help="Smear Percent (Int). Default=5 ")
	args = parser.parse_args()
	return args

def cli():

	"""
	This function writes everything to the L1 format

	Steps
		1: read the input file and create the output file
		2: copy event energies, times, and ids to to the output file
		6: open the calibration file
		7: Calculate the pulseheights from the energy

	"""

	args = parseargs()
	filename = args.infile
	name = filename.split('/')[-1]
	cal_filename = args.cal_file
	res_filename = args.r

	# -- SW Added --
	if args.v:
		verbose_flag = True
	else:
		verbose_flag = False

	smear = float(args.s)/100.0	# converting to percentage
	print(f'Smear = {args.s}% | ({smear})')
	# -- --

	path = '/'.join(filename.split('/')[:-1]) + '/'

	if path == '/':
		outpath = name[:-3] + '.L1.h5'
	else:
		outpath = path + name[:-3] + '.L1.h5'

	with h5py.File(filename, 'r') as fin:
		with h5py.File(outpath, 'w') as fout:
			header = fin['Header'][...]
			fout.create_dataset("Header",data = header)
			
			version = fin['DEE Version'][...]
			fout.create_dataset("DEE Version",data = version)
			
			energy = fin["/TKR/Energy"][...]
			
			if res_filename != "":
				res_arr = read_resolution(res_filename)
			
			n_events = len(energy)
			
			ecalib_table, enrow, encol = read_data(cal_filename,data_type='float')
			
			eid = fin["/TKR/event_id"][...]
			eid = np.concatenate((np.array([0]),eid))
			
			run_time = fin['/TKR/running_time'][...]
			run_time = np.concatenate((np.array([0]),run_time))
			
			sync_index = fin['/TKR/event_id'][...]
			sync_index = np.concatenate((np.array([0]),sync_index))
			
			system_time = fin['/TKR/running_time'][...]
			system_time = system_time * 10**9
			system_time = np.concatenate((np.array([0]),system_time))
				
			for i_layer in range(10):
			
				if not (energy[:,i_layer,:,:] == 0).all():
					print("Writing Layer " + str(i_layer))
				
					layer_grp_name = "layer0" + str(i_layer)
					layer_grp = fout.create_group(layer_grp_name)
				
					config_grp = layer_grp.create_group("config")
					data_grp = layer_grp.create_group("data")
					vdata_grp = layer_grp.create_group("vdata")
				
				
					data_grp.create_dataset('event_id', data = eid)
					data_grp.create_dataset('running_time', data = run_time)
					data_grp.create_dataset('sync_index', data = sync_index)
					data_grp.create_dataset('system_time', data = system_time)
				
					for a in range(12):
						config_grp.create_dataset("asic{:02d}/chan_disable".format(a), data = np.zeros(32))
					
					
				
					# Now to calculate pulseheights
				
					pulseheight = np.zeros((n_events+1,2,192))

					t_counter = 0 # SW Added
					t_counter1= 0 # SW Added
					for i_side in range(2):
						for i_strip in range(192):
						
							strip_energy = energy[:,i_layer,i_side,i_strip]
						
							# gaussian energy smearing
							if res_filename == "":
								energy_sigma = np.multiply(strip_energy,smear) # SW Updated to variable
								strip_energy[strip_energy<0] = 0
								t_counter1 += np.count_nonzero(strip_energy>0) #SW Added
								t_counter+=1 # SW Added
							elif res_arr[i_layer,i_side,i_strip] > 0:
								energy_fwhm = np.sqrt(res_arr[i_layer,i_side,i_strip] + 8*np.log(2)*3.66*0.114*energy[:,i_layer,i_side,i_strip]*1000)/1000
								energy_sigma = np.divide(energy_fwhm,np.sqrt(8*np.log(2)))
								strip_energy = np.random.normal(strip_energy,energy_sigma)
								strip_energy[strip_energy<0] = 0

								t_counter1 += np.count_nonzero(strip_energy>0) #SW Added
								t_counter+=1 # SW Added
							else:
								#strip_energy[:] = 0
								pass
						
							strip_energy = np.concatenate((np.array([0]),strip_energy))
						
							qf = np.where((ecalib_table[:,0] == i_layer) & (ecalib_table[:,1] == i_side) & (ecalib_table[:,2] == i_strip))
							indx = qf[0][0]

							slope = ecalib_table[indx,4]
							intercept = ecalib_table[indx,6]
							valid_calib = ecalib_table[indx,3]

							if ((valid_calib == -2) | (valid_calib == -1)):
								pulseheight[:,i_side, i_strip] = 0
							else:
								pulseheight[:,i_side, i_strip] = np.divide(np.subtract(strip_energy,intercept),slope)

							
					print(f'Layer: {i_layer}  | #ADC utilized: {t_counter} | # smearing (Hits) {t_counter1}') if verbose_flag else None		# SW Added	

					vdata_grp.create_dataset("channel_cm_sub",data = pulseheight)
					
			else:
				print("Layer " + str(i_layer) + " is all zero")

if  __name__ == '__main__': cli()












