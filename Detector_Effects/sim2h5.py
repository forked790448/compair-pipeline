########################################################################################################
# |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# |||                                                                                                  #
# ||| |||||||##############|||||||                                                                     #
# ||| |||                      |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| ||| I.) BACKGROUND & USE |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| |||                      |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| |||||||##############|||||||                                                                     #
# |||                                                                                                  #
# |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
########################################################################################################
#
########################################################################################################
#  |||*| |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| |*|||  #
#  |||*| |||********************************************************************************||| |*|||  #
#  |||*| |||*||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||*||| |*|||  #
#  |||*| |||*|||                                                                        |||*||| |*|||  #
#  |||*| |||*||| To run this file, please use the following format in the command line: |||*||| |*|||  #
#  |||*| |||*|||                                                                        |||*||| |*|||  #
#  |||*| |||*|||                                                                        |||*||| |*|||  #
#                                                                                                      #
#                              python3 sim2cud.py (Path to .sim file)                                  #<-------------------------------------------------------
#                                                                                                      #
#  |||*| |||*|||                                                                        |||*||| |*|||  #
#  |||*| |||*|||                                                                        |||*||| |*|||  #
#  |||*| |||*||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||*||| |*|||  #
#  |||*| |||********************************************************************************||| |*|||  #
#  |||*| |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| |*|||  #
########################################################################################################

########################################################################################################
#                                             ##
# I.1) REVERSE PIPELINE PART 1 (Cosima -> .sim.h5)  ##
#                                             ##
################################################

# This parser disects a Cosima .sim file to pull the following event properties:
#    Event ID            | Total simulated event number
#    Hit ID                | Relative hit number per event
#    Photon ID            | Total number of photons emitted before the hit detection
#    Time (s) of Hit            | Time since the start of the simulation
#    Energy (keV) of Hit        | Energy deposited by the hit in keV
#    Position (X, Y, Z) of Hit    | Detected position of hit (units relative to geomega model)

# The data is then written to a text file and an HDF5 file (default = Cosima.txt/h5)
# You may also include "-t (Name of text file)" to specify the text file output
# The following properties are saved as datasets to the HDF5 file under the group (Hit Data):
#    Event ID    Time (s)    Energy (keV)    Position (X, Y, Z)

# A histogram of the hit energies is generated in a new window through running this script.

# The established HDF5 output is categorized into groups via subsystem (Event Data):
#       TKR       CsI       CZT       ACD (Not Yet Implemented) <-------------------------------------------------------------------------

# Each subsytem group will have corresponding subgroups corresponding to energy, position, etc. of hits
# in the respective subsytem in the CUD format.

########################################################################################################



#------------------------------------------------------------------------------------------------------#
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
#------------------------------------------------------------------------------------------------------#

# Version Definitions
# 1.0  = gitlab version as of 7/10/2022
# 1.0a = write version to output files
# 1.0b = change the output to .sim.h5 - to not confuse it with .cud.h5
# 1.0c = renamed to sim2h5
# 1.0d = changed CZT position stored from global z to z relative to center of the bar
# 2.0  = Added CZT x/y positions sub-bar level; TKR layer 0 = top layer no matter how many layers are used in cosima
# 3.0  = Added ACD
# 3.1  = Fixed ACD Positions
VERSION = '3.1'


########################################################################################################
# |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# |||                                                                                                  #
# ||| |||||||##############|||||||                                                                     #
# ||| |||                      |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| ||| II.) IMPORT PACKAGES |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| |||                      |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| |||||||##############|||||||                                                                     #
# |||                                                                                                  #
# |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
########################################################################################################

import argparse
import numpy as np
import matplotlib.pyplot as plt
import h5py
import time
#import datetime
#import tzinfo

# Other Possibly Useful Packages

#import pickle
#from array import array
#import seaborn as sns
#import sys # system-specific parameters/functions
#import csv # comma-separated values
#import os # operating-system-dependant utilities




#------------------------------------------------------------------------------------------------------#
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
#------------------------------------------------------------------------------------------------------#




########################################################################################################
# |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# |||                                                                                                  #
# ||| |||||||###############|||||||                                                                    #
# ||| |||                       |||#####|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| ||| III.) FILES & OPTIONS |||#####|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| |||                       |||#####|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| |||||||###############|||||||                                                                    #
# |||                                                                                                  #
# |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
########################################################################################################

# Use argparse to call a .sim file as an input (see "||| I.) BACKGROUND & USE |||")
# Define optional command line inputs for specification of text file
# Assign default text output to Cosima.txt <-------------------------------------------------------------------------------------------------

def parseargs():

    parser = argparse.ArgumentParser(description = "Parse the .sim file")
    parser.add_argument("infile", help="Path to input .sim file", type=argparse.FileType('r'))
    parser.add_argument("-t", "--text", help="Path to text file output", default = 'Cosima.txt')
    args = parser.parse_args()
    return args




#------------------------------------------------------------------------------------------------------#
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
#------------------------------------------------------------------------------------------------------#




########################################################################################################
# |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# |||                                                                                                  #
# ||| |||||||##################|||||||                                                                 #
# ||| |||                          |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| ||| IV.) GEOMETRIC VARIABLES |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| |||                          |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| |||||||##################|||||||                                                                 #
# |||                                                                                                  #
# |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
########################################################################################################


# IV.1) TKR - most confident in these numbers

TKR_strip_pitch = 0.051 # 510 microns
TKR_width = 192 * TKR_strip_pitch # 192 strips of constant pitch
TKR_edge = TKR_width/2 # DSSD is centered at 0 in the xy plane, thus the edges are equivalent to half of the width
TKR_strip_centroid = TKR_edge - (TKR_strip_pitch)/2 # Gives the center of strip 191
TKR_layer_spacing = 1.9 # z spacing between layers
TKR_num_layers = 10 # There are 10 layers in the tracker
TKR_layer_thickness = 0.05 # The layers are 0.05 thick
TKR_top_layer = TKR_layer_thickness/2 + (TKR_num_layers - 1) * TKR_layer_spacing # Bottom of lowest layer is at z=0

# IV.2) CsI - medium confidence here

CsI_top_centroid = -19.60775 # Global z position of the top layer of the CsI (centroid)
CsI_layer_spacing = 1.9 # z spacing between layers
CsI_x_centroid = 4.75 # Was 4.25 for x and y. I think it should be 4.75, but I need to confirm what this means ******************************************************************
CsI_y_centroid = 4.75

# IV.3 CZT - confident about the bar numbers, less confident about the block numbers

CZT_edge = 6 # was 5.91
CZT_block_spacing = 3 # was 2.95
CZT_edge_midpoint = CZT_edge - CZT_block_spacing/2 # was 3.75, but not sure what this means; was 4.435
CZT_bar_edge = 1.44
CZT_bar_spacing = 0.72
CZT_z_midpoint = -4.7218

# IV.4 Coincidence Window
coinc_window = 100 * 10 ** -6 # 3.5 us



#------------------------------------------------------------------------------------------------------#
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
#------------------------------------------------------------------------------------------------------#




########################################################################################################
# |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# |||                                                                                                  #
# ||| |||||||#######|||||||                                                                            #
# ||| |||               |||#####|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| ||| V.) FUNCTIONS |||#####|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| |||               |||#####|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
# ||| |||||||#######|||||||                                                                            #
# |||                                                                                                  #
# |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
########################################################################################################

########################################################################################################
#                                             ##
# V.1) PINPOINT FUNCTIONS                     ##
#                                             ##
################################################

def TKR_pinpoint(xpos, ypos, zpos):

    '''Converts the (x,y,z) position of hits in silicon to corresponding
    strip numbers and layer number.

    Keyword Arguments:
    xpos = X position of hit in DSSD tracker
    ypos = Y position of hit in DSSD tracker
    zpos = Z layer of hit in DSSD tracker

    Returns:
    xstrip = x-oriented DSSD strip ranging from 0:191
    ystrip = y-oriented DSSD strip ranging from 0:191 
    zstrip = DSSD layer ranging from 0:9

    Converts the x and y positions of the hits in the TKR (HTsim 1) to a
    corresponding x-strip # and y-strip #, both ranging from 0:191. The
    z-position is then converted to a layer ranging from 0:9, with the top
    layer being layer 0. The conversion equations are obtained via the
    TKR geometry as follows:

    The TKR consists of 10 layers of 192 total strips with pitch 
    p = 510 microns, thus it has an effective width of W = 192*p. This 
    geometry is centered at zero, and thus ranges in x and y from 
    -W/2:+W/2, with strip "edges" defined in steps of p microns. The strip 
    centers, which correspond to the Cosima centroid output, are thus 
    defined by (-W+p)/2:(W-p)/2 in steps of p microns. Every position is 
    thus by nature divisible by p, and thus adding an initial offset of the 
    furthest-from-origin centeryields a 0-count of corresponding strip number 
    ranging from 0:191. Please note that the ystrip equation is modified 
    slightly to account for the orientation of the y-strips (strip 0 
    corresponds to 4.8705 as opposed to strip 191).

    The corresponding layers range from -4.5:4.5 in steps of 1, and thus
    the ten total layers are defined with the top layer corresponding to
    the zeroeth layer by subtracting the 4.5 offset.

    '''

    xstrip = round((float(xpos)+TKR_strip_centroid)/TKR_strip_pitch)
    ystrip = round(-(float(ypos)-TKR_strip_centroid)/TKR_strip_pitch)
    zlayer = round(abs((float(zpos)-TKR_top_layer)/TKR_layer_spacing))
    # print('X-strip # ' + str(xstrip) + '; Y-strip # ' + str(ystrip) + '; Z-layer # ' + str(zlayer))
    return xstrip, ystrip, zlayer
###############################################################################

def CsI_pinpoint(xpos, ypos, zpos):

    '''Converts the (x,y,z) position of hits in CsI to corresponding
    log number and layer number.

    Keyword Arguments:
    xpos = X position of hit in CsI
    ypos = Y position of hit in CsI
    zpos = Z layer of hit in CsI

    Returns:
    CsIlog = log number ranging from 0:5
    CsIlayer = layer number ranging from 0:4

    Converts the x and y positions of the hits in the CsI (HTsim 2) to a
    corresponding x or y log #, both ranging from 0:5. The z-position is
    then converted to a layer ranging from 0:5, with the top layer being 
    layer 0. The conversion equations are obtained via the CsI geometry 
    as follows:

    The corresponding layers exist in hodoscopic fasion, with the top layer
    defined as layer 0, and oriented in x. The five layers (centroid) range 
    from -20.641:-27.441 in steps of 1.7 with layer 1 oriented in y, layer 2
    oriented in x, and so on.

    Each layer contains 6 logs, which store pos_downlog info based on the
    orientation of the log (logs oriented in x store x info). The negative-
    most log in each orientation corresponds to log 0, and the position of
    each subsequent log is given by the addition of the width of a CsI log
    to the known location of the centroid of log 0.

    '''

    CsIlayer = 4-int(abs(round(float(zpos)-CsI_top_centroid,3))/CsI_layer_spacing)
    if CsIlayer % 2 ==0:
        CsIlog = int((float(ypos)+CsI_y_centroid)/CsI_layer_spacing)
    else:
        CsIlog = int((float(xpos)+CsI_x_centroid)/CsI_layer_spacing)
    return CsIlayer, CsIlog
###############################################################################

def CZT_pinpoint(xpos, ypos, zpos):

    '''Converts the (x,y,z) position of hits in CZT to corresponding
    block number and bar number.

    Keyword Arguments:
    xpos = X position of hit in CZT
    ypos = Y position of hit in CZT
    zpos = Z layer of hit in CZT

    Returns:
    CZT_Block = Corresponding block (0:15) of detected hit
    CZT_Bar = Returns bar (0:15) of detected hit inside of CZT_Block
    CZT_Xrel = The x-position of the hit relative to the center of CZT_Bar
    CZT_Yrel = The y-position of the hit relative to the center of CZT_Bar

    Converts the x and y positions of the hits in the CZT (HTsim 7) to a
    corresponding x or y block #, both ranging from 0:3 (there are 16 total
    blocks ranging from 0:15). Block 0 is defined as the negative-most block in
    x, and the most positive in y. Block 1 is defined by increasing x for a set y,
    likewise up to block 3. Block 4 is thus defined as the negative-most in
    x and the second-most positive in y. This continues up to block 16, which
    is the most positive in x and most negative in y.

    The x and y position relative to the center of the correpsonding block is
    then converted to a bar ranging from 0:15, defined similarly (256 total bars).
    The (x,y) position of the center of this bar is then calculated, and the position
    relative to this center is stored as CZT_(X,Y)rel.

    '''

    xpos = float(xpos)
    ypos = float(ypos)
    zpos = float(zpos)

    CZT_XBlock=int((CZT_edge-xpos)/CZT_block_spacing) # Defines a count for X-blocks (left=3, right=0)
    CZT_YBlock=int((CZT_edge-ypos)/CZT_block_spacing) # Defines a count for Y-blocks (top=0, bottom=3)
    CZT_Block=int(float(CZT_XBlock)+(4*float(CZT_YBlock))) # Defines block number in 4x4 grid (0:15, row 1 is block 0:3, row 2 is 4:7 etc)

    xpos_interim = xpos - 3/2 * CZT_block_spacing + CZT_XBlock * CZT_block_spacing # Defines the x position relative to the center of the block
    ypos_interim = ypos - 3/2 * CZT_block_spacing + CZT_YBlock * CZT_block_spacing # Defines the y position relative to the center of the block

    if CZT_YBlock % 2 != 0:
        CZT_XBar = 3 + int((xpos_interim - CZT_bar_edge)/CZT_bar_spacing) # Defines a count for x bars within the block (left=3, right=0)
        CZT_YBar = int((CZT_bar_edge - ypos_interim)/CZT_bar_spacing) # Defines a count for y bars within the block (top=0, bottom=3)
        
        CZT_Xrel = xpos_interim + 3/2 * CZT_bar_spacing - CZT_XBar * CZT_bar_spacing
        CZT_Yrel = ypos_interim - 3/2 * CZT_bar_spacing + CZT_YBar * CZT_bar_spacing
    
    else:
        CZT_XBar = int((CZT_bar_edge - xpos_interim)/CZT_bar_spacing) # Defines a count for x bars within the block (left=3, right=0)
        CZT_YBar = 3 + int((ypos_interim - CZT_bar_edge)/CZT_bar_spacing) # Defines a count for y bars within the block (top=0, bottom=3)
        
        CZT_Xrel = xpos_interim - 3/2 * CZT_bar_spacing + CZT_XBar * CZT_bar_spacing
        CZT_Yrel = ypos_interim + 3/2 * CZT_bar_spacing - CZT_YBar * CZT_bar_spacing
        
    CZT_Bar = CZT_XBar + 4*CZT_YBar

    CZT_Zrel = zpos - CZT_z_midpoint # Returns z position relative to the z-center of the bar
    
    
    return CZT_Block, CZT_Bar, CZT_Xrel, CZT_Yrel, CZT_Zrel

###############################################################################

def ACD_pinpoint(xpos,ypos,zpos):
    
    '''Converts the x,y,z positions into panel numbers.
    Panel 1: -x
    Panel 2: +y
    Panel 3: +x
    Panel 4: -y
    Panel 5: top
    
    first check if z > 19.5cm => panel 5
    else: check panels 1-4.
    
    Return panel number as 0-4 b/c python is 0-4 not 1-5
    '''
    
    xpos = float(xpos)
    ypos = float(ypos)
    zpos = float(zpos)
    
    if zpos > 19.5:
        panel = 4
    else:
        if xpos > 22.25 and ypos > -11.75:
            panel = 0
        elif ypos < -11.75 and xpos > -41.75:
            panel = 1
        elif xpos < -41.75 and ypos < 36.25:
            panel = 2
        elif ypos > 36.25 and xpos < 22.25:
            panel = 3
    
    return panel
    
###############################################################################

###############################################################################
###############################################################################

def cli():




    #--------------------------------------------------------------------------------------------------#
    #||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
    #||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
    #||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
    #--------------------------------------------------------------------------------------------------#




    ####################################################################################################
    # |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
    # |||                                                                                              #
    # ||| |||||||##########|||||||                                                                     #
    # ||| |||                  |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
    # ||| ||| VI.) DEFINITIONS |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
    # ||| |||                  |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
    # ||| |||||||##########|||||||                                                                     #
    # |||                                                                                              #
    # |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
    ####################################################################################################

    # Define output files:

    args = parseargs()
    filename = args.infile

    outf1 = open(args.text, 'w') # default text file output is "Cosima.txt"
    outf2 = h5py.File(filename.name + '.h5', 'w') # h5 output is *.sim.h5

    # Initialize counts:

    count = 0        #The corresponding hit number for a specified event
    SEcount = 0      #The corresponding event number (used for by-line analysis)
    SEtotal = 0      #The total number of events in the sim file
    HTcount = 0      #The total number of hits for all events
    TKR_count = 0   #The total number of hits in Silicon DSSD
    CsI_count = 0
    CZT_count = 0
    ACD_count = 0
    coinc_count = 0

    # Define arrays:

    hitID = []          #Array of event IDs
    hittime = []        #Time of the event (roughly the time of the corresponding hits)
    hitenergy = []      #Energy of each hit
    hitpositionx = []   #Corresponding (x,y,z) position of a hit
    hitpositiony = []
    hitpositionz = []

    # Write version in out f1
    # Define version attribute in outf2

    outf1.write("sim2h5.py Version: " + VERSION + "\n")
    outf2.attrs['version'] = VERSION

    #Define h5 groups:

    Hits = outf2.create_group('Hits')
    Events = outf2.create_group('Events')




    #--------------------------------------------------------------------------------------------------#
    #||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
    #||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
    #||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
    #--------------------------------------------------------------------------------------------------#




    ####################################################################################################
    # |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
    # |||                                                                                              #
    # ||| |||||||##############|||||||                                                                 #
    # ||| |||                      |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
    # ||| ||| VII.) WRITE TO FILES |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
    # ||| |||                      |||#####||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
    # ||| |||||||##############|||||||                                                                 #
    # |||                                                                                              #
    # |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| #
    ####################################################################################################

########################################################################################################
#                                               ##
# VII.1) ESTABLISH HDF5 DATASET ARRAYS          ##
#                                               ##
##################################################


    # Open sim file and count the total number of events
    try:
        with open(filename.name) as f:
            lines=f.readlines() # Pull all lines from the simulation file
            for line in lines: # Parse through each line
                if 'SE' in line: # Search for the start of an event
                    SEtotal = SEtotal + 1
    except: print('Could not count events')

    # Define H5 Dataset Arrays:
    #TKR_EventID = np.zeros([SEtotal, 10, 2, 192],dtype=float)
    #TKR_EventTime = np.zeros([SEtotal, 10, 2, 192],dtype=float)
    TKR_Energy = np.zeros([SEtotal, 10, 2, 192],dtype=float) # 10 layers, 2 orientations, 192 strips
    TKR_layers = []

    CsI_Energy = np.zeros([SEtotal,30],dtype=float) # 5 layers, 6 logs
    CsI_pos_down_log = np.zeros([SEtotal,30],dtype=float) # stores x/y info dependent on orientation

    #CZT_EventID = np.zeros([SEtotal,16,16],dtype=float)
    #CZT_EventTime = np.zeros([SEtotal,16,16],dtype=float)
    CZT_Energy = np.zeros([SEtotal,16,16],dtype=float) # 16 bars, 16 blocks
    CZT_Position = np.zeros([SEtotal,16,16,3],dtype=float) # stores position relative to center of bar
    
    ACD_Energy = np.zeros([SEtotal,5], dtype = float)

    UTC=[] # One time per event <----------------------------------------------------------------
    eventID = []
    eventTime = []

########################################################################################################
#                                             ##
# VII.2) PARSING THROUGH SIM FILE             ##
#                                             ##
################################################



    try:
        print(filename.name)
        with open(filename.name) as f:
            lines = f.readlines()
            SE_found = False
            header = ''
            for line in lines:
                if 'SE' in line: # Search for the start of an event
                    if not SE_found:
                        SE_found = True
                    outf1.write('\n')
                    count = 0
                    SEcount = SEcount + 1
                    outf1.write('SE ' + str(SEcount) + '\n')

                if 'ID' in line:
                    idsplit = line.split()
                    eventid = idsplit[1]
                    photonid = idsplit[2]
                    #eventID.append(int(eventid))
                    # ID.append(float(eventid))
                    # outf1.write(str(idsplit[0])  + '=' + str(eventid) + '\n')
                    # outf1.write('PHOTON ID = ' + str(photonid) + '\n'

                if 'TI' in line: # Search for the time associated with the start of an event
                    ti = line.split()
                    
                    if int(eventid) == 1:
                        ref_time = float(ti[1])
                        eventTime.append(float(ti[1]))
                        eventID.append(int(eventid))
                        # time.append(float(ti[1]))
                        outf1.write('HT Time (s) = ' + str(ti[1]) + '\n')
                        UTC.append(int(float(ti[1])*1e9))
                        
                    elif abs(float(ti[1]) - ref_time) < coinc_window:
                        SEcount -= 1
                        coinc_count += 1
                        outf1.write('Coincidence at Time (s) = ' + str(ti[1]) + '\n')
                        #eventID = eventID[:-1]
                    else:
                        ref_time = float(ti[1])
                        eventTime.append(float(ti[1]))
                        eventID.append(int(eventid))
                        UTC.append(int(float(ti[1])*1e9))
                        # time.append(float(ti[1]))
                        outf1.write('HT Time (s) = ' + str(ti[1]) + '\n')
                        

                if 'HT' in line: # Search for hits inside of an event
                    data = line.split(';')
                    hitenergy.append(float(data[4]))
                    count = count + 1
                    HTcount = HTcount + 1 #Internal hit counter per event
                    # Write to text file
                    outf1.write('HT# = ' + str(count) + '\n')
                    outf1.write('HT DETECTOR ID = ' + str(data[0]) + '\n')
                    outf1.write('HT POSITION (X, Y, Z) = (' + str(data[1])
                                + ', ' + str(data[2]) + ', ' + str(data[3])
                                + ')' + '\n')
                    outf1.write('HT Energy (keV) = ' + str(data[4]) + '\n')
                    # Write to datasets
                    hitpositionx.append(float(data[1]))
                    hitpositiony.append(float(data[2]))
                    hitpositionz.append(float(data[3]))
                    hitID.append(int(eventid))
                    hittime.append(float(ti[1]))

                if 'HTsim 1' in line: #Search for hits in TKR
                    TKR_count = TKR_count + 1
                    (xstrip,ystrip,zlayer)= TKR_pinpoint(data[1], data[2], data[3])
                    TKR_Energy[(int(SEcount)-1),int(zlayer),0,int(xstrip)]+=float(data[4])
                    TKR_Energy[(int(SEcount)-1),int(zlayer),1,int(ystrip)]+=float(data[4])
                    outf1.write('TKR' + '\n')
                    outf1.write('(X Strip = ' + str(xstrip) + '; Y Strip = ' + str(ystrip) 
                    + '; Layer = ' + str(zlayer) + ')' + '\n')
                    if not int(zlayer) in TKR_layers:
                        TKR_layers.append(int(zlayer))

                if 'HTsim 2' in line: # Search for hits in CsI
                    CsI_count = CsI_count + 1
                    (CsIlayer,CsIrow)= CsI_pinpoint(data[1],data[2],data[3])
                    CsIlognum = CsIlayer * 6 + CsIrow
                    if CsIlayer % 2 !=0: #Store y data if layer is odd
                        if CsI_pos_down_log[(int(SEcount)-1),int(CsIlognum)] == 0:
                            CsI_pos_down_log[(int(SEcount)-1),int(CsIlognum)]+= -float(data[2])*10 #CsI works in mm
                        else:
                            pos_old = CsI_pos_down_log[(int(SEcount)-1),int(CsIlognum)]
                            pos_new = -float(data[2])*10 #CsI works in mm
                            E_old = CsI_Energy[(int(SEcount)-1),int(CsIlognum)]
                            E_new = float(data[4])
                            pos_calc = (E_old * pos_old + E_new * pos_new)/(E_old + E_new)
                            CsI_pos_down_log[(int(SEcount)-1),int(CsIlognum)]=pos_calc
                        CsI_Energy[(int(SEcount)-1),int(CsIlognum)]+=float(data[4])
                        outf1.write('CsI' + '\n')
                        outf1.write('Layer = ' + str(CsIlayer) + '; Log = ' + str(CsIrow) 
                        + '; Y Down Log = ' + str(data[2]) + '\n')
                    else: # Store x data for even layer
                        if CsI_pos_down_log[(int(SEcount)-1),int(CsIlognum)] == 0:
                            CsI_pos_down_log[(int(SEcount)-1),int(CsIlognum)]+= -float(data[1])*10 #CsI works in mm
                        else:
                            pos_old = CsI_pos_down_log[(int(SEcount)-1),int(CsIlognum)]
                            pos_new = -float(data[1])*10 #CsI works in mm
                            E_old = CsI_Energy[(int(SEcount)-1),int(CsIlognum)]
                            E_new = float(data[4])
                            pos_calc = (E_old * pos_old + E_new * pos_new)/(E_old + E_new)
                            CsI_pos_down_log[(int(SEcount)-1),int(CsIlognum)]= pos_calc
                        CsI_Energy[(int(SEcount)-1),int(CsIlognum)]+=float(data[4])
                        outf1.write('CsI' + '\n')
                        outf1.write('Layer = ' + str(CsIlayer) + '; Log = ' + str(CsIrow) 
                        + '; X Down Log' + str(data[1]) + '\n')


                if 'HTsim 7' in line: # Search for hits in CZT
                    CZT_count=CZT_count + 1
                    (CZT_Block,CZT_Bar,CZT_Xrel,CZT_Yrel,CZT_Zrel)=CZT_pinpoint(data[1],data[2],data[3])
                    
                    if (CZT_Position[(int(SEcount)-1),int(CZT_Block),int(CZT_Bar),:] == [0,0,0]).all():
                        CZT_Position[(int(SEcount)-1),int(CZT_Block),int(CZT_Bar),0]=float(CZT_Xrel)
                        CZT_Position[(int(SEcount)-1),int(CZT_Block),int(CZT_Bar),1]=float(CZT_Yrel)
                        CZT_Position[(int(SEcount)-1),int(CZT_Block),int(CZT_Bar),2]=float(CZT_Zrel)
                    else:
                        pos_old = np.array(CZT_Position[(int(SEcount)-1),int(CZT_Block),int(CZT_Bar),:])
                        pos_new = np.array([float(CZT_Xrel),float(CZT_Yrel),float(CZT_Zrel)])
                        E_old = CZT_Energy[(int(SEcount)-1),int(CZT_Block),int(CZT_Bar)]
                        E_new = float(data[4])
                        pos_calc = (E_old * pos_old + E_new * pos_new)/(E_old + E_new)
                        CZT_Position[(int(SEcount)-1),int(CZT_Block),int(CZT_Bar),:] = pos_calc
                    CZT_Energy[(int(SEcount)-1),int(CZT_Block),int(CZT_Bar)]+=float(data[4])
                    outf1.write('CZT' +'\n')
                    outf1.write('(Block = ' + str(CZT_Block) + '; Bar = ' + str(CZT_Bar) 
                    + '; Xrel = ' + str(CZT_Xrel) + '; Yrel = ' + str(CZT_Yrel) + ')' + '; Zrel = ' + str(CZT_Zrel) +   '\n')
                
                if 'HTsim 4' in line: # Search for hits in ACD
                    ACD_count += 1
                    panel = ACD_pinpoint(data[1],data[2],data[3])
                    ACD_Energy[(int(SEcount)-1),panel] += float(data[4])
                    outf1.write('ACD' + '\n')
                    outf1.write('(Panel = ' + str(panel))
                    
                if not SE_found:
                    header = header + line + '\n'
    except FileNotFoundError:      
        print('The file could not be opened.')

    outf1.write('Total HT count = ' + str(HTcount))
    hitposition = np.dstack((hitpositionx, hitpositiony, hitpositionz))
    
    TKR_min_layer = min(TKR_layers)
    TKR_Energy[:,:(10-TKR_min_layer),:,:] = TKR_Energy[:,TKR_min_layer:,:,:]
    TKR_Energy[:,(10-TKR_min_layer):,:,:] = 0
    
    # Remove 0's at end of ***_Energy arrays due to chance coinc
    
    TKR_Energy = TKR_Energy[:SEcount,:,:,:]
    CZT_Energy = CZT_Energy[:SEcount,:,:]
    CZT_Position = CZT_Position[:SEcount,:,:,:]
    CsI_Energy = CsI_Energy[:SEcount,:]
    CsI_pos_down_log = CsI_pos_down_log[:SEcount,:]
    ACD_Energy = ACD_Energy[:SEcount,:]
    
    
    
    #eventID = np.arange(len(eventID)) + 1
            

########################################################################################################
#                                             ##
# VII.3) ASSIGN DATASETS TO HDF5 GROUPS       ##
#                                             ##
################################################

    Hits.create_dataset('EventID', data=hitID)
    Hits.create_dataset('Time(s)', data=hittime)
    Hits.create_dataset('Energy(keV)', data=hitenergy)
    Hits.create_dataset('Position(X,Y,Z)', data=hitposition)

    TKR=Events.create_group('TKR')
    TKR.create_dataset('Energy', data=TKR_Energy)
    TKR.create_dataset('event_id', data = eventID)
    TKR.create_dataset('running_time', data = eventTime)

    CsI=Events.create_group('CsI')
    CsI.create_dataset('erg',data=CsI_Energy) # We can probably change the names of these to keep it consistent with the other subsystems **************
    CsI.create_dataset('pos',data=CsI_pos_down_log) # At least it matched with cud2rr.py
    CsI.create_dataset('ID', data = eventID)
    CsI.create_dataset('time',data = eventTime)
    CsI.create_dataset('UTC', data = UTC)
    
    CZT=Events.create_group('CZT')
    CZT.create_dataset('Energy', data=CZT_Energy)
    CZT.create_dataset('Position',data=CZT_Position)
    CZT.create_dataset('event_id',data=eventID)
    CZT.create_dataset('event_time',data = eventTime)
    CZT.create_dataset('UTC', data = UTC)
    
    ACD=Events.create_group('ACD')
    ACD.create_dataset('Energy', data=ACD_Energy)
    ACD.create_dataset('EventID',data=eventID)
    ACD.create_dataset('UTC',data = UTC)

    Summary = Events.create_group('Summary')
    UTC=Summary.create_dataset('UTC', data=UTC) # This is a time for each event, not really 'Summary' ***************************************************
                        # It also seems like Sam is looking in Events/CZT/CorUTC for this info ********************************
    outf2.create_dataset('Header', data = header)
    outf2.create_dataset('DEE Version', data = [1])

    #--------------------------------------------------------------------------------------------------#
    #||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
    #||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
    #||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||#
    #--------------------------------------------------------------------------------------------------#


    outf2.close()

if __name__ == '__main__': cli()
