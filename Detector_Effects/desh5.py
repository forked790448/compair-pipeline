# REVERSE PIPELINE PART 2 (.sim.h5 -> subsystems h5)

# This parser diescts a .sim.h5 file generated by sim2h5.py

#########################################################
#			Imports				#
#########################################################

import argparse
import numpy as np
import h5py

#########################################################
#		Parse Arguments				#
#########################################################

def parseargs():
	"""
	This function handles the input
	The only input is to the .sim.h5 file that has been generated by sim2h5.py

	"""

	parser = argparse.ArgumentParser(description = "Parse the .sim.h5 file")
	parser.add_argument("infile", help = "Path to input .sim.h5 file")
	args = parser.parse_args()
	return args

#########################################################
#			main				#
#########################################################

def cli():
	"""
	This function just copies stuff from the .sim.h5 file to the .h5 files for the individual subsystems' reverse pipelines

	"""

	args = parseargs()
	filename = args.infile
	
	name = filename.split('/')[-1]
	path = '/'.join(filename.split('/')[:-1]) + '/'

	if path == '/':
		TKR_path = 'TKR_' + name[:-7] + '_sim.h5'
		CZT_path = 'CZT_' + name[:-7] + '_sim.h5'
		CsI_path = 'CsI_' + name[:-7] + '_sim.h5'
		TRG_path = 'TRG_' + name[:-7] + '_sim.h5'
		ACD_path = 'ACD_' + name[:-7] + '_sim.h5'
	else:
		TKR_path = path + 'TKR_' + name[:-7] + '_sim.h5'
		CZT_path = path + 'CZT_' + name[:-7] + '_sim.h5'
		CsI_path = path + 'CsI_' + name[:-7] + '_sim.h5'
		TRG_path = path + 'TRG_' + name[:-7] + '_sim.h5'
		ACD_path = path + 'ACD_' + name[:-7] + '_sim.h5'

	with h5py.File(filename, 'r') as simf:
		with h5py.File(TKR_path, 'w') as TKRf:
			with h5py.File(CZT_path, 'w') as CZTf:
				with h5py.File(CsI_path, 'w') as CsIf:
					with h5py.File(TRG_path, 'w') as TRGf:
						with h5py.File(ACD_path, 'w') as ACDf:

							simf.copy(simf["/Events/TKR"],TKRf["/"])
							simf.copy(simf["/Events/CZT"],CZTf["/"])
							simf.copy(simf["/Events/CsI"],CsIf["/"])
							simf.copy(simf["/Events/ACD"],ACDf["/"])
						
							header = simf['Header'][...]
							TKRf.create_dataset('Header', data = header)
							CZTf.create_dataset('Header', data = header)
							CsIf.create_dataset('Header', data = header)
							ACDf.create_dataset('Header', data = header)
						
							version = simf['DEE Version'][...]
							TKRf.create_dataset('DEE Version', data = version)
							CZTf.create_dataset('DEE Version', data = version)
							CsIf.create_dataset('DEE Version', data = version)
							ACDf.create_dataset('DEE Version', data = version)
# Need to make a part for the trigger file







if __name__ == '__main__': cli()
