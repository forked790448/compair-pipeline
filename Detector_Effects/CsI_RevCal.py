# This file will convert the CsI output of desh5.py from energy and relative position to anode, cathode, and pad pulseheights in each asic/block

# CsI Calibration temp has been 32C

# Steps:
# 1. read in CsI.sim.h5
# 2. for each event, get the energy and position from input file
# 3. set use_pos & use_erg to 1's
# 7. relative z position ~ c2a ratio
#	c2a means = (c2a max - c2a min)/(bar height) * (z_rel - barheight/2) + c2a min
# 8. energy = anode_ph*interpolated function
#	interpolated function accounts for depth effects:
#	c2a means are the ratio of cathode pulseheight to anode pulseheight (x axis)
#	c2a scalars are 661.7/anode_value in ADC (y axis)



import h5py
import pandas as pd
import numpy as np
import argparse
from scipy.interpolate import interp1d
import pickle
import math

import create_calibration_mask as calibMasks

import datetime
import time

def parseargs():

	"""
	This function handles getting arguments when this function is called

	input 1: path to the CsI sim file
	input 2: path to the CsI calibration file

	"""

	parser = argparse.ArgumentParser(description = "Parse the CsI.sim.h5 file")
	parser.add_argument("infile", help = "Path to input CsI.sim.h5 file")
	parser.add_argument("cal_file", help = "Path to calibration file")
	parser.add_argument("res_file", help = "Path to resolution file")
	args = parser.parse_args()
	return args
	
def loadCalibrationData(filename):

	"""
	This function loads the calibration information
	
	Looks like /.../.../calbrationData 
	"""
	
	with open(filename+"_posCenter.txt", "rb") as fp:
		posCenter =  pickle.load(fp)
	
	with open(filename+"_posRange.txt", "rb") as fp:
		posRangeNonLinearity = pickle.load(fp)
	    
	with open(filename+"_gain.txt", "rb") as fp:
		gain = pickle.load(fp)
		
	return [posCenter,posRangeNonLinearity,gain]
	
	
def loadResolutionData(filename):

	"""
	This function loads the calibration information
	
	Looks like /.../.../resolutionData 
	"""
	
	with open(filename+"511.txt", "r") as f:
		fwhm_bar_511_str =  f.readlines()
	
	with open(filename+"662.txt", "r") as f:
		fwhm_bar_662_str =  f.readlines()
	
	with open(filename+"1274.txt", "r") as f:
		fwhm_bar_1274_str =  f.readlines()
	
	with open(filename+"2614.txt", "r") as f:
		fwhm_bar_2614_str =  f.readlines()
	
	fwhm_bar_511 = np.zeros_like(fwhm_bar_511_str,dtype = float)
	fwhm_bar_662 = np.zeros_like(fwhm_bar_662_str,dtype = float)
	fwhm_bar_1274 = np.zeros_like(fwhm_bar_1274_str,dtype = float)
	fwhm_bar_2614 = np.zeros_like(fwhm_bar_2614_str,dtype = float)
	
	
	for i in range(30):
		string = fwhm_bar_511_str[i]
		fwhm_bar_511[i] = np.float64(string.split('e')[0]) * 10 ** np.int32(string.split('e')[1][1:])
		
		string = fwhm_bar_662_str[i]
		fwhm_bar_662[i] = np.float64(string.split('e')[0]) * 10 ** np.int32(string.split('e')[1][1:])
		
		string = fwhm_bar_1274_str[i]
		fwhm_bar_1274[i] = np.float64(string.split('e')[0]) * 10 ** np.int32(string.split('e')[1][1:])
		
		string = fwhm_bar_2614_str[i]
		fwhm_bar_2614[i] = np.float64(string.split('e')[0]) * 10 ** np.int32(string.split('e')[1][1:])
	
	fwhm_511 = np.median(fwhm_bar_511)
	fwhm_662 = np.median(fwhm_bar_662)
	fwhm_1274 = np.median(fwhm_bar_1274)
	fwhm_2614 = np.median(fwhm_bar_2614)
	
	return [[511,662,1274,2614],[fwhm_511,fwhm_662,fwhm_1274,fwhm_2614]]
	
	
def applyResolutions(ene_arr, res_arr):
	"""
		This function applies a gaussian smear to each event. The amount of smear is linearly interpolated between energies in res_arr
	"""
	
	res_es = res_arr[0]
	pfwhms = res_arr[1] # as a percent
	
	res_f = interp1d(res_es,pfwhms, fill_value = "extrapolate")
	
	[nevents,nlogs] = np.shape(ene_arr)
	new_arr = np.zeros_like(ene_arr)
	
	for ievent in range(nevents):
		for ilog in range(nlogs):
			en = ene_arr[ievent,ilog]
			pfwhm = res_f(en)
			fwhm = pfwhm * en / 100
			sigma = fwhm/np.sqrt(8*np.log(2))
			if sigma < 0:
				sigma = 0
			new_e = np.random.normal(en,sigma)
			new_arr[ievent,ilog] = new_e
	
	return new_arr
			
	
def quadraticRoot(poly,y):
	if len(poly) == 3:
		a = poly[0]
		b = poly[1]
		c = poly[2] - y
		x = (- b + np.sqrt(b**2 - 4 * a * c))/(2 * a)
	elif len(poly) == 2:
		b = poly[0]
		c = poly[1] - y
		x = -c/b
	
	return x

def cli():

	"""
	This function writes everything to the L1 format

	Steps
		1: read the input file and create the output file
		2: copy event times and ids to the output file
			write the UTC time as eventtime * 1e9
		3: open the calibration file
		4: Calculate the pulse heights from the energy and positions

	"""

	args = parseargs()
	filename = args.infile
	cal_filename = args.cal_file
	res_filename = args.res_file

	path = '/'.join(filename.split('/')[:-1]) + '/'
	name = filename.split('/')[-1]

	if path == '/':
		outpath = name[:-3] + '.L1.1.hdf5'
	else:
		outpath = path + name[:-3] + '.L1.1.hdf5'

	with h5py.File(filename, 'r') as fin:
		
		header = fin['Header'][...]
		ids = fin["CsI/ID"][...]
		times = fin["CsI/time"][...]
		pps = np.zeros_like(times)
		notrigackFlag = np.zeros_like(times)
		trigacktim = np.ones_like(times)
			
		days = times // 86400
		hours = (times - days * 86400) // 3600 
		mins = (times - days * 86400 - hours * 3600) // 60 
		secs = (times - days * 86400 - hours * 3600 - mins * 60)
		UTC = np.zeros(len(hours))
		for i in range(len(hours)):
			date_time = datetime.datetime(2022,1,1 + int(days[i]),int(hours[i]),int(mins[i]),int(secs[i]))
			UTC[i] = time.mktime(date_time.timetuple()) # UNIX time stamp for 1/1/2022 + event time rounded down to 1us
			

		# Now to load positions and energies => calculate pulseheights
			
		energies = np.array(fin["/CsI/erg"][...]) # shape [nevents, 30]
		positions = np.array(fin["CsI/pos"][...]) # shape [nevents, 30]
		[nevents,total_logs] = np.shape(energies)
			

		cmis_gain = 7 # Taken from CsI forward pipeline
		log_length = 100 #mm
		
		metaData = calibMasks.CreateCalibrationMasks(["void"])
		metaData.CMIS_gain = cmis_gain
		metaData.energy_of_interest = 2614 # Taken from CsI forward pipeline
		
		[posCenter, posRangeNonLinearity, gain] = loadCalibrationData(cal_filename)
		
		pos_edges_final = np.linspace(-1.6,1.6,int((np.shape(gain)[1])))*log_length/2

		# Apply Energy Resolutions
			
		res_arr = loadResolutionData(res_filename)
		energies = applyResolutions(energies,res_arr)


		# Transform from energy to adc = geometric mean of both SiPM channels
			
		adc_df = pd.DataFrame()
		
		for i_log in range(total_logs):
			
			metaData.setMinMaxLimits(i_log)
			
			adc = np.zeros(len(ids))
			
			pos = pd.Series(positions[:,i_log])
		
			pos[pos<pos_edges_final[0]] = pos_edges_final[0]
			pos[pos>pos_edges_final[-1]] = pos_edges_final[-1]
			
			for i in range(0, len(pos_edges_final)-1):
			
				indexOfInterest = pos.between(pos_edges_final[i], pos_edges_final[i+1])
				
				adc2E = np.array(gain)[i_log][i]
			
				adc[indexOfInterest] = quadraticRoot(adc2E,energies[indexOfInterest,i_log])	
				
			
			# Transform from position to adc_pos = (SiPM 1 - SiPM 2)/(SiPM 1 + SiPM 2)

			tmpEModel = np.poly1d(gain[ i_log ][int((np.shape(gain)[1]/2))] )
			tempE = tmpEModel(adc)
    		
			tempE[tempE > metaData.MIN_ADC] = metaData.MIN_ADC
			centerModel = np.poly1d(posCenter[i_log])
			posRangeModel = np.poly1d(posRangeNonLinearity[i_log])
			adc_pos = posRangeModel(tempE) / 100 * positions[:,i_log] + centerModel(tempE)
				
			# save adc_pos and adc
		
			adc_name = 'L' + str(i_log) + '_adc'
			pos_name = 'L' + str(i_log) + '_pos'
			
			# need to fool get_time_align in rosspadL2ToL3.py
			
			adc = np.append([0],adc)
			adc = np.append(adc,0)
			adc_pos = np.append([0],adc_pos)
			adc_pos = np.append(adc_pos,0)
			
			adc_df[adc_name] = adc
			adc_df[pos_name] = adc_pos
		
		
		# Need to fool get_time_align in rosspadL2ToL3.py
		
		ids = np.append([0],ids)
		ids -= 1
		frame = ids.copy()
		frame += 2
		
		ids = np.append(ids,ids[-1] + 1)
		frame = np.append(frame,frame[-1] + 1)
		
		times = np.append([0],times)
		times = np.append(times,times[-1])
		
		pps = np.append([0],pps)
		pps = np.append(pps,0)
		
		notrigackFlag = np.append([0],notrigackFlag)
		notrigackFlag = np.append(notrigackFlag, 0)
		
		trigacktim = np.append([0],trigacktim)
		trigacktim = np.append(trigacktim,1)
		
		trigackadc = np.zeros_like(trigacktim)
		
		UTC = np.append([1641013200.0],UTC) # start time
		UTC = np.append(UTC,UTC[-1])
				
		tim_d = {'frame': frame,'EventID': ids, 'pps': pps, 'trigack': trigacktim, 'UTC': UTC,'DroppedTrigAck': notrigackFlag}
		tim_df = pd.DataFrame(data=tim_d)
		tim_df.to_hdf(outpath, key = 'TIM', format = 'table')
		
		
		adc_df['frame'] = frame
		adc_df['pps'] = pps
		adc_df['trigack'] = trigackadc
		adc_df['timestamp'] = UTC
		adc_df['gpio'] = trigackadc
		adc_df.to_hdf(outpath, key = 'ADC', format = 'table')
			



if __name__ == '__main__': cli()












