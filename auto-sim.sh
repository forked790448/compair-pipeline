#!/bin/bash
# Script for reverse calibration. -- Sambid Wasti | Oct16, 2023
# Check if directory path is provided as a command-line argument
if [ $# -eq 0 ]; then
  echo "Error: Please provide a sim file and its path!"
  exit 1
fi

if [ $# -eq 1 ]; then
  echo "Error: Please provide a path to ecalib folder as teh secondary argument!"
  exit 1
fi

input_file=$1
directory=$2
aux_folder=$directory

input_dir="$(dirname "$input_file")"
input_filename="$(basename "$input_file")"

sim_file="$input_dir/$input_filename"

echo "--Step 1: Sim2h5--"
python Detector_Effects/sim2h5.py $sim_file
echo "** Step 1: Done **"


echo "--Step 2: Desh--"
simh5_file="$sim_file.h5"
python Detector_Effects/desh5.py $simh5_file
echo "** Step 2: Done **"

echo "--Step 3.1: Rev Trk--"
base_name="$input_filename.h5"
temp_simh5_tkr="$input_dir/TKR_$base_name"
simh5_tkr="${temp_simh5_tkr%???????}_sim.h5"
TKR_ecal=$(find "$aux_folder" -type f -name "*TKR*EnergyCalibrationCoefficients*")
python Detector_Effects/TKR_ReVCal.py $simh5_tkr $TKR_ecal
echo "**Step 3.1: Done**"

echo "--Step 3.2: Rev CZT--"
base_name="$input_filename.h5"
temp_simh5_czt="$input_dir/CZT_$base_name"
simh5_czt="${temp_simh5_czt%???????}_sim.h5"
CZT_ecal=$(find "$aux_folder" -type f -name "*CZT*cal.h5")
python Detector_Effects/CZT_RevCal.py $simh5_czt $CZT_ecal
echo "**Step 3.2: Done**"

echo "--Step 3.3: Rev CsI--"
base_name="$input_filename.h5"
temp_simh5_csi="$input_dir/CsI_$base_name"
simh5_csi="${temp_simh5_csi%???????}_sim.h5"
python Detector_Effects/CsI_RevCal.py $simh5_csi "$aux_folder/calibrationData" "$aux_folder/resolutionData"
echo "**Step 3.3: Done**"

echo "--Step 3.4: Rev ACD--"
base_name="$input_filename.h5"
temp_simh5_acd="$input_dir/ACD_$base_name"
simh5_acd="${temp_simh5_acd%???????}_sim.h5"
ACD_ecal=$(find "$aux_folder" -type f -name "*ACD_Calib*.h5")
python Detector_Effects/ACD_RevCal.py $simh5_acd $ACD_ecal
echo "**Step 3.4: Done**"
echo "---"


